
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                     
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($productdata1)){?>


             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Price</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="priceid" name="priceid" value="<?php echo $productdata1[0]['product_price_weight_id'];?>" />
					<input type="hidden" id="productid1" name="productid" value="<?php echo $productdata1[0]['product_id'];?>"/>

                             
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Product size</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Product Name" value="<?php echo $productdata1[0]['weight'];?>"> <span class="nameErr error1" style="color: red;"></span>  </div>

                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Actual Price</label>
                                <input type="text" id="price" name="price" class="form-control"  placeholder="Product Price" value="<?php echo $productdata1[0]['actual_price'];?>"> <span class="priceErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Discounted Price</label>
                                <input type="text" id="dis_price" name="dis_price" class="form-control"  placeholder="Discounted Price" value="<?php echo $productdata1[0]['dis_price'];?>"> <span class="dis_priceErr error1" style="color: red;"></span>  </div>
                            </div>

                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add Price</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         <form class="form-horizontal form-material" method="post" action="" id="formData0" enctype="multipart/form-data">
						 <input type="hidden" id="priceid" name="priceid" value=""/>
                    <input type="hidden" id="productid" name="productid" value="<?php echo $productaddprice[0]['product_id'];?>"  />
                           
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Product size</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Product Size" value=""> <span class="nameErr error1" style="color: red;"></span>  </div>

                               
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Actual Price</label>
                                <input type="text" id="price" name="price" class="form-control"  placeholder="Product Price" value=""> <span class="priceErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Discounted Price</label>
                                <input type="text" id="dis_price" name="dis_price" class="form-control"  placeholder="Discounted Price" value=""> <span class="dis_priceErr error1" style="color: red;"></span>  </div>
                            </div>
                            

                            <button type="submit" onclick="return formValidationAdd();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
   
             <?php include('script.php');?>





      <script>
        function formValidationAdd(){
         var data = new FormData($('#formData0')[0]);
         var hid = $("#id").val();
         var img = $("#img").val(); 
         var category_id = $("#category_id").val(); 
         var sub_category_id = $("#sub_category_id").val(); 
         var title = $("#name").val(); 
         var quantity = $("#quantity").val(); 
         var price = $("#price").val(); 
         var dis_price = $("#dis_price").val(); 
        
       
          
         
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".quantityErr").html("");
          $(".quantityErr").hide("");
          $(".priceErr").html("");
          $(".priceErr").hide("");
          $(".dis_priceErr").html("");
          $(".dis_priceErr").hide("");
          $(".user_idErr").html("");
          $(".user_idErr").hide("");
          $(".imgErr").html("");
          $(".imgErr").hide("");
         
         
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Product Size is required.");
            $("#name").focus();
            return false;
          }if(quantity==""){
            $(".quantityErr").slideDown('slow');
            $(".quantityErr").html("Quantity is required.");
            $("#quantity").focus();
            return false;
          }
           if(price==""){
            $(".priceErr").slideDown('slow');
            $(".priceErr").html("Price is required.");
            $("#price").focus();
            return false;
          }if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price is required.");
            $("#dis_price").focus();
            return false;
          }
         if(parseFloat(price) < parseFloat(dis_price)){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price Should be less than Price.");
            $("#dis_price").focus();
            return false;
          }
          
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditProductpriceSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/manageProduct'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Product has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","Product has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 3){
                swal("Error!","Product name already exists","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Product has not been Updated successfully","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 



        <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
		 //alert(hid);
       //  var img = $("#img").val(); 
        
         var title = $("#name").val(); 
         var quantity = $("#quantity").val(); 
         var price = $("#price").val(); 
         var dis_price = $("#dis_price").val(); 
         
          
         
          $(".nameErr").html("");
          $(".nameErr").hide("");
 
          $(".priceErr").html("");
          $(".priceErr").hide("");
          $(".dis_priceErr").html("");
          $(".dis_priceErr").hide("");
         
        
         
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Product Name is required.");
            $("#name").focus();
            return false;
          }
           if(price==""){
            $(".priceErr").slideDown('slow');
            $(".priceErr").html("Price is required.");
            $("#price").focus();
            return false;
          }if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price is required.");
            $("#dis_price").focus();
            return false;
          }
          if(parseFloat(price) < parseFloat(dis_price)){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price Should be less than Price.");
            $("#dis_price").focus();
            return false;
          }
         
         
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditProductpriceSubmit' ;?>";
			//alert(ctrlUrl);
            var adminRedirectUrl="<?php echo base_url().'Admin/manageProduct'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Product has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 3){
                swal("Error!","Product name already exists","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Product has not been Updated successfully","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 

      <script>
              CKEDITOR.replace( 'content', {
              });

              function getCategory(val){
          //alert(val);
          var ctrlUrl = "<?php echo base_url().'Admin/getSubCategoryByCategory' ;?>";
               //$("#loadDiv").show(); 
              $.ajax({
                type: "POST",
                url: ctrlUrl,
                data:({
                  category_id :val
                }),
                cache: false,
                success: function(data)
                { 
                    $("#sub_category_id").html(data);
                }
              });
        }
      </script>
<script type="text/javascript">
$('#price').keypress(function(event){
    console.log(event.which);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
    $(".priceErr").slideDown('slow');
    $(".priceErr").html("Enter numeric value only.");
    event.preventDefault();
}else{
	$(".priceErr").html("");	
}});

$('#dis_price').keypress(function(event){
    console.log(event.which);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
	  $(".dis_priceErr").slideDown('slow');
	    $(".dis_priceErr").html("Enter numeric value only.");
	    event.preventDefault();
	}else{
		$(".dis_priceErr").html("");	
	}});
$('#quantity').keypress(function(event){
    console.log(event.which);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
	  $(".quantityErr").slideDown('slow');
	    $(".quantityErr").html("Enter numeric value only.");
	    event.preventDefault();
	}else{
		$(".quantityErr").html("");	
	}});
$("#img").change(function() {

    var val = $(this).val();

    switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
        case 'gif': case 'jpg': case 'png':
            
            break;
        default:
            $(this).val('');
            // error message here
            $(".imgErr").slideDown('slow');
	    $(".imgErr").html("Select only jpg/gif/png images.");
            break;
    }
});
      </script>
  </body>
</html>