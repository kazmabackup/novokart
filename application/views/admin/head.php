<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Novokart</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/admin/images/logo.png">
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="<?php echo base_url();?>assets/admin/styles/shards-dashboards.1.0.0.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/styles/extras.1.0.0.min.css">
   
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/styles.css">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <style>
      /* Graph */
/* Containers */
#wrapper {
  margin: 25px auto;
  width: 100%;


}
.chart h2{
font-size: 1rem;
margin-left: 12px;
}
.wrapper-body{
background: #fff;
float: left;
width: 94%;
left: 0;
margin-left: 60px;
margin-bottom: 33px;
border-radius: 6px;
box-shadow: 0 2px 0 rgba(90,97,105,.11),0 4px 8px rgba(90,97,105,.12),0 10px 10px rgba(90,97,105,.06),0 7px 70px rgba(90,97,105,.1);
}
#figure {
  height: 380px;
  position: relative;
}
#figure ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
.graph {
  height: 283px;
  position: relative;
}

/* Legend */
.legend {
  background: #f0f0f0;
  border-radius: 4px;
  bottom: 0;
  position: absolute;
  text-align: left;
  width: 540px; 
}
.legend li {
  display: block;
  float: left;
  height: 20px;
  margin: 0;
  padding: 10px 30px;
  width: 120px;
}
.legend span.icon {
  background-position: 50% 0;
  border-radius: 2px;
  display: block;
  float: left;
  height: 16px;
  margin: 2px 10px 0 0;
  width: 16px;  
}

/* X-Axis */
.x-axis {
  bottom: 0;
  color: #555;
  position: absolute;
  text-align: center;
  width: 100%;
}
.x-axis li {
  float: left;
  margin: 0 15px;
  padding: 5px 0;
  width: 10%; 
}
.x-axis li span {
  float: left;
}
/* Y-Axis */
.y-axis {
  color: #555;
  position: absolute;
  text-align: right;
  width: 100%;
}
.y-axis li {
  border-top: 1px solid #ccc;
  display: block;
  height: 62px;
  width: 100%;
}
.y-axis li span {
  display: block;
  margin: -10px 0 0 -60px;
  padding: 0 10px;
  width: 40px;
}

/* Graph Bars */
.bars {
  height: 253px;
  position: absolute;
  width: 100%;
  z-index: 10;
}
.bar-group {
  float: left;
  height: 100%;
  margin: 0 15px;
  position: relative;
  width: 10%;
}
.bar {
  border-radius: 3px 3px 0 0;
  bottom: 0;
  cursor: pointer;  
  height: 0;
  position: absolute;
  text-align: center;
  width: 24px;
}
.bar span {
  background: #fefefe;
  border-radius: 3px;
  left: -8px;
  display: none;
  margin: 0;
  position: relative;
  text-shadow: rgba(255, 255, 255, 0.8) 0 1px 0;
  width: 40px;
  z-index: 20;
  
  -webkit-box-shadow: rgba(0, 0, 0, 0.6) 0 1px 4px;
  box-shadow: rgba(0, 0, 0, 0.6) 0 1px 4px;
}
.bar:hover span {
  display: block;
  margin-top: -25px;
}

#data-table.js {
  display: none;
}
.bar span {
  background: #fefefe;
}
.fig0 {
  background: #a22;
}
    </style>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/admin/css/sweetalert.css" />
</head>

<div class="modal-backdrop fade in" id="loadDiv" style="display: none;">
	<div class="ajaxLoadCss" style="margin-left: 656px;margin-top: 200px;">
		<img src="<?php echo base_url()?>assets/ajaxLoad2.gif" />
	</div>
</div>
