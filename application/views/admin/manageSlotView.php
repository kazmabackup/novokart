
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
           
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          
             
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">

                <div class="card-header border-bottom">
                   <a href="<?php echo base_url('Admin/addEditSlot');?>" class="btn btn-mini btn-success" > Add</a>
<!--                     <h6 class="m-0">Active Users</h6>
 -->                </div>
                <div class="card-body p-0 pb-3 text-center">
                  <table id="example" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>S. No.</th>
                <th>Slot Time</th>
                <th>Action</th>
                
            </tr>
               </thead>
               <tbody>
               <?php if(!empty($slotData)){ //print_r($customerData);die;
               $i=1;
							foreach ($slotData as $slot){
								$slotid = $slot['slot_id'];
								$stime = $slot['start_time'];
								$etime = $slot['end_time'];
								$slottime = $stime." to ".$etime;
								$slotdate = $slot['slot_date'];
								$status = $slot['status'];	
							?>
								<tr>
								<td><?php echo $i;?></td>
									<td><?php echo $slottime;?></td>
										<td>
							           <a href="#" onclick="deleteSlot(<?php echo $slotid;?>)" class="btn">Delete</a>
								<a href="<?php echo base_url();?>admin/addEditSlot/<?php echo $slotid;?>" class="btn">Edit</a>&nbsp;
							                <?php if($status == 'Active'){?>
											<a href="" class="btn btn-mini btn-success"
											onclick="inactive(<?php echo $slotid;?>)"><?php echo $status;?></a>
											<?php }else{?>
											<a href="" class="btn btn-mini btn-danger"
											onclick="active(<?php echo $slotid;?>)"><?php echo $status;?></a>
											<?php }?>
								
								</td>  
								</tr>
								<?php $i++;}
							}?>
          
           
             </tbody>
          </table>
                  </div>
                </div>
              </div>
            </div>
              
            <!-- End Default Light Table -->
           
          </div>
        <?php include('footer.php');?>

         <?php include('script.php');?>
		
				<script type="text/javascript">
function deleteSlot(val){	
	//alert("roushan");
    var id = val;
   //alert(id);
    var res = confirm("Are you sure you want to delete this Slot?");
    if(res == true) {
    	var url="<?php echo base_url()."admin/deleteSlot/"?>";
	  	var redirectUrl
      $.ajax({
      	type: "POST",
        	url: url,
        	data:({
        		id : id
         	}),
        	cache: false,
        	success: function(data)
        		{
        			//alert(data);
        	  		if(data == 1) {
                  	alert("Slot has been deleted successfully.");
              	} else {
                  	alert("Slot has not been deleted.");
              	}
        	  		window.location.reload();
	           }
   		});
		}
	}
</script>
<script>
	function inactive(id){
		//alert(id);die;
		 var res = confirm("Are you sure you want to Inactive this Slot ?");
    if(res == true) {
    var url="<?php echo base_url()."Admin/inactive_slot_status/"?>";
	var redirectUrl
       $.ajax({
        type: "POST",
        url: url,
        data:({
        	id: id
         }),
        cache: false,
        success: function(data)
        {
        	//alert(data);
        	  if(data == 1) {
                  alert("Slot has been Inactivated successfully.");
              } else {
                  alert("Slot has  not been Inactivated.");
              }
        	  window.location.reload();
        
        }
   });
}
	}
	function active(id){
		  var res = confirm("Are you sure you want to active this slot");
		    if(res == true) {
		    	url = "<?php echo base_url().'Admin/active_slot_status';?>";
		        $.ajax({
		          type:"POST",
		          url: url, 
		          data: {"id":id}, 
		          success: function(data) 
				  { //alert(data);
			          if(data == "1")
			          { 
				          alert("slot has been activated successfully");
		         			 location.reload();
			          }
		    	}
		 	});
		 }  
	}
</script>