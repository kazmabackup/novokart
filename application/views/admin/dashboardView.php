
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
         <?php include('nav.php');?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Small Stats Blocks -->
            <div class="row">
              
 			<div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Total Number of Order </span>
                        <a href="<?php echo base_url()?>Admin/order/" class="stats-small__value count my-3"><?php echo $total_order;?></a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div><div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Total Order Pending</span>
                        <?php $status = 'Pending'?>
                        <a href= "<?php echo base_url()?>Admin/order/<?php echo $status;?>" class="stats-small__value count my-3"><?php echo $pending_order;?></a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div><div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Total Sales Amount</span>
                         <?php $status = 'Approved'?>
                        <a href= "<?php echo base_url()?>Admin/order/<?php echo $status;?>" class="stats-small__value count my-3"><?php echo number_format($amount,2);?></a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div><div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Todays Order</span>
                        <?php $status = 'Today'?>
                        <a href = "<?php echo base_url()?>Admin/order/<?php echo $status;?>" class="stats-small__value count my-3"><?php echo $today_order;?></a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
             <!--  <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <a class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                       
                        <strong>Referral Data</strong>
                        <i style="color: #007bff;"  class="fa fa-long-arrow-right"></i>
                      </div>
                     
                    </div>
                  </a>
                </div>
              </div> -->
             
            </div>
            <!-- End Small Stats Blocks -->
            <div class="row">
            <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">TOTAL PAID DISCOUNT TO AFFILIATE </span>
                        <a href="<?php echo base_url()?>Admin/order/" class="stats-small__value count my-3"><?php echo number_format($total_affiliate_earning,2);?></a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">TOTAL PAID DISCOUNT TO CUSTOMER</span>
                        <a href="<?php echo base_url()?>Admin/order/" class="stats-small__value count my-3"><?php echo number_format($total_customer_discount,2);?></a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Our TOTAL PROFIT VALUE</span>
                        <a href="<?php echo base_url()?>Admin/order/" class="stats-small__value count my-3"><?php echo number_format($total_profit_value,2);?></a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>

            <?php include('script.php');?>
  
    
    </body>
</html>