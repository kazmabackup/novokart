
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  
  <body class="h-100">
      <style type="text/css">
    .even td a .fa-check{
    color: #008400;
    margin-right: 5px;
    }
     .odd td a .fa-check{
    color: #008400;
    margin-right: 5px;
    }
    .even td a .fa-times{
    color: #E8101D;
    margin-right: 5px;
    }
    .odd td a .fa-times{
    color: #E8101D;
    margin-right: 5px;
    }
    .odd td a i{
    font-size: 11px;
    }
    .even td a i{
    font-size: 12px;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
           
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          
             
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                  <form action="<?php echo base_url()?>Admin/orderexpoert" method="post" enctype="multipart/form-data">
                <div class="form-row">
                <div class="form-group col-md-4">
                <input type="text" id ="fromdatepicker" name ="fromdatepicker" placeholder="From date" class="form-control"/>
                <span class="fromdatepickerErr error1" style="color: red;"></span>
                </div>
                <div class="form-group col-md-4">
                <input type="text" placeholder="To date" id ="todatepicker" name ="todatepicker" class="form-control"/>
                <span class="todatepickerErr error1" style="color: red;"></span>
                </div>
                <div class="form-group col-md-2">
                <input type="submit" class="pull-right btn btn-primary btn-xs" onclick="return exportreport();" value="Export Data"><br/>
                </div>
                </div>
                </form>
            </div>
                <div class="card-body p-0 pb-3 text-center">
                <table id="example" class="display table" cellspacing="0" width="100%">
                <thead>
            <tr>
                <th>S. No.</th>
                <th>Order Date</th>
                 <th>Order Id</th>
                <th>Customer Name</th>
                <th>Total Bill Amount</th>
                <th>Hospital Discount(%)</th>
                <th>Hospital Discount Amount</th>
                <th>Affiliate Discount(%)</th>
                <th>Affiliate Discount Amount</th>
                <th>After Discount Bill Amount</th>
                
                <th>Delivery Address</th>
                <th>Order file</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
               </thead>
               <tbody>
                <?php
                if(!empty($detailData)){
                	date_default_timezone_set('Asia/Kolkata');
                    $i = 1;
                    $marginData = $this->Adminmodel->getTableData("company_margin");
                    $company_margin = $marginData[0]['company_margin'];
                    foreach ( $detailData as $detail ) {
                      $id = $detail ['manage_order_id'];
                      $orderid = $detail ['unique_order_id'];
                      $affiliate_percentage = $detail['dis_by_agent'];
                      $algorithm = $detail['algorithm'];
                      $affdis = $detail['affiliate_percentage'];
                      $agent_id = $detail ['agent_id'];
					  $where3 = "affiliate_id= $agent_id";
				      $affidata = $this->Adminmodel->getDataById ( 'affiliate', $where3 );
				      $affiname=$affidata[0]['unique_id'];
                      $user_id = $detail ['user_id'];
					  $where = "user_id= $user_id";
				      $usedata = $this->Adminmodel->getDataById ( 'user', $where );
				      $username = $usedata[0]['user_name'];
				     
				      
                      $product_id = $detail ['pro_id'];
				      $where1 = "product_id= $product_id";
				      $prodata = $this->Adminmodel->getDataById ( 'product', $where1 );
				      $proname=$prodata[0]['product_name'];
                      $address = $detail ['address_id'];
                      $whereaddress = "delivery_address_id= $address";
                      $addressdata = $this->Adminmodel->getDataById ( 'delivery_address', $whereaddress );
                      $addressname=$addressdata[0]['address'];
                      $order_date = date("d M Y h:i:s A",strtotime($detail ['order_date']));
                      $whereorderid = "unique_order_id='$orderid'";
                      $orderData = $this->Adminmodel->getDataById("manage_order",$whereorderid);
                      $status = $detail['order_status'];
                      $orderfile = $detail['order_file'];
                      
                      $total_discount_price = 0;
                      $total_cost_price = 0;
                      if (!empty($orderfile)){
                      	$hostital_dis_per = 0;
                      	$hostital_dis = 0;
                      	$affiliate_earning = 0;
                      	$total_bill_price = 0;
                      }else {
                      foreach ($orderData as $order){
                      	$order_quantity = $order ['order_quantity'];
                      	$pro_price = $order ['pro_price'];
                      	$pro_cost_price = $order['pro_cost_price'];
                      	$total_discount_price = $total_discount_price + ($pro_price * $order_quantity);
                      	$total_cost_price = $total_cost_price + ($pro_cost_price * $order_quantity);
                      }
                     
                      $status = $detail['order_status'];
                      $commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
                     
                      $company_profit = ($company_margin/100);
                      if ($algorithm=="1"){
                      $package_per = (($commulative_per/100)*1)-($affdis/100);
                      if ($package_per > $company_profit){
                      	$new_total_dis = ($total_discount_price - $total_cost_price)*($affdis/100);
                      	
                      }else {
                      	$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100-($company_margin/100);
                      }
                      } else {
                      	$package_per = ($commulative_per - $company_margin);
                      	if ($package_per >= $affdis){
                      		$new_total_dis = ($total_discount_price)*$affdis/100;
                      	}
                      }
                     // echo $package_per;
                      $hostital_dis = ($new_total_dis*$affiliate_percentage/100);
                      if ($hostital_dis < 0){
                      	$hostital_dis_per = 0;
                      	$affiliate_earning = $new_total_dis - 0;
                      	$total_bill_price = $total_discount_price - 0;
                      } else {
                      	$hostital_dis_per = ($hostital_dis/$total_discount_price)*100;
                      	$affiliate_earning = $new_total_dis - $hostital_dis;
                      	$total_bill_price = $total_discount_price - $hostital_dis;
                      }
                      }
                      ?>
           
            <tr>
                <td><?php echo $i;?></td>
                <td><?php echo $order_date;?></td>
                <td><?php echo $orderid;?></td>
                <td><?php echo $username;?></td>
                <td><?php echo number_format($total_discount_price,2);?></td>
                <td><?php echo number_format($hostital_dis_per,2);?></td>
                <td><?php echo number_format($hostital_dis,2);?></td>
                <td><?php echo $affdis;?></td>
                <td><?php echo number_format($affiliate_earning,2);?></td>
				<td><?php echo number_format($total_bill_price,2);?></td>
				<td><?php echo $addressname;?></td>
				<?php if (!empty($orderfile)){?>
				<td><?php echo $orderfile;?><a href="<?php echo base_url()?>uploads/orderfile/<?php echo $orderfile;?>" download="<?php echo $orderfile;?>" style="float:right;"><i class="fa fa-download"></i></a></td>
				<?php } else {?>
				<td><?php echo "---";?></td>
				<?php }?>
				<td><?php echo $status;?></td>
                <td>
                  <?php if ($status=='Pending'){ ?>
                   <a href=""  onclick="changeCountryStatus('<?php echo $orderid;?>','Approved')" ><i class="fas fa-check"></i></a>
                   <a href="" onclick="changeCountryStatus('<?php echo $orderid;?>', 'Cancel')" ><i class="fas fa-times"></i></a>
                   <?php if ($detail['is_add'] !='1'){?>
                   <a href="<?php echo base_url()?>Admin/editorder/<?php echo $orderid;?>"><i class="fas fa-edit"></i></a>
                  <?php }?>
                 <?php } else {?>
                 <a onclick="return order()"><i class="fas fa-check"></i></a>
                 <?php } ?>
                  <!-- <?php if($status == 'Active'){?>
                  <a href="" class="btn btn-mini btn-success" onclick="changeCountryStatus(<?php echo $id;?>,'In-Active')" ><?php echo $status;?></a>
                  <?php }else{?>
                  <a href="" class="btn btn-mini btn-danger" onclick="changeCountryStatus(<?php echo $id;?>, 'Active')" ><?php echo $status;?></a>
                  <?php }?> -->
                   <a href=""  onclick = " return order_details('<?php echo $orderid;?>');" data-toggle="modal" ><i class="far fa-eye"></i></a>
               
                </td>
            </tr>
            <?php $i++;} } ?>
             </tbody>
          </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>
          <div id="orderdetails" class="modal"  data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
          </div>
         <?php include('footer.php');?>
         <?php include('script.php');?>
             <div id="myModal" class="modal fade" role="dialog">
              <!-- <span id="ddetail"></span> -->
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <span id="ddetail"></span>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
  </div>
  <script type="text/javascript">
    function order() {
      swal("Order Status has been already Changed.");
      // body...
    }
  </script>
  <script type="text/javascript">
  function vendorDetail(val,type){
       var subCatUrl  ="<?php echo base_url();?>Admin/vendorDetail";
        $.ajax({
          type: "POST",
          url: subCatUrl,
          data:({
            val : val,
            type : type
             }),
          cache: false,
          success: function(data)
          {//alert(data);
            $("#ddetail").html(data);
          }
        });
      return True;      
  }
</script>


<script>
      function changeCountryStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this order?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeOrderStatus/"?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/order'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Order status has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Order status has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>
  <script>
function order_details(orderid){
	var ctrlUrl = "<?php echo base_url().'Admin/order_details';?>";
	$.ajax({
		type: "POST",
		url: ctrlUrl,
		data:({
			orderid : orderid
		}),
		cache: false,
		success: function(data)
		{ //alert(data);
	      $("#orderdetails").html(data);
	      $('#orderdetails').modal('show');
		}
	});
}
   </script>
   <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#fromdatepicker" ).datepicker();
  } );
  $( function() {
	    $( "#todatepicker" ).datepicker();
	  } );
  </script>
  <script>
  function exportreport(){
      var fromdate = $("#fromdatepicker").val();
      var todate = $("#todatepicker").val();
      $(".fromdatepickerErr").html("");
      $(".fromdatepickerErr").hide("");
      $(".todatepickerErr").html("");
      $(".todatepickerErr").hide("");
      if(fromdate==""){
          $(".fromdatepickerErr").slideDown('slow');
          $(".fromdatepickerErr").html("select from date.");
          $("#fromdatepicker").focus();
          return false;
        }
      if(todate==""){
          $(".todatepickerErr").slideDown('slow');
          $(".todatepickerErr").html("select to date.");
          $("#todatepicker").focus();
          return false;
        } 
      //return false;
	  }
  </script>
  <script>
  $('#download').click(function(e) {
	    e.preventDefault();  //stop the browser from following
	    window.location.href = 'uploads/file.doc';
	});
  </script>
  </body>
</html>