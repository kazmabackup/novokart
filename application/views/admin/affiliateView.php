
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                   <a href="<?php echo base_url('Admin/addEditAgent');?>" class="btn btn-mini btn-success" > Add</a>
<!--                     <h6 class="m-0">Active Users</h6>
 -->                </div>
                <div class="card-body p-0 pb-3 text-center">
                  <table id="example" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>S. No.</th>
                <th>Agent Name</th>
				 <th>Agent id</th>
				 <th>password</th>
                <th>Email</th>
                <th>Phone No </th>
				<!-- <th>Discount Percent </th> -->
                <th>Action</th>
            </tr>
               </thead>
               <tbody>
                <?php
                if(!empty($agentdata)){
                    $i = 1;
                    foreach ( $agentdata as $detail ) {
                      $id = $detail ['affiliate_id'];
					  $uniqueid = $detail ['unique_id'];
                      $name = $detail ['affiliate_name'];
					  $phone = $detail ['phone'];
					  $email = $detail ['email'];
                     // $discount = $detail ['discount'];
                      $password = $detail ['password'];
                      $status = $detail ['status'];
                      ?>
           
            <tr>
               <td><?php echo $i;?></td>
               <td><?php echo $name;?></td>
               <td><?php echo $uniqueid;?></td>
               <td><?php echo $password;?></td>
               <td><?php echo $email;?></td>
			   <td><?php echo $phone;?></td>
			  <!--  <td><?php echo $discount;?></td> -->
               <td><?php if($status == 'Active'){?>
                  <a href="" class="btn btn-mini btn-success" onclick="changeCountryStatus(<?php echo $id;?>,'In-Active')" ><?php echo $status;?></a>
                  <?php }else{?>
                  <a href="" class="btn btn-mini btn-danger" onclick="changeCountryStatus(<?php echo $id;?>, 'Active')" ><?php echo $status;?></a>
                  <?php }?>
                    <a href="<?php echo base_url('Admin/addEditAgent');?>/<?php echo $id;?>" class="btn btn-mini btn-success" > Edit</a>
                    <a  onclick="deleteData(<?php echo $id;?>)" class="btn btn-mini btn-success" > Delete</a>
                </td>
            </tr>
            <?php $i++;} }?>
             </tbody>
          </table>
                  </div>
                </div>
              </div>
            </div>
              
            <!-- End Default Light Table -->
           
          </div>
        <?php include('footer.php');?>

         <?php include('script.php');?>


          <script type="text/javascript">
      function deleteData(id) {
        var res = confirm("Are you sure you want to delete this?");
          if(res == true) {
          var url="<?php echo base_url()."Admin/deleteaffiliate/"?>";
        var redirectUrl
             $.ajax({
              type: "POST",
              url: url,
              data:({
                id: id
               }),
              cache: false,
              success: function(data)
              {
                if(data == 1) {
                  swal("Success!","Affilaite has been Deleted successfully","success");
                    } else {
                        alert(" Affiliate has  not been deleted.");
                    }
                window.location.reload();
              
              }
         });
      }
      }
</script>


         <script>
      function changeCountryStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this affiliate?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeagentStatus/"?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/affiliate'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Affiliate has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Affiliate has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>

  
  </body>
</html>