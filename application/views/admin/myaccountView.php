<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->
            <?php if(!empty($userdata)){?>
             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Account</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData" enctype="multipart/form-data">
                       <input type="hidden" id="id" name="id" value="<?php echo $userdata[0]['user_id'];?>" />
                             <div class="form-row">
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Enter Name" value="<?php echo $userdata[0]['user_name'];?>"> <span class="nameErr error" style="color: red;"></span>  </div>
                                <div class="form-group col-md-6">
                                <label for="feFirstName">email</label>
                                <input type="text" id="email" name="email" class="form-control"  placeholder="Enter email" value="<?php echo $userdata[0]['email'];?>"> <span class="emailErr error" style="color: red;"></span>  </div>
                           <div class="form-group col-md-6">
                                <label for="feFirstName">Contact number</label>
                                <input type="number" id="contact" name="contact" class="form-control"  placeholder="Enter contact" value="<?php echo $userdata[0]['contact'];?>"> <span class="contactErr error" style="color: red;"></span>
                                </div>
                                <div class="form-group col-md-6">
                                <label for="feFirstName">Address</label>
                                <textarea  id="address" name="address" class="form-control"  placeholder="Enter address"><?php echo $userdata[0]['address'];?></textarea> 
                                <span class="addressErr error" style="color: red;"></span>
                                </div>
                            </div>
                            <button type="submit" onclick="return formValidation();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
             <?php } ?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
      <?php include('script.php');?>
      <script>
        function formValidation(){
         var data = new FormData($('#formData')[0]);
         var hid = $("#id").val();
         var name = $("#name").val(); 
         var email = $("#email").val(); 
		 var contact = $("#contact").val();
		 var address = $("#address").val();
		 var emailRegexStr = /^[a-zA-Z0-9/+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	     var isvalid = emailRegexStr.test(email); 
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".emailErr").html("");
          $(".emailErr").hide("");
          $(".contactErr").html("");
          $(".contactErr").hide("");
          $(".addressErr").html("");
          $(".addressErr").hide("");
          if(name==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html("Name is required.");
            $("#name").focus();
            return false;
          }if(email==""){
            $(".emailErr").slideDown('slow');
            $(".emailErr").html("Email is required.");
            $("#email").focus();
            return false;
          }
          if(contact==""){
            $(".contactErr").slideDown('slow');
            $(".contactErr").html("contact is required.");
            $("#contact").focus();
            return false;
          }if(contact.length < 10){
    			$(".contactErr").slideDown('slow');
      			$(".contactErr").html("Mobile no must be 10 digits");
     			 $("#num").focus();
      			return false;
		   }if(contact.length > 10){
		          $(".contactErr").slideDown('slow');
		          $(".contactErr").html("Mobile no must be 10 digits");
		          $("#contact").focus();
		          return false;
		       }
	       if(address==""){
		            $(".addressErr").slideDown('slow');
		            $(".addressErr").html("Address is required.");
		            $("#address").focus();
		            return false;
		          }
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/updatemyaccountSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/myAccount'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Profile has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Profile has not been Updated","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 



        <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
       //  var img = $("#img").val();  
         var title = $("#name").val(); 
         var quantity = $("#percentage").val(); 
         var dis_price = $("#min_value").val(); 
      
          
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".quantityErr").html("");
          $(".quantityErr").hide("");

          $(".dis_priceErr").html("");
          $(".dis_priceErr").hide("");
         
         
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Package Name is required.");
            $("#name").focus();
            return false;
          }if(quantity==""){
            $(".quantityErr").slideDown('slow');
            $(".quantityErr").html("percentage is required.");
            $("#percentage").focus();
            return false;
          }
          if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("minmum price Price is required.");
            $("#min_value").focus();
            return false;
          }
         
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditPackageSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/package'?>"; 
            data.append('content', content);
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Package has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","Package has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 3){
                swal("Error!","Package name already exists","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Package has not been Updated successfully","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 

    
  </body>
</html>