
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
           
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          
             
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">

                <div class="card-header border-bottom">
                   <a href="<?php echo base_url('Admin/addEditSubCategory');?>" class="btn btn-mini btn-success" > Add</a>
<!--                     <h6 class="m-0">Active Users</h6>
 -->                </div>
                <div class="card-body p-0 pb-3 text-center">
                  <table id="example" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>S. No.</th>
                <th>Sub Category Name</th>
                <th>Category Name</th>
                <th>Image</th>
                <th>Action</th>
                
            </tr>
               </thead>
 
               <tbody>

                <?php
                if(!empty($subcategorydata)){
                    $i = 1;
                    foreach ( $subcategorydata as $detail ) {
                      $id = $detail ['sub_category_id'];
                      $sub_category_name = $detail ['sub_category_name'];
                      $catid = $detail ['category_id'];
					  $where="category_id=$catid";
					  $catData = $this->Adminmodel->getDataById ( 'category', $where );
					  $catname=$catData[0]['category_name'];
                      $status = $detail ['status'];
                      ?>
           
            <tr>
               <td><?php echo $i;?></td>
                <td><?php echo $sub_category_name;?></td>
                <td><?php echo $catname;?></td>
                  <td><img src="<?php echo base_url()?>uploads/subcategory/<?php echo $detail['image'];?>" width="50" height="40" /></td>
                <td><?php if($status == 'Active'){?>
                  <a href="" class="btn btn-mini btn-success" onclick="changeCountryStatus(<?php echo $id;?>,'In-Active')" ><?php echo $status;?></a>
                  <?php }else{?>
                  <a href="" class="btn btn-mini btn-danger" onclick="changeCountryStatus(<?php echo $id;?>, 'Active')" ><?php echo $status;?></a>
                  <?php }?>

                    <a href="<?php echo base_url('Admin/addEditSubCategory');?>/<?php echo $id;?>" class="btn btn-mini btn-success" > Edit</a>

                    <a  onclick="deleteData(<?php echo $id;?>)" class="btn btn-mini btn-success" > Delete</a>
                </td>
                
            </tr>

            <?php $i++;} }
              ?>
          
           
             </tbody>
          </table>
                  </div>
                </div>
              </div>
            </div>
              
            <!-- End Default Light Table -->
           
          </div>
        <?php include('footer.php');?>

         <?php include('script.php');?>


          <script type="text/javascript">
      function deleteData(id) {
        var res = confirm("Are you sure you want to delete this?");
          if(res == true) {
          var url="<?php echo base_url()."Admin/deleteSubCategory/"?>";
        var redirectUrl
             $.ajax({
              type: "POST",
              url: url,
              data:({
                id: id
               }),
              cache: false,
              success: function(data)
              {
                if(data == 1) {
                  swal("Success!","Data has been Deleted successfully","success");
                    } else {
                        alert(" Data has  not been deleted.");
                    }
                window.location.reload();
              
              }
         });
      }
      }
</script>


         <script>
      function changeCountryStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this Sub-category?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeSubCategoryStatus/"?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/sub_category'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Category status has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Category status has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>
  </body>
</html>