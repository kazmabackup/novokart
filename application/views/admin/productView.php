
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
           
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          
             
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">

                <div class="card-header border-bottom">
                   <a href="<?php echo base_url('Admin/addEditProduct');?>" class="btn btn-mini btn-success" > Add</a>
<!--                     <h6 class="m-0">Active Users</h6>
 -->                </div>
                <div class="card-body p-0 pb-3 text-center">
                  <table id="example" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>S. No.</th>
                 <th>Category</th>
                 <th>Sub Category</th>
                <!-- <th>Image</th> -->
                 <th>Product Name</th>
                 <th>Quantity</th>
                 <th>Price</th>
                 <th>Cost Price</th>
                 <th>Action</th>
            </tr>
               </thead>
 
               <tbody>
                <?php
                if(!empty($productdata)){
                    $i = 1;
                    foreach ( $productdata as $detail ) {
                      $id = $detail ['product_id'];
                      $product_name = $detail ['product_name'];
                      $sub_category_id = $detail ['sub_category_id'];
					  $where2="sub_category_id=$sub_category_id";
					   $subcatData = $this->Adminmodel->getDataById ( 'sub_category', $where2 );
					   $sub_category_name=$subcatData[0]['sub_category_name'];
					   //$category_name=$catData[0]['category_name'];
                      $category_id = $detail ['category_id'];
					  $where="category_id=$category_id";
					   $catData = $this->Adminmodel->getDataById ( 'category', $where );
					   $category_name=$catData[0]['category_name'];
					   
                      $price = $detail ['actual_price'];
                      $dis_price = $detail ['discount_price'];
                      $cost_price = $detail ['cost_price'];
                      $quantity = $detail ['quantity'];
                      $availability = $detail ['sold_quantity'];
                      $status = $detail ['status'];
                      ?>
           
            <tr>
               <td><?php echo $i;?></td>
               <td><?php echo $category_name;?></td>
               <td><?php echo $sub_category_name;?></td>
               <!-- <td><img src="<?php echo base_url()?>uploads/product/<?php echo $detail['image'];?>" width="50" height="40" /></td> -->
               <td><?php echo $product_name;?></td>
               <td><?php echo $quantity;?></td>
               <td><?php echo $dis_price;?></td>
               <td><?php echo $cost_price;?></td>
                  
                <td><?php if($status == 'Active'){?>
                  <a href="" class="btn btn-mini btn-success" onclick="changeCountryStatus(<?php echo $id;?>,'In-Active')" ><?php echo $status;?></a>
                  <?php }else{?>
                  <a href="" class="btn btn-mini btn-danger" onclick="changeCountryStatus(<?php echo $id;?>, 'Active')" ><?php echo $status;?></a>
                  <?php }?>
                    <a href="<?php echo base_url('Admin/addEditProduct');?>/<?php echo $id;?>" class="btn btn-mini btn-success" > Edit</a>
                    <a  onclick="deleteData(<?php echo $id;?>)" class="btn btn-mini btn-success" > Delete</a>
					<!--  <a href="<?php echo base_url('Admin/productPrice');?>/<?php echo $id;?>" class="btn btn-mini btn-success" > Manage price</a> -->
                </td>
            </tr>
            <?php $i++;} }?>
             </tbody>
          </table>
                  </div>
                </div>
              </div>
            </div>
              
            <!-- End Default Light Table -->
           
          </div>
        <?php include('footer.php');?>

         <?php include('script.php');?>


          <script type="text/javascript">
      function deleteData(id) {
        var res = confirm("Are you sure you want to delete this?");
          if(res == true) {
          var url="<?php echo base_url()."Admin/deleteproduct/"?>";
        var redirectUrl
             $.ajax({
              type: "POST",
              url: url,
              data:({
                id: id
               }),
              cache: false,
              success: function(data)
              {
                if(data == 1) {
                  swal("Success!","product has been Deleted successfully","success");
                    } else {
                        alert(" product has  not been deleted.");
                    }
                window.location.reload();
              
              }
         });
      }
      }
</script>


         <script>
      function changeCountryStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this Product?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeProductStatus/"?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/manageproduct'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Product status has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Product status has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>

  <script>
      function changeStockStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this Product?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeStockStatus/"?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/product'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Product Stock status has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Product Stock status has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>
  </body>
</html>