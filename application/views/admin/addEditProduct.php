
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($productdata)){?>


             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Product</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $productdata[0]['product_id'];?>" />

                             <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feInputState">Category</label>
                                <?php if ($categoryDropdown) {
                                  if (isset ( $_POST ["category_id"] )) {
                                    $location = $_POST ["category_id"];
                                  }else if($productdata[0]['category_id']){
                                   $location = $productdata[0]['category_id'];
                                   } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="category_id" name="category_id" onchange="getCategory(this.value)"';
                                  echo form_dropdown ( 'category_id', $categoryDropdown, $location, $js );
                                }
                                ?>
                                <span class="category_idErr error1" style="color: red;"></span> 
                              </div>
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Sub Category</label>
                                <?php if ($subcategoryDropdown) {
                                  if (isset ( $_POST ["sub_category_id"] )) {
                                    $location = $_POST ["sub_category_id"];
                                  }else if($productdata[0]['sub_category_id']){
                                   $location = $productdata[0]['sub_category_id'];
                                   } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="sub_category_id" name="sub_category_id"';
                                  echo form_dropdown ( 'sub_category_id', $subcategoryDropdown, $location, $js );
                                }
                                ?>
                                 <span class="sub_category_idErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Product Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Product Name" value="<?php echo $productdata[0]['product_name'];?>"> <span class="nameErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Quantity</label>
                                <input type="text" id="quantity" name="quantity" class="form-control"  placeholder="Quantity" value="<?php echo $productdata[0]['quantity'];?>"> <span class="quantityErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Price</label>
                                <input type="text" id="price" name="price" class="form-control"  placeholder="Product Price" value="<?php echo $productdata[0]['actual_price'];?>"> <span class="priceErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Our Price</label>
                                <input type="text" id="dis_price" name="dis_price" class="form-control"  placeholder="Discounted Price" value="<?php echo $productdata[0]['discount_price'];?>"> <span class="dis_priceErr error1" style="color: red;"></span>  </div>
                            </div>
                            <div class="form-row">
                             <div class="form-group col-md-6">
                                <label for="feFirstName">Cost Price</label>
                                <input type="text" id="cost_price" name="cost_price" class="form-control"  placeholder="Cost Price" value="<?php echo $productdata[0]['cost_price'];?>"> <span class="cost_priceErr error1" style="color: red;"></span>  </div>
                            <div class="form-group col-md-6">
                                <label for="feFirstName">Image</label>
                               <input class="form-control" type="file" id="img" name="img">
                               <img src="<?php echo base_url();?>uploads/product/<?php echo $productdata[0]['image'];?>" width="60" height="40" />
                                <span class="imgErr error1" style="color: red;"></span>  </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Description</label>
                                <textarea class="form-control" name="content" id="content" rows="5">
                                  <?php echo $productdata[0]['description'];?></textarea>
                                <span class="contentErr error1" style="color: red;"></span>
                              </div>
                            </div>

                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add Product</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         <form class="form-horizontal form-material" method="post" action="" id="formData0" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value=""/>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feInputState">Category</label>
                                <?php if ($categoryDropdown) {
                                  if (isset ( $_POST ["category_id"] )) {
                                    $location = $_POST ["category_id"];
                                  } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="category_id" name="category_id" onchange="getCategory(this.value)"';
                                  echo form_dropdown ( 'category_id', $categoryDropdown, 0, $js );
                                }
                                ?>
                                <span class="category_idErr error1" style="color: red;"></span> 
                              </div>
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Sub Category</label>
                                <!-- <input type="text" id="sub_category_id" name="sub_category_id" class="form-control"  placeholder="Sub Category Name" value=""> -->
                                <select class="form-control" id="sub_category_id" name="sub_category_id"></select>
                                 <span class="sub_category_idErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Product Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Product Name" value=""> <span class="nameErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Quantity</label>
                                <input type="text" id="quantity" name="quantity" class="form-control"  placeholder="Quantity" value=""> <span class="quantityErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Price</label>
                                <input type="text" id="price" name="price" class="form-control"  placeholder="Product Price" value=""> <span class="priceErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Our Price</label>
                                <input type="text" id="dis_price" name="dis_price" class="form-control"  placeholder="Discounted Price" value=""> <span class="dis_priceErr error1" style="color: red;"></span>  </div>
                            </div>
                            <div class="form-row">
                             <div class="form-group col-md-6">
                                <label for="feFirstName">Cost Price</label>
                                <input type="text" id="cost_price" name="cost_price" class="form-control"  placeholder="Cost Price" value=""> <span class="cost_priceErr error1" style="color: red;"></span>  </div>
							 <div class="form-group col-md-6">
                                <label for="feFirstName">Image</label>
                               <input class="form-control" type="file" id="img" name="img"> <span class="imgErr error1" style="color: red;"></span>  </div>
                             </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Description</label>
                                <textarea class="form-control" name="content" id="content" rows="5"></textarea>
                                <span class="contentErr error1" style="color: red;"></span>
                              </div>
                            </div>
                            <button type="submit" onclick="return formValidationAdd();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
   <?php include('script.php');?>

      <script>
        function formValidationAdd(){
         var data = new FormData($('#formData0')[0]);
         var hid = $("#id").val();
         var img = $("#img").val(); 
         var category_id = $("#category_id").val(); 
         var sub_category_id = $("#sub_category_id").val(); 
         var title = $("#name").val(); 
         var quantity = $("#quantity").val(); 
         var price = $("#price").val(); 
         var dis_price = $("#dis_price").val();
         var cost_price = $("#cost_price").val(); 
         var vendor_id = $("#user_id").val();  
         var content = CKEDITOR.instances['content'].getData();
          
          $(".category_idErr").html("");
          $(".category_idErr").hide("");
          $(".sub_category_idErr").html("");
          $(".sub_category_idErr").hide("");
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".quantityErr").html("");
          $(".quantityErr").hide("");
          $(".priceErr").html("");
          $(".priceErr").hide("");
          $(".dis_priceErr").html("");
          $(".dis_priceErr").hide("");
          $(".cost_priceErr").html("");
          $(".cost_priceErr").hide("");
          $(".user_idErr").html("");
          $(".user_idErr").hide("");
          $(".imgErr").html("");
          $(".imgErr").hide("");
          $(".contentErr").html("");
          $(".contentErr").hide("");
          if(category_id == 0){
                $(".category_idErr").slideDown('slow');
                $(".category_idErr").html(" Select From list.");
                $("#category_id").focus();
                return false;
          }
          if(sub_category_id == 0){
                $(".sub_category_idErr").slideDown('slow');
                $(".sub_category_idErr").html(" Select From list.");
                $("#sub_category_id").focus();
                return false;
          }
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Product Name is required.");
            $("#name").focus();
            return false;
          }if(quantity==""){
            $(".quantityErr").slideDown('slow');
            $(".quantityErr").html("Quantity is required.");
            $("#quantity").focus();
            return false;
          }
           if(price==""){
            $(".priceErr").slideDown('slow');
            $(".priceErr").html("Price is required.");
            $("#price").focus();
            return false;
          }if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price is required.");
            $("#dis_price").focus();
            return false;
          }
          
         if(parseFloat(price) < parseFloat(dis_price)){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price Should be less than Price.");
            $("#dis_price").focus();
            return false;
          }
         if(cost_price==""){
             $(".cost_priceErr").slideDown('slow');
             $(".cost_priceErr").html("Cost Price is required.");
             $("#cost_price").focus();
             return false;
           }
          if(vendor_id == 0){
                $(".user_idErr").slideDown('slow');
                $(".user_idErr").html(" Select From list.");
                $("#user_id").focus();
                return false;
          }if(img==""){
              $(".imgErr").slideDown('slow');
              $(".imgErr").html("Image is required.");
              $("#img").focus();
              return false;
          }if(content==""){
              $(".contentErr").slideDown('slow');
              $(".contentErr").html("Description is required.");
              $("#content").focus();
              return false;
          }
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditProductSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/manageProduct'?>"; 
            data.append('content', content);
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Product has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","Product has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 3){
                swal("Error!","Product name already exists","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Product has not been Updated successfully","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 



        <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
       //  var img = $("#img").val(); 
         var category_id = $("#category_id").val(); 
         var sub_category_id = $("#sub_category_id").val(); 
         var title = $("#name").val(); 
         var quantity = $("#quantity").val(); 
         var price = $("#price").val(); 
         var dis_price = $("#dis_price").val(); 
         var cost_price = $("#cost_price").val(); 
         var vendor_id = $("#user_id").val();  
         var content = CKEDITOR.instances['content'].getData();
          
          $(".category_idErr").html("");
          $(".category_idErr").hide("");
          $(".sub_category_idErr").html("");
          $(".sub_category_idErr").hide("");
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".quantityErr").html("");
          $(".quantityErr").hide("");
          $(".priceErr").html("");
          $(".priceErr").hide("");
          $(".dis_priceErr").html("");
          $(".dis_priceErr").hide("");
          $(".cost_priceErr").html("");
          $(".cost_priceErr").hide("");
          $(".user_idErr").html("");
          $(".user_idErr").hide("");
          // $(".imgErr").html("");
          // $(".imgErr").hide("");
          $(".contentErr").html("");
          $(".contentErr").hide("");
          if(category_id == 0){
                $(".category_idErr").slideDown('slow');
                $(".category_idErr").html(" Select From list.");
                $("#category_id").focus();
                return false;
          }
          if(sub_category_id == 0){
                $(".sub_category_idErr").slideDown('slow');
                $(".sub_category_idErr").html(" Select From list.");
                $("#sub_category_id").focus();
                return false;
          }
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Product Name is required.");
            $("#name").focus();
            return false;
          }if(quantity==""){
            $(".quantityErr").slideDown('slow');
            $(".quantityErr").html("Quantity is required.");
            $("#quantity").focus();
            return false;
          }
           if(price==""){
            $(".priceErr").slideDown('slow');
            $(".priceErr").html("Price is required.");
            $("#price").focus();
            return false;
          }if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price is required.");
            $("#dis_price").focus();
            return false;
          }
          if(parseFloat(price) < parseFloat(dis_price)){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discounted Price Should be less than Price.");
            $("#dis_price").focus();
            return false;
          }
          if(cost_price=="0"){
              $(".cost_priceErr").slideDown('slow');
              $(".cost_priceErr").html("Cost Price is required.");
              $("#cost_price").focus();
              return false;
            }
          if(content==""){
              $(".contentErr").slideDown('slow');
              $(".contentErr").html("Description is required.");
              $("#content").focus();
              return false;
          }
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditProductSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/manageProduct'?>"; 
            data.append('content', content);
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Product has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","Product has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 3){
                swal("Error!","Product name already exists","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Product has not been Updated successfully","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 

      <script>
              CKEDITOR.replace( 'content', {
              });

              function getCategory(val){
          //alert(val);
          var ctrlUrl = "<?php echo base_url().'Admin/getSubCategoryByCategory' ;?>";
               //$("#loadDiv").show(); 
              $.ajax({
                type: "POST",
                url: ctrlUrl,
                data:({
                  category_id :val
                }),
                cache: false,
                success: function(data)
                { 
                    $("#sub_category_id").html(data);
                }
              });
        }
      </script>
<script type="text/javascript">
$('#price').keypress(function(event) {
	  if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
	    ((event.which < 48 || event.which > 57) &&
	      (event.which != 0 && event.which != 8))) {
	    event.preventDefault();
	  }

	  var text = $(this).val();

	  if ((text.indexOf('.') != -1) &&
	    (text.substring(text.indexOf('.')).length > 2) &&
	    (event.which != 0 && event.which != 8) &&
	    ($(this)[0].selectionStart >= text.length - 2)) {
	    event.preventDefault();
	  }
	});
/* $('#price').keypress(function(event){
    console.log(event.which);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
    $(".priceErr").slideDown('slow');
    $(".priceErr").html("Enter numeric value only.");
    event.preventDefault();
}else{
	$(".priceErr").html("");	
}}); */
/* $('#cost_price').keypress(function(event){
    console.log(event.which);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
	  $(".cost_priceErr").slideDown('slow');
	    $(".cost_priceErr").html("Enter numeric value only.");
	    event.preventDefault();
	}else{
		$(".cost_priceErr").html("");	
	}}); */
	$('#cost_price').keypress(function(event) {
		  if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
		    ((event.which < 48 || event.which > 57) &&
		      (event.which != 0 && event.which != 8))) {
		    event.preventDefault();
		  }

		  var text = $(this).val();

		  if ((text.indexOf('.') != -1) &&
		    (text.substring(text.indexOf('.')).length > 2) &&
		    (event.which != 0 && event.which != 8) &&
		    ($(this)[0].selectionStart >= text.length - 2)) {
		    event.preventDefault();
		  }
		});
/* $('#dis_price').keypress(function(event){
    console.log(event.which);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
	  $(".dis_priceErr").slideDown('slow');
	    $(".dis_priceErr").html("Enter numeric value only.");
	    event.preventDefault();
	}else{
		$(".dis_priceErr").html("");	
	}}); */
	$('#dis_price').keypress(function(event) {
		  if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
		    ((event.which < 48 || event.which > 57) &&
		      (event.which != 0 && event.which != 8))) {
		    event.preventDefault();
		  }

		  var text = $(this).val();

		  if ((text.indexOf('.') != -1) &&
		    (text.substring(text.indexOf('.')).length > 2) &&
		    (event.which != 0 && event.which != 8) &&
		    ($(this)[0].selectionStart >= text.length - 2)) {
		    event.preventDefault();
		  }
		});
$('#quantity').keypress(function(event){
    console.log(event.which);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
	  $(".quantityErr").slideDown('slow');
	    $(".quantityErr").html("Enter numeric value only.");
	    event.preventDefault();
	}else{
		$(".quantityErr").html("");	
	}});
$("#img").change(function() {

    var val = $(this).val();

    switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
        case 'gif': case 'jpg': case 'png':
            
            break;
        default:
            $(this).val('');
            // error message here
            $(".imgErr").slideDown('slow');
	    $(".imgErr").html("Select only jpg/gif/png images.");
            break;
    }
});
      </script>
  </body>
</html>