<div class="modal-dialog" style="max-width:775px;">
                                 <div class="modal-content" >
                                    <!-- <img class="white-logo" src="img/logo-white.png"> -->
                                    <div class="modal-header">
                                       
                                       <h4 class="modal-title">
                                          Order details
                                       </h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                       ×
                                       </button>
                                    </div>
                                    <div class="modal-body" style="float:left;width:100%;">
                                    <table>
                                     <?php if ($orderdata){
                                     	$userid = $orderdata[0]['user_id'];
                                     	$uniqueid = $orderdata[0]['unique_order_id'];
                                     	$whereuser="user_id='$userid'";
                                     	$userdata = $this->Adminmodel->getDataById ( 'user', $whereuser );
                                     ?>
                                    <tr>
                                    <td style="float:left;width:100%">Name: <?php echo $userdata[0]['user_name'];?></td>
                                    <td style="float:left;width:100%">Order Id: <?php echo $uniqueid;?></td>
                                    </tr>
                                    <?php }?>
                                    </table>
           
                 <table class="cart" style="width: 100%;">
                 
                    <thead>
                       <tr>
                          <th>Product name</th>
                          <th style="">Quantity</th>
                          <th>Cost price</th>
                          <th>Discounted Price</th>
                          <th>Product  Wise Margin(%)</th>
                         
                       </tr>
                    </thead>
                    <tbody>
                    <?php if ($orderdata){
                    	$discounted_price = 0;
                    	$cost_price = 0;
                    	$total_price = 0;
                    	$is_add = $orderdata[0]['is_add'];
                    	if ($is_add !="1"){
                    foreach ($orderdata as $order){
                    	$productid = $order['pro_id'];
                    	$affiliate_percentage = $order['affiliate_percentage'];
                    	$affdis = $order['dis_by_agent'];
                    	$algorithm = $order['algorithm'];
                    	$wherepro="product_id='$productid'";
                    	$productdata = $this->Adminmodel->getDataById ( 'product', $wherepro );
                    	$productname = $productdata[0]['product_name'];
                    	$productimage = $productdata[0]['image'];
                    	$order_quantity = $order['order_quantity'];
                    	$price = $order['pro_price'] * $order_quantity;
                    	$total_price = $total_price + $price;
                    	$cost_price1 = ($order['pro_cost_price'] * $order_quantity);
                    	$cost_price = $cost_price + ($order['pro_cost_price'] * $order_quantity);
                    	$margin = ($price - $cost_price1)/$price;
                    	$commulative_per = (($total_price - $cost_price)/$total_price)*100;
                    	$company_profit = ($company_margin/100);
                    	if ($algorithm=="1"){
                    	$package_per = (($commulative_per/100)*1)-($affdis/100);
                    	if ($package_per > $company_profit){
                    		$new_total_dis = ($price - $cost_price1)*$affiliate_percentage/100;
                    	}else {
                    		$new_total_dis = (($price - $cost_price1)*$affiliate_percentage/100)-$company_margin/100;
                    	}
                    	} else {
                    		$package_per = ($commulative_per - $company_margin);
                    		if ($package_per > $affiliate_percentage){
                    			$new_total_dis = ($price)*$affiliate_percentage/100;
                    		}
                    	}
                    	// echo $new_total_dis;
                    	$hostital_dis = ($new_total_dis*$affdis/100);
                    	$hostital_dis_per = ($hostital_dis/$price)*100;
                    	$afterdisamount = ($price - $hostital_dis);
                    	$discounted_price = $discounted_price+($price);
                    ?>
                       <tr>
                          <td><div><?php echo $productname;?></div>
                          </td>
                          <td>
                             <?php echo $order_quantity;?>
                          </td>
                          <td>
                            <i class="fa fa-rupee"></i> <?php echo number_format($cost_price1,2);?>
                          </td>
                          <td>
                             <i class="fa fa-rupee"></i> <?php echo number_format($price,2);?>
                          </td>
                          <td>
                              <?php echo number_format($margin*100,2);?>
                          </td>
                          
                       </tr>
                      <?php }
                   ?>
                    <tr>
                    <hr style="float: left:width:100%">
                    </tr>
                    <tr style="border-top:1px solid #ddd;">
                <td>
                             Total
                          </td>  
                          <td>
                             
                          </td>
                           
                          <td>
                             <i class="fa fa-rupee"></i> <?php echo number_format($cost_price,2);?>
                          </td> 
                          <td>
                            <i class="fa fa-rupee"></i> <?php echo number_format($discounted_price,2);?>
                          </td>
                           <td>
                             <?php echo number_format(($total_price-$cost_price)*100/$total_price,2);?>
                          </td>
                          </tr>  
                          <?php  } else {?><span>Order is not completed</span><?php } }?>
                    </tbody>
                      
                 </table>
                 
               </div>
               <div class="modal-footer">
                        
                </div>
                </div>
               </div>