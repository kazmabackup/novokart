
	<!DOCTYPE html>
	<html>
	<head>
		<title>Novokart | Login</title>
		 <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/admin/images/logo.png">
		  <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Novokart</title>
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="<?php echo base_url();?>assets/admin/styles/shards-dashboards.1.0.0.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/styles/extras.1.0.0.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/styles.css">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
		<style>

			/* Design based on Blue Login Field of Kevin Sleger https://codepen.io/MurmeltierS/pen/macKb */
body {
  background: #F5F6FA;
  background-size: cover;
  font-family: "Roboto";
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
body::before {
  z-index: -1;
  content: '';
  position: fixed;
  top: 0;
  left: 0;
  background: #F5F6FA;
  /* IE Fallback */
  /*background: rgba(68, 196, 231, 0.8);*/
  width: 100%;
  height: 100%;
}
.form {
  position: absolute;
  top: 44%;
  left: 50%;
  background: #fff;
  width: 340px;
  margin: -140px 0 0 -182px;
  padding: 40px;
  box-shadow: 0 2px 0 rgba(90,97,105,.11),0 4px 8px rgba(90,97,105,.12),0 10px 10px rgba(90,97,105,.06),0 7px 70px rgba(90,97,105,.1);
	border-radius: 7px;
}
.form h2 {
  margin: 0 0 20px;
  line-height: 1;
  color: #44c4e7;
  font-size: 18px;
  font-weight: 400;
}
.form input {
  outline: none;
  display: block;
  width: 100%;
  margin: 0 0 20px;
  padding: 10px 15px;
  border: 1px solid #ccc;
  color: #444;
  font-family: "Roboto";
  box-sizing: border-box;
  font-size: 14px;
  font-wieght: 400;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  transition: 0.2s linear;
}
.form inputinput:focus {
  color: #333;
  border: 1px solid #44c4e7;
}
.form button {
  cursor: pointer;
  /*background: #44c4e7;*/
  width: 100%;
  padding: 10px 15px;
  border: 0;
  color: #fff;
  /*font-family: "Roboto";*/
  font-size: 16px;
  font-weight: 400;
}
/*.form button:hover {
  background: #369cb8;
}*/
.error,
.valid {
  display: none;
}
.error{
height: auto;
}
.btn{
	color: #fff;

border-color: #C39E00;

background-color: #C39E00;

box-shadow: none;
}
.form-control:focus
		</style>
	</head>
	<body>
	<!--Correct username: invitado / pass: hgm2015-->

<section class="form animated flipInX">
  <!-- <h2>Highways</h2> -->
  <img src="<?php echo base_url();?>assets/admin/images/logo.png" style="width: 100%;margin-bottom: 15px;">
  <p class="valid">Valid. Please wait a moment.</p>
  <p class="error">Error. Please enter correct Username &amp; password.</p>
  <form action="" method="post" class="loginbox" autocomplete="off">
    <input class="form-control" placeholder="Email" type="text" name="email" id="email"></input>
    <span class="emailErr error" style=" color:red;"></span>

    <input placeholder="Password" class="form-control" type="password" id="password" name="password"></input>
    <span class="passwordErr error" style=" color:red;"></span>
    <input type="submit" class="btn" id="loginBtn" value="Login" style="color: #fff;">
		<!-- <button id="submit" class="btn">Login</button> -->		
			<span class="invalidLogin error" style="color:red; margin-left: 100px;"></span>

</form>
</section>
	<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
	$(document).ready(function() {
    //$('#username').focus();

    $('#submit').click(function() {

        event.preventDefault(); // prevent PageReLoad

       var ValidEmail = $('#username').val() === 'invitado'; // User validate
var ValidPassword = $('#password').val() === 'hgm2015'; // Password validate

        if (ValidEmail === true && ValidPassword === true) { // if ValidEmail & ValidPassword
            $('.valid').css('display', 'block');
            window.location = "http://arkev.com"; // go to home.html
        }
        else {
            $('.error').css('display', 'block'); // show error msg
        }
    });
});
</script>

<script>
	$(document).ready(function() {
	$("#loginBtn").click(function() { 
		var email= $("#email").val(); 
		var password=$("#password").val();
		$(".emailErr").html("");
		$(".passwordErr").html("");
		$(".emailErr").hide();
		$(".passwordErr").hide();
		var emailRegexStr = /^[a-zA-Z0-9/+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		var isvalid = emailRegexStr.test(email); 
		if(email==""){
			$(".emailErr").slideDown('slow');
			$(".emailErr").html("Email field is required.");
		}if(!isvalid){
			$(".emailErr").slideDown('slow');
			$(".emailErr").html("Please enter valid Email address.");
		}
		if(password==""){
			$(".passwordErr").slideDown('slow');
			$(".passwordErr").html("Password field is required.");
		}else{
			var ctrlUrl = "<?php echo base_url().'Admin/login';?>";
			var adminRedirectUrl="<?php echo base_url().'Admin/dashboard'?>"; 
			$.ajax({
				type: "POST",
				url: ctrlUrl,
				data:({
					email 	 : email,
					password : password,
				}),
				cache: false,
				success: function(data)
				{ //alert(data);
				if(data== 0){
					//alert("bbbb");
					$(".invalidLogin").html("Invalid login details.<br/>");
					$(".invalidLogin").slideDown('slow');
				}else {
				window.location.href=adminRedirectUrl;
				}
				}
			});

		}
		return false;
		});
	});
	$(document).ready(function(){
	 $('#password').keypress(function(e){
		 if(e.keyCode==13)
		 $('#loginBtn').click();
		});
	});
	</script>

	</body>
	</html>