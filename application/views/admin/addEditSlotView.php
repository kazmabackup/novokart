<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $bredcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($slotData)){?>


             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Slot</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $slotData[0]['slot_id'];?>" />
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Start Time</label>
                                <input type="text" id="timepicker1" name="timepicker1" class="form-control"  placeholder="Start Time" value="<?php echo $slotData[0]['start_time'];?>"> <span class="timepicker1Err error1" style="color: red;"></span>  </div>


                                <div class="form-group col-md-6">
                                <label for="feFirstName">End Time</label>
                                <input type="text" id="timepicker2" name="timepicker2" class="form-control"  placeholder="End Time" value="<?php echo $slotData[0]['end_time'];?>"> <span class="timepicker2Err error1" style="color: red;"></span>  </div>
                            </div>
                         <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add Slot</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                          <input type="hidden" id="id" name="id" value=""/>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Start Time</label>
                                <input type="text" id="timepicker1" name="timepicker1" class="form-control"  placeholder="Start Time" value=""> <span class="timepicker1Err error1" style="color: red;"></span>  </div>


                                <div class="form-group col-md-6">
                                <label for="feFirstName">End Time</label>
                                <input type="text" id="timepicker2" name="timepicker2" class="form-control"  placeholder="End Time" value=""> <span class="timepicker2Err error1" style="color: red;"></span>  </div>
                            </div>
                           
                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
      <?php include('script.php');?>



             <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
         var sttime = $("#timepicker1").val();
         var endtime = $("#timepicker2").val();
              $(".timepicker1Err").html("");
               $(".timepicker1Err").hide("");
               $(".timepicker2Err").html("");
               $(".timepicker2Err").hide("");
              if(sttime==""){
                $(".timepicker1Err").slideDown('slow');
                $(".timepicker1Err").html("Choose Start time.");
                $("#timepicker1").focus();
                return false;
              }
              if(endtime==""){
                $(".timepicker2Err").slideDown('slow');
                $(".timepicker2Err").html("Choose end time.");
                $("#timepicker2").focus();
                return false;
              }
              
              else{
                var ctrlUrl = "<?php echo base_url().'Admin/addEditSlotSubmit' ;?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/manageSlot'?>"; 
                $("#loadDiv").show(); 
                $.ajax({
                  type: "POST",
                  url: ctrlUrl,
                  data:data,
                  mimeType: "multipart/form-data",
                  contentType: false,
                  cache: false,
                  processData: false,
                  success: function(data)
                  {// alert(data);
                    $("#loadDiv").hide(); 
                  if(data==1){
                    swal("Success!","Data has been Added successfully","success");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }else if(data== 2){
                    swal("Success!","Data has been Updated successfully","success");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }else if(data== 3){
                     swal("This Slot Already Exist.");

                  }
                  else {
                    swal("Error!","Data has not been Updated successfully","error");
                     
                  } 
                }
                });

              }
              return false;
        }
      </script> 

      <!--  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css"> -->
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
        <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
          <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script> -->
          <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

      <script>
        $( function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd'});
    $('#timepicker1').timepicker({
       
      timeFormat: 'h:mm p',
      interval: 60,
      minTime: '10',
      maxTime: '6:00pm',
      defaultTime: '',
      startTime: '10:00',
      dynamic: false,
      dropdown: true,
      scrollbar: true
  });
    $('#timepicker2').timepicker({
      timeFormat: 'h:mm p',
      interval: 60,
      minTime: '10',
      maxTime: '6:00pm',
      defaultTime: '',
      startTime: '10:00',
      dynamic: false,
      dropdown: true,
      scrollbar: true
  });
        });
    </script>

  </body>
</html>