
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3 text-center">
                <?php if($responce = $this->session->flashdata('Successfully')): ?>
      <div class="box-header">
        <div class="col-lg-6">
           <div class="alert alert-success"><?php echo $responce;?></div>
        </div>
      </div>
       <?php endif;?>
       <button class="btn btn-sm btn-primary add_more_button">Add More Products</button>
                <form action="<?php echo base_url()?>Admin/editorder/<?php echo $orderid;?>" method = "post">
                 <table id="example1" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>Category</th>
				<th>Sub category</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>price</th>
                <th>Cost price</th>
                <th>Product wise margin(%)</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
               </thead>
              
               <tbody class="input_fields_container">
               <?php if (!empty($orderdata)){
               	$i=0;
               foreach ($orderdata as $order){
               	$manage_order_id = $order['manage_order_id'];
               	$quantity = $order['order_quantity'];
                $pro_id = $order['pro_id'];
                $affiliate_percentage = $order['affiliate_percentage'];
                $affdis = $order['dis_by_agent'];
               	$whereproid= "product_id='$pro_id'";
               	$productdata = $this->Adminmodel->getDataByuserId("product", $whereproid);
               	$cateid = $productdata[0]['category_id'];
               	$Price = $productdata[0]['discount_price'];
               	$costPrice = $productdata[0]['cost_price'];
               //	$costPrice = $productdata[0]['cost_price'];
               	$wherecatid= "category_id='$cateid'";
               	$subcategorydata = $this->Adminmodel->getDataByuserId("sub_category", $wherecatid);
               	$subcatid = $productdata[0]['sub_category_id'];
               	$whereproductid= "category_id='$cateid' AND sub_category_id = '$subcatid'";
               	$productdropdata = $this->Adminmodel->getDataByuserId("product", $whereproductid);
               	$margin = ($Price - $costPrice)/$Price;
               /* 	$commulative_per = (($Price - $costPrice)/$Price)*100;
               	$company_profit = ($company_margin/100);
               	$package_per = (($commulative_per/100)*1)-($affdis/100);
               	if ($package_per > $company_profit){
               		$new_total_dis = ($Price - $costPrice)*$affiliate_percentage/100;
               	}else {
               		$new_total_dis = (($Price - $costPrice)*$affiliate_percentage/100)-$company_margin/100;
               	}
               	// echo $new_total_dis;
               	$hostital_dis = ($new_total_dis*$affdis/100);
               	$hostital_dis_per = ($hostital_dis/$Price)*100;
               	$discounted_price = $discounted_price+($Price - $hostital_dis); */
              ?>
                <tr>
                <td><select class="from-control" name="cat[]" id="cat" onchange="getsubcat(this.value,<?php echo $i;?>)" required>
                <option value="0">select category</option>
                <?php if ($categoryDropdown){
                foreach ($categoryDropdown as $cat){
                	$catid = $cat['category_id'];
                	$catname = $cat['category_name'];
               ?>
                <option value="<?php echo $catid;?>" <?php if($cateid==$catid){ echo 'selected'; }?>><?php echo $catname;?></option>
                <?php  }
                }?>
                 </select>
                  <span class="catErr error" style="color: red;"></span> 
                 </td>
                 <td><select class="from-control" name="subcat[]" id="subcat<?php echo $i;?>" onchange="getproduct(this.value,<?php echo $i;?>)" required>
                 <?php if ($subcategorydata){
                 foreach ($subcategorydata as $subcat){
                 	$subid = $subcat['sub_category_id'];
                 	$subname = $subcat['sub_category_name'];
               ?>
                 <option value="<?php echo $subid;?>" <?php if($subcatid==$subid){ echo 'selected'; }?>><?php echo $subname;?></option>
                 <?php }}?>
                 </select>
                 <select class="from-control" name="subcat[]" id="subcat0" onchange="getproduct(this.value,<?php echo $i;?>)" style="display:none">
                 <option value="0">select sub category</option>
                 </select>
                  <span class="subcatErr error" style="color: red;"></span> 
                 </td>
                 <td><select class="from-control" name="product[]" id="product<?php echo $i;?>" required onchange="getprice(this.value,<?php echo $i;?>)">
                 <?php if ($productdropdata){
                 foreach ($productdropdata as $subcat){
                 	$proid = $subcat['product_id'];
                 	$proname = $subcat['product_name'];
               ?>
                 <option value="<?php echo $proid;?>" <?php if($pro_id==$proid){ echo 'selected'; }?>><?php echo $proname;?></option>
                 <?php }}?>
                 </select>
                 <select class="from-control pro" name ="product[]" id="product<?php echo $i;?>" disabled style="display:none">
                 <option value="0">select product</option>
                 </select>
                  <span class="productErr error" style="color: red;"></span>
                 </td>
                 <td><input class="form-control" id="quantity0" onchange="getTotal(this.value,<?php echo $i;?>)" name="quantity[]" value="<?php echo $quantity;?>" required>
                  <span class="quantityErr error" style="color: red;"></span>
                 </td>
                  <td><input class="form-control" id="price<?php echo $i;?>" name="price" value="<?php echo $Price;?>" readonly></td>
                 
                 <td><input class="form-control" id="costprice<?php echo $i;?>" name="costprice" value="<?php echo $costPrice;?>" readonly></td>
                 <td><input class="form-control" id="margin<?php echo $i;?>" name="margin" value="<?php echo number_format($margin*100,2);?>" readonly></td>
                 <td><input class="form-control" id="total<?php echo $i;?>" name="total" value="<?php echo number_format($Price*$quantity,2);?>" readonly></td>
                <td> <a class="" onclick="return deleteorder(<?php echo $manage_order_id;?>)">delete</a></td>
                 
                 </tr>
                 <?php $i++;}}?>
             </tbody>
             </table>
              <input type="submit" value="submit" onclick="return formvalidation();"/> 
              </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>
        <?php include('footer.php');?>
         <?php include('script.php');?>
  </body>
</html>
<script>
    $(document).ready(function() {
    var max_fields_limit = 10; //set limit for maximum input fields
    var x = "<?php echo count($orderdata);?>"; //initialize counter for text box
    $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
             //counter increment
            $('.input_fields_container').append('<tr><td><select class="from-control" name="cat[]" onchange="getsubcat(this.value,'+x+')" required><option value="">select category</option> <?php if ($categoryDropdown){foreach ($categoryDropdown as $cat){$catid = $cat['category_id'];$catname = $cat['category_name']; ?><option value="<?php echo $catid;?>"><?php echo $catname;?></option><?php  }}?></select></td><td><select class="from-control" name="subcat[]" id="subcat'+x+'" onchange="getproduct(this.value,'+x+')" required><option value="0">select sub category</option></select> </td> <td><select class="from-control" name="product[]" id="product'+x+'" required onchange="getprice(this.value,'+x+')"><option value="">select product</option></select></td><td><input class="form-control" onchange="getTotal(this.value,'+x+')" id="quantity" name="quantity[]" value="" required></td><td><input class="form-control" id="price'+x+'" name="price" value="" readonly></td> <td><input class="form-control" id="costprice'+x+'" name="costprice" value="" readonly></td> <td><input class="form-control" id="margin'+x+'" name="margin" value="" readonly></td><td><input class="form-control" id="total'+x+'" name="total" value="" readonly></td><td><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></td></tr>'); //add input field
            x++;
            }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){//alert("ok"); //user click on remove text links
        e.preventDefault(); $(this).closest('tr').remove(); x--;
    })
});
</script>
<script>
function getsubcat(ses,x){  
	//alert(ses);
	//alert(x);
	if(ses) { 
		var catid = ses;
		var baseUrl 	= "<?php echo base_url();?>";
		// url to get the search data
		var subCatUrl 	= baseUrl+"Admin/getsubcatbycatid";
		 //$("#loadDiv").show();
		$.ajax({
			type: "POST",
			url: subCatUrl,
			data:({
				catid : catid
				 }),
			cache: false,
			success: function(data)
			{  //alert(data);
				 $("#subcat").hide();
				 $("#subcat"+x).html(data).show();
			}
		});
	}
}
function getproduct(ses,x){  
	//alert(ses);
	//alert(x);
	if(ses) { 
		var subcatid = ses;
		var baseUrl 	= "<?php echo base_url();?>";
		// url to get the search data
		var subCatUrl 	= baseUrl+"Admin/getproductbysubcatid";
		 //$("#loadDiv").show();
		$.ajax({
			type: "POST",
			url: subCatUrl,
			data:({
				subcatid : subcatid
				 }),
			cache: false,
			success: function(data)
			{  //alert(data);
				//$('#pro').prop('disabled', 'true');
				 $("#product").hide();
				 $("#product"+x).html(data).show();
			}
		});
	}
}
function getprice(ses,x){  
	//alert(ses);
	//alert(x);
	if(ses) { 
		var proid = ses;
		var baseUrl 	= "<?php echo base_url();?>";
		// url to get the search data
		var subCatUrl 	= baseUrl+"Admin/getproductprice";
		 //$("#loadDiv").show();
		$.ajax({
			type: "POST",
			url: subCatUrl,
			data:({
				proid : proid
				 }),
			cache: false,
			success: function(data)
			{  //alert(data);
			var result = data.split(',');
			var price = result[0];
			var cost_price = result[1];
			var margin = result[2];
				//$('#pro').prop('disabled', 'true');
			 $("#price"+x).val(price);
			 $("#costprice"+x).val(cost_price);
			 $("#margin"+x).val(margin);
			}
		});
	}
}
function getTotal(ses,x){
	var quantity = ses;
	var price = $("#price"+x).val();
	var total = price*quantity;
	$("#total"+x).val(total);
	//alert(total);
}
</script>
<script>
function deleteorder(ses){  
	//alert(ses);
 var res = confirm("Are you sure you want to delete this product?");
	if(res) { 
		var manageorderid = ses;
		var baseUrl 	= "<?php echo base_url();?>";
		// url to get the search data
		var subCatUrl 	= baseUrl+"Admin/deleteOrder";
		 //$("#loadDiv").show();
		$.ajax({
			type: "POST",
			url: subCatUrl,
			data:({
				manageorderid : manageorderid
				 }),
			cache: false,
			success: function(data)
			{ // alert(data);
			if(data == 1) {
                swal("Success!","Product has been deleted from order successfully","success");
                setTimeout(function () 
                        {
                            window.location.href=adminRedirectUrl },4000);
                  } else {
                      alert("Product has not been deleted from order.");
                  }
              window.location.reload();	
			}
		});
	}
}

</script>

