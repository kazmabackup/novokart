
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
           
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          
             
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">

                <div class="card-header border-bottom">
                   <a href="<?php echo base_url();?>admin/addPrice/<?php echo $productdata[0]['product_id'];?>" class="btn btn-mini btn-success" > Add</a>
<!--                     <h6 class="m-0">Active Users</h6>
 -->                </div>
                <div class="card-body p-0 pb-3 text-center">
                  <table id="example" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>S. No.</th>
                <th>Size</th>
                <th>Actual Price</th>
                <th>Discounted Price</th>
       
                <th>Action</th>
                
            </tr>
               </thead>
               <tbody>
                <?php
                if(!empty($product_priceData)){
                    $i = 1;
                    foreach ( $product_priceData as $detail ) {
                    $id = $detail ['product_price_weight_id'];
                    $size = $detail ['weight'];
					 $actual_price = $detail ['actual_price'];
					 $dis_price = $detail ['dis_price'];
					 $discounted_price = $detail ['dis_price'];
                      
                      ?>
           
            <tr>
               <td><?php echo $i;?></td>
     
               <td><?php echo  $size;?></td>
               <td><?php echo $actual_price;?></td>
               <td><?php echo  $discounted_price;?></td>
                  
                <td>

                    <a href="<?php echo base_url('Admin/editPrice');?>/<?php echo $id;?>" class="btn btn-mini btn-success" > Edit</a>

                    <a  onclick="deleteData(<?php echo $id;?>)" class="btn btn-mini btn-success" > Delete</a>
                </td>
                
            </tr>

            <?php $i++;} }
              ?>
          
           
             </tbody>
          </table>
                  </div>
                </div>
              </div>
            </div>
              
            <!-- End Default Light Table -->
           
          </div>
        <?php include('footer.php');?>

         <?php include('script.php');?>


          <script type="text/javascript">
      function deleteData(id) {
        var res = confirm("Are you sure you want to delete this?");
          if(res == true) {
          var url="<?php echo base_url()."Admin/deleteprice/"?>";
        var redirectUrl
             $.ajax({
              type: "POST",
              url: url,
              data:({
                id: id
               }),
              cache: false,
              success: function(data)
              {
                if(data == 1) {
                  swal("Success!","product price has been Deleted successfully","success");
                    } else {
                        alert(" product has  not been deleted.");
                    }
                window.location.reload();
              
              }
         });
      }
      }
</script>


         <script>
      function changeCountryStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this Product?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeProductStatus/"?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/manageproduct'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Product status has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Product status has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>

  <script>
      function changeStockStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this Product?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeStockStatus/"?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/product'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Product Stock status has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Product Stock status has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>
  </body>
</html>