   <div class="modal-header">
          <h4 class="modal-title"><?php echo $type;?> Detail
           <button type="button" class="close" data-dismiss="modal">&times;</button>
          </h4>
      </div>
<?php if($type=='Product'){ ?>

<div class="modal-body">
          <p>Product Name : <?php echo $userData[0]['product_name'];?></p>
          <p>Product Price : <?php echo $userData[0]['price'];?> </p>
          <p>Discounted Price : <?php echo $userData[0]['dis_price'];?></p>
          <p>Category : <?php echo strip_tags($userData[0]['category_name']);?></p>
          <p>Sub Category : <?php echo strip_tags($userData[0]['sub_category_name']);?></p>
      </div>


 <?php } else { ?> 
 
      <div class="modal-body">
          <p>Name : <?php echo $userData[0]['firstname']." ".$userData[0]['lastname'];?></p>
          <p>Email : <?php echo $userData[0]['email'];?> </p>
          <p>Contact : <?php echo $userData[0]['contact'];?></p>
          <p>Address : <?php echo strip_tags($userData[0]['address']);?></p>

       <?php   if($type=='Vendor'){?>
          <p>GST : <?php echo $userData[0]['gst'];?></p>
          <p>Firm Detail : <?php echo strip_tags($userData[0]['firm_detail']);?></p>
          <?php } ?>
      </div>
 <?php }?>
     