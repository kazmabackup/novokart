<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   <style type="text/css">
     .error{
  height: auto;
     }
   </style>
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->
            <?php if(!empty($userdata)){?>
             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Customer</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $userdata[0]['user_id'];?>" />
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feInputState">Select Affiliate</label>
                                <?php if ($categoryDropdown) {
                                  if (isset ( $_POST ["affiliate_id"] )) {
                                    $location = $_POST ["affiliate_id"];
                                  }else if($userdata[0]['agent_id']){
                                   $location = $userdata[0]['agent_id'];
                                   } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="affiliate_id" name="affiliate_id" onchange="getCategory(this.value)"';
                                  echo form_dropdown ( 'affiliate_id', $categoryDropdown, $location, $js );
                                }
                                ?>
                                <span class="affiliate_idErr error1" style="color: red;"></span> 
                              </div>
							  <div class="form-group col-md-6">
                                <label for="feInputState">Select Package</label>
                                <?php if ($packageDropdown) {
                                  if (isset ( $_POST ["package_id"] )) {
                                    $location = $_POST ["package_id"];
                                  }else if($userdata[0]['package_id']){
                                   $location = $userdata[0]['package_id'];
                                   } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="package_id" name="package_id" onchange="getCategory(this.value)"';
                                  echo form_dropdown ( 'package_id', $packageDropdown, $location, $js );
                                }
                                ?>
                                <span class="package_idErr error1" style="color: red;"></span> 
                              </div>
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Discount(%)</label>
                                <input type="text" id="dis_per" name="dis_per" class="form-control"  placeholder="Discount Percentage" value="<?php echo $userdata[0]['package_percentage'];?>">
                                <span class="dis_perErr error" style="color: red;"></span>  </div> 
                                <div class="form-group col-md-6">
                                <label for="feFirstName">Minimum purches value</label>
                                <input type="number" id="min_value" name="min_value" class="form-control"  placeholder="Min purches value" value="<?php echo $userdata[0]['min_value'];?>">
                                <span class="min_valErr error" style="color: red;"></span>  </div>
                                 <div class="form-group col-md-6">
                                <label for="feFirstName">Algorithm</label><br/>
                                 <input type="radio" name="algo" <?php if ($userdata[0]['algorithm']=='1'){ echo "checked";}?> value="1"> Algo 1
                                 <input type="radio" name="algo" <?php if ($userdata[0]['algorithm']=='2'){ echo "checked";}?> value="2"> Algo 2<br>
                                  </div> 
                            </div>
                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add Affiliate</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         <form class="form-horizontal form-material" method="post" action="" id="formData0" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value=""/>
                           
                            <div class="form-row">
                               <div class="form-group col-md-6">
                                <label for="feInputState">Select Agent</label>
                                <?php if ($categoryDropdown) {
                                  if (isset ( $_POST ["affiliate_id"] )) {
                                    $location = $_POST ["affiliate_id"];
                                  } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="affiliate_id" name="affiliate_id" onchange="getCategory(this.value)"';
                                  echo form_dropdown ( 'affiliate_id', $categoryDropdown, 0, $js );
                                }
                                ?>
                                <span class="category_idErr error1" style="color: red;"></span> 
                              </div>
                                <div class="form-group col-md-6">
                                <label for="feFirstName">Discount(%)</label>
                                <input type="text" id="dis_per" name="dis_per" class="form-control"  placeholder="Discount Percentage" value="">
                                 <span class="dis_perErr error" style="color: red;"></span>  </div> 

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Minimum purches value</label>
                                <input type="number" id="min_value" name="min_value" class="form-control"  placeholder="Min purches value" value="">
                                 <span class="min_valErr error" style="color: red;"></span>  </div>
                            </div>
                            <button type="submit" onclick="return formValidationAdd();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
    <?php include('script.php');?>
      <script>
        function formValidationAdd(){
         var data = new FormData($('#formData0')[0]);
         if($("input:radio[name='algo']").is(":checked")){alert("ok");
         var hid = $("#id").val();
         var img = $("#img").val(); 
         var title = $("#name").val(); 
         var email = $("#email").val(); 
		 var phone = $("#phone").val(); 
		 var dis_price = $("#discount").val();
		 var emailRegexStr = /^[a-zA-Z0-9/+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		 var isvalid = emailRegexStr.test(email); 
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".quantityErr").html("");
          $(".quantityErr").hide("");
          $(".phoneErr").html("");
          $(".phoneErr").hide("");
          $(".dis_priceErr").html("");
          $(".dis_priceErr").hide("");
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Affiliate Name is required.");
            $("#name").focus();
            return false;
          }if(email==""){
            $(".quantityErr").slideDown('slow');
            $(".quantityErr").html("email is required.");
            $("#percentage").focus();
            return false;
          }if(!isvalid){
			$(".quantityErr").slideDown('slow');
			$(".quantityErr").html("Please enter valid Email address.");
			return false;
		}if(phone==""){
            $(".phoneErr").slideDown('slow');
            $(".phoneErr").html("Phone number is required.");
            $("#phone").focus();
            return false;
          }if(phone.length < 10 ){
			$(".phoneErr").slideDown('slow');
			$(".phoneErr").html("enter 10 digit mobile number.");
			return false;
		}
		 if(phone.length > 10 ){
			$(".phoneErr").slideDown('slow');
			$(".phoneErr").html("enter 10 digit mobile number.");
			return false;
		}
           if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discount Percentage is required.");
            $("#discount").focus();
            return false;
          } 
        
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEdituserSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/affiliate'?>"; 
            data.append('content', content);
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Affiliate has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","Affiliate has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Affiliate has not been Updated successfully","error");
              }
              }
            });

          }
         } else {
         alert("select algorithm");
             }
              return false;
        }
      </script> 
        <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         if($("input:radio[name='algo']").is(":checked")){
         var hid = $("#id").val();
         var affiliate_id = $("#affiliate_id").val();
         var package_id = $("#package_id").val();
         var dis_per = $("#dis_per").val();
         var min_value = $("#min_value").val();
          $(".affiliate_idErr").html("");
          $(".affiliate_idErr").hide("");
          $(".package_idErr").html("");
          $(".package_idErr").hide("");
          $(".dis_perErr").html("");
          $(".dis_perErr").hide("");
          $(".min_valErr").html("");
          $(".min_valErr").hide("");
		 if(affiliate_id == "0"){
                $(".affiliate_idErr").slideDown('slow');
                $(".affiliate_idErr").html(" Select From list.");
                $("#affiliate_id").focus();
                return false;
          }if(package_id == 0){
              $(".package_idErr").slideDown('slow');
              $(".package_idErr").html(" Select package From list.");
              $("#package_id").focus();
              return false;
        }if(dis_per == 0){
            $(".dis_perErr").slideDown('slow');
            $(".dis_perErr").html("Enter discount percentage.");
            $("#dis_per").focus();
            return false;
      } if(min_value == 0){
          $(".min_valErr").slideDown('slow');
          $(".min_valErr").html("Enter minimum purchage amount.");
          $("#min_value").focus();
          return false;
    }
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEdituserSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/customer'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Affiliate has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","User has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Affiliate has not been Updated successfully","error");
                  
              }
              }
            });

          }
         } else {
          alert("select algorithm");
             }
              return false;
        }
      </script> 
  </body>
</html>