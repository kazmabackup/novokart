
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($detailData)){?>


             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Pincode</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $detailData[0]['pin_code_id'];?>" />
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Pincode</label>
                                <input type="number" id="name" name="name" class="form-control"  placeholder="Pincode" value="<?php echo $detailData[0]['pin_num'];?>"> <span class="nameErr error1" style="color: red;"></span>  </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-4">
                                <label for="feInputState">City</label>
                                <?php if ($menuDropdown) {
                                  if (isset ( $_POST ["city_id"] )) {
                                    $location = $_POST ["city_id"];
                                  } else if($detailData[0]['city_id']){
                                   $location = $detailData[0]['city_id'];
                                   } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="city_id" name="city_id"';
                                  echo form_dropdown ( 'city_id', $menuDropdown, $location, $js );
                                }
                                ?>
                                <span class="city_idErr error1" style="color: red;"></span> 
                              </div>
                              
                            </div>
                           
                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add Pincode</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                          <input type="hidden" id="id" name="id" value=""/>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Pincode</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Pincode" value=""> <span class="nameErr error1" style="color: red;"></span>  </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-4">
                                <label for="feInputState">City</label>
                                <?php if ($menuDropdown) {
                                  if (isset ( $_POST ["city_id"] )) {
                                    $location = $_POST ["city_id"];
                                  } else{
                                    $location = "";
                                  }
                                  $js = 'class="form-control" id="city_id" name="city_id"';
                                  echo form_dropdown ( 'city_id', $menuDropdown, 0, $js );
                                }
                                ?>
                                <span class="city_idErr error1" style="color: red;"></span> 
                              </div>
                              
                            </div>
                           
                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
   
             <?php include('script.php');?>


             <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
                 var title = $("#name").val(); 
                 var city_id = $("#city_id").val();  
                 $(".nameErr").html("");
                 $(".nameErr").hide("");
                  $(".city_idErr").html("");
                 $(".city_idErr").hide("");
              if(title==""){
                $(".nameErr").slideDown('slow');
                $(".nameErr").html("Name is required.");
                $("#name").focus();
                return false;
              }
              if(city_id == 0){
                $(".city_idErr").slideDown('slow');
                $(".city_idErr").html(" Select From list.");
                $("#city_id").focus();
                return false;
              
              }
              
              else{
                var ctrlUrl = "<?php echo base_url().'Admin/addEditPinSubmit' ;?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/pincode'?>"; 
                $("#loadDiv").show(); 
                $.ajax({
                  type: "POST",
                  url: ctrlUrl,
                  data:data,
                  mimeType: "multipart/form-data",
                  contentType: false,
                  cache: false,
                  processData: false,
                  success: function(data)
                  {// alert(data);
                    $("#loadDiv").hide(); 
                  if(data==1){
                    swal("Success!","Pincode has been Added successfully","success");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }else if(data== 2){
                    swal("Success!","Pin has been Updated successfully","success");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }else if(data== 3){
                     swal("Duplicate name.");

                  }
                  else {
                    swal("Error!","Pin has not been Updated successfully","error");
                     
                  } 
                }
                });

              }
              return false;
        }
      </script> 
<script type="text/javascript">
$('#name').keypress(function(event){
    console.log(event.which);
    var value =  $(this).val();
   // alert(value.length);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))  ){
	  $(".nameErr").slideDown('slow');
	    $(".nameErr").html("Enter numeric value only.");
	    event.preventDefault();
	} else if(value.length>7){
		 event.preventDefault();
		}else{
		$(".nameErr").html("");	
	}});
	</script>
  </body>
</html>