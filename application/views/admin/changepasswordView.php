<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->
             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Change password</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $userdata[0]['user_id'];?>" />
                            <div class="form-row">
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Password</label>
                                <input type="text" id="password" name="password" class="form-control"  placeholder="Enter new password" value="<?php echo $userdata[0]['password'];?>"> <span class="passErr error" style="color: red;"></span>  </div>
                            </div>
                            <button type="submit" onclick="return formValidation();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
    <?php include('script.php');?>
      <script>
        function formValidation(){
         var data = new FormData($('#formData')[0]);
         var hid = $("#id").val();
		 var pass = $("#password").val();
          $(".passErr").html("");
          $(".passErr").hide("");
          if(pass==""){
            $(".passErr").slideDown('slow');
            $(".passErr").html(" Password is required.");
            $("#password").focus();
            return false;
          }
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/changepassSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/dashboard'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Password has been changed successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Password has not been Changed","error");
              }
              }
            });
          }
              return false;
        }
      </script> 
  </body>
</html>