
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3 text-center">
                <?php if($responce = $this->session->flashdata('Successfully')): ?>
      <div class="box-header">
        <div class="col-lg-6">
           <div class="alert alert-success"><?php echo $responce;?></div>
        </div>
      </div>
       <?php endif;?>
                <form action="<?php echo base_url()?>Admin/addtocart/<?php echo $userid;?>" method = "post">
                 <table id="example1" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>Category</th>
				<th>Sub category</th>
                <th>Product</th>
                <th>Quantity</th>
                 <th>price</th>
                <th>Cost price</th>
                <th>Product wise margin(%)</th>
                 <th>Total Amount</th>
                <th>Action</th>
            </tr>
               </thead>
              
               <tbody class="input_fields_container">
               
                <tr>
                <td><select class="from-control" name="cat[]" id="cat" onchange="getsubcat(this.value,0)" required>
                <option value="0">select category</option>
                <?php if ($categoryDropdown){
                foreach ($categoryDropdown as $cat){
                	$catid = $cat['category_id'];
                	$catname = $cat['category_name'];
               ?>
                <option value="<?php echo $catid;?>"><?php echo $catname;?></option>
                <?php  }
                }?>
                 </select>
                  <span class="catErr error" style="color: red;"></span> 
                 </td>
                 <td><select class="from-control" name="subcat[]" id="subcat0" onchange="getproduct(this.value,0)" required>
                 <option value="0">select sub category</option>
                 </select>
                  <span class="subcatErr error" style="color: red;"></span> 
                 </td>
                 <td><select class="from-control" name ="product[]" id="product0" required onchange="getprice(this.value,0)">
                 <option value="0">select product</option>
                 </select>
                  <span class="productErr error" style="color: red;"></span>
                 </td>
                 <td><input class="form-control" id="quantity0" onchange="getTotal(this.value,0)" name="quantity[]" value="" required>
                  <span class="quantityErr error" style="color: red;"></span>
                 </td>
                  <td><input class="form-control" id="price0" name="price" value="" readonly></td>
                 
                 <td><input class="form-control" id="costprice0" name="costprice" value="" readonly></td>
                 <td><input class="form-control" id="margin0" name="margin" value="" readonly></td>
                  <td><input class="form-control" id="total0" name="total" value="" readonly></td>
                 <td><button class="btn btn-sm btn-primary add_more_button">Add More</button>
                 </td>
                 </tr>
             </tbody>
             </table>
              <input type="submit" class="btn btn-sm btn-primary" value="submit" onclick="return formvalidation();"/> 
              </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>
        <?php include('footer.php');?>
         <?php include('script.php');?>
  </body>
</html>
<script>
    $(document).ready(function() {
    var max_fields_limit = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
             //counter increment
            $('.input_fields_container').append('<tr><td><select class="from-control" name="cat[]" onchange="getsubcat(this.value,'+x+')" required><option value="0">select category</option> <?php if ($categoryDropdown){foreach ($categoryDropdown as $cat){$catid = $cat['category_id'];$catname = $cat['category_name']; ?><option value="<?php echo $catid;?>"><?php echo $catname;?></option><?php  }}?></select></td><td><select class="from-control" name="subcat[]" id="subcat'+x+'" onchange="getproduct(this.value,'+x+')" required><option value="0">select sub category</option></select> </td> <td><select class="from-control" name="product[]" id="product'+x+'" required onchange="getprice(this.value,'+x+')"><option value="0">select product</option></select></td><td><input class="form-control" id="quantity" onchange="getTotal(this.value,'+x+')" name="quantity[]" value="" required></td><td><input class="form-control" id="price'+x+'" name="price" value="" readonly></td><td><input class="form-control" id="costprice'+x+'" name="costprice" value="" readonly></td><td><input class="form-control" id="margin'+x+'" name="margin" value="" readonly></td><td><input class="form-control" id="total'+x+'" name="total" value="" readonly></td><td><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></td></tr>'); //add input field
            x++;
            }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){//alert("ok"); //user click on remove text links
        e.preventDefault(); $(this).closest('tr').remove(); x--;
    })
});
</script>
<script>
function getsubcat(ses,x){  
	//alert(ses);
	//alert(x);
	if(ses) { 
		var catid = ses;
		var baseUrl 	= "<?php echo base_url();?>";
		// url to get the search data
		var subCatUrl 	= baseUrl+"Admin/getsubcatbycatid";
		 //$("#loadDiv").show();
		$.ajax({
			type: "POST",
			url: subCatUrl,
			data:({
				catid : catid
				 }),
			cache: false,
			success: function(data)
			{  //alert(data);
				 //$("#loadDiv").hide();
				 $("#subcat"+x).html(data);
			}
		});
	}
}
function getproduct(ses,x){  
	//alert(ses);
	//alert(x);
	if(ses) { 
		var subcatid = ses;
		var baseUrl 	= "<?php echo base_url();?>";
		// url to get the search data
		var subCatUrl 	= baseUrl+"Admin/getproductbysubcatid";
		 //$("#loadDiv").show();
		$.ajax({
			type: "POST",
			url: subCatUrl,
			data:({
				subcatid : subcatid
				 }),
			cache: false,
			success: function(data)
			{  //alert(data);
				 //$("#loadDiv").hide();
				 $("#product"+x).html(data);
			}
		});
	}
}
function getprice(ses,x){  
	//alert(ses);
	//alert(x);
	if(ses) { 
		var proid = ses;
		var baseUrl 	= "<?php echo base_url();?>";
		// url to get the search data
		var subCatUrl 	= baseUrl+"Admin/getproductprice";
		 //$("#loadDiv").show();
		$.ajax({
			type: "POST",
			url: subCatUrl,
			data:({
				proid : proid
				 }),
			cache: false,
			success: function(data)
			{  //alert(data);
			var result = data.split(',');
			var price = result[0];
			var cost_price = result[1];
			var margin = result[2];
				//$('#pro').prop('disabled', 'true');
			 $("#price"+x).val(price);
			 $("#costprice"+x).val(cost_price);
			 $("#margin"+x).val(margin);
			}
		});
	}
}
function getTotal(ses,x){
	var quantity = ses;
	var price = $("#price"+x).val();
	var total = price*quantity;
	$("#total"+x).val(total);
	//alert(total);
}
</script>
<!-- <script>
 function formvalidation(){
	 var category_id = $("#cat").val(); 
	 var sub_category_id = $("#subcat0").val(); 
	 var product_id = $("#product0").val();
	 var quantity = $("#quantity0").val(); 
	 $(".catErr").html("");
     $(".catErr").hide("");
     $(".subcatErr").html("");
     $(".subcatErr").hide("");
     $(".productErr").html("");
     $(".productErr").hide("");
     $(".quantityErr").html("");
     $(".quantityErr").hide("");
     if(category_id == 0){
         $(".catErr").slideDown('slow');
         $(".catErr").html(" Select category.");
         $("#cat").focus();
         return false;
   }
     if(sub_category_id == 0){
         $(".subcatErr").slideDown('slow');
         $(".subcatErr").html(" Select sub category.");
         return false;
   }
     if(product_id == 0){
         $(".productErr").slideDown('slow');
         $(".productErr").html(" Select product.");
         return false;
   }
     if(quantity == ""){
         $(".quantityErr").slideDown('slow');
         $(".quantityErr").html("Enter quantity.");
         return false;
   }
	 return true;
 }
</script> -->