<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($detailData)){?>

             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Feature Collection</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $detailData[0]['feature_collection_id'];?>" />
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Feature Collection Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Feature Collection Name" value="<?php echo $detailData[0]['feature_collection_name'];?>"> <span class="nameErr error1" style="color: red;"></span>  </div>
                            </div>
                             <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Image</label>
                               <input class="form-control" type="file" id="img" name="img">
                    <img src="<?php echo base_url();?>uploads/category/<?php echo $detailData[0]['image'];?>" width="60" height="40" />
                                <span class="imgErr error1" style="color: red;"></span>  </div>
                            </div>
                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add Feature Collection</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                        <form class="form-horizontal form-material" method="post" action="" id="formData0" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value=""/>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Category Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Category Name" value=""> <span class="nameErr error1" style="color: red;"></span>  </div>
                            </div>
                             <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Image</label>
                               <input class="form-control" type="file" id="img" name="img"> <span class="imgErr error1" style="color: red;"></span>  </div>
                            </div>

                           
                            <button type="submit" onclick="return formValidationAdd();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
   
             <?php include('script.php');?>


              <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
             var title = $("#name").val(); 
             $(".nameErr").html("");
             $(".nameErr").hide("");
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html("Feature collection Name is required.");
            $("#name").focus();
            return false;
          }
         
              else{
                var ctrlUrl = "<?php echo base_url().'Admin/addEditFeaturecollectionSubmit' ;?>";
                var adminRedirectUrl="<?php echo base_url().'Admin/featureCollection'?>"; 
                $("#loadDiv").show(); 
                $.ajax({
                  type: "POST",
                  url: ctrlUrl,
                  data:data,
                  mimeType: "multipart/form-data",
                  contentType: false,
                  cache: false,
                  processData: false,
                  success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide(); 
                  if(data==1){
                    swal("Success!","Feature collection has been Added successfully","success");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }else if(data == 2){
                    swal("Success!","Feature collection has been Updated successfully","success");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }else if(data == 3){
                    swal("Error!","Feature collection name alreay exists","error");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }
                  else {
                    swal("Error!","Feature collection has not been Updated successfully","error");
                      setTimeout(function () 
                      {
                     window.location.href=adminRedirectUrl },4000);
                  }
                  }
                });

              }
              return false;
        }
      </script> 


      <script>
        function formValidationAdd(){
         var data = new FormData($('#formData0')[0]);
         var hid = $("#id").val();
             var title = $("#name").val(); 
             var img = $("#img").val(); 
            
             $(".nameErr").html("");
             $(".nameErr").hide("");
             
             $(".imgErr").html("");
             $(".imgErr").hide("");
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html("Feature collection Name is required.");
            $("#name").focus();
            return false;
          }
           if(img==""){
              $(".imgErr").slideDown('slow');
              $(".imgErr").html("Image is required.");
              $("#img").focus();
              return false;
            }
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditFeaturecollectionSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/featureCollection'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Feature collection has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data == 2){
                swal("Success!","Feature collection has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data == 3){
                swal("Error!","Feature collection name already exists","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Feature collection has not been Updated successfully","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              }
            });

          }
              return false;
        }
      </script> 

     

  </body>
</html>