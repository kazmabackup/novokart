<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($detailData)){?>


             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Profile</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $detailData[0]['user_id'];?>" />
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">First Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="First Name" value="<?php echo $detailData[0]['firstname'];?>"> <span class="nameErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Last Name</label>
                                <input type="text" id="lname" name="lname" class="form-control"  placeholder="Last Name" value="<?php echo $detailData[0]['lastname'];?>"> <span class="lnameErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Email</label>
                                <input type="text" id="email" name="email" class="form-control"  placeholder="Email" value="<?php echo $detailData[0]['email'];?>"> <span class="emailErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Email</label>
                                <input type="number" id="contact" name="contact" class="form-control"  placeholder="Contact" value="<?php echo $detailData[0]['contact'];?>"> <span class="contactErr error1" style="color: red;"></span>  </div>
                            </div>
                           <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Address</label>
                                <textarea class="form-control" name="content" id="content" rows="5"><?php echo $detailData[0]['address'];?></textarea>
                                <span class="contentErr error1" style="color: red;"></span>
                              </div>
                            </div>

                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }  ?>
                   </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
   
             <?php include('script.php');?>



			<script>
				function formValidationEdit(){
				 var data = new FormData($('#formData1')[0]);
				 var hid = $("#id").val();
				 var name = $("#name").val();
				 var lname = $("#lname").val();
				 var contact = $("#contact").val();
                 var email = $("#email").val(); 
                 var content = CKEDITOR.instances['content'].getData();
                 var emailRegexStr = /^[a-zA-Z0-9/+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
   				 var isvalid = emailRegexStr.test(email); 
                 $(".lnameErr").html("");
              	 $(".lnameErr").hide("");
              	 $(".contactErr").html("");
              	 $(".contactErr").hide("");
              	 $(".nameErr").html("");
              	 $(".nameErr").hide("");
                 $(".contentErr").html("");
              	 $(".contentErr").hide("");
              	 $(".emailErr").html("");
              	 $(".emailErr").hide("");

              	 if(name==""){
             		$(".nameErr").slideDown('slow');
             		$(".nameErr").html("First Name is required.");
             		$("#name").focus();
             		return false;
             	}
             	if(lname==""){
             		$(".lnameErr").slideDown('slow');
             		$(".lnameErr").html("Last Name is required.");
             		$("#lname").focus();
             		return false;
             	}if(email==""){
             		$(".emailErr").slideDown('slow');
             		$(".emailErr").html("Email is required.");
             		$("#email").focus();
             		return false;
             	}if(!isvalid){
			        $(".emailErr").slideDown('slow');
			        $(".emailErr").html("Please enter valid Email address.");
			        $("#email").focus();
			          return false;
			    }if(contact==""){
             		$(".contactErr").slideDown('slow');
             		$(".contactErr").html("Contact is required.");
             		$("#contact").focus();
             		return false;
             	}if(contact.length < 10){
          			$(".contactErr").slideDown('slow');
          			$(".contactErr").html("Mobile no must be 10 digits");
         			 $("#num").focus();
          			return false;
			      }if(contact.length > 10){
			          $(".contactErr").slideDown('slow');
			          $(".contactErr").html("Mobile no must be 10 digits");
			          $("#contact").focus();
			          return false;
			       }
             	 if(content==""){
              		$(".contentErr").slideDown('slow');
              		$(".contentErr").html("Address is required.");
              		$("#content").focus();
              		return false;
              	}
             	else{
             		var ctrlUrl = "<?php echo base_url().'Admin/editProfileSubmit' ;?>";
             		var adminRedirectUrl="<?php echo base_url().'Admin/editProfile'?>"; 
             		data.append('content', content);
             		$("#loadDiv").show(); 
             		$.ajax({
             			type: "POST",
             			url: ctrlUrl,
             			data:data,
			            mimeType: "multipart/form-data",
			            contentType: false,
			            cache: false,
			            processData: false,
             			success: function(data)
             			{// alert(data);
             				$("#loadDiv").hide(); 
             			if(data==1){
             				swal("Success!","Data has been Added successfully","success");
             			    setTimeout(function () 
             			    {
             			   window.location.href=adminRedirectUrl },4000);
             			}else if(data== 2){
             				swal("Success!","Data has been Updated successfully","success");
             			    setTimeout(function () 
             			    {
             			   window.location.href=adminRedirectUrl },4000);
             			}
             			else {
             				swal("Error!","Data has not been Updated successfully","error");
             			    setTimeout(function () 
             			    {
             			   window.location.href=adminRedirectUrl },4000);
             			}
             			}
             		});

             	}
             	return false;
				}
			</script>	

			 <script>
	            CKEDITOR.replace( 'content', {
	            });
			</script>

	
</body>
</html>
