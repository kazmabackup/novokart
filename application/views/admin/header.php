<nav class="main-menu">
		<ul>
			<?php 
             $home="";$about="";$awards="";$faqs="";$c_profile="";$disclaimer="";$contact="";$sub_service_menu="";$service_menu="";$customer="";$feedback="";$service_request="";$appointment="";$location="";$out_of_station_request="";$webiner_request="";$webiner="";$sub_request_menu="";$request_menu="";$request_appointment="";
            if($activeMenu == 1){
                $home='class="active"';
            }elseif($activeMenu == 2){
                $about='class="active"';
            }elseif($activeMenu == 3){
                $awards='class="active"';
            }elseif($activeMenu == 4){
                $faqs='class="active"';
            }elseif($activeMenu == 5){
                $c_profile='class="active"';
            }elseif($activeMenu == 6){
                $disclaimer='class="active"';
            }elseif($activeMenu == 7){
                $contact='class="active"';
            }elseif($activeMenu == 8){
                $service_menu='class="active"';
            }elseif($activeMenu == 9){
                $sub_service_menu='class="active"';
            }elseif($activeMenu == 10){
                $customer='class="active"';
            }elseif($activeMenu == 11){
                $feedback='class="active"';
            }elseif($activeMenu == 12){
                $service_request='class="active"';
            }elseif($activeMenu == 13){
                $appointment='class="active"';
            }elseif($activeMenu == 14){
                $location='class="active"';
            }elseif($activeMenu == 15){
                $out_of_station_request='class="active"';
            }elseif($activeMenu == 16){
                $webiner_request='class="active"';
            }elseif($activeMenu == 17){
                $webiner='class="active"';
            }elseif($activeMenu == 18){
                $request_menu='class="active"';
            }elseif($activeMenu == 19){
                $sub_request_menu='class="active"';
            }elseif($activeMenu == 20){
                $request_appointment='class="active"';
            }
                              
            ?>
			<li <?php echo $home;?>>
				<a href="<?php echo base_url('Admin/dashboard');?>">
					<i class="fa fa-home nav_icon"></i>
					<span class="nav-text">Manage Dashboard </span>
				</a>
			</li>
			<li <?php echo $location;?>>
				<a href="<?php echo base_url('Admin/location');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Location</span>
				</a>
			</li>

			<li <?php echo $webiner;?>>
				<a href="<?php echo base_url('Admin/webinar');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Webiner</span>
				</a>
			</li>
			<li <?php echo $service_menu;?>>
				<a href="<?php echo base_url('Admin/service_menu');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Service Menu</span>
				</a>
			</li>
			<li <?php echo $sub_service_menu;?>>
				<a href="<?php echo base_url('Admin/sub_service_menu');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Sub Service Menu</span>
				</a>
			</li>

			<li <?php echo $request_menu;?>>
				<a href="<?php echo base_url('Admin/request_menu');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Request Menu</span>
				</a>
			</li>
			<li <?php echo $sub_request_menu;?>>
				<a href="<?php echo base_url('Admin/sub_request_menu');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Sub Request Menu</span>
				</a>
			</li>
			<li <?php echo $appointment;?>>
				<a href="<?php echo base_url('Admin/appointment');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Appointment</span>
				</a>
			</li>
			<li <?php echo $request_appointment;?>>
				<a href="<?php echo base_url('Admin/request_appointment');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Request Appointment</span>
				</a>
			</li>
			<li <?php echo $customer;?>>
				<a href="<?php echo base_url('Admin/customer');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Customer</span>
				</a>
			</li>
			<li <?php echo $feedback;?>>
				<a href="<?php echo base_url('Admin/feedback');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage FeedBack</span>
				</a>
			</li>
			<li <?php echo $service_request;?>>
				<a href="<?php echo base_url('Admin/video_Conferense_request');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Video Conferense Request</span>
				</a>
			</li>
			<li <?php echo $out_of_station_request;?>>
				<a href="<?php echo base_url('Admin/out_of_station_request');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Out Of Station Request</span>
				</a>
			</li>
			<li <?php echo $webiner_request;?>>
				<a href="<?php echo base_url('Admin/webiner_request');?>">
					<i class="icon-font nav-icon"></i>
					<span class="nav-text">Manage Webinar Request</span>
				</a>
			</li>
			<div class="collapse-chevron">
		     
               <li data-toggle="collapse" href="#collapse1" class="has-subnav">
					<a href="javascript:;">
					<i class="fa fa-check-square-o nav_icon"></i>
					<span class="nav-text">Manage CMS </span>
					<i class="icon-angle-right"></i><i class="icon-angle-down"></i>
					</a>
				</li>
		      <div id="collapse1" class="panel-collapse collapse">
		          <ul class="list-group">
		            <li <?php echo $about;?>><a href="<?php echo base_url('Admin/about');?>"><i class="icon-font nav-icon"></i><span class="nav-text"> Manage About Us</span></a></li>
		            <li <?php echo $awards;?>><a href="#" onclick="appointment()"><i class="icon-font nav-icon"></i><span class="nav-text"> Manage Awards</a></li>
		            <li <?php echo $faqs;?>><a href="<?php echo base_url('Admin/faqs');?>"><i class="icon-font nav-icon"></i><span class="nav-text">Manage Faqs</a></li>
		            <li <?php echo $c_profile;?>><a href="<?php echo base_url('Admin/consultant_profile');?>"><i class="icon-font nav-icon"></i><span class="nav-text"> Manage Consultant Profile</a></li>
		            <li <?php echo $disclaimer;?>><a href="<?php echo base_url('Admin/disclaimer');?>"><i class="fa fa-bar-chart nav_icon"></i><span class="nav-text">Manage Disclaimer</a></li>
		            <li <?php echo $contact;?>><a href="<?php echo base_url('Admin/contact');?>"><i class="fa fa-map-marker" aria-hidden="true"></i><span class="nav-text">Manage Contact Us</a></li>		              		            
		      	  </ul>
		      </div>
		
		</ul>
		<!-- <ul class="logout">
			<li>
			<a href="login.html">
			<i class="icon-off nav-icon"></i>
			<span class="nav-text">
			Logout
			</span>
			</a>
			</li>
		</ul> -->
	</nav>

	<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
			<i class="icon-proton-logo"></i>
			<i class="icon-reorder"></i>
			</a>
	</nav>

	<script type="text/javascript">
		function appointment() {
			swal("Comming Soon");
		}
	</script>