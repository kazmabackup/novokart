
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
    <style type="text/css">
    .even td a .fa-check{
    color: #008400;
    margin-right: 5px;
    }
     .odd td a .fa-check{
    color: #008400;
    margin-right: 5px;
    }
    .even td a .fa-times{
    color: #E8101D;
    margin-right: 5px;
    }
    .odd td a .fa-times{
    color: #E8101D;
    margin-right: 5px;
    }
    .odd td a i{
    font-size: 11px;
    }
    .even td a i{
    font-size: 12px;
    }
    </style>
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
           
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          
             
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">

                <div class="card-header border-bottom">
                   <!-- <a href="<?php echo base_url('Admin/addEditVendor/');?>" class="btn btn-mini btn-success" > Add</a> -->
<!--                     <h6 class="m-0">Active Users</h6>
 -->                </div>
                <div class="card-body p-0 pb-3 text-center">
                  <table id="example" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                 <th>S. No.</th>
                <th>Order Id</th>
                <th>Order Date</th>
                <th>Customer Name</th>
                <th>Quantity</th>
                <th>Address</th>
                <th>Affiliate Id</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
                
            </tr>
               </thead>
 
               <tfoot>
            <tr>
                <th>S. No.</th>
                 <th>Order Id</th>
                <th>Order Date</th>
                <th>Customer Name</th>
                <th>Quantity</th>
                <th>Address</th>
                <th>Affiliate id</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
              </tfoot>
 
               <tbody>

                <?php
                if(!empty($detailData)){
                    $i = 1;
                    foreach ( $detailData as $detail ) {
                      $id = $detail ['manage_order_id'];
                      $orderid = $detail ['unique_order_id'];
                      $agent_id = $detail ['agent_id'];
					  $where3 = "affiliate_id= $agent_id";
				     $affidata = $this->Adminmodel->getDataById ( 'affiliate', $where3 );
				    $affiname=$affidata[0]['unique_id'];
                    $user_id = $detail ['user_id'];
					$where = "user_id= $user_id";
				   $usedata = $this->Adminmodel->getDataById ( 'user', $where );
				   $username=$usedata[0]['user_name'];
                   $product_id = $detail ['pro_id'];
				   $where1 = "product_id= $product_id";
				  $prodata = $this->Adminmodel->getDataById ( 'product', $where1 );
				  $proname=$prodata[0]['product_name'];
                      //$product_name = $detail ['product_name'];
                      $pro_price = $detail ['pro_price'];
                      $address = $detail ['address_id'];
                      $whereaddress = "delivery_address_id= $address";
                      $addressdata = $this->Adminmodel->getDataById ( 'delivery_address', $whereaddress );
                      $addressname=$addressdata[0]['address'];
                      $order_date = $detail ['order_date'];
                      $order_quantity = $detail ['order_quantity'];
            
                      $status = $detail['order_status'];
                      ?>
           
            <tr>
               <td><?php echo $i;?></td>
               <td><?php echo $orderid;?></td>
               <td><?php echo $order_date;?></td>
                <td><?php echo $username;?></td>
                <td><?php echo $order_quantity;?></td>
                <td><?php echo $addressname;?></td>
				<td><?php echo $affiname;?></td>
				<td><?php echo $status;?></td>
                <td>
                  <?php if ($status=='Pending'){ ?>
                   <a href=""  onclick="changeCountryStatus(<?php echo $id;?>,'Approved')" ><i class="fas fa-check"></i></a>
                   <a href="" onclick="changeCountryStatus(<?php echo $id;?>, 'Cancel')" ><i class="fas fa-times"></i></a>

                 <?php } else {?>
                 <a onclick="return order()" class="btn btn-mini btn-success"><?php echo $status;?></a>
                 <?php } ?>
                  
                  <!-- <?php if($status == 'Active'){?>
                  <a href="" class="btn btn-mini btn-success" onclick="changeCountryStatus(<?php echo $id;?>,'In-Active')" ><?php echo $status;?></a>
                  <?php }else{?>
                  <a href="" class="btn btn-mini btn-danger" onclick="changeCountryStatus(<?php echo $id;?>, 'Active')" ><?php echo $status;?></a>
                  <?php }?> -->

                   <a href=""  onclick = " return order_details('<?php echo $orderid;?>');" data-toggle="modal" ><i class="far fa-eye"></i></a>

                </td>
                
            </tr>

            <?php $i++;} }
              ?>
          
           
             </tbody>
          </table>
                  </div>
                </div>
              </div>
            </div>
              
            <!-- End Default Light Table -->
           
          </div>
          <div id="orderdetails" class="modal"  data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                              
                           </div>
        <?php include('footer.php');?>

         <?php include('script.php');?>

             <div id="myModal" class="modal fade" role="dialog">
              <!-- <span id="ddetail"></span> -->
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <span id="ddetail"></span>
                
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
  </div>

  <script type="text/javascript">
    function order() {
      swal("Order Status has been already Changed.");
      // body...
    }
  </script>


  <script type="text/javascript">
  function vendorDetail(val,type){
       var subCatUrl  ="<?php echo base_url();?>Admin/vendorDetail";
        $.ajax({
          type: "POST",
          url: subCatUrl,
          data:({
            val : val,
            type : type
             }),
          cache: false,
          success: function(data)
          {//alert(data);
            $("#ddetail").html(data);
          }
        });
      return True;      
  }
</script>


<script>
      function changeCountryStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this order?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeOrderStatus/"?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/order'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Order status has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Order status has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>
  <script>
function order_details(orderid){
	var ctrlUrl = "<?php echo base_url().'Admin/order_details';?>";
	$.ajax({
		type: "POST",
		url: ctrlUrl,
		data:({
			orderid : orderid
		}),
		cache: false,
		success: function(data)
		{ //alert(data);
	      $("#orderdetails").html(data);
	      $('#orderdetails').modal('show');
		}
	});
}
                    </script>
  </body>
</html>