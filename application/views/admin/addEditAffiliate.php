
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($agentdata)){?>


             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Affiliate</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $agentdata[0]['affiliate_id'];?>" />

                             <div class="form-row">
                              
                              
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-4">
                                <label for="feFirstName">Affiliate Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder=" Name" value="<?php echo $agentdata[0]['affiliate_name'];?>"> <span class="nameErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-4">
                                <label for="feFirstName">Email</label>
                                <input type="text" id="email" name="email" class="form-control"  placeholder="percenatage" value="<?php echo $agentdata[0]['email'];?>"> <span class="quantityErr error1" style="color: red;"></span>  </div>
                                 <div class="form-group col-md-4">
                                <label for="feFirstName">Password</label>
                                <input type="text" id="password" name="password" class="form-control"  placeholder="password" value="<?php echo $agentdata[0]['password'];?>"> <span class="passwordErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              

                                <div class="form-group col-md-6">
                                <label for="feFirstName">Phone No.</label>
                                <input type="text" id="phone" name="phone" class="form-control"  placeholder="phone no" value="<?php echo $agentdata[0]['phone'];?>"> <span class="phoneErr error1" style="color: red;"></span>  </div>
								<!-- <div class="form-group col-md-6">
                                <label for="feFirstName">Discount percentage</label>
                                <input type="text" id="discount" name="discount" class="form-control"  placeholder="discount percentage" value="<?php echo $agentdata[0]['discount'];?>"> <span class="dis_priceErr error1" style="color: red;"></span> 
                                 </div> -->
                            </div>

                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add Affiliate</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         <form class="form-horizontal form-material" method="post" action="" id="formData0" enctype="multipart/form-data">
                         <input type="hidden" id="id" name="id" value=""/>
                           
                            <div class="form-row">
                              <div class="form-group col-md-4">
                                <label for="feFirstName">Affiliate Name</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder=" Name" value=""> <span class="nameErr error1" style="color: red;"></span>  </div>

                                <div class="form-group col-md-4">
                                <label for="feFirstName">Email</label>
                                <input type="text" id="email" name="email" class="form-control"  placeholder="Email" value=""> <span class="quantityErr error1" style="color: red;"></span>  </div>
                            <div class="form-group col-md-4">
                                <label for="feFirstName">Password</label>
                                <input type="text" id="password" name="password" class="form-control"  placeholder="password" value=""> <span class="passwordErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="feFirstName">Phone no</label>
                                <input type="text" id="phone" name="phone" class="form-control"  placeholder="contact no" value="">
                                 <span class="phoneErr error" style="color: red;"></span>  </div>
								  <!-- <div class="form-group col-md-6">
                                <label for="feFirstName">Discount Percentage</label>
                                <input type="text" id="discount" name="discount" class="form-control"  placeholder="Discount" value=""> 
                                <span class="dis_priceErr error" style="color: red;"></span>  </div> -->
                            </div>
                            <button type="submit" onclick="return formValidationAdd();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
   
             <?php include('script.php');?>

<!--  <script type="text/javascript">
$('#discount').keypress(function(event){
    console.log(event.which);
    var value =  $(this).val();
   // alert(value.length);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))  ){
	  $(".dis_priceErr").slideDown('slow');
	    $(".dis_priceErr").html("Enter numeric value only.");
	    event.preventDefault();
	} else{
		$(".dis_priceErr").html("");	
	}});
	</script>-->
<script type="text/javascript">
$('#phone').keypress(function(event){
    console.log(event.which);
    var value =  $(this).val();
   // alert(value.length);
if(event.which != 8 && isNaN(String.fromCharCode(event.which))  ){
	  $(".phoneErr").slideDown('slow');
	    $(".phoneErr").html("Enter digits only.");
	    event.preventDefault();
	} else if(value.length>9){
		 event.preventDefault();
		}else{
		$(".phoneErr").html("");	
	}});
	</script>


      <script>
        function formValidationAdd(){
         var data = new FormData($('#formData0')[0]);
         var hid = $("#id").val();
         var img = $("#img").val(); 
         var title = $("#name").val(); 
         var email = $("#email").val(); 
         var password = $("#password").val(); 
		 var phone = $("#phone").val(); 
		// var dis_price = $("#discount").val();
		 var emailRegexStr = /^[a-zA-Z0-9/+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		 var isvalid = emailRegexStr.test(email); 
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".quantityErr").html("");
          $(".quantityErr").hide("");
          $(".phoneErr").html("");
          $(".phoneErr").hide("");
         // $(".dis_priceErr").html("");
         // $(".dis_priceErr").hide("");
          $(".passwordErr").html("");
          $(".passwordErr").hide("");
         
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Affiliate Name is required.");
            $("#name").focus();
            return false;
          }if(email==""){
            $(".quantityErr").slideDown('slow');
            $(".quantityErr").html("email is required.");
            $("#percentage").focus();
            return false;
          }if(!isvalid){
			$(".quantityErr").slideDown('slow');
			$(".quantityErr").html("Please enter valid Email address.");
			return false;
		}if(password==""){
            $(".passwordErr").slideDown('slow');
            $(".passwordErr").html("password is required.");
            $("#password").focus();
            return false;
          }if(phone==""){
            $(".phoneErr").slideDown('slow');
            $(".phoneErr").html("Phone number is required.");
            $("#phone").focus();
            return false;
          }if(phone.length < 10 ){
			$(".phoneErr").slideDown('slow');
			$(".phoneErr").html("enter 10 digit mobile number.");
			return false;
		}
		 if(phone.length > 10 ){
			$(".phoneErr").slideDown('slow');
			$(".phoneErr").html("enter 10 digit mobile number.");
			return false;
		}
          /* if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("Discount Percentage is required.");
            $("#discount").focus();
            return false;
          } */
        
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditAgentSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/affiliate'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Affiliate has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","Affiliate has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 'dupemail'){
            	  $(".quantityErr").slideDown('slow');
                  $(".quantityErr").html("This email id already exists.");
                  $("#percentage").focus();
                  return false;
              }else if(data== 'dupmob'){
            	  $(".phoneErr").slideDown('slow');
                  $(".phoneErr").html("This mobile number already exists.");
                  $("#phone").focus();
                  return false;
              }
              else {
                swal("Error!","Affiliate has not been Updated successfully","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 



        <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
       //  var img = $("#img").val();  
         var title = $("#name").val(); 
         var email = $("#email").val(); 
		  var phone = $("#phone").val(); 
        // var dis_price = $("#min_value").val(); 
		 var emailRegexStr = /^[a-zA-Z0-9/+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		   var isvalid = emailRegexStr.test(email);
      
          
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".quantityErr").html("");
          $(".quantityErr").hide("");
		   $(".phoneErr").html("");
          $(".phoneErr").hide("");

         // $(".dis_priceErr").html("");
         // $(".dis_priceErr").hide("");
         
         
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html(" Affiliate Name is required.");
            $("#name").focus();
            return false;
          }if(email==""){
            $(".quantityErr").slideDown('slow');
            $(".quantityErr").html("email is required.");
            $("#email").focus();
            return false;
          }if(!isvalid){
			$(".quantityErr").slideDown('slow');
			$(".quantityErr").html("Please enter valid Email address.");
		}
		if(phone==""){
            $(".phoneErr").slideDown('slow');
            $(".phoneErr").html("Phone number is required.");
            $("#phone").focus();
            return false;
          }if(phone.length < 10 ){
			$(".phoneErr").slideDown('slow');
			$(".phoneErr").html("enter 10 digit mobile number.");
			return false;
		}
		 if(phone.length > 10 ){
			$(".phoneErr").slideDown('slow');
			$(".phoneErr").html("enter 10 digit mobile number.");
			return false;
		}
          /* if(dis_price==""){
            $(".dis_priceErr").slideDown('slow');
            $(".dis_priceErr").html("minmum price Price is required.");
            $("#min_value").focus();
            return false;
          } */
         
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditAgentSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/affiliate'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","Affiliate has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","Affiliate has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","Affiliate has not been Updated successfully","error");
                  
              }
              }
            });

          }
              return false;
        }
      </script> 

    
  </body>
</html>