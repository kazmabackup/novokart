 <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
  <?php  $home="";$country="";
            ?>
          
          <div class="nav-wrapper nav">
            <ul class=" flex-column nav__list" style="padding: 0; float: left; width: 100%;">
              <li class="nav-item">
                <a  <?php if($activeMenu == 1){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/dashboard');?>">
                  <i class="material-icons">home</i>
                  <span>Dashboard</span>
                </a>
              </li>
               <li class="nav-item">
                <a <?php if($activeMenu == 21){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/manageMargin');?>">
                  <!-- <i class="fa fa-users material-icons"></i> -->
                  <i class="material-icons">extension</i>
                  <span>Margin Management</span>
                </a>
              </li>
               <li class="nav-item">
                <a <?php if($activeMenu == 22){?> class="nav-link active" <?php } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/manageCustomertype');?>">
                  <i class="material-icons">perm_identity</i>
                  <span>Customer Type Management</span>
                </a>
              </li>
              <li class="nav-item">
                  <a href="#"  <?php if($activeMenu == 2){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?>><i class="material-icons">room</i>
                        <span>Location Management <em class="fa fa-angle-right"></em></span> </a>
                  <ul class="group-list">
                      <li><a href="<?php echo base_url('Admin/country');?>">Country</a></li>
                      <li><a href="<?php echo base_url('Admin/city');?>">City</a></li>
                      <li><a href="<?php echo base_url('Admin/pincode');?>">Pin Code</a></li>
                  </ul>
       		 </li>

       		 <li class="nav-item">
               <a href="<?php echo base_url('Admin/banner');?>"  <?php if($activeMenu == 3){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?>>
                  <i class="material-icons">mms</i>
                  <span>Promotional Banner</span>
                </a>
              </li>

              <li class="nav-item">
                  <a href="#"  <?php if($activeMenu == 4){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?>><i class="material-icons">view_comfy </i>
                        <span>Categories <em class="fa fa-angle-right"></em></span> </a>
                  <ul class="group-list">
                      <li><a href="<?php echo base_url('Admin/category');?>" >Category</a></li>
                      <li><a href="<?php echo base_url('Admin/subcategory');?>" >Sub-Category</a></li>
                  </ul>
       		 </li>
       		 <li class="nav-item">
                <a <?php if($activeMenu == 13){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/manageProduct');?>" >
                  <i class="material-icons">work</i>
                  <span>Product Management</span>
                </a>
              </li>
              <li class="nav-item">
                <a <?php if($activeMenu == 12){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/package');?>">
                  <i class="material-icons">assignment_returned</i>
                  <span>package  Management</span>
                </a>
              </li>
			   <li class="nav-item">
                <a <?php if($activeMenu == 11){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/affiliate');?>">
                  <i class="material-icons">bookmarks</i>
                  <span>Affiliate Management</span>
                </a>
              </li>
              <li class="nav-item">
                <a <?php if($activeMenu == 7){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/manageSlot');?>">
                  <i class="material-icons">explore</i>
                  <span>Slot Management</span>
                </a>
              </li>
			   <li class="nav-item">
                <a <?php if($activeMenu == 10){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/customer');?>">
                  <i class="material-icons">group</i>
                  <span>Customer Management</span>
                </a>
              </li>
            
              <li class="nav-item">
                <a <?php if($activeMenu == 8){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="<?php echo base_url('Admin/order');?>">
                  <i class="material-icons">local_grocery_store</i>
                  <span>Order Management</span>
                </a>
              </li>
              <!--<li class="nav-item">
                <a <?php if($activeMenu == 9){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="#" onclick="return comming()">
                  <i class="material-icons">note_add</i>
                  <span>Sales Report</span>
                </a>
              </li>-->


             <!--  <li class="nav-item">
                <a <?php if($activeMenu == 10){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?> href="#" onclick="return comming()">
                  <i class="material-icons">note_add</i>
                  <span>User Management</span>
                </a>
              </li> -->
              
              <li class="nav-item">
                <a href="#" <?php if($activeMenu == 14){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?>><i class="material-icons">person </i>
                  <span>My account <em class="fa fa-angle-right"></em></span> </a>
                 <ul class="group-list">
                    <li><a href="<?php echo base_url('Admin/changePassword');?>">Edit Password</a></li>
                    <li><a href="<?php echo base_url('Admin/myAccount');?>">Edit Profile</a></li>
                </ul>
              </li> 
              <li class="nav-item">
               <a href="<?php echo base_url('Admin/managecms');?>"  <?php if($activeMenu == 3){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?>>
                  <i class="material-icons">image</i>
                  <span>Manage CMS</span>
                </a>
              </li>
              <li class="nav-item">
               <a href="<?php echo base_url('Admin/featureCollection');?>"  <?php if($activeMenu == 9){  ?> class="nav-link active" <?php  } else { ?> class="nav-link" <?php } ?>>
                  <i class="material-icons">note_add</i>
                  <span>Manage feature colletion</span>
                </a>
              </li>
             
              <li class="nav-item">
                <a href="<?php echo base_url('Admin/logout');?>" class="nav-link"><i class="material-icons">power_settings_new </i>
                  <span>Logout</span>
                </a>
              </li>
            </ul>
          </div>
        </aside>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-12 col-md-12 col-sm-12" style="padding-left: 55px;">
       <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
             
              <div class="input-group input-group-seamless ml-3">
               <a href="<?php echo base_url('Admin/dashboard');?>">
                <!-- <img id="main-logo" class="d-inline-block align-top mr-1" src="<?php echo base_url();?>assets/admin/images/logo.png" alt="Highway"> -->
                
                <img id="main-logo" class="d-inline-block align-top mr-1" src="http://novokart.kazmatechnology.in/assets/frontend/img/logo02.png" alt="Highway"> 
              </a>
                  
                  </div>
              <ul class="navbar-nav border-left flex-row ">
               
                <li class="nav-item">
                           <a class="nav-link  text-nowrap px-3">
                           <!-- <img class="user-avatar rounded-circle mr-2" src="<?php echo base_url();?>assets/admin/img/icon.png" alt="User Avatar"> -->
                          
                           <img class="user-avatar rounded-circle mr-2" src=" https://cdn3.iconfinder.com/data/icons/business-and-finance-icons/512/Business_Man-512.png" alt="User Avatar">
                           <span class="d-none d-md-inline-block">
                            <?php $sessdata = $this->session->userdata ( 'adminData' ); echo ucfirst($sessdata['userName']);?><!-- Sierra Brooks --></span>
                           </a>
                           
                        </li>
              </ul>
              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>


          <script type="text/javascript">
          	function comming(){
          	swal("Coming Soon");
          	}
          </script>