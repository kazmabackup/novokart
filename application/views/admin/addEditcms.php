
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
  <body class="h-100">
   
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
       <?php include('nav.php');?>
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
                      <!-- <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> Your profile has been updated! </div> -->
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
<!--                 <span class="text-uppercase page-subtitle">Overview</span>
 -->                <h3 class="page-title"><?php echo $breadcrum;?></h3>
              </div>
            </div>
            <!-- End Page Header -->

            <?php if(!empty($cmsdata)){?>


             <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit CMS Page</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                       <form class="form-horizontal form-material" method="post" action="" id="formData1" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<?php echo $cmsdata[0]['cms_id'];?>" />
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Title</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Title" value="<?php echo $cmsdata[0]['title'];?>"> <span class="nameErr error1" style="color: red;"></span>  </div>
                            </div>
                         <?php if ($cmsdata[0]['cms_id']==7){?>
                         <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Facebook Link</label>
                                <input type="text" id="fb" name="fb" class="form-control"  placeholder="Facebook Link" value="<?php echo $cmsdata[0]['fb_link'];?>"> <span class="fbErr error" style="color: red;"></span>  </div>
                             </div>
                              <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Twitter Link</label>
                                <input type="text" id="tw" name="tw" class="form-control"  placeholder="Twitter Link" value="<?php echo $cmsdata[0]['tw_link'];?>"> <span class="twErr error" style="color: red;"></span>  </div>
                              </div>
                              <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Google+ Link</label>
                                <input type="text" id="g_plus" name="g_plus" class="form-control"  placeholder="Google+ Link" value="<?php echo $cmsdata[0]['g_plus_link'];?>"> <span class="g_plusErr error" style="color: red;"></span>  </div>
                             </div>
                             <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Linkedin Link</label>
                                <input type="text" id="lin" name="lin" class="form-control"  placeholder="Linkedin Link" value="<?php echo $cmsdata[0]['lin_link'];?>"> <span class="linErr error" style="color: red;"></span>  </div>
                              </div>
                              <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Instagram Link</label>
                                <input type="text" id="insta" name="insta" class="form-control"  placeholder="Instagram Link" value="<?php echo $cmsdata[0]['insta_link'];?>"> <span class="instaErr error" style="color: red;"></span>  </div>
                         </div>
              <?php } elseif ($cmsdata[0]['cms_id']==5){?>
              <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="feFirstName">Contact</label>
                                <input type="text" id="contact" name="contact" class="form-control"  placeholder="Contact" value="<?php echo $cmsdata[0]['contact'];?>"> <span class="contactErr error" style="color: red;"></span>  </div>
                              </div>
                              <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Email</label>
                                <input type="text" id="email" name="email" class="form-control"  placeholder="Email" value="<?php echo $cmsdata[0]['email'];?>"> <span class="emailErr error" style="color: red;"></span>  </div>
                      </div>
                      <?php }?>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Description</label>
                                <textarea class="form-control" name="content" id="content" rows="5"><?php echo $cmsdata[0]['description'];?></textarea>
                                <span class="contentErr error1" style="color: red;"></span>
                              </div>
                            </div>
                         
                            
                            <button type="submit" onclick="return formValidationEdit();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php } else { ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Add CMS Page</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                        <form class="form-horizontal form-material" method="post" action="" id="formData0" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value=""/>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">Title</label>
                                <input type="text" id="name" name="name" class="form-control"  placeholder="Title" value=""> <span class="nameErr error1" style="color: red;"></span>  </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Description</label>
                                <textarea class="form-control" name="content" id="content" rows="5"></textarea>
                                <span class="contentErr error1" style="color: red;"></span>
                              </div>
                            </div>
                            <button type="submit" onclick="return formValidationAdd();" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->

             <?php }?>
          </div>
          <?php include('footer.php');?>
        </main>
      </div>
    </div>
             <?php include('script.php');?>
              <script>
        function formValidationEdit(){
         var data = new FormData($('#formData1')[0]);
         var hid = $("#id").val();
         var title = $("#name").val();
         $(".nameErr").html("");
         $(".nameErr").hide(""); 
         if(hid ==7){
             var fb = $("#fb").val();
             var tw = $("#tw").val();
             var g_plus = $("#g_plus").val();
             var lin = $("#lin").val();
             var insta = $("#insta").val();
             $(".fbErr").html("");
             $(".fbErr").hide("");
             $(".twErr").html("");
             $(".twErr").hide("");
             $(".g_plusErr").html("");
             $(".g_plusErr").hide("");
             $(".linErr").html("");
             $(".linErr").hide("");
             $(".instaErr").html("");
             $(".instaErr").hide("");
             if(title==""){
                 $(".nameErr").slideDown('slow');
                 $(".nameErr").html("Title is required.");
                 $("#name").focus();
                 return false;
               }
             if(fb==""){
                 $(".fbErr").slideDown('slow');
                 $(".fbErr").html("Facebook link is required.");
                 $("#fb").focus();
                 return false;
               } if(tw==""){
                   $(".twErr").slideDown('slow');
                   $(".twErr").html("Twitter link is required.");
                   $("#tw").focus();
                   return false;
                 }
               if(g_plus==""){
                     $(".g_plusErr").slideDown('slow');
                     $(".g_plusErr").html("G_plus link is required.");
                     $("#g_plus").focus();
                     return false;
                   }
               if(lin==""){
                   $(".linErr").slideDown('slow');
                   $(".linErr").html("Linked link is required.");
                   $("#lin").focus();
                   return false;
                 }
               if(insta==""){
                   $(".instaErr").slideDown('slow');
                   $(".instaErr").html("Instagram link is required.");
                   $("#insta").focus();
                   return false;
                 }else{
                     var ctrlUrl = "<?php echo base_url().'Admin/addEditCmsSubmit' ;?>";
                     var adminRedirectUrl="<?php echo base_url().'Admin/managecms'?>"; 
                     data.append('content', content);
                     $("#loadDiv").show(); 
                     $.ajax({
                       type: "POST",
                       url: ctrlUrl,
                       data:data,
                       mimeType: "multipart/form-data",
                       contentType: false,
                       cache: false,
                       processData: false,
                       success: function(data)
                       { //alert(data);
                         $("#loadDiv").hide(); 
                       if(data==1){
                         swal("Success!","CMS page has been Added successfully","success");
                           setTimeout(function () 
                           {
                          window.location.href=adminRedirectUrl },4000);
                       }else if(data== 2){
                         swal("Success!","CMS page has been Updated successfully","success");
                           setTimeout(function () 
                           {
                          window.location.href=adminRedirectUrl },4000);
                       }
                       else {
                         swal("Error!","CMS page has not been Updated successfully","error");
                           setTimeout(function () 
                           {
                          window.location.href=adminRedirectUrl },4000);
                       }
                       }
                     });

                   }
         } else {
        	 var content = CKEDITOR.instances['content'].getData();
        	 $(".contentErr").html("");
             $(".contentErr").hide("");
             if(title==""){
                 $(".nameErr").slideDown('slow');
                 $(".nameErr").html("Title is required.");
                 $("#name").focus();
                 return false;
               }
             if(content==""){
                 $(".contentErr").slideDown('slow');
                 $(".contentErr").html("content is required.");
                 $("#content").focus();
                 return false;
               }else{
                   var ctrlUrl = "<?php echo base_url().'Admin/addEditCmsSubmit' ;?>";
                   var adminRedirectUrl="<?php echo base_url().'Admin/managecms'?>"; 
                   data.append('content', content);
                   $("#loadDiv").show(); 
                   $.ajax({
                     type: "POST",
                     url: ctrlUrl,
                     data:data,
                     mimeType: "multipart/form-data",
                     contentType: false,
                     cache: false,
                     processData: false,
                     success: function(data)
                     { //alert(data);
                       $("#loadDiv").hide(); 
                     if(data==1){
                       swal("Success!","CMS page has been Added successfully","success");
                         setTimeout(function () 
                         {
                        window.location.href=adminRedirectUrl },4000);
                     }else if(data== 2){
                       swal("Success!","CMS page has been Updated successfully","success");
                         setTimeout(function () 
                         {
                        window.location.href=adminRedirectUrl },4000);
                     }
                     else {
                       swal("Error!","CMS page has not been Updated successfully","error");
                         setTimeout(function () 
                         {
                        window.location.href=adminRedirectUrl },4000);
                     }
                     }
                   });

                 }
         }
             
            
         
           
              
              return false;
        }
      </script> 


      <script>
        function formValidationAdd(){
         var data = new FormData($('#formData0')[0]);
         var hid = $("#id").val();
             var title = $("#name").val(); 
             var content = CKEDITOR.instances['content'].getData();
            
             $(".nameErr").html("");
             $(".nameErr").hide("");
             $(".contentErr").html("");
             $(".contentErr").hide("");
             
          if(title==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html("Title is required.");
            $("#name").focus();
            return false;
          }
           if(content==""){
              $(".contentErr").slideDown('slow');
              $(".contentErr").html("content is required.");
              $("#content").focus();
              return false;
            }
          else{
            var ctrlUrl = "<?php echo base_url().'Admin/addEditCmsSubmit' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/managecms'?>"; 
            data.append('content', content);
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              {//alert(data);
                $("#loadDiv").hide(); 
              if(data==1){
                swal("Success!","CMS page has been Added successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }else if(data== 2){
                swal("Success!","CMS page has been Updated successfully","success");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              else {
                swal("Error!","CMS page has not been Updated successfully","error");
                  setTimeout(function () 
                  {
                 window.location.href=adminRedirectUrl },4000);
              }
              }
            });

          }
              return false;
        }
      </script> 

       <script>
              CKEDITOR.replace( 'content', {
              });
      </script>

  </body>
</html>