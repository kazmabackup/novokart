
<!doctype html>
<html class="no-js h-100" lang="en">
  <?php include('head.php');?>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <body class="h-100">
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php include('nav.php');?>

        
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
             <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4><?php echo $breadcrum;?></h4>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
          <div class="row">
            <div class="col">
              <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3 text-center">
                <form action="<?php echo base_url()?>Admin/customerexpoertdata" method="post" enctype="multipart/form-data">
                <div class="form-row">
                <div class="form-group col-md-4">
                <input type="text" id ="fromdatepicker" name ="fromdatepicker" placeholder="From date" class="form-control"/>
                <span class="fromdatepickerErr error1" style="color: red;"></span>
                </div>
                <div class="form-group col-md-4">
                <input type="text" placeholder="To date" id ="todatepicker" name ="todatepicker" class="form-control"/>
                <span class="todatepickerErr error1" style="color: red;"></span>
                </div>
                <div class="form-group col-md-2">
                <input type="submit" class="pull-right btn btn-primary btn-xs" onclick="return exportreport();" value="Export Data"><br/>
                </div>
                </div>
                </form>
                 <table id="example" class="display table" cellspacing="0" width="100%">
                 <thead>
            <tr>
                <th>S. No.</th>
                <th>Hospital or Diagnostic</th>
				<th>Email</th>
                <th>Contact</th>
                <th>Password</th>
                <th>package </th>
				<th>Affiliate Name </th>
				<th>Min. purchase value</th>
				<th>Customer discount</th> 
				<th>Licence no.</th> 
				 <th>Algorithm</th>
                <th>Action</th>
            </tr>
               </thead>
               <tbody>
                <?php
                if(!empty($userdata)){
                    $i = 1;
                    foreach ( $userdata as $detail ) {
                      $id = $detail ['user_id'];
					  $username = $detail ['user_name'];
					  $algorithm = $detail['algorithm'];
                      $email = $detail ['email'];
                      $password = $detail ['password'];
					  $contact = $detail ['contact'];
				      $adhar = $detail ['adhar_number'];
				      $pckgid = $detail ['package_id'];
				      $where = "package_id= $pckgid";
				      $packagedata = $this->Adminmodel->getDataById ( 'package', $where );
				      $packagename=$packagedata[0]['package_name'];
				      $min_value = $detail['min_value'];
				      $package_percentage = $detail['affiliate_percentage'];
			          $agentid = $detail ['agent_id'];
				      $where = "affiliate_id= $agentid";
				      $affiliatedata = $this->Adminmodel->getDataById ( 'affiliate', $where );
				      $agentname=$affiliatedata[0]['affiliate_name'];
				      $licence = $detail ['licence_number'];
				      $cityid = $detail ['city_id'];
				      $pinid = $detail ['pincode_id'];
				      $where = "city_id= $cityid";
				      $citydata = $this->Adminmodel->getDataById ( 'city', $where );
				      $cityname=$citydata[0]['city_name'];
				      $where1 = "pin_code_id=  $pinid";
				      $pindata = $this->Adminmodel->getDataById ( 'pin_code', $where1 );
				      $piname=$pindata[0]['pin_num'];
                      $status = $detail ['status'];
                      $whereorder = "user_id= $id AND order_status='Pending' AND order_file !=''";
                      $orderdata = $this->Adminmodel->getDataById ( 'manage_order', $whereorder );
                      ?>
              <tr>
               <td><?php echo $i;?></td>
               <td><?php echo $username;?></td>
               <td><?php echo $email;?></td>
               <td><?php echo $contact;?></td>
               <td><?php echo $password;?></td>
				 <?php if($pckgid == 0){?>
			   <td><?php echo "select package";?></td>
				 <?php }else{?>
               <td><?php echo $packagename;?></td>
				  <?php }?>
				  <?php if($agentid == 0){?>
			   <td><?php echo "select Affiliate";?></td>
				 <?php }else{?>
               <td><?php echo $agentname;?></td>
				 <?php }?>
			  <td><?php echo  $min_value;?></td>
			  <td><?php echo  $package_percentage;?></td>
			  <td><?php echo  $licence;?></td>
			  <td><?php if ($algorithm=='1'){echo "Algorithm 1";} else { echo "Algorithm 2";}?></td>
              <td><?php if($status == 'Active'){?>
                  <a href="" class="btn btn-mini btn-success" onclick="changeCountryStatus(<?php echo $id;?>,'In-Active')" ><?php echo $status;?></a>
                  <?php }else{?>
                  <a href="" class="btn btn-mini btn-danger" onclick="changeCountryStatus(<?php echo $id;?>, 'Active')" ><?php echo $status;?></a>
                  <?php }?>
                  <a href="<?php echo base_url('Admin/addEditUser');?>/<?php echo $id;?>" class="btn btn-mini btn-success" >Assign</a>
                  <?php if (!empty($orderdata)){?>
                  <a href="<?php echo base_url('Admin/addtocart');?>/<?php echo $id;?>" class="btn btn-mini btn-success" >Add to cart</a>
              <?php }?>
              </td>
            </tr>
            <?php $i++;} }?>
             </tbody>
             </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>
        <?php include('footer.php');?>
         <?php include('script.php');?>

         <script>
      function changeCountryStatus(id, status){
      var res = confirm("Are you sure, You want to "+status+" this customer?");
           if(res == true) {
            var url="<?php echo base_url()."Admin/changeuserStatus/"?>";
            var adminRedirectUrl="<?php echo base_url().'Admin/customer'?>"; 

            $("#loadDiv").show();
              $.ajax({
                type: "POST",
                url: url,
                data:({
                id : id, 
                status: status 
                }),
                cache: false,
                success: function(data)
                  { //alert(data);
                    $("#loadDiv").hide();
                      if(data == 1) {
                        swal("Customer has been changed successfully.");
                        setTimeout(function () 
                      {
                          window.location.href=adminRedirectUrl },4000);
                        } else {
                          swal("Customer has not been changed successfully.");
                        }
                   }
            });
        }
      }
  </script>
 
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#fromdatepicker" ).datepicker();
  } );
  $( function() {
	    $( "#todatepicker" ).datepicker();
	  } );
  </script>
  <script>
  function exportreport(){
      var fromdate = $("#fromdatepicker").val();
      var todate = $("#todatepicker").val();
      $(".fromdatepickerErr").html("");
      $(".fromdatepickerErr").hide("");
      $(".todatepickerErr").html("");
      $(".todatepickerErr").hide("");
      if(fromdate==""){
          $(".fromdatepickerErr").slideDown('slow');
          $(".fromdatepickerErr").html("select from date.");
          $("#fromdatepicker").focus();
          return false;
        }
      if(todate==""){
          $(".todatepickerErr").slideDown('slow');
          $(".todatepickerErr").html("select to date.");
          $("#todatepicker").focus();
          return false;
        } 
      //return false;
	  }
  </script>
  
  </body>
</html>