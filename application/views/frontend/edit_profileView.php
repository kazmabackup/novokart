<html class="fa-events-icons-ready">
<?php include 'head.php';?>
   <body>
     <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/profile">My profile</a></li>
               <li class="breadcrumb-item active" aria-current="page">Update profile</li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="prof pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-9">
                  <h1>Update my profile</h1>
                  <br>
                   <?php if($responce = $this->session->flashdata('Successfully')): ?>
      <div class="box-header">
        <div class="col-lg-6">
           <div class="alert alert-success"><?php echo $responce;?></div>
        </div>
      </div>
       <?php endif;?>
                  <!-- <article> -->
                  <form action="<?php echo base_url()?>Home/editProfile" method="post">
                     <ul class="list-unstyled row">
                        <li class="col-md-6"><h5 >User Name:</h5> <h6 > <input type="text" class="form-control" name="name" id="name" value="<?php echo $userDatadata[0]['user_name'];?>"></h6></li>
                        <li class="col-md-6"><h5 >Email id:</h5> <h6><input class="form-control" type="text" name="email" id="email" value="<?php echo $userDatadata[0]['email'];?>"></h6></li>
                        <li class="col-md-6"><h5 >Mobile number:</h5> <h6 ><input class="form-control" type="text" name="mobile" id="mobile" value="<?php echo $userDatadata[0]['contact'];?>"></h6></li>
                        <li class="col-md-6"><h5 >Your Address:</h5> <h6 ><input class="form-control" type="text" name="address" id="address" value="<?php echo $userDatadata[0]['address'];?>"></h6></li>
                        <li class="col-md-6"><h5 >Pan card no.:</h5> <h6><input class="form-control" type="text" name="aadhar_no" value="<?php echo $userDatadata[0]['adhar_number'];?>"></h6></li>
                     </ul>
                     <input type="submit" class="btn btn-success" value="Save">
                     </form>
                  <!-- </article> -->
               </div>
                <div class="col-md-3">
                  <ul class="list-unstyled">
               <li>
                  <i class="fa fa-user-circle"></i> <?php echo $userDatadata[0]['email'];?>
               </li>
               <li class="active">
                  <a href="<?php echo base_url()?>Home/profile"> My Profile</a>
               </li>
                <li>
                  <a href="<?php echo base_url()?>Home/my_earning"> My earnings</a>
               </li>
               
                <li>
                  <a href="<?php echo base_url()?>Home/logout"><i class="fa fa-power-off" aria-hidden="true"></i> Log out</a>
               </li>
            </ul>
               </div>
            </div>
              
           </div>
        </section>
      </main>
      <span class="clearfix"></span>
      <?php include 'footer.php';?>
</body></html>