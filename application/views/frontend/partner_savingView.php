<html class="fa-events-icons-ready">
<?php include 'head.php';?>
   <body>
     <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/partnersaving">Calculate saving</a></li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="prof pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1>Partner savings</h1>
                  <br>
                  <?php if ($algo2==""){?>
                  <span>CALL CUSTOMER CARE FOR MORE DETAILS</span>
                  <?php } else {?>
                 <table class="cart">
                 <?php if ($cartData){?>
                    <thead>
                       <tr>
                          <th>Product list</th>
                          <th>Quantity</th>
                          <th>Price (MRP)</th>
                          <th>Discount Percentage</th>
                       </tr>
                    </thead>
                    <tbody>
                     <?php
                     	$partnersaving = 0;
                     	$total_cost_price = 0;
                     	$total_discount_price = 0;
                     	$marginData = $this->Homemodel->getTableData("company_margin");
                     	$company_margin = $marginData[0]['company_margin'];
                     	foreach ($cartData as $cart){
                     		$cartid = $cart['add_to_cart_id'];
                     		$productid = $cart['product_id'];
                     		$userid = $cart['user_id'];
                     		$quantity = $cart['quantity'];
                     		$whereuser = "user_id='$userid'";
                     		$userData = $this->Homemodel->getDataById("user",$whereuser);
                     		$affiliate_percentage = $userData[0]['package_percentage'];
                     		$affdis = $userData[0]['affiliate_percentage'];
                     		$wherepro = "product_id='$productid'";
                     		$productData = $this->Homemodel->getDataById("product",$wherepro);
                     		$productname = $productData[0]['product_name'];
                     		$productimage =  $productData[0]['image'];
                     		$actualprice =  $productData[0]['actual_price'];
                     		$discountprice =  $productData[0]['discount_price'];
                     		$total_discount_price = $total_discount_price + ($discountprice * $quantity);
                     		$costprice = $productData[0]['cost_price'];
                     		$total_cost_price = $total_cost_price + ($costprice * $quantity);
                     		
                     		//$packageprice = ($discountprice*$package_percentage)/100;
                     		//$real_price = $discountprice - $packageprice;
                     		
                     		//$price = $real_price;//$cart['price'];
                     		//$whereaffliate = "affiliate_id='$affliate_id'";
                     		//$affiliateData = $this->Homemodel->getDataById("affiliate",$whereaffliate);
                     		//$affdis = $affiliateData[0]['discount'];
                     		//$total_price = $price;
                     		?>
                       <tr>
                          <td><?php echo $productname;?> </td>
                          <td>
                             <?php echo $quantity;?>
                          </td>
                           
                          <td>
                           <i class="fa fa-rupee"></i>  <?php  echo ($discountprice *$quantity);?>
                          </td>
                          <td>
                             <?php echo number_format($hostital_dis_per,2);?>%
                          </td> 
                       </tr>
                      <?php }
                      $commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
                      $company_profit = ($company_margin/100);
                      //echo $affdis;die;
                      if ($algorithm=='1'){
                       $package_per = (($commulative_per/100)*1)-($affdis/100);
                       if ($package_per > $company_profit){
                       	$new_total_dis = ($total_discount_price - $total_cost_price)*$affiliate_percentage/100;
                       }else {
                       	$new_total_dis = (($total_discount_price - $total_cost_price)*$affiliate_percentage/100)-$company_margin/100;
                       }
                       } else {
                       	$package_per = ($commulative_per - $company_margin);
                       	if ($package_per > $affiliate_percentage){
                       		 $new_total_dis = ($total_discount_price)*$affiliate_percentage/100;
                       	}
                       }
                      // echo $new_total_dis;die;
                       $hostital_dis = ($new_total_dis*$affdis/100);
                       $hostital_dis_per = ($hostital_dis/$total_discount_price)*100;
                       $affiliate_earning = $new_total_dis - $hostital_dis;
                     
                      ?>
                    </tbody>
                    <tfoot>
                    <td></td>
                     <td width="30%">Total amount</td>
                     <td colspan="" width="" style="border-right: none;"><i class="fa fa-rupee"></i> <?php echo number_format($total_discount_price,2);?></td>
                     <tr>
                     <td></td>
                     <td colspan="">Partner discount price</td>
                     <td><i class="fa fa-rupee"></i> <?php echo number_format($hostital_dis,2)?></td>
                      <td colspan="" width="10%" style="border-left: none;">
                     </tr>
                     <tr>
                      <td></td>
                     <td width="30%">After Discount amount</td>
                      <td colspan="" width="" style="text-align: ; border-right: none;"><i class="fa fa-rupee"></i> <?php echo number_format(($total_discount_price - $hostital_dis),2);?></td>
                       <a href="#" onclick = "return send_otp();" data-toggle="modal"  style="float: right; border-radius: 25px;" class="btn btn-success">Send OTP</a>
                       <span class="affliateidErr error" style="color: red;"></span>
                     </td>
                     </tr>
                    </tfoot>
                    <?php }?>
                 </table>
                 <?php }?>
                 
                        <div class="costumModal">
                           <!-- <a href="#costumModal9" role="button" class="btn btn-default" data-toggle="modal">
                              expandIn
                              </a> -->
                           <div id="otp" class="modal otp" data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                            
                           </div>
                        </div>
               </div>
               
            </div>
              
           </div>
        </section>
      </main>
      <span class="clearfix"></span>
    <?php include 'footer.php';?>
</body></html>

  <script>
       function send_otp(){
            var ctrlUrl = "<?php echo base_url().'Home/sendotp';?>";
       		//var adminRedirectUrl="<?php echo base_url().'Home/partnersaving/'?>" ; 
       			$.ajax({
       				type: "POST",
       				url: ctrlUrl,
       				data:({}),
       				cache: false,
       				success: function(data)
       				{ //alert(data);
       				if(data== 0){
       					$(".affliateidErr").html("try again");
       					$(".affliateidErr").slideDown('slow');
       				} else {
       					$("#otp").html(data);
           				$("#otp").modal("show");
       				}
       				}
       			});
           return false;
       }
       </script>
        <script>
       function checkotp(){
    	var data = new FormData($('#form1')[0]);
    	 var ctrlUrl = "<?php echo base_url().'Home/validateotp';?>";
    	 var adminRedirectUrl="<?php echo base_url().'Home/pay_now'?>" ; 
    			$.ajax({
    				type: "POST",
         			url: ctrlUrl,
         			data:data,
		            mimeType: "multipart/form-data",
		            contentType: false,
		            cache: false,
		            processData: false,
    				success: function(data)
    				{ //alert(data);
    				 if(data==0){
    					 $(".invalidotpErr").html("invalid otp");
        				 $(".invalidotpErr").slideDown('slow');
        				 } else {
        					 var delay = 1000; 
            				 setTimeout(function(){ window.location = adminRedirectUrl; }, delay);
            				 }
    				}
    			});
        return false;
       }
       </script>
       <script>
       $("input").keyup(function () {
   	    if (this.value.length == this.maxLength) {
   	      $(this).next("input").focus();
   	    }
   	});
       </script>
       