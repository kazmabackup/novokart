<html class="fa-events-icons-ready">
<?php include 'head.php';?>
    <!-- ALL END -->
    <style>
        
.file-upload {
  background-color: #ffffff;
  width: 400px;
  /* margin: 0 auto; */
  /* padding: 20px; */
  margin-bottom: 20px;
}

.file-upload-btn {
    width: 100%;
    margin: 0;
    color: #fff;
    background: #0C4164;
    border: none;
    padding: 10px;
    /* border-radius: 4px; */
    transition: all .2s ease;
    outline: none;
    margin-top: 15px;
}

.file-upload-btn:hover {
  background: #0C4164;
  color: #ffffff;
  transition: all .2s ease;
  cursor: pointer;
}

.file-upload-btn:active {
  border: 0;
  transition: all .2s ease;
}

.file-upload-content {
  display: none;
  /* text-align: center; */
}

.file-upload-input {
  position: absolute;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
  outline: none;
  opacity: 0;
  cursor: pointer;
}

.image-upload-wrap {
  /* margin-top: 20px; */
  border: 4px dashed #0C4164;
  position: relative;
}


.image-upload-wrap:hover {
  background-color: #0C4164;
  border: 4px dashed #ffffff;
}

.image-title-wrap {
  /* padding: 0 15px 15px 15px; */
  color: #222;
}

.drag-text {
  text-align: center;
}

.drag-text h3 {
  font-weight: 100;
  color: #1CA5AA;
  padding: 60px 15px;
}
.image-upload-wrap:hover  .drag-text h3{
    color: #fff;
}
.file-upload-image {
    max-height: 300px;
    max-width: 100%;
    margin: 15px auto;
    /* padding: 20px; */
    width: 100%;
    object-fit: cover;
}

.remove-image {
  /* width: 200px; */
  margin: 0;
  /* color: #fff; */
  /* background: #cd4535; */
  border: none;
  padding: 10px;
  border-radius: 4px;
  transition: all .2s ease;
  outline: none;
}

.remove-image:hover {
  /* background: #c13b2a; */
  /* color: #ffffff; */
  transition: all .2s ease;
  cursor: pointer;
}

.remove-image:active {
  border: 0;
  transition: all .2s ease;
}
    </style>
 <link href="https://use.fontawesome.com/b4b0414965.css" media="all" rel="stylesheet"></head>
 <body>
   <?php include 'header.php';?>
    <span class="clearfix"></span>
    <nav aria-label="breadcrumb">
       <ol class="breadcrumb">
          <div class="container">
             <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
             <li class="breadcrumb-item active" aria-current="page">Order prescription</li>
          </div>
       </ol>
    </nav>
    <span class="clearfix"></span>
    <main>
      <section class="prof pro inner">
       <form  method="post" action="" id="fileformData" enctype="multipart/form-data">
         <div class="container">
             <h1>Order Medicine</h1>
                <div class="file-upload">
                         <!--  <form  method="post" action="" id="fileformData" enctype="multipart/form-data">
                        <input type="file" id="img" name="img"/> 
                        <span class="uploadErr error" style="color: red;"></span>
                        <button type="submit" onclick="return uploadvalidation();">Submit</button>
                        </form> -->
                      
                        <div class="image-upload-wrap">
                          <input class="file-upload-input" type='file' onchange="readURL(this);" id="img" name="img" accept="image/*" />
                          <div class="drag-text">
                            <h3>Drag and drop a file or click upload prescription</h3>
                          </div>
                        </div>
                        
                        <div class="file-upload-content">
                          <img class="file-upload-image" src="#" alt="your image" />
                          <div class="image-title-wrap">
                            <button type="button" onclick="removeUpload()" class="remove-image"> <i class="fa fa-times"></i> Remove <span class="image-title">Uploaded Image</span></button>
                          </div>
                        </div><br/>
                         <span class="uploadsuccessErr error" style="color: green;"></span>
                        <span class="uploadErr error" style="color: red;"></span>
                        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Upload prescription</button>
                      </div>
                      <span class="clearfix"></span>
                       <button type="submit" class="btn btn-success" onclick="return uploadvalidation();">Order now</button>
                      <!-- <a href="#" class="btn btn-success">Order now</a> -->
         </div>
         </form>
      </section>
    </main>
    <span class="clearfix"></span>
    <?php include 'footer.php';?>
    <script>
function uploadvalidation(){
	var data = new FormData($('#fileformData')[0]);
	 var file = $("#img").val();
     $(".uploadErr").html("");
     $(".uploadErr").hide("");
     if(file==""){
    	 $(".uploadErr").slideDown('slow');
         $(".uploadErr").html("Choose order prescription.");
         return false;
     } else{
         var ctrlUrl = "<?php echo base_url().'Home/uploadorder';?>";
         var adminRedirectUrl="<?php echo base_url().'Home'?>"; 
         $.ajax({
           type: "POST",
           url: ctrlUrl,
           data:data,
           mimeType: "multipart/form-data",
           contentType: false,
           cache: false,
           processData: false,
           success: function(data)
           {//alert(data);
           if(data==1){
        	   $(".uploadsuccessErr").slideDown('slow');
               $(".uploadsuccessErr").html("Success! Order has been uploaded").css({ 'font-weight': 'bold' });
              setTimeout(function () 
               {
              window.location.href=adminRedirectUrl },4000);
           }
           else {
        	   $(".uploadErr").slideDown('slow');
               $(".uploadErr").html("Error! Try again");
               return false;
           }
           }
         });

       }
     return false;
}
  </script>
    <!-- ALL JS END -->
    <!-- ---------------------------------RANGE SLIDER---------------------- -->
    <script src="js/range.js"></script>
    <!-- PRODUCT ZOOM -->
    <script src="https://unpkg.com/xzoom/dist/xzoom.min.js"></script>
    <script src="https://hammerjs.github.io/dist/hammer.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
            function readURL(input) {
  if (input.files && input.files[0]) {

    var reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      $('.image-title').html(input.files[0].name);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
		$('.image-upload-wrap').addClass('image-dropping');
	});
	$('.image-upload-wrap').bind('dragleave', function () {
		$('.image-upload-wrap').removeClass('image-dropping');
});

    </script>
    
    <!-- ---------------------------------RANGE SLIDER END---------------------- -->
 

</body></html>