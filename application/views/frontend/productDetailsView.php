
<!DOCTYPE html>
<html>
   <?php include 'head.php';?>
   <body>
    <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/productlist/<?php echo $sub_cat_id;?>">Product list</a></li>
               <li class="breadcrumb-item active" aria-current="page">Product Details</li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main >
         <section class="category-list inner">
            <div class="container">
               <div class="row">
                  <div class="col-md-3">
                   <!--   <h4>FILTER BY PRICE</h4>
                     <div class="wrapper" style="padding:30px 5px;">
                        <div class="range-slider">
                           <input type="text" class="js-range-slider" value="" />
                        </div>
                        
                     </div> -->
                      <div class="panel">
                        <h4>CATEGORIES</h4>
                        <ul class="list-unstyled">
                        <?php if ($categoryData){
                        foreach ($categoryData as $category){
                        	$categoryid = $category['category_id'];
                        	$categoryname = $category['category_name'];
                        	$wherecat = "category_id=$categoryid";
                        	$productData = $this->Homemodel->getDataById ( 'product', $wherecat );
                        	if (!empty($productData)){
                        	$productcount = count($productData);
                        	} else {
                        		$productcount = 0;
                        	}
                        ?>
                           <li>
                              <a href="<?php echo base_url()?>Home/filterproduct/<?php echo $categoryid;?>"><i class="fa fa-long-arrow-right"></i> <span><?php echo $categoryname;?></span> <strong>(<?php echo $productcount;?>)</strong></a>
                           </li>
                          <?php }
                        }?>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-9">
                     <!-- default start -->
                     <section id="default" class="padding-top0">
                        <div class="row">
                           <div class="col-md-5">
                              <div class="xzoom-container">
                                 <img class="xzoom" id="xzoom-default" src="<?php echo base_url()?>uploads/product/<?php echo $productdata[0]['image']?>" xoriginal="<?php echo base_url()?>uploads/product/<?php echo $productdata[0]['image']?>" />
                                 <!-- <div class="xzoom-thumbs">

                                    <a href="<?php echo base_url()?>assets/img/big.png"><img class="xzoom-gallery" width="60px" height="60px" src="<?php echo base_url()?>assets/img/big.png"  xpreview="<?php echo base_url()?>assets/img/big.png" title="The description goes here"></a>

                                    <a href="https://st3.depositphotos.com/1026649/15889/i/1600/depositphotos_158897122-stock-photo-hospital-bed-with-medical-equipments.jpg"><img class="xzoom-gallery" width="60px" height="60px" src="https://st3.depositphotos.com/1026649/15889/i/1600/depositphotos_158897122-stock-photo-hospital-bed-with-medical-equipments.jpg" title="The description goes here"></a>

                                    <a href="https://media.istockphoto.com/photos/surgery-instruments-picture-id470455057?s=2048x2048"><img class="xzoom-gallery" width="60px" height="60px" src="https://media.istockphoto.com/photos/surgery-instruments-picture-id470455057?s=2048x2048" title="The description goes here"></a>

                                    <a href="https://media.istockphoto.com/photos/surgical-equipment-picture-id81711266?s=2048x2048"><img class="xzoom-gallery" width="60px" height="60px" src="https://media.istockphoto.com/photos/surgical-equipment-picture-id81711266?s=2048x2048" title="The description goes here"></a>
                                 </div> -->
                              </div>
                           </div>
                           <div class="col-md-7">
                              <h4><?php echo $productdata[0]['product_name'];?></h4>
                              <?php $sessionUserId = $this->session->userdata('userData');
                        if ($sessionUserId) {
                        ?>
                              <h6 style="color: #1CA5AA;"><span><i class="fa fa-rupee"></i> <?php echo $productdata[0]['discount_price'];?></span> <strike style="float: none;margin-left: 7px;"><i class="fa fa-rupee"></i><?php echo $productdata[0]['actual_price'];?></strike></h6>
                              <?php }?>
                              <h6><i>Description</i></h6>
                              <p>
                                 <?php echo $productdata[0]['description'];?>
                              </p>
                              
                              <span class="clearfix"></span>
                              
                              <span class="clearfix"></span>
                              <input type="hidden" id="dis_price<?php echo $productdata[0]['product_id']?>" value="<?php echo $productdata[0]['discount_price'];?>"/>
                              <a class="btn btn-success" style="height: 40px;line-height: 26px;margin-left: 15px;font-size: 14px;" onclick="return add_to_cart(<?php echo $productdata[0]['product_id'];?>,1);"><i class="fa fa-shopping-basket"></i> Add to cart</a>
                              <a onclick = "return buy_now(<?php echo $productdata[0]['product_id'];?>,1);" class="btn btn-default" style="height: 40px;line-height: 26px;margin-left: 15px;">Buy now</a>
                           </div>
                        </div>
                        <span class="clearfix"></span>
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#home">Reviews</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#menu1">Write a review</a>
                           </li>
                        </ul>
                        <div class="tab-content" style="display: block;">
                           <div id="home" class="tab-pane comments active">
                              <br>
                              <h3>Feedbacks :</h3>
                              <ul class="list-unstyled">
                                 <li>
                                    <div class="row">
                                       <div class="col-md-1"><img src="img/big.png"></div>
                                       <div class="col-md-10">
                                          <h6>John Wick</h6>
                                          <span class="rating"><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                          <p>
                                             Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit.
                                          </p>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div id="menu1" class="tab-pane fade"><br>
                            <h3>Write a review :</h3>
                            <textarea></textarea>

                            <div class="rating-control" style="margin-top: 7px; color: #222;">
                              <span style="line-height: 4; padding-right: 7px;">Rate this product :</span>
                                 <input type="radio" name="rating" value="5" id="id_rating_0" required="">
                                 <label for="id_rating_0">5</label>
                                 <input type="radio" name="rating" value="4" id="id_rating_1" required="">
                                 <label for="id_rating_1">4</label>
                                 <input type="radio" name="rating" value="3" id="id_rating_2" required="" checked="">
                                 <label for="id_rating_2">3</label>
                                 <input type="radio" name="rating" value="2" id="id_rating_3" required="">
                                 <label for="id_rating_3">2</label>
                                 <input type="radio" name="rating" value="1" id="id_rating_4" required="">
                                 <label for="id_rating_4">1</label>
                              </div>
                              <span class="clearfix"></span>
                              <a class="btn btn-success" style="color: #fff !important;">Submit</a>
                           </div>
                        </div>
                     </section>
                     <!-- default end -->
                     <span class="clearfix"></span>
                  </div>
               </div>
            </div>
            </div>
         </section>
      </main>
      <span class="clearfix"></span>
      <?php include 'footer.php';?>
   </body>
</html>
