
<html class="fa-events-icons-ready">
<?php include 'head.php';?>
   <body>
   <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/my_earning">My saving</a></li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="earnings pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-9">
                  <h1>My savings</h1>
                  <br>
                  <?php if (!empty($earningData)){
                  	$marginData = $this->Homemodel->getTableData("company_margin");
                  	$company_margin = $marginData[0]['company_margin'];
                  foreach ($earningData as $earning){
                  	$algorithm = $earning['algorithm'];
                  	$orderid = $earning['unique_order_id'];
                  	$invoiceno = $earning['invoice_no'];
                  	$orderdate = $earning['order_date'];
                  	$date = date('d M Y',strtotime($orderdate));
                  	$time = date('h:i a',strtotime($orderdate));
                  	$whereunique="unique_order_id='$orderid'";
                  	$historyData1 = $this->Homemodel->getDataById ( 'manage_order', $whereunique );
                  	//print_r($historyData1);
                  	$id = $earning ['unique_order_id'];
                  	$userid = $earning ['user_id'];
                  	$quantity1 = $earning ['order_quantity'];
                    $affiliate_percentage = $earning['dis_by_agent'];
                    $affiliateper = $earning['affiliate_percentage'];
                  	if ($historyData1) {
                  		$total_discount_price=0;
                  		$total_cost_price = 0;
                  		foreach ( $historyData1 as $history1 ) {
                  			$pro_id = $history1 ['pro_id'];
                  			$pro_price2 = $history1['pro_price'];
                  			$pro_cost_price2 = $history1['pro_cost_price'];
                  			$quan = $history1['order_quantity'];
                  			$total_discount_price = $total_discount_price + (($pro_price2 * $quan));
                  			$total_cost_price = $total_cost_price + ($pro_cost_price2 * $quan);
                  			$whereproduct = "product_id='$pro_id'";
                  			$productData = $this->Homemodel->getDataById ( 'product', $whereproduct );
                  			$whereuser = "user_id='$userid'";
                  			$userData = $this->Homemodel->getDataById ( 'user', $whereuser );
                  			$affiliateid = $userData[0]['agent_id'];
                  			$affdis = $userData[0]['package_percentage'];
                  			$whereaffiliate = "affiliate_id='$affiliateid'";
                  			$affiliateData = $this->Homemodel->getDataById ( 'affiliate', $whereaffiliate );
                  		}
                  		
                  		if ($total_discount_price==0){
                  			$commulative_per=0;
                  		}else{
                  		    $commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
                  		}
                  		$company_profit = ($company_margin/100);
                  		if ($algorithm=='1'){
                     		$package_per = (($commulative_per/100)*1)-($affdis/100);
                     		if ($package_per > $company_profit){
                     			$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100;
                     		}else {
                     			$new_total_dis = (($total_discount_price - $total_cost_price)*$affdis/100)-$company_margin/100;
                     		}
                  		} else {
                  			$package_per = ($commulative_per - $company_margin);
                  			if ($package_per > $affdis){
                  				$new_total_dis = ($total_discount_price)*$affdis/100;
                  			}
                  		}
                  		
                  		$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
                  		$hostital_dis_per = ($hostital_dis*$total_discount_price/100);
                  		$affiliate_earning = $new_total_dis - $hostital_dis;
                  		$total_bill_price = $total_discount_price - $hostital_dis;
                  		//echo $affiliate_percentage;
                  	}
                  	
                  ?>
               <article>
                  <div class="row">
                     <div class="col-md-10">
                        <small><?php echo $date;?>/ <?php echo $time;?></small>
                        <h5>Order Id: <?php echo $orderid;?></h5>
                        <h5>Invoice No: <?php echo $invoiceno;?></h5>
                     </div>
                     <div class="col-md-2">
                        <a class="fa fa-download" href="<?php echo base_url()?>Home/invoice"></a>
                     </div>
                     <div class="col-md-10">
                        <h5>Total saving amount</h5>
                     </div>
                     <div class="col-md-2">
                        <h5><i class="fa fa-rupee"></i> <?php echo number_format($hostital_dis,2);?></h5>
                     </div>
                  </div>
               </article>
               <?php }die;
                  }?>
               </div>
               <div class="col-md-3">
                  <ul class="list-unstyled">
               <li>
                  <i class="fa fa-user-circle"></i> <?php echo $userDatadata[0]['email'];?>
               </li>
               <li class="active">
                  <a href="<?php echo base_url()?>Home/profile"> My Profile</a>
               </li>
               <li>
               <a href="<?php echo base_url()?>Home/change_password">Change password</a>
               </li>
                <li>
                  <a href="<?php echo base_url()?>Home/my_earning">My saving</a>
               </li>
                <li>
                  <a href="<?php echo base_url()?>Home/logout"><i class="fa fa-power-off" aria-hidden="true"></i> Log out</a>
               </li>
            </ul>
               </div>
            </div>
           </div>
        </section>
      </main>
      <span class="clearfix"></span>
     <?php include 'footer.php';?>
   

</body></html>