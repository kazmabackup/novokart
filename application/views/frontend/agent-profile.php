<html class="fa-events-icons-ready">
      <?php include 'head.php';?>
   <body>
     <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="#">My profile</a></li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="prof pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-9">
                  <h1>My profile</h1>
                  <br>
                   <?php if($responce = $this->session->flashdata('Successfully')): ?>
      <div class="box-header">
        <div class="col-lg-6">
           <div class="alert alert-success"><?php echo $responce;?></div>
        </div>
      </div>
    <?php endif;?>
                  <!-- <article> -->
                     <ul class="list-unstyled">
                        <li class="row"><h5 class="col-md-2">Name:</h5> <h6 class="col-md-10"><?php echo $userDatadata[0]['user_name'];?></h6></li>
                        <li class="row"><h5 class="col-md-2">Email id:</h5> <h6 class="col-md-10"><?php echo $userDatadata[0]['email'];?></h6></li>
                        <li class="row"><h5 class="col-md-2">Mobile number:</h5> <h6 class="col-md-10"><?php echo $userDatadata[0]['contact'];?></h6></li>
                        <li class="row"><h5 class="col-md-2">Your Address:</h5> <h6 class="col-md-10"><?php echo $userDatadata[0]['address'];?></h6></li>
                        <li class="row"><h5 class="col-md-2" style="border-bottom: 1px solid #eee;">Aadhar card no.:</h5> <h6 class="col-md-10" style="border-bottom: 1px solid #eee;"><?php echo $userDatadata[0]['adhar_number'];?></h6></li>

                     </ul>
                     <a href="<?php echo base_url()?>Home/editProfile" class="btn btn-dark">Edit</a>
                  <!-- </article> -->
               </div>
               <div class="col-md-3">
                  <ul class="list-unstyled">
               <li>
                  <i class="fa fa-user-circle"></i> <?php echo $userDatadata[0]['email'];?>
               </li>
               <li class="active">
                  <a href="<?php echo base_url()?>Home/profile"> My Profile</a>
               </li>
               <li>
               <a href="<?php echo base_url()?>Home/change_password">Change password</a>
               </li>
                <li>
                  <a href="<?php echo base_url()?>Home/my_earning">My saving</a>
               </li>
               
                <li>
                  <a href="<?php echo base_url()?>Home/logout"><i class="fa fa-power-off" aria-hidden="true"></i> Log out</a>
               </li>
            </ul>
               </div>
            </div>
              
           </div>
        </section>
      </main>
      <span class="clearfix"></span>
      
     <?php include 'footer.php';?>
      
   

</body></html>