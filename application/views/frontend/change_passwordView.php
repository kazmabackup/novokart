<html class="fa-events-icons-ready">
<?php include 'head.php';?>
   <body>
     <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/profile">My profile</a></li>
               <li class="breadcrumb-item active" aria-current="page">Change Password</li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="prof pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-9">
                  <h1>Change Password</h1>
                  <br>
                   <?php if($responce = $this->session->flashdata('Successfully')): ?>
      <div class="box-header">
        <div class="col-lg-6">
           <div class="alert alert-success"><?php echo $responce;?></div>
        </div>
      </div>
       <?php endif;?>
                  <!-- <article> -->
                     <ul class="list-unstyled row">
                        <li class="col-md-6"><h5 >New password:</h5> <h6 > <input type="password" class="form-control" name="new_pass" id="new_pass" value="">
                         <span class="new_passErr error" style="color: red;"></span></h6></li>
                        
                        <li class="col-md-6"><h5 >Re-enter new password:</h5> <h6><input class="form-control" type="password" name="re_new_pass" id="re_new_pass" value="">
                         <span class="re_new_passErr error" style="color: red;"></span></h6></li>
                        
                     </ul>
                      <span class="failedErr error" style="color: red;"></span>
                      <span class="successErr error" style="color: green;"></span>
                     <input type="button" onclick ="submitchangepass();" class="btn btn-success" value="Save">
                  <!-- </article> -->
               </div>
                <div class="col-md-3">
                  <ul class="list-unstyled">
               <li>
                  <i class="fa fa-user-circle"></i> <?php echo $userDatadata[0]['email'];?>
               </li>
               <li class="active">
                  <a href="<?php echo base_url()?>Home/profile"> My Profile</a>
               </li>
               <li>
                 <a href="<?php echo base_url()?>Home/change_password">Change password</a>
               </li>
               <li>
                  <a href="<?php echo base_url()?>Home/my_earning">My earnings</a>
               </li>
               
                <li>
                  <a href="<?php echo base_url()?>Home/logout"><i class="fa fa-power-off" aria-hidden="true"></i> Log out</a>
               </li>
            </ul>
               </div>
            </div>
              
           </div>
        </section>
      </main>
      <span class="clearfix"></span>
      <?php include 'footer.php';?>
</body></html>
<script>
       function submitchangepass(){
           var new_pass = $("#new_pass").val();
           var re_new_pass = $("#re_new_pass").val();
           $(".new_passErr").html("");
           $(".new_passErr").hide("");
           $(".re_new_passErr").html("");
           $(".re_new_passErr").hide("");
           $(".successErr").html("");
           $(".successErr").hide("");
           if(new_pass == ""){
     	   $(".new_passErr").slideDown('slow');
           $(".new_passErr").html("New password is required.");
           $("#new_pass").focus();
           return false;
           }
           if(re_new_pass == ""){
         	   $(".re_new_passErr").slideDown('slow');
               $(".re_new_passErr").html("Password is required.");
               $("#re_new_pass").focus();
               return false;
               }
           if(new_pass != re_new_pass){
         	   $(".re_new_passErr").slideDown('slow');
               $(".re_new_passErr").html("new password doesn't match.");
               $("#re_new_pass").focus();
               return false;
               } else {
            	   var ctrlUrl = "<?php echo base_url().'Home/changepasswordsubmit';?>";
       			var adminRedirectUrl="<?php echo base_url().'Home'?>"; 
       			$.ajax({
       				type: "POST",
       				url: ctrlUrl,
       				data:({
       					new_pass : new_pass
       				}),
       				cache: false,
       				success: function(data)
       				{ //alert(data);
       				if(data== 0){
       					//alert("bbbb");
       					$(".failedErr").html("please try again!<br/>");
       					$(".failedErr").slideDown('slow');
       				}else {
       					$(".successErr").html("Password has been changed Successfully.<br/>");
       					$(".successErr").slideDown('slow');
       					var delay = 3000; 
       					setTimeout(function(){ window.location = adminRedirectUrl; }, delay);
       				}
       				}
       			});
                   }
           return false;
       }
      
      </script>