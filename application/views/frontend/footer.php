<footer>
         <div class="container">
            <div class="row ">
               <div class="col-md-3 ">
                  <h4>About Store</h4>
                  <!-- <p><?php echo $aboutstoredata[0]['description'];?></p> -->
                  <p>Copyright © 2018 All rights reserved. Designed and developed by<a href="#"> Kazma Technology Pvt. Ltd."</a></p>
                  <p>
                  </p>
                  <ul class="social list-unstyled">
                     <li><a href="<?php echo $socialmediadata[0]['fb_link'];?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                     <li><a href="<?php echo $socialmediadata[0]['tw_link'];?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                     <li><a href="<?php echo $socialmediadata[0]['g_plus_link'];?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                     <li><a href="<?php echo $socialmediadata[0]['lin_link'];?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                     <li><a href="<?php echo $socialmediadata[0]['insta_link'];?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                  <p></p>
               </div>
               <div class="col-md-2 ">
                  <h4>Customer Care</h4>
                  <ul class="list-unstyled" >
                     <li><a href="<?php echo base_url()?>Home/contact">Contact</a></li>
                     <li><a href="#">Returns/Exchange</a></li>
                     <li><a href="#">Gift Voucher</a></li>
                     <li><a href="#">Wishlist</a></li>
                     <li><a href="#">Special</a></li>
                     <li><a href="#">Customer Services</a></li>
                     <li><a href="#">Site maps</a></li>
                  </ul>
                  <p></p>
               </div>
               <div class="col-md-2">
                  <h4>Information</h4>
                  <p>
                  </p>
                  <ul class="list-unstyled" >
                     <li><a href="<?php echo base_url()?>Home/aboutus">About us</a></li>
                     <li><a href="<?php echo base_url()?>Home/deliveryinfo">Delivery Information</a></li>
                     <li><a href="<?php echo base_url()?>Home/privacypolicy">Privacy Policy</a></li>
                     <li><a href="<?php echo base_url()?>Home/support">Support</a></li>
                      <li><a href="<?php echo base_url()?>Home/support">Career</a></li> 
                     <li><a href="#">Order Tracking</a></li>
                  </ul>
                  <p></p>
               </div>
               <div class="col-md-2">
                  <h4>News</h4>
                  <ul class="list-unstyled">
                     <li><a href="#">Blog</a></li>
                     <li><a href="#">Press</a></li>
                     <li><a href="#">Exhibitions</a></li>
                  </ul>
               </div>
               <div class="col-md-3">
                  <h4>Contact Information</h4>
                  <ul class="list-unstyled" >
                     <li><?php echo $contactdata[0]['description'];?></li>
                     <li><a href="tel://<?php echo $contactdata[0]['contact'];?>"><?php echo $contactdata[0]['contact'];?></a></li>
                     <li><a href="mailto:<?php echo $contactdata[0]['email'];?>"><i class="fa fa-envelope" style="margin-right: 7px;"></i><?php echo $contactdata[0]['email'];?></a></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="footer-bottom" align="centre">
            <p>
               Copyright © 2018 All rights reserved. Designed and developed by <a style="color: #1CA5AA;" href="http://kazmatechnology.com/" target="_blank">Kazma Technology pvt. ltd.</a>
            </p>
         </div>
      </footer>
      <!-- to-top -->
      <div class='to-top'>
         <a class='back-to-top fa fa-angle-up' href='#'></a>
      </div>
      <!-- to-top-end -->
      <!-- ALL JS -->
 
      <script src="<?php echo base_url()?>assets/frontend/js/jquery.js"></script>
      <script src="https://use.fontawesome.com/b4b0414965.js"></script>
     
      <script src="<?php echo base_url()?>assets/frontend/js/owl.carousel.js"></script>
      <script>
         $(document).ready(function () {
  
              $("#owl1").owlCarousel({
                 pagination : false,
                  items:1,
                  nav:true,
                  autoplay: false,
                  loop: true,
              });
              $("#owl2").owlCarousel({
                 pagination : false,
                  items:1,
                  nav:true,
                  autoplay: true,
                  loop: true,
              });
              $("#owl3").owlCarousel({
                 pagination : false,
                  items:1,
                  nav:false,
                  autoplay: true,
                  loop: true,
              });
                
         });
      </script>
      <script src="<?php echo base_url()?>assets/frontend/js/bootstrap.js"></script>
      <script>
         $(document).ready(function(){
         $(".accoa").click(function(){
             $(".accou").toggle();
         });
      </script>
      <script>
         $(document).ready(function(){
         $('.tab-link').click(function(){
         var tab_id = $(this).attr('data-tab');
         $('.tab-link').removeClass('current');
         $('.tab-content').removeClass('current');
         
         $(this).addClass('current');
         $("#"+tab_id).addClass('current');
         })
         })
      </script>	
      <script>
         //offset() measures how far down the sticky element is from the top of the window
         var stickyTop = $(".sticky").offset().top;
         
         //whenever the user scrolls, measure how far we have scrolled
         $(window).scroll(function() {
         var windowTop = $(window).scrollTop();
         
         //check to see if we have scrolled past the original location of the sticky element
         if (windowTop > stickyTop) {
         
         //if so, change the sticky element to fised positioning
         $(".sticky").addClass("stuck");
         } else {   
         $(".sticky").removeClass("stuck");
         }
         });
      </script>
      <!-- ---------------- -->
      <script>
         $(".menu-icon").on("click", function(event){
         event.preventDefault();
         
         $(this).next().toggleClass("active");
         $(this).closest(".box").toggleClass("active");
         });
         
         $(".search-icon").on("click", function(event){
         event.preventDefault();
         
         $(this).closest(".search").toggleClass("visible");
         });
      </script>
      <!-- to top js -->
      <script >
         $(function () {
         
         var offset = 300,
         duration = 500,
         top_section = $('.to-top'),
         toTopButton = $('a.back-to-top');
         // showing and hiding button according to scroll amount (in pixels)
         $(window).scroll(function () {
         if( $(this).scrollTop() > offset ) {
         $(top_section).fadeIn(duration);
         } else{
         $(top_section).fadeOut(duration);
         
         }});
         
         // activate smooth scroll to top when clicking on the button
         
         $(toTopButton).click(function(e) {
         e.preventDefault();
         $('html, body').animate({
         scrollTop: 0
         }, 700);
         });
         
         });
      </script>
      <!-- to- top js end -->
      <script>
         /* When the user clicks on the button, 
         toggle between hiding and showing the dropdown content */
         function myFunction() {
         document.getElementById("myDropdown").classList.toggle("show");
         }
         
         // Close the dropdown if the user clicks outside of it
         window.onclick = function(event) {
         if (!event.target.matches('.dropbtn')) {
         
         var dropdowns = document.getElementsByClassName("dropdown-content");
         var i;
         for (i = 0; i < dropdowns.length; i++) {
         var openDropdown = dropdowns[i];
         if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
         }
         }
         }
         }
      </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js "></script>
      <script>
         $(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
      </script>
      <!-- ALL JS END -->
      <!-- ---------------------------------RANGE SLIDER---------------------- -->
      <script src="<?php echo base_url()?>assets/frontend/js/range.js"></script>
      
      <!-- PRODUCT ZOOM -->
      <script src="https://unpkg.com/xzoom/dist/xzoom.min.js"></script>
      <script src="https://hammerjs.github.io/dist/hammer.min.js"></script>
      <script src="<?php echo base_url()?>assets/frontend/js/custom.js"></script>
      
      <!-- ---------------------------------RANGE SLIDER END---------------------- -->
     
      <script>
        function registration(){
         var data = new FormData($('#formData')[0]);
         var name = $("#name").val(); 
         var mobile = $("#mobile").val(); 
		 var email = $("#email").val(); 
		
         var password = $("#password").val(); 
         var confirm_pass = $("#confirm_pass").val();
         var customertype = $("#customer_type").val();
         //alert(customertype);
         var city = $("#city").val();
         var pin = $("#pin").val();
         var licence_no = $("#licence_no").val();
       //  var licence_img = $("#licence_img").val();
       //  var aadhar_no = $("#aadhar_no").val(); 
        // var aadhar_img = $("#aadhar_img").val(); 
		 var emailRegexStr = /^[a-zA-Z0-9/+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		 var isvalid = emailRegexStr.test(email);
          $(".nameErr").html("");
          $(".nameErr").hide("");
          $(".mobileErr").html("");
          $(".mobileErr").hide("");
		  $(".emailErr").html("");
          $(".emailErr").hide("");
          $(".passwordErr").html("");
          $(".passwordErr").hide("");
          $(".conpasswordErr").html("");
          $(".conpasswordErr").hide("");
          $(".customertypeErr").html("");
          $(".customertypeErr").hide("");
          $(".cityErr").html("");
          $(".cityErr").hide("");
          $(".pinErr").html("");
          $(".pinErr").hide("");
          $(".licence_noErr").html("");
          $(".licence_noErr").hide("");
          $(".licence_imgErr").html("");
          $(".licence_imgErr").hide("");
          $(".aadhar_noErr").html("");
          $(".aadhar_noErr").hide("");
          $(".aadhar_imgErr").html("");
          $(".aadhar_imgErr").hide("");
          if(name==""){
            $(".nameErr").slideDown('slow');
            $(".nameErr").html("Name is required.");
            $("#name").focus();
            return false;
          } if(mobile==""){
              $(".mobileErr").slideDown('slow');
              $(".mobileErr").html("Mobile no is required.");
              $("#mobile").focus();
              return false;
            }
          if(mobile.length < 10 || mobile.length > 10 ){
    			$(".mobileErr").slideDown('slow');
    			$(".mobileErr").html("enter 10 digit mobile number.");
    			return false;
    		}
    		if(email ==""){
            $(".emailErr").slideDown('slow');
            $(".emailErr").html("Email is required.");
            $("#email").focus();
            return false;
          }if(!isvalid){
			$(".quantityErr").slideDown('slow');
			$(".quantityErr").html("Please enter valid Email address.");
		}
          if(password==""){
            $(".passwordErr").slideDown('slow');
            $(".passwordErr").html("Password is required.");
            $("#password").focus();
            return false;
          }
          if(confirm_pass==""){
              $(".conpasswordErr").slideDown('slow');
              $(".conpasswordErr").html("Confirm Password is required.");
              $("#confirm_pass").focus();
              return false;
            }
          if(password !=confirm_pass){
              $(".conpasswordErr").slideDown('slow');
              $(".conpasswordErr").html("Confirm Password does not match.");
              $("#conpassword").focus();
              return false;
            }
          if(customertype == "0"){
              $(".customertypeErr").slideDown('slow');
              $(".customertypeErr").html("Select customer type.");
              $("#customertype").focus();
              return false;
            }
          if(city == "0"){
              $(".cityErr").slideDown('slow');
              $(".cityErr").html("Select city.");
              $("#city").focus();
              return false;
            }
          if(pin == "0"){
              $(".pinErr").slideDown('slow');
              $(".pinErr").html("Select pin.");
              $("#pin").focus();
              return false;
            }
          if(licence_no == ""){
              $(".licence_noErr").slideDown('slow');
              $(".licence_noErr").html("Licence no is required.");
              $("#licence_no").focus();
              return false;
            }
          /*  if(licence_img == ""){
              $(".licence_imgErr").slideDown('slow');
              $(".licence_imgErr").html("Upload licence image.");
              $("#licence_img").focus();
              return false;
            }
         if(aadhar_no == ""){
              $(".aadhar_noErr").slideDown('slow');
              $(".aadhar_noErr").html("AAdhar no is required.");
              $("#aadhar_no").focus();
              return false;
            }
          if(aadhar_img == ""){
              $(".aadhar_imgErr").slideDown('slow');
              $(".aadhar_imgErr").html("Upload aadhar image.");
              $("#aadhar_img").focus();
              return false;
            } */
         
          else{
            var ctrlUrl = "<?php echo base_url().'Home/registration' ;?>";
            var adminRedirectUrl="<?php echo base_url().'Home'?>"; 
            $("#loadDiv").show(); 
            $.ajax({
              type: "POST",
              url: ctrlUrl,
              data:data,
              mimeType: "multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,
              success: function(data)
              { //alert(data);
               if(data==1){
            	   $(".regsuccessErr").html("You have sucessfully registerd.<br/>");
  				   $(".regsuccessErr").slideDown('slow');
  				 setTimeout(function() {$('#regpop').modal('hide');}, 4000);
  				setTimeout(function(){ $('#loginpop').modal('show'); }, 4000);
                   } else if (data == "dupmob"){
                	  $(".mobileErr").slideDown('slow');
           			  $(".mobileErr").html("This mobile no already exixts.");
                       }
                   else if (data == "dupemail"){
                	   $(".emailErr").slideDown('slow');
           			  $(".emailErr").html("This email-id already exixts.");
                       }
                   else {
                	$(".invalidreg").html("Please try again.<br/>");
     				$(".invalidreg").slideDown('slow');
                    }
              }
            });

          }
              return false;
        }
      </script> 
      <script>
       function login(){
           var user = $("#user").val();
           var pass = $("#pass").val();
           $(".userErr").html("");
           $(".userErr").hide("");
           $(".passErr").html("");
           $(".passErr").hide("");
           $(".successErr").html("");
           $(".successErr").hide("");
           if(user == ""){
     	   $(".userErr").slideDown('slow');
           $(".userErr").html("User name is required.");
           $("#user").focus();
           return false;
           }
           if(pass == ""){
         	   $(".passErr").slideDown('slow');
               $(".passErr").html("Password is required.");
               $("#pass").focus();
               return false;
               } else {
            	   var ctrlUrl = "<?php echo base_url().'Home/login';?>";
       			var adminRedirectUrl="<?php echo base_url().'Home'?>"; 
       			$.ajax({
       				type: "POST",
       				url: ctrlUrl,
       				data:({
       					user : user,
       					pass : pass,
       				}),
       				cache: false,
       				success: function(data)
       				{ //alert(data);
       				if(data== 0){
       					//alert("bbbb");
       					$(".invalidLogin").html("Invalid login details.<br/>");
       					$(".invalidLogin").slideDown('slow');
       				} else if(data == "dup"){
       					$(".successErr").html("Successfully logged in.<br/>");
       					$(".successErr").slideDown('slow');
       					var delay = 1000; 
       					setTimeout(function(){ window.location = "<?php echo base_url()?>Home/my_cart"; }, delay);
           				}else {
       					$(".successErr").html("Successfully logged in.<br/>");
       					$(".successErr").slideDown('slow');
       					var delay = 1000; 
       					setTimeout(function(){ window.location = adminRedirectUrl; }, delay);
       				}
       				}
       			});
                   }
           return false;
       }
       $(document).ready(function(){
    		 $('#pass').keypress(function(e){
    			 if(e.keyCode==13)
    				 login();
    			});
    		});
      </script>
      <script>
       function forgotpass(){
           var mobile = $("#forgotmobile").val();
           $(".forgotmobileErr").html("");
           $(".forgotmobileErr").hide("");
           $(".forgotsuccessErr").html("");
           $(".forgotsuccessErr").hide("");
           if(email == ""){
     	   $(".forgotmobileErr").slideDown('slow');
           $(".forgotmobileErr").html("Mobile no is required.");
           $("#forgotmobile").focus();
           return false;
           }
           if(mobile.length < 10 || mobile.length > 10 ){
   			$(".forgotmobileErr").slideDown('slow');
   			$(".forgotmobileErr").html("enter 10 digit mobile number.");
   			return false;
   		} else {
            	   var ctrlUrl = "<?php echo base_url().'Home/forgotpass';?>";
       			var adminRedirectUrl="<?php echo base_url().'Home'?>"; 
       			$.ajax({
       				type: "POST",
       				url: ctrlUrl,
       				data:({
       					mobile : mobile
       				}),
       				cache: false,
       				success: function(data){
       				if(data== 0){
       					$(".invalidmobile").html("Please enter registered mobile no.<br/>");
       					$(".invalidmobile").slideDown('slow');
       				} else if(data == 1){
       					$(".forgotsuccessErr").html("Password has been sent to your mobile no.<br/>");
       					$(".forgotsuccessErr").slideDown('slow');
       					var delay = 1000; 
       					setTimeout(function(){ window.location = adminRedirectUrl; }, delay);
           				}
       				else {
       					$(".invalidmobile").html("Try again.<br/>");
       					$(".invalidmobile").slideDown('slow');
       					var delay = 1000; 
       					setTimeout(function(){ window.location = adminRedirectUrl; }, delay);
       				}
       				}
       			});
                   }
           return false;
       }
       $(document).ready(function(){
    		 $('#forgotmobile').keypress(function(e){
    			 if(e.keyCode==13)
    				 login();
    			});
    		});
      </script>
      <script type="text/javascript">
  $('#forgotmobile').keypress(function(event){
	    console.log(event.which);
	    var mobile = $("#forgotmobile").val();
	if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
	    $(".forgotmobileErr").slideDown('slow');
	    $(".forgotmobileErr").html("Enter numeric value only.");
	    event.preventDefault();
	} else if(mobile.length > 9){
		event.preventDefault();
		}else{
		$(".forgotmobileErr").html("");	
	}});
</script>
      <script>
   function add_to_cart(productid,quantity){
     var productid = productid;
     var quantity = quantity;
     var amount = $("#dis_price"+productid).val();
     var cart_amount = $("#cart_amount").val();
     var total = (parseFloat(amount) + parseFloat(cart_amount)).toFixed(2);
     var package_percentage = "<?php echo $package_percentage;?>";
    
     var ctrlUrl = "<?php echo base_url().'Home/add_to_cart';?>";
	 var adminRedirectUrl="<?php echo base_url().'Home/my_cart'?>"; 
		$.ajax({
			type: "POST",
			url: ctrlUrl,
			data:({
				productid : productid,
				quantity : quantity
			}),
			cache: false,
			success: function(data)
			{ //alert(data);
		     if(data=="dup"){
			     alert("This product is already in cart");
			     window.location.href = adminRedirectUrl;
			     }
		      else if(data=="login"){
		    	 $('#loginpop').modal('show'); 
				} else {
					$("#cart_amount").val(total);
					$("#total_amount").html(total);
			    	$("#cartcount").html(data);
				  }
			}
		});
}
</script>
      <script>
     function openlogin(){
         $("#regpop").modal("hide");
         $("#forgotpop").modal("hide");
         $("#loginpop").modal("show");
         }
     function openreg(){
    	 $("#loginpop").modal("hide");
         $("#regpop").modal("show");
         }
     function openforgot(){
    	 $("#loginpop").modal("hide");
         $("#forgotpop").modal("show");
         }
      </script>
       <script>
   function getpin(){
     var cityid = $("#city").val();
     var ctrlUrl = "<?php echo base_url().'Home/getcitybypin';?>";
	// var adminRedirectUrl="<?php echo base_url().'Home/my_cart'?>"; 
		$.ajax({
			type: "POST",
			url: ctrlUrl,
			data:({
				cityid : cityid
			}),
			cache: false,
			success: function(data)
			{ //alert(data);
		     $("#pin").html(data);
		      
			}
		});
}
</script>
        <script>
   function catsortbyprice(){ //alert("ok");
     var slct2 = $("#slct2").val();
     var catid = "<?php echo $id;?>";
   //  alert(catid);
     var ctrlUrl = "<?php echo base_url().'Home/catsortbyprice';?>";
	// var adminRedirectUrl="<?php echo base_url().'Home/my_cart'?>"; 
		$.ajax({
			type: "POST",
			url: ctrlUrl,
			data:({
				slct2 : slct2,
				catid : catid
			}),
			cache: false,
			success: function(data)
			{ //alert(data);
			$("#list").hide();
		    $(".product-grid").html(data);
		      
			}
		});
}
</script>
        <script>
   function sortbyprice(){ //alert("ok");
     var slct2 = $("#slct2").val();
     var subcatid = "<?php echo $id;?>";
   //  alert(catid);
     var ctrlUrl = "<?php echo base_url().'Home/sortbyprice';?>";
	// var adminRedirectUrl="<?php echo base_url().'Home/my_cart'?>"; 
		$.ajax({
			type: "POST",
			url: ctrlUrl,
			data:({
				slct2 : slct2,
				subcatid : subcatid
			}),
			cache: false,
			success: function(data)
			{ //alert(data);
			$("#list").hide();
		    $(".product-grid").html(data);
		      
			}
		});
}
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <?php $productarr = $this->pdo->query("select product_name,product_id from product")->result_array();
  ?>
  <script>
  $( function() {
    var availableTags = [    
       <?php foreach($productarr as $proarr){?>
       {label:"<?php echo $proarr['product_name'];?>", value:"<?php echo $proarr['product_id'];?>"},
     
       <?php } ?>
    ];
    $( "#txtinput" ).autocomplete({
      source: availableTags,
      select: function(event, ui) {
    	  $("#txtinput").val(ui.item.label);
      location.href="<?php echo base_url()?>Home/productDetails/" +ui.item.value;
      return false;
  }
    });
   
  } );
  </script>
  <script>
  $( function() {
  
  $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 1000,
      values: [ 0, 500 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +
      " - ₹" + $( "#slider-range" ).slider( "values", 1 ) );

   var loader="<img src='<?php echo base_url()?>assets/ajaxLoad2.gif' />";
   
   $('.ui-slider-handle').on('click',function(){
      $('.product-grid').html(loader);
      var categoryId = '<?php echo $id;?>';
      
       var min_price=$( "#slider-range" ).slider( "values", 0 );
       var max_price=$( "#slider-range" ).slider( "values", 1 );
    
      var qs="categoryId="+categoryId+"&min_price="+min_price+"&max_price="+max_price;
       //alert(ctr_id);
       var ctrlUrlfilter = "<?php echo base_url().'Home/pricerange';?>"; 
       $.ajax({
          url:ctrlUrlfilter,
          type:'POST',
          data:qs,
          success:function(output){//alert(output);
                $('.product-grid').fadeOut('slow',function(){
                   $('.product-grid').html(output).fadeIn('fast');
                
                });
                
             
                }
       
       });
   });
   });
</script>
  <script>
   function buy_now(productid,quantity){
     var productid = productid;
     var quantity = quantity;
     var amount = $("#dis_price"+productid).val();
     var cart_amount = $("#cart_amount").val();
     var total = (parseFloat(amount) + parseFloat(cart_amount)).toFixed(2);
     var package_percentage = "<?php echo $package_percentage;?>";
    
     var ctrlUrl = "<?php echo base_url().'Home/add_to_cart';?>";
	 var adminRedirectUrl="<?php echo base_url().'Home/my_cart'?>"; 
		$.ajax({
			type: "POST",
			url: ctrlUrl,
			data:({
				productid : productid,
				quantity : quantity
			}),
			cache: false,
			success: function(data)
			{ //alert(data);
		     if(data=="dup"){
			     alert("This product is already in cart");
			     window.location.href = adminRedirectUrl;
			     }
		      else if(data=="login"){
		    	 $('#loginpop').modal('show'); 
				} else {
					$("#cart_amount").val(total);
					$("#total_amount").html(total);
			    	$("#cartcount").html(data);
			    	window.location.href = adminRedirectUrl;
				  }
			}
		});
}
</script>
  
	 
	 