
<html class="fa-events-icons-ready">
<?php include 'head.php';?>
   <body>
      <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/my_cart">My cart</a></li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="prof pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1>Cart</h1>
                  <br>
                  <?php if ($cartData){?>
                 <table class="cart">
                    <thead>
                       <tr>
                          <th>Product details</th>
                          <th>Price</th>
                          <th style="text-align: center;">Quantity</th>
                          <th>Total</th>
                          <th></th>
                       </tr>
                    </thead>
                     
                    <tbody>
                   <?php 
                    foreach ($cartData as $cart){
                    	$cartid = $cart['add_to_cart_id'];
                    	$productid = $cart['product_id'];
                    	$userid = $cart['user_id'];
                    	$whereuser = "user_id='$userid'";
                    	$userData = $this->Homemodel->getDataById("user",$whereuser);
                    	$package_percentage = $userData[0]['package_percentage'];
                    	$wherepro = "product_id='$productid'";
                    	$productData = $this->Homemodel->getDataById("product",$wherepro);
                    	$productname = $productData[0]['product_name'];
                    	$productimage =  $productData[0]['image'];
                    	$actualprice =  $productData[0]['actual_price'];
                    	$discountprice =  $productData[0]['discount_price'];
                    	$packageprice = ($discountprice*$package_percentage)/100;
                    	$real_price = $discountprice;
                    	$quantity = $cart['quantity'];
                    	$price = $real_price;//$cart['price'];
                    ?>
                    <input type="hidden" id="price<?php echo $cartid;?>" name="price" value="<?php echo $price;?>"/>
                       <tr>
                          <td><img src="<?php echo base_url()?>uploads/product/<?php echo $productimage;?>"> <span><?php echo $productname;?></span></td>
                          <td>
                             <span><i class="fa fa-rupee"></i> <?php echo $price?></span>
                             <br>
                             <strike><i class="fa fa-rupee"></i> <?php echo $actualprice;?></strike>
                          </td>
                          <td>
                             <input type="text" value="<?php echo $quantity;?>" class="form-control" name="quantity" id="quantity<?php echo $cartid;?>" onchange="myFunction(<?php echo $cartid?>)">
                             <span class="successErr<?php echo $cartid;?> error" style="color: green;"></span>
                          </td>
                          <td>
                             <i class="fa fa-rupee"></i> <span id="total_price<?php echo $cartid;?>"><?php echo ($price * $quantity);?></span>
                          </td>
                         
                          <td style="text-align: center;">
                             <a class="fa fa-times" onclick = "return deletecart(<?php echo $cartid;?>);"></a>
                          </td>
                       </tr>
                     <?php }?>
                    </tbody>
                    <tfoot>
                       <tr>
                          <td  >
                          <input type="hidden" id="amt" value="<?php echo $total_price?>"/>
                            Total : <i class="fa fa-rupee"></i> <span id="total_amount_append"><?php echo number_format($total_price,2);?></span>
                          </td>
                          <td colspan="3"><a  onclick = "return checkminval('<?php echo $purchase_value;?>','<?php echo $total_price;?>');" data-toggle="modal" style="float: right; border-radius: 25px;" class="btn btn-success">Calculate my savings</a></td>
                       </tr>
                        <span class="minvalueErr error" style="color: red;"></span>
                    </tfoot>
                 </table>
                  <?php  } else {?>
                   <tr>
                    <span> Cart is empty</span>
                   </tr>
                    <?php }?>
               </div>
            </div>
           </div>
        </section>
      </main>
      <div class="costumModal">
                           <!-- <a href="#costumModal9" role="button" class="btn btn-default" data-toggle="modal">
                              expandIn
                              </a> -->
                           <div id="affliate" class="modal" data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- <img class="white-logo" src="img/logo-white.png"> -->
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                       ×
                                       </button>
                                       <h4 class="modal-title">
                                          Affliate ID
                                       </h4>
                                    </div>
                                    <div class="modal-body">
                                       <form class="row">
                                          <div class="col-md-12">
                                             <input type="text" placeholder="Affiliate id" class="form-control" name="affliateid" id="affliateid">
                                             <span class="affliateidErr error" style="color: red;"></span>
                                          </div>
                                          <span class="successErr error" style="color: green;"></span>
                                       </form>
                                    </div>
                                    <div class="modal-footer">
                                       <!-- <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                                          Login
                                          </button>
                                          -->
                                       <a onclick="return sendaffiliateid();" class="btn btn-primary">Submit</a>
                                       
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
      <span class="clearfix"></span>
     <?php include 'footer.php';?>
</body></html>

<script>
function myFunction(cartid) {
	var quantity = $("#quantity"+cartid).val();
	var url="<?php echo base_url()."Home/update_cart/"?>";
  	var redirectUrl
  $.ajax({
  	type: "POST",
    	url: url,
    	data:({
    		cartid : cartid,
    		quantity : quantity
     	}),
    	cache: false,
    	success: function(data)
    		{
    	   if(data == 1) {
    		   $(".sucessErr"+cartid).html("Cart product has been updated successfully.");
					$(".sucessErr"+cartid).slideDown('slow');
              	//alert("Cart product has been updated successfully.");
          	} else {
              	alert("Cart product has not been updated.");
          	}
    	   var delay = 1000; 
				setTimeout(function(){ window.location.reload(); }, delay);
    	  		
           }
		});
	/*  var quantity = $("#quantity"+cartid).val();
	  var price = $("#price"+cartid).val();
	  var total_amount = $("#amt").val();
	  var total_price = (price * quantity);
	  var overallamount = (parseInt(total_amount) + parseInt(total_price));
	  $("#total_price"+cartid).html(total_price);
	  $("#total_amount_append").html(overallamount); */
	  //$("#amount").html(total_price);
	  //$("#total_amount").html(total_price);
}                
/* $("#quantity").keyup(function (event) {
	if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
    event.preventDefault();
	} 
	 }); */
  </script> 
          <script>
           function deletecart(id){
                	   var id = id;
                	   var res = confirm("Are you sure you want to delete cart product ?");
                	    if(res == true) {
                	    	var url="<?php echo base_url()."Home/deletecart/"?>";
                		  	var redirectUrl
                	      $.ajax({
                	      	type: "POST",
                	        	url: url,
                	        	data:({
                	        		id : id
                	         	}),
                	        	cache: false,
                	        	success: function(data)
                	        		{
                	        	   if(data == 1) {
                	                  	alert("Cart product has been deleted successfully.");
                	              	} else {
                	                  	alert("Cart product has not been deleted.");
                	              	}
                	        	  		window.location.reload();
                		           }
                	   		});
                			}
                   }
                    </script>
                    <script>
       function sendaffiliateid(){
           var affliateid = $("#affliateid").val();
           $(".affliateidErr").html("");
           $(".affliateidErr").hide("");
           $(".successErr").html("");
           $(".successErr").hide("");
           if(affliateid == ""){
     	   $(".affliateidErr").slideDown('slow');
           $(".affliateidErr").html("Affiliate id is required.");
           $("#affliateid").focus();
           return false;
           }
           else {
            	var ctrlUrl = "<?php echo base_url().'Home/sendaffliateid';?>";
       			var adminRedirectUrl="<?php echo base_url().'Home/partnersaving/'?>" ; 
       			$.ajax({
       				type: "POST",
       				url: ctrlUrl,
       				data:({
       					affliateid : affliateid,
       				}),
       				cache: false,
       				success: function(data)
       				{ //alert(data);
       				if(data== 0){
       					$(".affliateidErr").html("Invalid affiliate id.<br/>");
       					$(".affliateidErr").slideDown('slow');
       				} else {
       					var delay = 1000; 
       					setTimeout(function(){ window.location = adminRedirectUrl+data; }, delay);
       				}
       				}
       			});
                   }
           return false;
       }
       </script>
       <script>
   function checkminval(min_amount,total_amount){
	  //alert(parseFloat(min_amount));
	  // alert(parseFloat(total_amount));
	   //if (parseFloat(a.toFixed(2)) > parseFloat(b.toFixed(2)))
	  if(parseFloat(total_amount) > parseFloat(min_amount)){
		  $("#affliate").modal("show");
		  } else {
			  $(".minvalueErr").html("Minimum purchase amount should be "+ min_amount);
	          $(".minvalueErr").slideDown('slow');
		         return false;
			  }
   }
       </script>