  <?php $sessionUserId = $this->session->userdata('userData');?>
 <header>
         <div class="container">
            <nav class="top-navbar">
               <div class="row">
                  <div class="social col-md-6 col-xs-12">
                     <!-- <ul class="list-unstyled">
                         <li>
                           <a class="fa fa-facebook" href="<?php echo $socialmediadata[0]['fb_link'];?>" target="_blank"></a>
                        </li>
                        <li>
                           <a class="fa fa-twitter" href="<?php echo $socialmediadata[0]['tw_link'];?>" target="_blank"></a>
                        </li>
                        <li>
                           <a class="fa fa-google-plus" href="<?php echo $socialmediadata[0]['g_plus_link'];?>" target="_blank"></a>
                        </li>
                        <li><a href="<?php echo $socialmediadata[0]['lin_link'];?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li>
                           <a class="fa fa-instagram" href="<?php echo $socialmediadata[0]['insta_link'];?>" target="_blank"></a>
                         -->
                       <a class="navbar-brand" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/frontend/img/logo02.png"></a>
                         
                        <!-- end -->
                        
                        
                        <div class="costumModal">
                           <!-- <a href="#costumModal9" role="button" class="btn btn-default" data-toggle="modal">
                              expandIn
                              </a> -->
                           <div id="loginpop" class="modal" data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- <img class="white-logo" src="img/logo-white.png"> -->
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                       ×
                                       </button>
                                       <h4 class="modal-title">
                                          Login
                                       </h4>
                                    </div>
                                    <div class="modal-body">
                                       <form class="row">
                                          <div class="col-md-12">
                                             <input type="text" placeholder="User/Mobile no." class="form-control" name="user" id="user">
                                             <span class="userErr error" style="color: red;"></span>
                                          </div>
                                          <div class="col-md-12">
                                             <input type="password" placeholder="Password" class="form-control" name="pass" id="pass">
                                             <span class="passErr error" style="color: red;"></span>
                                          </div>
                                          <span class="successErr error" style="color: green;"></span>
                                          <span class="invalidLogin error" style="color: red;"></span>
                                       </form>
                                    </div>
                                    <div class="modal-footer">
                                       <!-- <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                                          Login
                                          </button>
                                          -->
                                       <a onclick="return login();" class="btn btn-primary">Login</a>
                                       <a onclick = "return openreg();" data-toggle="modal" style="float: right;margin-top: 6px;">Register.</a>
                                       <br>
                                       <a style="float: left; margin-top: 15px;" onclick = "return openforgot();" data-toggle="modal">Forgot password ?</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Forgot password start -->
                        <div class="costumModal">
                           <!-- <a href="#costumModal9" role="button" class="btn btn-default" data-toggle="modal">
                              expandIn
                              </a> -->
                           <div id="forgotpop" class="modal" data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- <img class="white-logo" src="img/logo-white.png"> -->
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                       ×
                                       </button>
                                       <h4 class="modal-title">
                                          Forgot Password
                                       </h4>
                                    </div>
                                    <div class="modal-body">
                                       <form class="row">
                                          <div class="col-md-12">
                                             <input type="text" placeholder="Enter mobile no." class="form-control" name="forgotmobile" id="forgotmobile">
                                             <span class="forgotmobileErr error" style="color: red;"></span>
                                          </div>
                                          <span class="forgotsuccessErr error" style="color: green;"></span>
                                          <span class="invalidmobile error" style="color: red;"></span>
                                       </form>
                                    </div>
                                    <div class="modal-footer">
                                       <a onclick="return forgotpass();" class="btn btn-primary">submit</a>
                                       <a onclick = "return openlogin();" data-toggle="modal" style="float: right;margin-top: 6px;">Login.</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Forgot password end -->
                        <div class="costumModal">
                           <!-- <a href="#costumModal9" role="button" class="btn btn-default" data-toggle="modal">
                              expandIn
                              </a> -->
                          <div id="regpop" class="modal" data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- <img class="white-logo" src="img/logo-white.png"> -->
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                       ×
                                       </button>
                                       <h4 class="modal-title">
                                          Registration
                                       </h4>
                                    </div>
                                    <div class="modal-body">
                                       <form class="row" method="post" action="" id="formData" enctype="multipart/form-data">
                                          <div class="col-md-12">
                                                <input type="text" placeholder="Organisation Name" class="form-control" name="name" id="name">
                                                <span class="nameErr error" style="color: red;"></span>
                                             </div>
                                             <div class="col-md-12">
                                                <input type="text" placeholder="Mobile no." class="form-control" name="mobile" id="mobile">
                                                <span class="mobileErr error" style="color: red;"></span>
                                             </div>
                                           <div class="col-md-12">
                                             <input type="text" placeholder="Email" class="form-control" name="email" id="email">
                                             <span class="emailErr error" style="color: red;"></span>
                                             </div>
                                             <div class="col-md-12">
                                                <input type="password" placeholder="password" class="form-control" name="password" id="password">
                                                <span class="passwordErr error" style="color: red;"></span>
                                             </div>
                                             <div class="col-md-12">
                                                <input type="password" placeholder="Conform Password" class="form-control" name="confirm_pass" id="confirm_pass">
                                                <span class="conpasswordErr error" style="color: red;"></span>
                                             </div>
                                             <div class="col-md-4">
                                                <select class="form-control" name="customer_type" id="customer_type">
                                                   <option value="0">select customer type</option>
                                                   <?php if ($customertypeData){
                                                   foreach ($customertypeData as $customertype){
                                                   	$id = $customertype['customer_type_id'];
                                                   	$name = $customertype['customer_type_name'];
                                                   ?>
                                                  <option value="<?php echo $id;?>"><?php echo $name;?></option>
                                                  <?php }
                                                   }?>
                                                </select>
                                                <span class="customertypeErr error" style="color: red;"></span>
                                             </div>
                                             <div class="col-md-4">
                                                <select class="form-control" name="city" id="city" onchange="return getpin();">
                                                   <option value="0">select city</option>
                                                   <?php if ($citydata){
                                                   foreach ($citydata as $city){
                                                   	$cityid = $city['city_id'];
                                                   	$cityname = $city['city_name'];
                                                   ?>
                                                  <option value="<?php echo $cityid;?>"><?php echo $cityname;?></option>
                                                  <?php }
                                                   }?>
                                                </select>
                                                <span class="cityErr error" style="color: red;"></span>
                                             </div>
                                             <div class="col-md-4">
                                                <select class="form-control" name="pin" id="pin">
                                                  <option value="0">select pin code</option>
                                                 
                                                </select>
                                                <span class="pinErr error" style="color: red;"></span>
                                             </div>
                                              <div class="col-md-6">
                                                <input type="text" placeholder="Licence number" class="form-control" name="licence_no" id="licence_no">
                                                <span class="licence_noErr error" style="color: red;"></span>
                                             </div>
                                              <!-- <div class="col-md-6">
                                                <input type="file" placeholder="Licence number" class="form-control" name="licence_img" name="licence_img">
                                                 <span class="licence_imgErr error" style="color: red;"></span>
                                             </div> -->
                                             <div class="col-md-6">
                                                <input type="text" placeholder="PAN NUMBER" class="form-control" name="pan_no" id="pan_no">
                                                 <span class="aadhar_noErr error" style="color: red;"></span>
                                             </div>
                                              <!-- <div class="col-md-6">
                                                <input type="file" placeholder="Aadhar number" class="form-control" name="aadhar_img" id="aadhar_img">
                                                <span class="aadhar_imgErr error" style="color: red;"></span>
                                             </div> -->
                                       </form>
                                    </div>
                                    <div class="modal-footer">
                                       <!-- <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                                          Login
                                          </button>
                                          -->
                                          <span class="regsuccessErr error" style="color: green;"></span>
                                          <span class="invalidreg error" style="color: red;"></span>
                                       <a onclick="return registration();" class="btn btn-primary">Register</a>
                                       <a onclick = "return openlogin();" style="float: right;margin-top: 6px;">Login.</a>
                                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </ul>
                  </div>
                  <div class="col-md-6 col-xs-12" style="text-align: right;">
                     <ul class="list-unstyled">
                     <!-- loggedin -->
                        <?php
                        if ($sessionUserId) { //print_r($sessionUserId['userName']);die;
                        ?>
                          <li class="account-drop ">
                           <a class=" accoa"><i class="fa fa-user-circle"></i><?php echo $sessionUserId['userName'];?></a>
                           <div class="accou dropdown-menu">
                              <ul>
                           
                                 <li>
                                    <a href="<?php echo base_url()?>Home/profile">My profile</a>
                                 </li>
                                  <li>
                                    <a href="<?php echo base_url()?>Home/change_password">Change password</a>
                                 </li>
                                  <li>
                                    <a href="<?php echo base_url()?>Home/order_history">My order history</a>
                                 </li>
                                  <li>
                                    <a href="<?php echo base_url()?>Home/my_earning">My saving</a>
                                 </li>
                                  <li>
                                    <a href="<?php echo base_url()?>Home/logout"><i class="fa fa-power-off" style="font-size: 12px;" aria-hidden="true"></i>  Log out</a>
                                 </li>
                                
                              </ul>
                           </div>
                          
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>Home/upload_order" class="btn btn-success" style="color: #fff !important;

                            text-transform: capitalize;
                            
                            font-size: 12px !important;
                            
                            padding: 4px 8px;
                            
                            border-radius: 3px; background: #1CA5AA;">Upload your order</a>
                      </li>
                         <?php } else {?>

                                  <li >
                           <a href="#loginpop" data-toggle="modal">Login</a>
                        </li>
                        <li>
                           <a href="#regpop" data-toggle="modal">Register</a>
                        </li>
                        <li style="font-size: 20px;">|</li>
                         <li>
                           <a > <i class="fa fa-envelope"></i> <span><?php echo $contactdata[0]['email'];?></span></a>
                        </li>
                        <li>
                           <a > <i class="fa fa-phone"></i> <span><?php echo $contactdata[0]['contact'];?> </span></a>
                        </li>
                                 <?php }?>
                      
                       
                        <!-- <li>
                           <a > <i class="fa fa-sign-in" aria-hidden="true"></i> <span>Login</span></a>
                           </li>
                           <li>
                           <a > <i class="fa fa-book" aria-hidden="true"></i> <span>Register</span></a>
                           </li> -->
                     </ul>
                  </div>
               </div>
            </nav>
            <?php
					$home = "";
					$about = "";
					$product = "";
					$career = "";
					$benifits = "";
					if ($activeMenu == 1) {
						$home = "active";
					} elseif ($activeMenu == 2) {
						$about = "active";
					} elseif ($activeMenu == 3) {
						$product = "active";
					} elseif ($activeMenu == 4) {
						$career = "active";
					}elseif ($activeMenu == 5) {
						$benifits = "active";
					}
					?>
            <nav class="navbar navbar-expand-lg navbar-light bg-light sticky">
               <div class="container">
                  <!-- <a class="navbar-brand" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/frontend/img/logo02.png"></a> -->
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse bs-example-js-navbar-collapse dropdown" id="navbarSupportedContent">
                     <ul class="navbar-nav mr-auto">
                        <li class="nav-item <?php echo $home;?>">
                           <a class="nav-link" href="<?php echo base_url();?>"> <i class="fa fa-home fa-fw"></i>Home <span class="sr-only">(current)</span></a>
                        </li>
                        
                        <li class="nav-item  products-drop <?php echo $product;?>">
                           <a id="drop1" 	class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Our Products</a>
                           <div class="dropdown-menu" aria-labelledby="drop1">
                              <div class="row">
                              <?php if ($categoryData){
                              foreach ($categoryData as $category){
                              	$categoryid = $category['category_id'];
                              	$categoryname = $category['category_name'];
                              	$where = "category_id=$categoryid AND status='Active'";
                              	$subcatdata= $this->Homemodel->getDataById ( 'sub_category', $where );
                              ?>
                                 <ul class="list-unstyled col-md-3">
                                    <li>
                                       <h6><?php echo $categoryname;?></h6>
                                    </li>
                                    <?php if ($subcatdata){
                                    foreach ($subcatdata as $subcat){
                                    	$subcatid = $subcat['sub_category_id'];
                                    	$subcatname = $subcat['sub_category_name'];
                             ?>
                                    <li>
                                       <a class="dropdown-item" href="<?php echo base_url()?>Home/productList/<?php echo $subcatid;?>"><?php echo $subcatname;?></a>
                                    </li>
                                    <?php  }}?>
                                 </ul>
                                 <?php }
                              }?>
                              </div>
                           </div>
                            </li>
                           <!--  <li class="nav-item <?php echo $benifits;?>">
                           <a class="nav-link" href="<?php echo base_url()?>Home/benifits">Your Benifits</a>
                        </li> -->
                         <!--   <li class="nav-item <?php echo $about;?>">
                           <a class="nav-link" href="<?php echo base_url()?>Home/aboutus">About us</a>
                        </li> -->
                          <!--  <li class="nav-item <?php echo $career;?>">
                     <a class="nav-link " href="<?php echo base_url()?>Home/career">Career</a>
                     </li> -->
                  </div>
                 
                  </ul>
                  <form class="form-inline my-2 my-lg-0 search" action="<?php echo base_url()?>search" method="get">
                     <a class=" search-icon" ><i class="fa fa-search"></i> </a>
                     <input name="keyword" class="biginput" type="text" id="txtinput" placeholder="Search for a product">
                     <!-- <div id="myDropdown" class="dropdown-content">
                        <a href="#home">Home</a>
                        <a href="#about">About</a>
                        <a href="#contact">Contact</a>
                     </div> -->
                      <div id="outputbox">
        
      </div>
                     </form>
                      
                     <!-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
                     <?php 
                     if ($sessionUserId) {
                        ?>
                        <a class="" style="color: #0C4164; margin-top: 1px;" href="<?php echo base_url()?>Home/my_cart"><i class="fa fa-rupee"></i>
                        <input type="hidden" id="cart_amount" value="<?php echo $total_price;?>"/>
                         <span id="total_amount"><?php echo $total_price;?></span> (<span id="cartcount"><?php echo $cartcount;?></span> items) <i class="fa fa-shopping-basket"></i> <br> </a>
                      
                        <?php } else {?>
                          <a class="" style="color: #0C4164; margin-top: 1px;" href="#loginpop" data-toggle="modal"><i class="fa fa-rupee"></i>
                         <span><?php echo number_format(0,2);?></span> (<span>0</span> items) <i class="fa fa-shopping-basket"></i><br></a>
                         <?php }?>
                       <!--<div class="dropdown-menu" aria-labelledby="">
                           <div class="col-md-12" >
                              <ul class="list-unstyled" style="float: left; width: 100%;">
                                 <li>
                                    <a>
                                       <img src="img/big.png">
                                       <h6>Lorem ipsum dolor sit</h6>
                                       <small>2 x <i class="fa fa-rupee"></i> 30500</small>
                                    </a>
                                 </li>
                                 <li>
                                    <a>
                                       <img src="img/big.png">
                                       <h6>Lorem ipsum dolor sit</h6>
                                       <small>2 x <i class="fa fa-rupee"></i> 30500</small>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <div class="col-md-12" style="float: left; width: 100%;">
                              <a href="cart.html" class="btn-success btn-small">Cart</a> <a href="partner-savings.html" class="btn-danger btn-small">Calculate my savings</a> <span style="float: right;">Total : <i class="fa fa-rupee"></i> 30500</span>
                           </div>
                        </div> -->
                   
                  
               </div>
         </div>
         </nav>
         </div>
      </header>