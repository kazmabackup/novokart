<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
</head>
<body>
	<h1 align="center">Invoice</h1>
<table border="1" width="100%" cellpadding="7">
	<tr>
		<td>
			<img src="logo.png" width="200px;">
			<br>
			<div style="float: left; width: 100%; width: 100%; padding: 4px 0;">
				<b>Name:</b> Lorem ipsum dolor
			</div>
			<div style="float: left; width: 100%; width: 100%;padding: 4px 0;">
				<b>Address: London, Glasgow</b>
			</div>
			<div style="float: left; width: 100%; width: 100%;padding: 4px 0;">
				<b>GSTIN/UIN: 4458798</b>
			</div>
			<div style="float: left; width: 100%; width: 100%;padding: 4px 0;">
				<b>State Name: Delhi </b>
			</div>
			<div style="float: left; width: 100%; width: 100%;padding: 4px 0;">
				<b>Code: ed7741</b>
			</div>
			<div style="float: left; width: 100%; width: 100%;padding: 4px 0;">
				<b>CIN: efiohoi</b>
			</div>
			<div style="float: left; width: 100%; width: 100%;padding: 4px 0;">
				<b>E-Mail: abcd@gmail.com</b>
			</div>
			<div style="float: left; width: 100%; width: 100%;padding: 4px 0;">
				<b>(Under Composition Scheme)</b>
			</div>	
		</td>
		<td>
			Invoice No:  13546
			<hr>
			<b>Order No.: 4668</b>
		</td>
		<td>
			Mode/Terms of Payment:
			<br>
			Cash on Delivery
			<hr>
			<b>Dated: 31/12/18</b>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<b>Customer Details:</b> <span>
				 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat.
			</span>
			<br>
			Name: John Wick
			<br>
			Address: London, Glasgow
		</td>
		
	</tr>
</table>
<table width="100%" border="1" cellpadding="7">
	<thead><tr>
		<th>
			Sl No.
		</th>
		<th>Description of Goods</th>
		<th>Quantity</th>
		<th>Rate</th>
		<th>Amount</th>
	</tr></thead>
	<tbody>
		<tr>
			<td>1</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td align="right">Total</td>
			<td align="center">2</td>
			<td align="center">50</td>
			<td align="center">100</td>
		</tr>
	</tfoot>
</table>
<h4>Amount Chargeable (in words)</h4>
<p>One hundred rupees.</p>
</body>
</html>