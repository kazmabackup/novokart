
<html class="fa-events-icons-ready">
<?php include 'head.php';?>
   <body>
     <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="#"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="#">Library</a></li>
               <li class="breadcrumb-item active" aria-current="page">Data</li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="prof pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1>Payment overview</h1>
                  <br>
<h4>Select your payment mode</h4>
  <div class="radio">
  <input type="radio" id="radio1" name="mode" value="cod" checked>
  <label for="radio1">COD</label>
</div>
  <div class="radio">
  <input type="radio" id="radio2" name="mode" value="online">
  <label for="radio2">Online</label>
</div>

<div class="row">
  <div class="col-md-6 ">
    <article>
    <p style="margin: 0;"><i class="fa fa-location-arrow" aria-hidden="true"></i> <b>Delivery location :</b> <?php if ($addressData){?>  <textarea id="address" name="address" rows="" cols="" readonly><?php echo $addressData[0]['address'];?></textarea> <?php } else{?>
      <textarea id="address" name="address" rows="" cols=""></textarea><a style="float: right;color: #1CA5AA;line-height: 1.5;font-size: 20px;" class="fa fa-plus-circle">
       <span class="addressErr error" style="color: red;"></span>
    <?php }?></a></p>
</article>
  </div>
</div>
                  <a href="#" data-toggle="modal" onclick = "return pay();"class="btn btn-success">Pay now</a>
                 
                        <div class="costumModal">
                           <!-- <a href="#costumModal9" role="button" class="btn btn-default" data-toggle="modal">
                              expandIn
                              </a> -->
                           <div id="slot" class="modal slot" data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- <img class="white-logo" src="img/logo-white.png"> -->
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                       ×
                                       </button>
                                       <h4 class="modal-title" style="margin: 0 auto;">
                                         Choose your delivery time
                                       </h4>
                                    </div>
                                    <div class="modal-body">
                                      <p >Your prefered slot time</p>
                                      <ul class="list-unstyled">
                                       <?php if ($slotData){
                                         foreach ($slotData as $slot){
                                        	$slotid = $slot['slot_id'];
                                        	$slot_start = $slot['start_time'];
                                        	$slot_end = $slot['end_time'];
                                        ?>
                                        <li style="border: none;">
                                           <div class="radio">
                                            <input type="radio" id="slot<?php echo $slotid;?>" name="alot" value="<?php echo $slotid;?>">
                                            <label for="slot<?php echo $slotid;?>"><?php echo $slot_start;?> to <?php echo $slot_end;?></label>
                                          </div>
                                        </li>
                                        <?php }}?>
                                        <span class="slotErr error" style="color: red;"></span>
                                        <span class="slotsuccessErr error" style="color: green;"></span>
                                      </ul>
                                      
                                       <a  href = "<?php base_url()?>Home/confirm_order" class="btn btn-primary" onclick="return order_confirm();">
                                       Ok
                                       </a>
                                    </div>
                                    <!-- <div class="modal-footer">
                                       <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                                          Login
                                          </button>
                                    </div> -->
                                 </div>
                              </div>
                           </div>
                        </div>
               </div>
            </div>
           </div>
        </section>
      </main>
      <span class="clearfix"></span>
      <?php include 'footer.php';?>
   

</body></html>
<script>
function pay(){
	var address = $("#address").val();
	  $("#addressErr").hide();
	  $("#addressErr").show();
	  if(address == ""){
    	   $(".addressErr").slideDown('slow');
          $(".addressErr").html("Address is required.");
          $("#address").focus();
          return false;
          } else {
              $("#slot").modal("show");
          }
}

 </script>
 <script>
function order_confirm(){
	 $("#slotErr").hide();
	  $("#slotErr").show();
	 if($("input:radio[name='alot']").is(":checked")) {
	     var payment_mode = $("input[name='mode']:checked").val();
	     var slot = $("input[name='alot']:checked").val();
	     var address = $("#address").val();
	     var ctrlUrl = "<?php echo base_url().'Home/confirm_order';?>";
			var adminRedirectUrl="<?php echo base_url().'Home/order_history'?>"; 
			$.ajax({
				type: "POST",
				url: ctrlUrl,
				data:({
					payment_mode : payment_mode,
					slot : slot,
					address : address
				}),
				cache: false,
				success: function(data)
				{ //alert(data);
			     if(data==1){
			    	 $(".slotsuccessErr").slideDown('slow');
			          $(".slotsuccessErr").html("Order has been placed sucessfully.");
			    	 var delay = 1000; 
    					setTimeout(function(){ window.location = adminRedirectUrl; }, delay);
				     } else if(data=="dup"){
				    	 $(".slotErr").slideDown('slow');
				          $(".slotErr").html("you have already order in pending.");
				           return false;
					     } else {
				    	 $(".slotErr").slideDown('slow');
				          $(".slotErr").html("please try again.");
				           return false;
					     }
				}
			});
	     return false;
	  } else {
		  $(".slotErr").slideDown('slow');
          $(".slotErr").html("please select any one slot.");
           return false;
		  }
}

 </script>