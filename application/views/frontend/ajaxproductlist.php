 <div class="row">
                        <?php if ($productlistdata){
                        foreach ($productlistdata as $product){
                        	$productid = $product['product_id'];
                        	$productname = $product['product_name'];
                        	$image = $product['image'];
                        	$actualprice = $product['actual_price'];
                        	$discountprice = $product['discount_price'];
                        ?>
                           <div class="col-md-4 col-xs-12">
                              <a href="<?php echo base_url()?>Home/productDetails/<?php echo $productid;?>">
                                 <span class="tag">New</span>
                                 <img style="width: 100%;" src="<?php echo base_url()?>uploads/product/<?php echo $image;?>">
                                 <h5><?php echo $productname;?></h5>
                                   <input type="hidden" id="dis_price<?php echo $productid;?>" value="<?php echo $discountprice;?>"/>
                                 <span class="rating"><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                 <h6><i class="fa fa-rupee"></i> <?php echo $discountprice;?> <strike><i class="fa fa-rupee"></i><?php echo $actualprice;?></strike></h6>
                              <a onclick="return add_to_cart(<?php echo $productid;?>,1);" class="btn btn-success"><i class="fa fa-shopping-basket"></i> Add to cart</a>
                              <a class="buy" onclick="return buy_now(<?php echo $productid;?>,1);">Buy now</a>
                              </a>
                           </div>
                          <?php }
                        } else {?>
                        <div class="col-md-4 col-xs-12">
                             <span>No product found</span>
                           </div>
                        <?php }?>
                        </div>