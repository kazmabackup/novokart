<div class="modal-dialog" style="max-width:775px;">
                                 <div class="modal-content" >
                                    <!-- <img class="white-logo" src="img/logo-white.png"> -->
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                       ×
                                       </button>
                                       <h4 class="modal-title">
                                          Order details
                                       </h4>
                                    </div>
                                    <div class="modal-body" style="float:left;width:100%;">
           
                 <table class="cart">
                    <thead>
                       <tr>
                          <th>Product name</th>
                          <th>Product Image</th>
                          <th style="text-align: center;">Quantity</th>
                          <th>Price</th>
                         
                       </tr>
                    </thead>
                    <tbody>
                    <?php if ($orderdata){
                    foreach ($orderdata as $order){
                    	$productid = $order['pro_id'];
                    	$wherepro="product_id='$productid'";
                    	$productdata = $this->Homemodel->getDataById ( 'product', $wherepro );
                    	$productname = $productdata[0]['product_name'];
                    	$productimage = $productdata[0]['image'];
                    	$price = $order['pro_price'];
                    	$order_quantity = $order['order_quantity'];
                    ?>
                       <tr>
                          <td><div><?php echo $productname;?></div>
                          </td>
                           <td><img src="<?php echo base_url()?>uploads/product/<?php echo $productimage;?>"> 
                          </td>
                          <td>
                             <?php echo $order_quantity;?>
                          </td>
                          <td>
                             <i class="fa fa-rupee"></i> <?php echo $price;?>
                          </td>
                          
                       </tr>
                      <?php }
                    }?>
                    </tbody>
                 </table>
               </div>
               <div class="modal-footer">
                                      
                </div>
                </div>
               </div>