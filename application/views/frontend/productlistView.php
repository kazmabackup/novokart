
<!DOCTYPE html>
<html>
  <?php include 'head.php';?>
   <body>
      <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/productList">Product list</a></li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main >
         <section class="category-list inner">
            <div class="container">
               <div class="row">
                  <div class="col-md-3">
                     <h4>FILTER BY PRICE</h4>
                     <div class="wrapper" style="padding:30px 5px;">
                        <div class="range-slider">
                        <input type="text" id="amount" readonly style="border:0; color:#1CA5AA; font-weight:bold;">
                        <div id="slider-range"></div>
                           <!-- <input type="text" class="js-range-slider" value="" onchange="return filterpricerange();"/> -->
                        </div>
                        <!-- <hr> -->
                        <!-- <div class="extra-controls form-inline">
                           <div class="form-group">
                             <input type="text" class="js-input-from form-control" value="0" />
                             <input type="text" class="js-input-to form-control" value="0" />
                           </div>
                           </div> -->
                     </div>
                     <div class="panel">
                        <h4>CATEGORIES</h4>
                        <ul class="list-unstyled">
                        <?php if ($categoryData){
                        foreach ($categoryData as $category){
                        	$categoryid = $category['category_id'];
                        	$categoryname = $category['category_name'];
                        	$wherecat = "category_id=$categoryid";
                        	$productdata = $this->Homemodel->getDataById ( 'product', $wherecat );
                        	if (!empty($productdata)){
                        	$productcount = count($productdata);
                        	} else {
                        		$productcount = 0;
                        	}
                        ?>
                           <li>
                              <a href="<?php echo base_url()?>Home/filterproduct/<?php echo $categoryid;?>"><i class="fa fa-long-arrow-right"></i> <span><?php echo $categoryname;?></span> <strong>(<?php echo $productcount;?>)</strong></a>
                           </li>
                          <?php }
                        }?>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-9">
                     
                     <div class="select" style="margin-left: 15px;float: right;">
                        <select name="slct" id="slct2" onchange="return sortbyprice();">
                           <option>Sort by</option>
                           <option value="1">Low to High</option>
                           <option value="2">High to Low</option>
                        </select>
                     </div>
                     <span class="clearfix"></span>
                     <br>
                     <section class="product-grid">
                        <div class="row" id ="list">
                        <?php if ($productlistdata){
                        foreach ($productlistdata as $product){
                        	$productid = $product['product_id'];
                        	$productname = $product['product_name'];
                        	$image = $product['image'];
                        	$actualprice = $product['actual_price'];
                        	$discountprice = $product['discount_price'];
                        ?>
                           <div class="col-md-4 col-xs-12">
                              <a href="<?php echo base_url()?>Home/productDetails/<?php echo $productid;?>">
                                 <span class="tag">New</span>
                                 <img style="width: 100%;" src="<?php echo base_url()?>uploads/product/<?php echo $image;?>">
                                 <h5><?php echo $productname;?></h5>
                                   <input type="hidden" id="dis_price<?php echo $productid;?>" value="<?php echo $discountprice;?>"/>
                                 <span class="rating"><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                 <?php $sessionUserId = $this->session->userdata('userData');
                        if ($sessionUserId) {
                        ?>
                                 <h6><i class="fa fa-rupee"></i> <?php echo $discountprice;?> <strike><i class="fa fa-rupee"></i><?php echo $actualprice;?></strike></h6>
                             <?php }?>
                              <a onclick="return add_to_cart(<?php echo $productid;?>,1);" class="btn btn-success"><i class="fa fa-shopping-basket"></i> Add to cart</a>
                              <a class="buy" onclick="return buy_now(<?php echo $productid;?>,1);">Buy now</a>
                              </a>
                           </div>
                          <?php }
                        } else {?>
                        <div class="col-md-4 col-xs-12">
                             <span>No product found</span>
                           </div>
                        <?php }?>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
            </div>
         </section>
      </main>
      <span class="clearfix"></span>
     <?php include 'footer.php';?>
   </body>
</html>
 
