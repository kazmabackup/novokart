<!DOCTYPE html>
<html>
  <?php include 'head.php';?>
   <body>
       <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item active" aria-current="page">Delivery Information</li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main >
         <section class="pro  inner inner">
              <div class="container">
                 <h1>
                   <?php echo $deliverinfodata[0]['title'];?>
                 </h1>
                 <article>
                  <?php echo $deliverinfodata[0]['description'];?>
               </article>
              </div>
         </section>
      </main>
      <span class="clearfix"></span>
     <?php include 'footer.php';?>
   </body>
</html>
