 <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" href="<?php echo base_url()?>assets/frontend/img/fav.png" type="image/png" sizes="16x16">
      <title>Novokart</title>
      <!-- ALL CSS -->
      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/css/bootstrap.css">
       <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/css/owl.carousel.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/css/owl.theme.default.min.css">
      
      <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/css/layout.css">
      <!-- ALL END -->
      <link href="https://use.fontawesome.com/b4b0414965.css" media="all" rel="stylesheet"></head>
   </head>