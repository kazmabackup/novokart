<!DOCTYPE html>
<html>
  <?php include 'head.php';?>
   <body>
      <?php include 'header.php';?>
      <section class="hero">
        <!--  <div class="banner" style="background-image: url(<?php echo base_url()?>uploads/banner/<?php echo $bannerData[0]['web_image'];?>);">
            <div class="banner-caption">
               <h1><?php echo $bannerData[0]['title'];?></h1>
               <p><?php echo $bannerData[0]['description'];?></p>
            </div>
         </div> -->
         <div id="owl1" class="owl-carousel owl-theme">
         <?php if ($bannerData){
         foreach ($bannerData as $banner){
         	$title = $banner['title'];
         	$description = $banner['description'];
         	$image = $banner['web_image'];
         ?>
  <div class="item banner" style="background-image: url(<?php echo base_url()?>uploads/banner/<?php echo $image;?>);">
     <div class="banner-caption">
               <h1><?php echo $title;?></h1>
               <p><?php echo $description;?></p>
               <!-- <a class="btn">View </a> -->
            </div>
  </div>
  <?php }
         }?>
</div>
      </section>
      <span class="clearfix"></span>
      <main >
         <section class="home1">
            <div class="container">
               <article class="head-centre">
                  <h3>Product category</h3>
                  <!-- <p><?php echo $featurecontentdata[0]['description'];?> -->
                 
               </article>
              <!--  <div class="row">
               <?php if ($featureproductdata){
               foreach ($featureproductdata as $feature){
               	$name = $feature['feature_collection_name'];
               	$image = $feature['image'];
              ?>
                  <div class="col-md-4 col-xs-12">
                     <a >
                        <h5><?php echo $name;?></h5>
                        <img src="<?php echo base_url()?>uploads/feature_collection/<?php echo $image;?>">
                     </a>
                  </div>
                  <?php  }
               }?>
               </div> -->
               <div id="owl3" class="owl-carousel owl-theme">
                <?php if ($categoryData){
                	$i=0;
               foreach ($categoryData as $category){
$cid = $category['category_id'];
               	$name = $category['category_name'];
               	$image = $category['image'];
               	if ($i==0){
               	?>
                  <div class="row item">
                  <?php }?>
                  <div class="col-md-4 col-xs-12">
                     <a href="<?php echo base_url()?>Home/filterproduct/<?php echo $cid;?>">
                        <h5><?php echo $name;?></h5>
                        <img src="<?php echo base_url()?>uploads/category/<?php echo $image;?>">
                     </a>
                  </div>
               
           <?php $i++;if ($i==3){$i=0?></div><?php }}}?>
            </div>
            </div>
         </section>
         <span class="clearfix"></span>
         <section class="product-grid home">
            <div class="container">
               <article class="head-centre">
                  <h3>PRODUCT YOU MAY LIKE</h3>
               </article>
               <div class="row">
               <?php if ($newproductdata){
               foreach ($newproductdata as $newproduct){
               	$productid = $newproduct['product_id'];
               	$product_name = $newproduct['product_name'];
               	$image = $newproduct['image'];
               	$actual_price = $newproduct['actual_price'];
               	$discount_price = $newproduct['discount_price'];
              ?>
                  <div class="col-md-2 col-xs-12">
                     <a href="<?php echo base_url()?>Home/productDetails/<?php echo $productid;
                     ?>">
                        <span class="tag">New</span>
                        <img style="width: 100%;" src="<?php echo base_url()?>uploads/product/<?php echo $image;?>">
                        <h5> <?php echo $product_name;?></h5>
                        <span class="rating"><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                       <input type="hidden" id="dis_price<?php echo $productid?>" value="<?php echo $discount_price;?>"/>
                        <?php $sessionUserId = $this->session->userdata('userData');
                        if ($sessionUserId) {
                        ?>
                        <h6><i class="fa fa-rupee"></i> <?php echo $discount_price;?> <strike><i class="fa fa-rupee"></i><?php echo $actual_price;?></strike></h6>
                     <?php }?>
                     <a class="btn btn-success" onclick="return add_to_cart(<?php echo $productid;?>,1);"><i class="fa fa-shopping-basket"></i> Add to cart</a>
                     <a class="buy" onclick="return buy_now(<?php echo $productid;?>,1);">Buy now</a>
                     </a>
                  </div>
                <!--   <div class="col-md-2 col-xs-12">
                     <a href="<?php echo base_url()?>Home/productDetails/<?php echo $productid;
                     ?>">
                        <span class="tag">New</span>
                        <img style="width: 100%;" src="<?php echo base_url()?>uploads/product/<?php echo $image;?>">
                        <h5> <?php echo $product_name;?></h5>
                        <span class="rating"><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star rated"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                       <input type="hidden" id="dis_price<?php echo $productid?>" value="<?php echo $discount_price;?>"/>
                        <?php $sessionUserId = $this->session->userdata('userData');
                        if ($sessionUserId) {
                        ?>
                        <h6><i class="fa fa-rupee"></i> <?php echo $discount_price;?> <strike><i class="fa fa-rupee"></i><?php echo $actual_price;?></strike></h6>
                     <?php }?>
                     <a class="btn btn-success" onclick="return add_to_cart(<?php echo $productid;?>,1);"><i class="fa fa-shopping-basket"></i> Add to cart</a>
                     <a class="buy" onclick="return buy_now(<?php echo $productid;?>,1);">Buy now</a>
                     </a>
                  </div> -->
                 <?php }}?>

               </div>
                <span class="clearfix"></span>
               <!-- <a class="btn btn-success" style="background: #000;" href="<?php echo base_url()?>Home/productList">View all <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> -->
               
            </div>
         </section>
         <span class="clearfix"></span>
         <section class="news-letter">
            <div class="overlay"></div>
            <div class="container">
               <div class="row">
                  <div class="col-md-6" align="centre">
                     <h2><i class="fa fa-paper-plane-o" style="margin-right: 15px;"></i>Sign Up for a Newsletter</h2>
                  </div>
                  <div class="col-md-6">
                     <form class="form-inline qbstp-header-subscribe">
                        <input type="text" class="form-control" id="email" placeholder="Enter your email">
                        <button type="submit" class="btn btn-primary">Subscribe</button>
                     </form>
                  </div>
               </div>
            </div>
         </section>
         <span class="clearfix"></span>
      </main>
      <span class="clearfix"></span>
      
     
      <?php include 'footer.php';?>
   </body>
</html>
