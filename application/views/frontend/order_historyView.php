
<html class="fa-events-icons-ready">
<?php include 'head.php'?>
   <body>
     <?php include 'header.php';?>
      <span class="clearfix"></span>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <div class="container">
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>"> <i class="fa fa-home"></i>Home</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home/order_history">Order history</a></li>
            </div>
         </ol>
      </nav>
      <span class="clearfix"></span>
      <main>
        <section class="prof pro inner">
           <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1>Order History</h1>
                  <br>
                 <table class="cart">
                    <thead>
                       <tr>
                          <th>Product details</th>
                          <th style="text-align: center;">Quantity</th>
                          <th>Total Price</th>
                           <th>Action</th>
                       </tr>
                    </thead>
                    <tbody>
                    <?php if ($historyData){
                    	$marginData = $this->Homemodel->getTableData("company_margin");
                    	$company_margin = $marginData[0]['company_margin'];
                    foreach ($historyData as $order){
                    	$orderid = $order['unique_order_id'];
                    	$payment_mode = $order['order_mode'];
                    	$order_status = $order['order_status'];
                    	$is_add = $order['is_add'];
                    	$affiliate_percentage = $order['dis_by_agent'];
                    	if ($order_status==""){
                    		$staus = "Pending";
                    	}
                    	else {
                    		$staus = $order_status;
                    	}
                    	$whereunique="unique_order_id='$orderid'";
                    	$historyData1 = $this->Homemodel->getDataById ( 'manage_order', $whereunique );
                    	$userid = $historyData1[0]['user_id'];
                    	$whereuser = "user_id='$userid'";
                    	$userData = $this->Homemodel->getDataById ( 'user', $whereuser );
                    	$affiliateid = $userData[0]['agent_id'];
                    	$algorithm = $userData[0]['algorithm'];
                    	$affdis = $userData[0]['package_percentage'];
                    	$whereaffiliate = "affiliate_id='$affiliateid'";
                    	$affiliateData = $this->Homemodel->getDataById ( 'affiliate', $whereaffiliate );
                    	//$affdis = $affiliateData[0]['discount'];
                    	if ($historyData1) {
                    		$total_discount_price=0;
                    		$total_cost_price =0;
                    		foreach ( $historyData1 as $history1 ) {
                    			$pro_price2 = $history1['pro_price'];
                    			$pro_cost_price2 = $history1['pro_cost_price'];
                    			$quan = $history1['order_quantity'];
                    			$total_discount_price = $total_discount_price + ($pro_price2 * $quan);
                    			$total_cost_price = $total_cost_price + ($pro_cost_price2 * $quan);
                    		}
                    		if ($is_add=="1"){
                    			$hostital_dis = 0;
                    		$hostital_dis_per = 0;
                    		$affiliate_earning = 0;
                    		$total_bill_price = 0;
                    		$commulative_per=0;
                    		} else {
                    		$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
                    		$company_profit = ($company_margin/100);
                    		if ($algorithm==1){
                    			$package_per = (($commulative_per/100)*1)-($affdis/100);
                    			if ($package_per > $company_profit){
                    				$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100;
                    			}else {
                    				$new_total_dis = (($total_discount_price - $total_cost_price)*$affdis/100)-$company_margin/100;
                    			}
                    		} else {
                    			$package_per = ($commulative_per - $company_margin);
                    			if ($package_per > $affdis){
                    				$new_total_dis = ($total_discount_price)*$affdis/100;
                    			}
                    		}
                    		//echo $new_total_dis;die;
                    		
                    		$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
                    		$hostital_dis_per = ($hostital_dis*$total_discount_price/100);
                    		$affiliate_earning = $new_total_dis - $hostital_dis;
                    		$total_bill_price = $total_discount_price - $hostital_dis;
                    		}
                    		
                    	}
                    	$quandata1 = $this->Homemodel->invoicequantity ($orderid );
                    	$quantity = $quandata1[0] ['quantity'];
                   ?>
                       <tr>
                          <td><div><?php echo $orderid;?></div>
                            <div><b>Payment method</b> :  <?php echo $payment_mode;?></div>
                            <div><b>Status : </b> <span style="color: green;"><?php echo $staus;?></span></div>
                          </td>
                          <td>
                             <?php echo $quantity;?>
                          </td>
                          <td>
                             <i class="fa fa-rupee"></i> <?php echo number_format($total_bill_price,2);?>
                          </td>
                          <td>
                             <a style="color: #1CA5AA;" href="#" onclick = " return order_details('<?php echo $orderid;?>');"data-toggle="modal">Order details</a>
                          </td>
                       </tr>
                      
                      <?php  }
                    } else {?>
                    
                    <?php }?>
                    </tbody>

                 </table>
               </div>
               
            </div>
              
           </div>
        </section>
      </main>
         <div id="orderdetails" class="modal"  data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                              
                           </div>
      <span class="clearfix"></span>
     <?php include 'footer.php';?>
   

</body></html>
<script>
function order_details(orderid){
	var ctrlUrl = "<?php echo base_url().'Home/order_details';?>";
	$.ajax({
		type: "POST",
		url: ctrlUrl,
		data:({
			orderid : orderid
		}),
		cache: false,
		success: function(data)
		{ //alert(data);
	      $("#orderdetails").html(data);
	      $('#orderdetails').modal('show');
		}
	});
}
                    </script>