<?php
include("class.phpmailer.php");
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
function mailer($from,$to,$subject,$content,$attachment = "",$cc = "",$bcc = ""){
       
$mail             = new PHPMailer();
$img = "<?php echo base_url();?>assets/admin/images/logo.png";
$body = '
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>COOLBRAINS</title>

    <style type="text/css">
      .social {
        text-align: center;
      }
      .social tbody tr td a {
        background: #1F1F20;
        color: #fff;
        padding: 0;
        display: inline-block;
        height: 34px;
        border-radius: 50%;
        width: 34px;
        text-align: center;
        margin: 0 2px;
      }
      .social tbody tr td a i {
        line-height: 2.4;
        font-size: 14px;
      }
      @import url(https://fonts.googleapis.com/css?family=Roboto:300,300italic);
      body, #bodyTable, #bodyCell {
        height: 100% !important;
        margin: 0;
        padding: 0;
        width: 100% !important;
      }
      table {
        border-collapse: collapse;
      }
      img, a img {
        border: 0;
        outline: none;
        text-decoration: none;
      }
      h1, h2, h3, h4, h5, h6 {
        margin: 0;
        padding: 0;
      }
      p {
        margin: 1em 0;
        padding: 0;
      }
      a {
        word-wrap: break-word;
      }
      .ReadMsgBody {
        width: 100%;
      }
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%;
      }
      table, td {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      #outlook a {
        padding: 0;
      }
      img {
        -ms-interpolation-mode: bicubic;
      }
      body, table, td, p, a, li, blockquote {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }
      #bodyCell {
        padding: 20px;
      }
      .themezyImage {
        vertical-align: bottom;
      }
      .themezyTextContent img {
        height: auto !important;
      }
      body, #bodyTable {
        background-color: #1F1F20;
      }
      #bodyCell {
        border-top: 0;
      }
      #templateContainer {
        border: 0;
      }
      h1 {
        color: #ffffff !important;
        display: block;
        font-family: Arial, Helvetica;
        font-size: 40px;
        font-style: normal;
        font-weight: normal;
        line-height: 100%;
        letter-spacing: normal;
        margin: 0;
        text-align: center;
      }
      h2 {
        color: #D5AF46 !important;
        display: block;
        font-family: Arial, Helvetica;
        font-size: 30px;
        font-style: normal;
        font-weight: normal;
        line-height: 100%;
        letter-spacing: normal;
        margin: 0;
        text-align: center;
      }
      h3 {
        color: #78777d !important;
        display: block;
        font-family: Arial, Helvetica;
        font-size: 18px;
        font-style: normal;
        font-weight: normal;
        line-height: 125%;
        letter-spacing: normal;
        margin: 0;
        text-align: center;
      }
      h4 {
        color: #78777d !important;
        display: block;
        font-family: Arial, Helvetica;
        font-size: 16px;
        font-style: normal;
        font-weight: bold;
        line-height: 125%;
        letter-spacing: normal;
        margin: 0;
        text-align: left;
      }
      h1, h2 {
        font-family: Roboto, Arial, Helvetica;
        font-weight: 300;
      }
      #templatePreheader {
        background-color: #1F1F20;
        border-top: 0;
        border-bottom: 0;
      }
      .preheaderContainer .themezyTextContent, .preheaderContainer .themezyTextContent p {
        color: #606060;
        font-family: Arial, Helvetica;
        font-size: 11px;
        line-height: 125%;
        text-align: left;
      }
      .preheaderContainer .themezyTextContent a {
        color: #606060;
        font-weight: normal;
        text-decoration: underline;
      }
      #templateHeader {
        background-color: #1F1F20;
        border-top: 0;
        border-bottom: 0;
      }
      .headerContainer .themezyTextContent, .headerContainer .themezyTextContent p {
        color: #606060;
        font-family: Arial, Helvetica;
        font-size: 15px;
        line-height: 150%;
        text-align: left;
      }
      .headerContainer .themezyTextContent a {
        color: #6DC6DD;
        font-weight: normal;
        text-decoration: underline;
      }
      #templateBody {
        background-color: #ffffff;
        border-top: 0;
        border-bottom: 0;
      }
      .bodyContainer .themezyTextContent, .bodyContainer .themezyTextContent p {
        color: #606060;
        font-family: Arial, Helvetica;
        font-size: 15px;
        line-height: 150%;
        text-align: left;
      }
      .bodyContainer .themezyTextContent a {
        color: #D5AF46;
        font-weight: normal;
        text-decoration: none;
      }
      #templateFooter {
        background-color: #1F1F20;
        border-top: 0;
        border-bottom: 0;
      }
      .footerContainer .themezyTextContent, .footerContainer .themezyTextContent p {
        color: #8f8f95;
        font-family: Arial, Helvetica;
        font-size: 11px;
        line-height: 150%;
        text-align: center;
      }
      .footerContainer .themezyTextContent a {
        color: #8f8f95;
        font-weight: normal;
        text-decoration: underline;
      }
      @media only screen and (max-width: 480px) {
        body, table, td, p, a, li, blockquote {
          -webkit-text-size-adjust: none !important;
        }
        body {
          width: 100% !important;
          min-width: 100% !important;
        }
        td[id=bodyCell] {
          padding: 10px !important;
        }
        table[class=themezyTextContentContainer] {
          width: 100% !important;
        }
        table[class=themezyBoxedTextContentContainer] {
          width: 100% !important;
        }
        table[class=mcpreview-image-uploader] {
          width: 100% !important;
          display: none !important;
        }
        img[class=themezyImage] {
          width: 100% !important;
        }
        table[class=themezyImageGroupContentContainer] {
          width: 100% !important;
        }
        td[class=themezyImageGroupContent] {
          padding: 9px !important;
        }
        td[class=themezyImageGroupBlockInner] {
          padding-bottom: 0 !important;
          padding-top: 0 !important;
        }
        tbody[class=themezyImageGroupBlockOuter] {
          padding-bottom: 9px !important;
          padding-top: 9px !important;
        }
        table[class=themezyCaptionTopContent], table[class=themezyCaptionBottomContent] {
          width: 100% !important;
        }
        table[class=themezyCaptionLeftTextContentContainer], table[class=themezyCaptionRightTextContentContainer], table[class=themezyCaptionLeftImageContentContainer], table[class=themezyCaptionRightImageContentContainer], table[class=themezyImageCardLeftTextContentContainer], table[class=themezyImageCardRightTextContentContainer] {
          width: 100% !important;
        }
        td[class=themezyImageCardLeftImageContent], td[class=themezyImageCardRightImageContent] {
          padding-right: 18px !important;
          padding-left: 18px !important;
          padding-bottom: 0 !important;
        }
        td[class=themezyImageCardBottomImageContent] {
          padding-bottom: 9px !important;
        }
        td[class=themezyImageCardTopImageContent] {
          padding-top: 18px !important;
        }
        td[class=themezyImageCardLeftImageContent], td[class=themezyImageCardRightImageContent] {
          padding-right: 18px !important;
          padding-left: 18px !important;
          padding-bottom: 0 !important;
        }
        td[class=themezyImageCardBottomImageContent] {
          padding-bottom: 9px !important;
        }
        td[class=themezyImageCardTopImageContent] {
          padding-top: 18px !important;
        }
        table[class=themezyCaptionLeftContentOuter] td[class=themezyTextContent], table[class=themezyCaptionRightContentOuter] td[class=themezyTextContent] {
          padding-top: 9px !important;
        }
        td[class=themezyCaptionBlockInner] table[class=themezyCaptionTopContent]:last-child td[class=themezyTextContent] {
          padding-top: 18px !important;
        }
        td[class=themezyBoxedTextContentColumn] {
          padding-left: 18px !important;
          padding-right: 18px !important;
        }
        td[class=themezyTextContent] {
          padding-right: 18px !important;
          padding-left: 18px !important;
        }
        table[id=templateContainer], table[id=templatePreheader], table[id=templateHeader], table[id=templateBody], table[id=templateFooter] {
          max-width: 600px !important;
          width: 100% !important;
        }
        h1 {
          font-size: 24px !important;
          line-height: 125% !important;
        }
        h2 {
          font-size: 20px !important;
          line-height: 125% !important;
        }
        h3 {
          font-size: 18px !important;
          line-height: 125% !important;
        }
        h4 {
          font-size: 16px !important;
          line-height: 125% !important;
        }
        table[class=themezyBoxedTextContentContainer] td[class=themezyTextContent], td[class=themezyBoxedTextContentContainer] td[class=themezyTextContent] p {
          font-size: 18px !important;
          line-height: 125% !important;
        }
        table[id=templatePreheader] {
          display: block !important;
        }
        td[class=preheaderContainer] td[class=themezyTextContent], td[class=preheaderContainer] td[class=themezyTextContent] p {
          font-size: 14px !important;
          line-height: 115% !important;
          text-align: center !important;
        }
        td[class=headerContainer] td[class=themezyTextContent], td[class=headerContainer] td[class=themezyTextContent] p {
          font-size: 18px !important;
          line-height: 125% !important;
        }
        td[class=bodyContainer] td[class=themezyTextContent], td[class=bodyContainer] td[class=themezyTextContent] p {
          font-size: 18px !important;
          line-height: 125% !important;
        }
        td[class=footerContainer] td[class=themezyTextContent], td[class=footerContainer] td[class=themezyTextContent] p {
          font-size: 14px !important;
          line-height: 115% !important;
        }
        td[class=footerContainer] a[class=utilityLink] {
          display: block !important;
        }

      }
    </style>
  </head>
  <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    <center>
      <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tbody><tr>
          <td align="center" valign="top" id="bodyCell"><!-- BEGIN TEMPLATE // -->
          <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tbody><tr>
              <td align="center" valign="top"><!-- BEGIN PREHEADER // -->
              <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                <tbody><tr>
                  <td valign="top" class="preheaderContainer" style="padding-top:9px;"></td>
                </tr>
              </tbody></table><!-- // END PREHEADER --></td>
            </tr>
            <tr>
              <td align="center" valign="top"><!-- BEGIN HEADER // -->
              <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                <tbody><tr>
                  <td valign="top" class="headerContainer">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyImageBlock">
                    <tbody class="themezyImageBlockOuter">
                      <tr>
                        <td valign="top" style="padding:9px" class="themezyImageBlockInner">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="themezyImageContentContainer">
                          <tbody>
                            <tr>
                              <td class="themezyImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;"><a href="#"><img align="center" alt="" src="'.$img.'" width="217" style="max-width:135px; padding-bottom: 0; display: inline !important; vertical-align: bottom; background: #fff;padding: 4px; border-radius: 4px;" class="themezyImage"></a></td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                    <tbody class="themezyTextBlockOuter">
                      <tr>
                        <td valign="top" class="themezyTextBlockInner">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyButtonBlock">
                    <tbody class="themezyButtonBlockOuter">
                      <tr>
                        <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="themezyButtonBlockInner">
                        </td>
                      </tr>
                    </tbody>
                  </table></td>
                </tr>
              </tbody></table><!-- // END HEADER --></td>
            </tr>
            <tr>
              <td align="center" valign="top"><!-- BEGIN BODY // -->
              <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody" style="border-radius: 5px;">
                <tbody><tr>
                  <td valign="top" class="bodyContainer">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyDividerBlock">
                    <tbody class="themezyDividerBlockOuter">
                      <tr>
                        <td class="themezyDividerBlockInner" style="padding: 30px 18px 0px;">
                        <table class="themezyDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td><span></span></td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                    <tbody class="themezyTextBlockOuter">
                      <tr>
                        <td valign="top" class="themezyTextBlockInner">
                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                          <tbody>
                            <tr>

                              <td valign="top" class="themezyTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                              ';
                                                  $body .=$content;
                                                  $body .= '</td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                    </tbody>
                  </table>
                  
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyDividerBlock">
                    <tbody class="themezyDividerBlockOuter">
                      <tr>
                        <td class="themezyDividerBlockInner" style="padding: 30px 18px 0px;">
                        <table class="themezyDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td><span></span></td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                    </tbody>
                  </table>
                  
                  
                  
                  
                  </td>
                </tr>
              </tbody></table><!-- // END BODY --></td>
            </tr>
            <tr>
              <td align="center" valign="top"><!-- BEGIN FOOTER // -->
              <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
                <tbody><tr>
                  <td valign="top" class="footerContainer" style="padding-bottom:9px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyDividerBlock">
                    <tbody class="themezyDividerBlockOuter">
                      <tr>
                        <td class="themezyDividerBlockInner" style="padding: 15px 18px 0px;">
                        <table class="themezyDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td><span></span></td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyFollowBlock">
                    <tbody class="themezyFollowBlockOuter">
                      <tr>
                        <td align="center" valign="top" style="padding:9px" class="themezyFollowBlockInner">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyFollowContentContainer">
                          <tbody>
                            <tr>
                              <td align="center" style="padding-left:9px;padding-right:9px;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyFollowContent">
                                <tbody>
                                  <tr>
                                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;"></td>
                                  </tr>
                                </tbody>
                              </table></td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                    <tbody class="themezyTextBlockOuter">
                      <tr>
                        <td valign="top" class="themezyTextBlockInner">
                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                          <tbody>
                            <tr>

                              <td valign="top" class="themezyTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;"><span style="color:#535353">Copyright 2018 COOLBRAINS. All rights reserved.</span></td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                    </tbody>
                  </table></td>
                </tr>
              </tbody></table><!-- // END FOOTER --></td>
            </tr>
          </tbody></table><!-- // END TEMPLATE --></td>
        </tr>
      </tbody></table>
    </center>
  

</body></html>
';
$mail->IsSMTP();
//$mail->IsMail();
$mail->SMTPAuth   = true;                  // enable SMTP authentication
//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
$mail->SMTPSecure = 'ssl';

$mail->Host       = "smtp.zoho.com";      // sets GMAIL as the SMTP server
$mail->Port       = 25;                     // set the SMTP port for the GMAIL server
$mail->Username   = "info@novokart.in";  // GMAIL username
$mail->Password   = "Novo@2019";            // GMAIL password
$mail->AddReplyTo($from,"Novokart Team");

$mail->From       = "info@novokart.in";
$mail->FromName   = $from;

$mail->Subject    = $subject;

$mail->Body       = "<br>".$body."<br>";                      //HTML Body
$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
$mail->WordWrap   = 50; 



//$mail->MsgHTML($body);
$mail->AddAddress($to,"");
if(!empty($cc)) {
       $s = explode(',',$cc);
       foreach($s as $k ) {
               $mail->AddCC($k,"");
       }
}
if(!empty($bcc)) {
       $s = explode(',',$bcc);
       foreach($s as $k ) {
               $mail->AddBCC($k,"");
       }
}
//$mail->AddCC($cc,"");
if($attachment) {
       $mail->AddAttachment($attachment);
}

$mail->IsHTML(true); // send as HTML

if(!$mail->Send()) {
/* echo "Mailer Error: " . $mail->ErrorInfo;
 die;*/
       return false;
} else {
         return true;
}
}

