<?php

class Adminmodel extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function checklogin($email, $password)
    {
        $this->pdo->where("password", $password);
        $this->pdo->where("email", $email);
        $this->pdo->where("role_id", 1);
        $query = $this->pdo->get('user');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    function getDataById($table, $where)
    {
        $this->pdo->where($where);
        $query = $this->pdo->get($table);
        if ($query->num_rows() > 0) {
            $resultData = $query->result_array();
        } else {
            $resultData = false;
        }
        return $resultData;
    }
	 function getDataByuserId($table, $where)
    {
        $this->pdo->where($where);
		 $this->pdo->order_by($table . '_id', "desc");
        $query = $this->pdo->get($table);
        if ($query->num_rows() > 0) {
            $resultData = $query->result_array();
        } else {
            $resultData = false;
        }
        return $resultData;
    }

    function insert($table, $insertData)
    {
        $this->pdo->insert($table, $insertData);
       // echo $this->pdo->last_query ();die;
        return $this->pdo->insert_id();
    }

    function update($table, $data, $where)
    {
        $this->pdo->where($where);
        return $this->pdo->update($table, $data);
    }
    function updatewebbanner($table, $data)
    {
    	return $this->pdo->update($table, $data);
    }

    function delete($table, $where)
    {
        $this->pdo->where($where);
        return $this->pdo->delete($table);
    }

    function getTableData($table)
    {
        $query = $this->pdo->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
        return $resultData;
    }

    function getTableDataDesc($table) {
        $this->pdo->order_by($table . '_id', "desc");
        $query = $this->pdo->get ( $table );
        if ($query->num_rows () > 0) {
            $resultData = $query->result_array ();
        } else {
            $resultData = false;
        }
        return $resultData;
    }
    function getTableData1Desc($table) {
    	$this->pdo->order_by($table . '_id', "desc");
    	$this->pdo->group_by("unique_order_id");
    	$query = $this->pdo->get ( $table );
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
    }

   
   function menuDropdown($table,$where) {
		$query = $this->pdo->query ( "select * from $table where $where" );		
		if ($query->num_rows () > 0) {
			$catArray = $query->result_array ();
		} else {
			$catArray = "";
		}
		$catDataArray [0] = "Select $table";
		if (! empty ( $catArray )) {
			foreach ( $catArray as $category ) {
				$id = $category [$table.'_id'];
				$cName = $category [$table.'_name'];
				$catDataArray [$id] = $cName;
			}
		} else {
			$catDataArray = "";
		}
		return $catDataArray;
	}

   
 

   
}
?>