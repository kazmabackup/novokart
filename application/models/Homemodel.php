<?php

class Homemodel extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function checklogin($where)
    {
        $this->pdo->where($where);
        $query = $this->pdo->get('user');
        //echo $this->pdo->last_query ();die;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    function getDataById($table, $where)
    {
        $this->pdo->where($where);
        $query = $this->pdo->get($table);
        if ($query->num_rows() > 0) {
            $resultData = $query->result_array();
        } else {
            $resultData = false;
        }
        return $resultData;
    }
	 function getDataByuserId($table, $where)
    {
        $this->pdo->where($where);
		 $this->pdo->order_by($table . '_id', "desc");
        $query = $this->pdo->get($table);
        if ($query->num_rows() > 0) {
            $resultData = $query->result_array();
        } else {
            $resultData = false;
        }
        return $resultData;
    }

    function insert($table, $insertData)
    {
        $this->pdo->insert($table, $insertData);
        return $this->pdo->insert_id();
    }

    function update($table, $data, $where)
    {
        $this->pdo->where($where);
        return $this->pdo->update($table, $data);
    }

    function delete($table, $where)
    {
        $this->pdo->where($where);
        return $this->pdo->delete($table);
    }

    function getTableData($table)
    {
        $query = $this->pdo->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
        return $resultData;
    }

    function getTableDataDesc($table) {
        $this->pdo->order_by($table . '_id', "desc");
        $query = $this->pdo->get ( $table );
        if ($query->num_rows () > 0) {
            $resultData = $query->result_array ();
        } else {
            $resultData = false;
        }
        return $resultData;
    }

   
   function menuDropdown($table,$where) {
		$query = $this->pdo->query ( "select * from $table where $where" );		
		if ($query->num_rows () > 0) {
			$catArray = $query->result_array ();
		} else {
			$catArray = "";
		}
		$catDataArray [0] = "Select $table";
		if (! empty ( $catArray )) {
			foreach ( $catArray as $category ) {
				$id = $category [$table.'_id'];
				$cName = $category [$table.'_name'];
				$catDataArray [$id] = $cName;
			}
		} else {
			$catDataArray = "";
		}
		return $catDataArray;
	}
	function getnewProduct($table, $where)
	{
		$this->pdo->where($where);
		$this->pdo->order_by($table . '_id', "desc");
		$this->pdo->limit(10);
		$query = $this->pdo->get($table);
		//echo $this->pdo->last_query ();die;
		if ($query->num_rows() > 0) {
			$resultData = $query->result_array();
		} else {
			$resultData = false;
		}
		return $resultData;
	}
	public function textMessageIndia($messageData = NULL) {
		$mobile = $messageData [0]->number;
        $message = $messageData [0]->message;
        $msg = urlencode ( $message );
        $url = "http://cloud.smsindiahub.in/vendorsms/pushsms.aspx?user=kazma&password=9831942957&msisdn=$mobile&sid=MYQAPP&msg=" . $msg . "&fl=0&gwid=2";
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec ( $ch );
        //print_r($output);die;
        $jsondecode = json_decode ( $output );
        $responseMessage = $jsondecode->ErrorMessage;
        if ($responseMessage == "Success") {
            return "Success";
        } else {
            return "Fail";
        }
	}
 
	function invoicequantity($id) {
		$this->pdo->select('SUM(order_quantity)  as quantity', FALSE);
		$this->pdo->from ( 'manage_order' );
		$this->pdo->where ( 'unique_order_id', $id );
		$query = $this->pdo->get ();
		// $this->pdo->where('product.pro_id');
		// echo $this->pdo->last_query();die;
		if ($query->num_rows () > 0) {
			return $query->result_array ();
		} else {
			return false;
		}
	}
   
}
?>