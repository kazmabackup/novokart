<?php
defined('BASEPATH') or exit('No direct script access allowed');
include ('application/libraries/REST_Controller.php');

class Crmservices extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        // load the pdo for db connection
        $this->pdo = $this->load->database('pdo', true);
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->helper('url');
        $this->load->helper('text');
        // load the models
        $this->load->model('Webservicemodel');
        include ('application/libraries/phpmailer/sendEmail.php');
        date_default_timezone_set("Asia/Calcutta");
    }
   

    function add_category_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $data = json_decode(file_get_contents ( "php://input" ));
       
        $name = $data->name;
        $masterid =$data->master_id;
        if (! $name || ! $masterid) {
            $resArr['message'] = "Please pass all parameters";
        } else {
            $whereName = "category_name ='$name'";
            $duplicateName = $this->Webservicemodel->getDataById('category', $whereName);
            if (! empty($duplicateName)) {//echo "p";die;
                     $insertData = array (
                        'category_name' => $name,
                     	'master_category_id' => $masterid 
                );
                $where = "category_name='$name'";
                $result = $this->Webservicemodel->update ( 'category', $insertData, $where );
            }
            else {//echo "p1";die;
                $insertArr = array (
                    'category_name' => $name,
                	'master_category_id' => $masterid,
					'status' => 'Active',
                    );
                //print_r($insertArr);die;
                $result = $this->Webservicemodel->insert('category', $insertArr);
                //print_r($result);die;
             }
                if ($result) {
                    $resArr['result'] = 1;
                    $resArr['message'] = "Category have added successfully";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Category has not added";
                }
           
        }
        echo $this->response($resArr, 200);
    }
    
    function add_sub_category_post()
    {
    	$resArr['result'] = 0;
    	$resArr['message'] = "";
    	$data = json_decode(file_get_contents ( "php://input" ));
    	$subname = $data->sub_cat_name;
    	$catname = $data->cat_name;
    	$masterid = $data->master_id;
    	if (! $subname || ! $catname || ! $masterid) {
    		$resArr['message'] = "Please pass all parameters";
    	} else {
    		$wherecat = "category_name ='$catname'";
    		$categoryData = $this->Webservicemodel->getDataById('category', $wherecat);
    		$category_id = $categoryData[0]['category_id'];
    	    $whereName = "sub_category_name ='$subname' AND category_id = $category_id";
            $duplicateName = $this->Webservicemodel->getDataById('sub_category', $whereName);
            if (! empty($duplicateName)) {
               $insertData = array (
                    'sub_category_name' => $subname,
                    'category_id' => $category_id,
               		'master_sub_category_id' => $masterid 
                );
                $where = "sub_category_name='$subname'";
                $result = $this->Webservicemodel->update ( 'sub_category', $insertData, $where );
            }
    		else {
    			$insertData = array (
                    'sub_category_name' => $subname,
                    'category_id' => $category_id,
    				'master_sub_category_id' => $masterid,
					'status' => 'Active',
                 );
                $result = $this->Webservicemodel->insert ( 'sub_category', $insertData );
    		}
    			if ($result) {
    				$resArr['result'] = 1;
    				$resArr['message'] = "Sub category have added successfully";
    			} else {
    				$resArr['result'] = 0;
    				$resArr['message'] = "Sub category has not added";
    			}
    		
    	}
    	echo $this->response($resArr, 200);
    }
    function add_product_post()
    {
    	$resArr['result'] = 0;
    	$resArr['message'] = "";
    	$data = json_decode(file_get_contents ( "php://input" ));
    	$subname = $data->sub_cat_name;
    	$catname = $data->cat_name;
    	$productname = $data->product_name;
    	$productprice = $data->product_price;
    	$productdisprice = $data->product_discount_price;
    	$cost_price = $data->product_cost_price;
    	$quantity = $data->quantity;
    	$description = $data->description;
    	$masterid = $data->master_id;
    	if (! $subname || ! $catname || ! $productname || ! $productprice || ! $productdisprice || ! $cost_price || ! $quantity) {
    		$resArr['message'] = "Please pass all parameters";
    	} else {
    		 $wherecat = "category_name ='$catname'";
    		$categoryData = $this->Webservicemodel->getDataById('category', $wherecat);
    		$category_id = $categoryData[0]['category_id'];
    		 $wheresubcat = "sub_category_name ='$subname' AND category_id = $category_id";
    		$subcatData = $this->Webservicemodel->getDataById('sub_category', $wheresubcat);
    		 //print_r($subcatData);die;
    		$subcatid = $subcatData[0]['sub_category_id'];
    		$wherename = "product_name='$productname' AND category_id=$category_id AND sub_category_id=$subcatid";
    		$productnameData = $this->Webservicemodel->getDataById ( 'product', $wherename );
    		if (! empty($productnameData)) {
    		$insertData = array (
					'product_name' => $productname,
					'category_id' => $category_id,
					'sub_category_id' => $subcatid,
					'actual_price' => number_format($productprice,2),
					'discount_price' => number_format($productdisprice,2),
					'cost_price' => number_format($cost_price,2),
					'description' => ! empty($description) ? $description:"",
					'quantity' => $quantity,
    				'master_product_id' => $masterid
			);
			$where = "product_name='$productname'";
			$result = $this->Webservicemodel->update ( 'product', $insertData, $where );
    		}
    		else {
    			$insertData = array (
					'product_name' => $productname,
					'category_id' => $category_id,
					'sub_category_id' => $subcatid,
					'description' => $description,
					'actual_price' => number_format($productprice,2),
					'discount_price' => number_format($productdisprice,2),
					'cost_price' => number_format($cost_price,2),
					'quantity' => $quantity,
    				'master_product_id' => $masterid,
					'status' => 'Active',
			);
			$result = $this->Webservicemodel->insert ( 'product', $insertData );
    		}
    			if ($result) {
    				$resArr['result'] = 1;
    				$resArr['message'] = "Product has been added successfully";
    			} else {
    				$resArr['result'] = 0;
    				$resArr['message'] = "Product has not been added";
    			}
    		
    	}
    	echo $this->response($resArr, 200);
    }
    
    function customer_list_post() {
    	$resArr ['result'] = 0;
    	$resArr ['message'] = "";
    	$where = "role_id=2";
    	$userdata = $this->Webservicemodel->getDataById ( 'user',$where );
    	if ($userdata) {
    		foreach ( $userdata as $list ) {
    			$listData['user_id'] = $list ['user_id'];
    			$listData ['user_name'] = $list['user_name'];
    			$listData ['email'] = $list ['email'];
    			$listData ['contact'] = $list ['contact'];
    			$listData ['licence_no'] = $list ['licence_no'];
    			$listData ['address'] = $list ['address'];
    			$cityid = $list['city_id'];
    			$wherecityid = "city_id=$cityid";
    			$cityData = $this->Webservicemodel->getDataById('city',$wherecityid);
    			$listData['city_name'] = $cityData[0]['city_name'];
    			$pinid = $list['pin_code_id'];
    			$wherepin = "pin_code_id=$pinid";
    			$pinData = $this->Webservicemodel->getDataById('pin_code',$wherepin);
    			$listData['pin_number'] = $pinData[0]['pin_number'];
    			$listData['created_date'] = $list['created_date'];
    			$listData ['status'] = $list ['status'];
    			
    			$resArr ['result'] = 1;
    			$resArr ['message'] = "Success.";
    			$resArr ['Data'] [] = $listData;
    		}
    	} else {
    		$resArr ['result'] = 0;
    		$resArr ["message"] = "No Data found on server.";
    	}
    	echo $this->response ( $resArr, 200 );
    }
    
    function order_list_post() {
    	$resArr ['result'] = 0;
    	$resArr ['message'] = "";
    	$where = "(order_status='Approved') group by unique_order_id";
    	$orderdata = $this->Webservicemodel->getDataById ( 'manage_order',$where );
    	$marginData = $this->Webservicemodel->getTableData("company_margin");
    	$company_margin = $marginData[0]['company_margin'];
    	if ($orderdata) {
    		foreach ( $orderdata as $list ) {
    			$unique_order_id = $list ['unique_order_id'];
    			$listData['order_id'] = $unique_order_id;
    			$whereorder = "(unique_order_id = '$unique_order_id' AND order_status='Approved')";
    			$orderuniqdata = $this->Webservicemodel->getDataById ( 'manage_order',$whereorder );
    			$customer_id = $list['user_id'];
    			$whereuser = "user_id = '$customer_id'";
    			$userdata = $this->Webservicemodel->getDataById ( 'user',$whereuser );
    			$listData['name'] = $userdata[0]['user_name'];
    			$affiliate_percentage = $list['dis_by_agent'];
    			$algorithm = $list['algorithm'];
    			$affdis = $list['affiliate_percentage'];
    			$order_date = date("d M Y h:i:s A",strtotime($list ['order_date']));
    			if (!empty($orderuniqdata)){
    				$order_quantity= 0;
    				$total_discount_price=0;
    				$total_cost_price = 0;
    				foreach ($orderuniqdata as $order){
    					$product_id = $order['pro_id'];
    					$wherepro = "product_id = '$product_id'";
    					$prodata = $this->Webservicemodel->getDataById ( 'product',$wherepro );
    					$quantity = $order['order_quantity'];
    					$productlist['product_id'] = $prodata[0]['master_product_id'];
    					$productlist['quantity'] = $quantity;
    					$order_quantity = $order_quantity + $quantity;
    					$pro_price = $order ['pro_price'];
    					$pro_cost_price = $order['pro_cost_price'];
    					$total_discount_price = $total_discount_price + ($pro_price * $quantity);
    					$total_cost_price = $total_cost_price + ($pro_cost_price * $quantity);
    					$listData['productlist'][] = $productlist;
    					$productlist="";
    				}
    				
    				$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
    				 
    				$company_profit = ($company_margin/100);
    				if ($algorithm=="1"){
    					$package_per = (($commulative_per/100)*1)-($affdis/100);
    					if ($package_per > $company_profit){
    						$new_total_dis = ($total_discount_price - $total_cost_price)*($affdis/100);
    						 
    					}else {
    						$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100-($company_margin/100);
    					}
    				} else {
    					$package_per = ($commulative_per - $company_margin);
    					if ($package_per >= $affdis){
    						$new_total_dis = ($total_discount_price)*$affdis/100;
    					}
    				}
    				// echo $package_per;
    				$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
    				$hostital_dis_per = ($hostital_dis/$total_discount_price)*100;
    				$affiliate_earning = $new_total_dis - $hostital_dis;
    				$total_bill_price = $total_discount_price - $hostital_dis;
    			}
    			$listData['Total_quantity'] = $order_quantity;
    			$listData['order_date'] = $order_date;
    			$listData['total_bill_price'] = number_format($total_bill_price,2);
    			$listData['affiliate_earning'] = number_format($affiliate_earning,2);
    			$listData['customer_saving'] = number_format($hostital_dis);
    			$listData['affiliate_percentage'] = $affdis;
    			$listData['customer_percentage'] = $affiliate_percentage;
    			$listData['customer_id'] = $customer_id;
    			 
    			$resArr ['result'] = 1;
    			$resArr ['message'] = "Success.";
    			$resArr ['Data'] [] = $listData;
    			$listData = "";
    		}//die;
    	} else {
    		$resArr ['result'] = 0;
    		$resArr ["message"] = "No Data found on server.";
    	}
    	echo $this->response ( $resArr, 200 );
    }
    
}
