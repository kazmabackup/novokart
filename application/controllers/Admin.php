<?php

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->pdo = $this->load->database('pdo', true);
        $this->load->library("excel");
        $this->load->helper('text');
        $this->load->helper('url');
        $this->load->model("Adminmodel");
        date_default_timezone_set ( "Asia/Calcutta" );
        include ('application/libraries/phpmailer/sendEmail.php');
    }
    
    public function index()
    {
    	
        $this->load->view('admin/loginView');

    }

    function login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $checklogin = $this->Adminmodel->checklogin($email, $password);
        if (! empty($checklogin)) {
            $session = array(
                'id' => $checklogin[0]['user_id'],
                'userName' => $checklogin[0]['user_name'],
                'user_type' => $checklogin[0]['role_id'],
                'email' => $checklogin[0]['email']
            );
            $sesrole = $this->session->set_userdata('adminData', $session);
            echo 1;
        } else {
            echo 0;
        }
    }

    function userSessionId()
    {
        $sessionUserId = $this->session->userdata('adminData');
        return $sessionUserId['id'];
    }

    /* Mehar
     *
     * Method Name: Dashboard
     */

    function dashboard($id=NULL)
    {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
        	$where = "(order_status !='') group by unique_order_id";
        	$orderData = $this->Adminmodel->getDataById("manage_order",$where);
        	//print_r($orderData);die;
        	$marginData = $this->Adminmodel->getTableData("company_margin");
        	$company_margin = $marginData[0]['company_margin'];
        	//print_r($company_margin);die;
        	/* if (!empty($orderData)){
        		$total_quantity = 0;
        		foreach ($orderData as $order){
        			$quantity = $order['order_quantity'];
        			$total_quantity = $total_quantity + $quantity;
        		}
        	} else {
        		$total_quantity = 0;
        	} */
        	if (!empty($orderData)){
        		$total_order = count($orderData);
        	} else {
        		$total_order = 0;
        	}
        	$data['total_order'] = $total_order;
        	$wherepending = "order_status='Pending'";
        	$pendingorder = $this->Adminmodel->getDataById("manage_order",$wherepending);
        	if (!empty($pendingorder)){
        		$data['pending_order'] = count($pendingorder);
        	} else {
        		$data['pending_order'] = 0;
        	}
        	$whereapproved = "order_status='Approved'";
        	$approvedorder = $this->Adminmodel->getDataById("manage_order",$whereapproved);
        	//print_r($approvedorder);die;
        	if (!empty($approvedorder)){
        		$total_amount = 0;
        		$pro_cost_price =0;
        		$affiliate_earning = 0;
        		$hostital_dis = 0;
        		$new_total_dis1 = 0;
        		foreach ($approvedorder as $approve){
        			$algorithm = $approve['algorithm'];
        			$price = $approve['pro_price'];
        			$customer_per = $approve['dis_by_agent'];
        			$costprice = $approve['pro_cost_price'];
        			$quantity = $approve['order_quantity'];
        			$total_amount = $total_amount + ($price * $quantity);
        			$pro_cost_price = $pro_cost_price + ($costprice * $quantity);
        			$affiliate_percentage = $approve['affiliate_percentage'];
        			$commulative_per = (($total_amount - $pro_cost_price)/$total_amount)*100;
        			$company_profit = ($company_margin/100);
        			if ($algorithm=='1'){
        			$package_per = (($commulative_per/100)*1)-($affiliate_percentage/100);
        			if ($package_per > $company_profit){
        				$new_total_dis = ($total_amount - $pro_cost_price)*$affiliate_percentage/100;
        			}else {
        				$new_total_dis = (($total_amount - $pro_cost_price)*$affiliate_percentage/100)-$company_margin/100;
        				$new_total_dis1 = $new_total_dis1 + $new_total_dis;
        			}
        			} else {
        				$package_per = ($commulative_per - $company_margin);
        				if ($package_per > $affiliate_percentage){
        					$new_total_dis = ($total_amount)*$affiliate_percentage/100;
        					$new_total_dis1 = $new_total_dis1 + $new_total_dis;
        				} 
        			}
        			$hostital_dis = $hostital_dis + ($new_total_dis*$customer_per/100);
        		}//die;
        		//echo $hostital_dis;die;
        		
        		$hostital_dis_per = ($hostital_dis*$total_amount/100);
        		$aff_earn = $new_total_dis1 - $hostital_dis;
        		$affiliate_earning = $affiliate_earning + $aff_earn;
        	} else {
        		$total_amount = 0;
        		$affiliate_earning = 0;
        		$pro_cost_price = 0;
        		$hostital_dis = 0;
        	}
        	//echo $pro_cost_price;die;
        	$data['total_affiliate_earning'] = $affiliate_earning;
        	$data['total_customer_discount'] = $hostital_dis;
        	$toal_profit = ($total_amount) - ($pro_cost_price + $affiliate_earning);
        	$data['total_profit_value'] = $toal_profit;
        	$currentdate = date('Y-m-d');
            $wheretodayorder = "(order_date LIKE '$currentdate%') group by unique_order_id";
        	$toadayorder = $this->Adminmodel->getDataById("manage_order",$wheretodayorder);
        	if (!empty($toadayorder)){
        		$data['today_order'] = count($toadayorder);
        	}else {
        		$data['today_order'] = 0;
        	}
        	$data['amount'] = $total_amount;
            $data['activeMenu'] = 1;
            $data['breadcrum'] = "Manage Dashboard";
            $this->load->view('admin/dashboardView', $data);
        }else{
            redirect('Admin');
        }
       
    }


   
    function country() {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
            $data['detailData'] = $this->Adminmodel->getTableData ("country");
            $data ['activeMenu'] = 2;
            $data ['breadcrum'] = "Manage Country";
            $this->load->view ( 'admin/countryview', $data );
        } else {
            redirect ( 'Admin' );
        }
    }
   function addEditcountry($id = NULL) {
		$sesUserId = $this->userSessionId ();
		$currentdate = date ( "Y-m-d" );
		if ($sesUserId) {
			
		if ($id != '') {
			$where = "country_id=$id";
			$cityData = $this->Adminmodel->getDataById ( 'country', $where );
			$data ['detailData'] = $cityData;
			 $catStatus= "status='Active'";
			$data ['breadcrum'] = "<a href=".base_url()."Admin/>Country </a> / Edit Country";
		} else {
			
			
			$data ['breadcrum'] = "<a href=".base_url()."Admin/>country </a> / Add Country";
		}
		$data ['activeMenu'] = 2;
		$this->load->view ( 'admin/addEditCountry', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	 function addEditcountrySubmit($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $hid = $_POST ['id'];
        $name = $_POST ['name'];

        
        if ($hid != '') {
            $wherecoun = "country_name ='$name' AND country_id != $hid";
            $duplicatecoun = $this->Adminmodel->getDataById('country', $wherecoun);
            if (! empty($duplicatecoun)) {
                echo 3;
                
            }else {
               
                $insertData = array (
                        'country_name' => $name
                       

                );
                $where = "country_id=$hid";
                $result = $this->Adminmodel->update ( 'country', $insertData, $where );
                if ($result) {
                   echo 2;
                } else {
                    echo 0;
                }         
            }
        } else {
            $wherecoun1 = "country_name ='$name'";
            $duplicateName1 = $this->Adminmodel->getDataById('country',  $wherecoun1);
            if (! empty($duplicateName1)) {
                echo 3;
            }else {
                $insertData = array (
                    'country_name' => $name,
					'status' => 'Active'

                        );
                $result = $this->Adminmodel->insert ( 'country', $insertData );
                if ($result) {
                   echo 1;
                } else {
                    echo 0;
                }
            }
        }
       
    }
	
	 function deleteCountry()
    {
        $id = $_POST['id'];
        $where = "country_id = $id";
        $res = $this->Adminmodel->delete('country', $where);
        if ($res) {
             $detailData = $this->Adminmodel->getDataById ( 'city', $where );
             if($detailData) {
                foreach ($detailData as $detail) {
                    $city_id = $detail ['city_id'];
                    $wheresub_city_id = "city_id = $city_id";
                     $res = $this->Adminmodel->delete('city', $wheresub_city_id);
                }
             }
             
             echo 1;
        } else {
            echo 0;
        }
    }



    function changeCountryStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "country_id =  '$id' ";
            $result = $this->Adminmodel->update('country', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

     function city() {
		
		$sesUserId = $this->userSessionId ();
		$currentdate = date ( "Y-m-d" );
		if ($sesUserId) {
			
			
			$data ['activeMenu'] = 2;
			$data ['breadcrum'] = "Manage City";
			$data['citydata'] = $this->Adminmodel->getTableDataDesc("city");
			$this->load->view ( 'admin/cityview', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditcity($id = NULL) {
		$sesUserId = $this->userSessionId ();
		$currentdate = date ( "Y-m-d" );
		if ($sesUserId) {
			
		if ($id != '') {
			$where = "city_id=$id";
			$cityData = $this->Adminmodel->getDataById ( 'city', $where );
			$data ['detailData'] = $cityData;
			 $catStatus= "status='Active'";
        $data['menuDropdown'] = $this->Adminmodel->menuDropdown("country", $catStatus);
			$data ['breadcrum'] = "<a href=".base_url()."Admin/>City </a> / Edit City";
		} else {
			$catStatus= "status='Active'";
        $data['menuDropdown'] = $this->Adminmodel->menuDropdown("country", $catStatus);
			
			$data ['breadcrum'] = "<a href=".base_url()."Admin/>city </a> / Add City";
		}
		$data ['activeMenu'] = 2;
		$this->load->view ( 'admin/addEditCity', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	 function addEditcitySubmit($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $hid = $_POST ['id'];
        $name = $_POST ['name'];
        $state_id = $_POST ['state_id'];
        if ($hid != '') {
            $whereName = "city_name ='$name' AND country_id = $state_id AND city_id != $hid";
            $duplicateName = $this->Adminmodel->getDataById('city', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
                die();
            }else {
                $insertData = array (
                        'city_name' => $name,
                        'country_id' => $state_id 
                );
                $where = "city_id=$hid";
                $result = $this->Adminmodel->update ( 'city', $insertData, $where );
                if ($result) {
                   echo 2;
                } else {
                    echo 0;
                }         
            }
        } else {
            $whereName = "city_name ='$name' AND country_id = $state_id";
            $duplicateName = $this->Adminmodel->getDataById('city', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
                die();
            }else {
                $insertData = array (
                        'city_name' => $name,
                        'country_id' => $state_id ,
						'status' => 'Active' 

                        );
                $result = $this->Adminmodel->insert ( 'city', $insertData );
                if ($result) {
                   echo 1;
                } else {
                    echo 0;
                }
            }
        }
       
    }

	 function substate() {
		$countryid = $_POST ['id'];
		$where = "country_id = $countryid";
		$sub = $this->Adminmodel->getDataById ( 'state',$where );
		$html = "";
		if (! empty ( $sub )) {
			foreach ( $sub as $row ) {
				$id = $row ['state_id'];
				$name = $row ['state_name'];
				$html .= "<option value='$id'>$name</option>";
			}
		}
		echo $html;
	}
	
	
	 function deletecity()
    {
        $id = $_POST['id'];
        $where = "city_id = $id";
        $res = $this->Adminmodel->delete('city', $where);
        if ($res) {
             $detailData = $this->Adminmodel->getDataById ( 'pin_code', $where );
             if($detailData) {
                foreach ($detailData as $detail) {
                    $city_id = $detail ['pin_code_id'];
                    $wheresub_city_id = "pin_code_id = $city_id";
                     $res = $this->Adminmodel->delete('pin_code', $wheresub_city_id);
                }
             }
             
             echo 1;
        } else {
            echo 0;
        }
    }
	 function changeCityStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "city_id =  '$id' ";
            $result = $this->Adminmodel->update('city', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
 function pincode() {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
            $data['detailData'] = $this->Adminmodel->getTableDataDesc("pin_code");
            $data ['activeMenu'] = 2;
            $data ['breadcrum'] = "Manage Pin Code";
            $this->load->view ( 'admin/pincodeView', $data );
        } else {
            redirect ( 'Admin' );
        }
    }

   
     function addEditPin($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $whereStatus= "status='Active'";
        $data['menuDropdown'] = $this->Adminmodel->menuDropdown("city", $whereStatus);
        if ($sesUserId) {
            if ($id != '') {
                $where = "pin_code_id=$id";
                $detailData = $this->Adminmodel->getDataById ( 'pin_code', $where );
                $data ['detailData'] = $detailData;
                $data ['breadcrum'] =  "<a href=" . base_url() . "Admin/pincode>Manage Pin</a>";
            } else {
                $data ['breadcrum'] = "<a href=" . base_url() . "Admin/pincode>Manage Pin</a>";
            }
            $data ['activeMenu'] = 2;
            $this->load->view ( 'admin/addEditPin', $data );
            } else {
                redirect('Admin');
        }
    }
   

    function changePinStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "pin_code_id =  '$id' ";
            $result = $this->Adminmodel->update('pin_code', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
      public function hassan(){
        $pincode = "700020\r\n700046\r\n700005\r\n700035\r\n700027\r\n700027\r\n700027\r\n700027\r\n700003\r\n700007\r\n700108\r\n700014\r\n700044\r\n700086\r\n700003\r\n700022\r\n700019\r\n700019\r\n700019\r\n700014\r\n700007\r\n700036\r\n700008\r\n700018\r\n700006\r\n700077\r\n700034\r\n700034\r\n700010\r\n700037\r\n700037\r\n700054\r\n700036\r\n700025\r\n700006\r\n700066\r\n700032\r\n700108\r\n700042\r\n700012\r\n700088\r\n700034\r\n700027\r\n700073\r\n700017\r\n700054\r\n700007\r\n700073\r\n700002\r\n700002\r\n700001\r\n700001\r\n700035\r\n700031\r\n700013\r\n700029\r\n700028\r\n700074\r\n700065\r\n700107\r\n700016\r\n700069\r\n700021\r\n700047\r\n700019\r\n700024\r\n700075\r\n700047\r\n700019\r\n700030\r\n700003\r\n700046\r\n700020\r\n700095\r\n700019\r\n700078\r\n700082\r\n700022\r\n700005\r\n700065\r\n700072\r\n700088\r\n700069\r\n700002\r\n700033\r\n700014\r\n700108\r\n700078\r\n700032\r\n700061\r\n700074\r\n700034\r\n700080\r\n700017\r\n700068\r\n700010\r\n700006\r\n700028\r\n700024\r\n700085\r\n700020\r\n700082\r\n700031\r\n700053\r\n700063\r\n700007\r\n700026\r\n700099\r\n700026\r\n700089\r\n700028\r\n700054\r\n700042\r\n700061\r\n700052\r\n700026\r\n700001\r\n700023\r\n700052\r\n700052\r\n700002\r\n700053\r\n700073\r\n700001\r\n700028\r\n700036\r\n700020\r\n700045\r\n700029\r\n700001\r\n700025\r\n700067\r\n700014\r\n700071\r\n700016\r\n700060\r\n700080\r\n700006\r\n700023\r\n700071\r\n700027\r\n700074\r\n700099\r\n700053\r\n700028\r\n700047\r\n700011\r\n700027\r\n700090\r\n700040\r\n700053\r\n700087\r\n700001\r\n700090\r\n700037\r\n700028\r\n700024\r\n700002\r\n700034\r\n700094\r\n700017\r\n700016\r\n700060\r\n700009\r\n700063\r\n700041\r\n700006\r\n700032\r\n700054\r\n700001\r\n700002\r\n700072\r\n700030\r\n700004\r\n700026\r\n700001\r\n700065\r\n700018\r\n700001\r\n700047\r\n700023\r\n700009\r\n700028\r\n700044\r\n700054\r\n700025\r\n700029\r\n700092\r\n700040\r\n700001\r\n700040\r\n700071\r\n700094\r\n700026\r\n700038\r\n700006\r\n700015\r\n700012\r\n700075\r\n700075\r\n700029\r\n700061\r\n700002\r\n700015\r\n700014\r\n700034\r\n700030\r\n700017\r\n700004\r\n700006\r\n700050\r\n700041\r\n700043\r\n700072\r\n700043\r\n700050\r\n700026\r\n700007\r\n700065\r\n700010\r\n700033\r\n700024\r\n700002\r\n700014\r\n700015\r\n700088\r\n700088\r\n700001\r\n700063\r\n700073\r\n700033\r\n700001\r\n700004\r\n700067\r\n700007\r\n700075\r\n700001\r\n700062\r\n700023\r\n700001\r\n700012";
        $explodedpindcode = explode("\r\n",$pincode);
        foreach ($explodedpindcode as $xpl){
            $insertData = array (
                        'pin_num' => $xpl,
                        'city_id' => 25,
                        'status' => 'Active' 

                        );
           // print_r($insertData);
                //$result = $this->Adminmodel->insert ( 'pin_code', $insertData );
        }
        
        
    }
  

    function addEditPinSubmit($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $hid = $_POST ['id'];
        $name = $_POST ['name'];
        $city_id = $_POST ['city_id'];
        if ($hid != '') {
            $whereName = "pin_num ='$name' AND city_id = $city_id AND pin_code_id != $hid";
            $duplicateName = $this->Adminmodel->getDataById('pin_code', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
                die();
            }else {
                $insertData = array (
                       'pin_num' => $name,
                       'city_id' => $city_id 
                );
                $where = "pin_code_id=$hid";
                $result = $this->Adminmodel->update ( 'pin_code', $insertData, $where );
                if ($result) {
                   echo 2;
                } else {
                    echo 0;
                }         
            }
        } else {
             
            $whereName = "pin_num ='$name' AND city_id = $city_id";
            $duplicateName = $this->Adminmodel->getDataById('pin_code', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
                die();
            }else {
                $insertData = array (
                        'pin_num' => $name,
                        'city_id' => $city_id ,
						'status' => 'Active' 

                        );
                $result = $this->Adminmodel->insert ( 'pin_code', $insertData );
                if ($result) {
                   echo 1;
                } else {
                    echo 0;
                }
            }
        }
       
    }
   
    
    function deletePin()
    {
    	$id = $_POST['id'];
    	$wherePin = "pin_code_id = $id";
    	$res = $this->Adminmodel->delete('pin_code', $wherePin);
    	if ($res){
    		echo 1;
    	} else {
    		echo 0;
    	}
    
    	 
    }

  
    
    function category() {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
        $data['detailData'] = $this->Adminmodel->getTableDataDesc("Category");
            $data ['activeMenu'] = 4;
            $data ['breadcrum'] = "Manage Category";
            $this->load->view ( 'admin/categoryView', $data );
        } else {
            redirect ( 'Admin' );
        }
    }

    
     function addEditCategory($id = NULL) {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
            if ($id != '') {
                $where = "category_id=$id";
                $detailData = $this->Adminmodel->getDataById ( 'category', $where );
                $data ['detailData'] = $detailData;
                $data ['breadcrum'] =  "<a href=" . base_url() . "Admin/category>Manage Category</a>";
            } else {
                $data ['breadcrum'] = "<a href=" . base_url() . "Admin/category>Manage Category</a>";
            }
            $data ['activeMenu'] = 4;
            $this->load->view ( 'admin/addEditCategory', $data );
            } else {
                redirect('Admin');
        }
    }
   

   
    function addEditCategorySubmit($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $hid = $_POST ['id'];
        $name = $_POST ['name'];

         if (! empty ( $_FILES ['img'] ['name'] )) {
            $imageName = $_FILES ['img'] ['name'];
            $config ['upload_path'] = FCPATH . 'uploads/category/';
            $config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config ['max_size'] = '1024';
            $time = strtotime ( date ( 'Y-m-d H:i:s' ) );
            $config ['file_name'] = $time;
            $this->upload->initialize ( $config );
            if (! $this->upload->do_upload ( 'img' )) {
                $error = $this->upload->display_errors ();
                $data ['imgErr'] = $error;
                $failure = 1;
                $uploadedimgName = "";
            } else {
                $a = $this->upload->data ();
                $failure = 0;
                $uploadedimgName = $a ['file_name'];
            }
        } else {
            $uploadedimgName = "";
            $failure = 0;
        }
        if ($hid != '') {
            $whereName = "category_name ='$name' AND category_id != $hid";
            $duplicateName = $this->Adminmodel->getDataById('category', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
                
            }else {
                $where = "category_id=$hid";
                $offerData = $this->Adminmodel->getDataById ( 'category', $where );
                $insertData = array (
                        'category_name' => $name,
                        'image' => ($uploadedimgName) ? $uploadedimgName : $offerData [0] ['image'],

                );
                $where = "category_id=$hid";
                $result = $this->Adminmodel->update ( 'category', $insertData, $where );
                if ($result) {
                   echo 2;
                } else {
                    echo 0;
                }         
            }
        } else {
            $whereName = "category_name ='$name'";
            $duplicateName = $this->Adminmodel->getDataById('category', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
            }else {
                $insertData = array (
                    'category_name' => $name,
                    'image' => $uploadedimgName,
					'status' => 'Active',

                        );
                $result = $this->Adminmodel->insert ( 'category', $insertData );
                if ($result) {
                   echo 1;
                } else {
                    echo 0;
                }
            }
        }
       
    }


   

    function deleteCategory()
    {
        $id = $_POST['id'];
        $where = "category_id = $id";
        $res = $this->Adminmodel->delete('category', $where);
        if ($res) {
             echo 1;
        } else {
            echo 0;
        }
    }
	 function changeCategoryStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "category_id =  '$id' ";
            $result = $this->Adminmodel->update('category', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
function subcategory() {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
        $data['subcategorydata'] = $this->Adminmodel->getTableDataDesc("sub_category");
            $data ['activeMenu'] = 4;
            $data ['breadcrum'] = "Manage sub category";
            $this->load->view ( 'admin/subcategoryView', $data );
        } else {
            redirect ( 'Admin' );
        }
    }
function addEditSubCategory($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $whereStatus= "status='Active'";
        $data['menuDropdown'] = $this->Adminmodel->menuDropdown("category", $whereStatus);
        if ($sesUserId) {
            if ($id != '') {
                $where = "sub_category_id=$id";
                $detailData = $this->Adminmodel->getDataById ( 'sub_category', $where );
                $data ['detailData'] = $detailData;
                $data ['breadcrum'] =  "<a href=" . base_url() . "Admin/subcategory>Manage Sub Category</a>";
            } else {
                $data ['breadcrum'] = "<a href=" . base_url() . "Admin/subcategory>Manage Sub Category</a>";
            }
            $data ['activeMenu'] = 4;
            $this->load->view ( 'admin/addEditSubCategory', $data );
            } else {
                redirect('Admin');
        }
    }
    

    function changeSubCategoryStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "sub_category_id =  '$id' ";
            $result = $this->Adminmodel->update('sub_category', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }


    function addEditSubCategorySubmit($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $hid = $_POST ['id'];
        $name = $_POST ['name'];
        $category_id = $_POST ['category_id'];
         if (! empty ( $_FILES ['img'] ['name'] )) {
            $imageName = $_FILES ['img'] ['name'];
            $config ['upload_path'] = FCPATH . 'uploads/subcategory/';
            $config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config ['max_size'] = '1024';
            $time = strtotime ( date ( 'Y-m-d H:i:s' ) );
            $config ['file_name'] = $time;
            $this->upload->initialize ( $config );
            if (! $this->upload->do_upload ( 'img' )) {
                $error = $this->upload->display_errors ();
                $data ['imgErr'] = $error;
                $failure = 1;
                $uploadedimgName = "";
            } else {
                $a = $this->upload->data ();
                $failure = 0;
                $uploadedimgName = $a ['file_name'];
            }
        } else {
            $uploadedimgName = "";
            $failure = 0;
        }
        if ($hid) {
            $whereName = "sub_category_name ='$name' AND category_id = $category_id AND sub_category_id != $hid";
            $duplicateName = $this->Adminmodel->getDataById('sub_category', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
                die();
            }else {
                  $where = "sub_category_id=$hid";
                $offerData = $this->Adminmodel->getDataById ( 'sub_category', $where );
                $insertData = array (
                    'sub_category_name' => $name,
                    'image' => ($uploadedimgName) ? $uploadedimgName : $offerData [0] ['image'],
                    'category_id' => $category_id 
                );
                $where = "sub_category_id=$hid";
                $result = $this->Adminmodel->update ( 'sub_category', $insertData, $where );
                if ($result) {
                   echo 2;
                } else {
                    echo 0;
                }         
            }
        } else {
            $whereName = "sub_category_name ='$name' AND category_id = $category_id";
            $duplicateName = $this->Adminmodel->getDataById('sub_category', $whereName);
            if (! empty($duplicateName)) {
                echo 3;
                die();
            }else {
                $insertData = array (
                    'sub_category_name' => $name,
                    'category_id' => $category_id,
                    'image' => $uploadedimgName,
					 'status' => 'Active',
                 );
                $result = $this->Adminmodel->insert ( 'sub_category', $insertData );
                if ($result) {
                   echo 1;
                } else {
                    echo 0;
                }
            }
        }
       
    }

    

    function deleteSubCategory()
    {
        $id = $_POST['id'];
        $where = "sub_category_id = $id";
        $res = $this->Adminmodel->delete('sub_category', $where);
        if ($res) {
           
            echo 1;
        } else {
            echo 0;
        }
    }

function manageProduct() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data = "";
		$data['productdata'] = $this->Adminmodel->getTableDataDesc("product");
			$data ['heading'] = "Manage Product";
			 $data ['activeMenu'] = 13;
            $data ['breadcrum'] = "Manage Product";
			$this->load->view ( 'admin/ProductView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditProduct($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			//$data ['sub_catdata'] = $this->Adminmodel->getDropdown ( $table = "sub_category" );
			if ($id != '') {
				$where = "product_id=$id";
				$data ['productdata'] = $this->Adminmodel->getDataById ( 'product', $where );
				 $catStatus= "status='Active'";
        $data['categoryDropdown'] = $this->Adminmodel->menuDropdown("category", $catStatus);
			 $subStatus= "status='Active'";
        $data['subcategoryDropdown'] = $this->Adminmodel->menuDropdown("sub_category", $subStatus);
				$data ['heading'] = "Edit Product";
				 $data ['activeMenu'] = 13;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/manageProduct'>Manage Product</a> / Edit Product";
			} else {
				$data = "";
				$data ['heading'] = "Add Product";
				 $data ['activeMenu'] = 13;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/manageProduct'>Manage Product</a> / Add Product";
			}
			
			 $catStatus= "status='Active'";
        $data['categoryDropdown'] = $this->Adminmodel->menuDropdown("category", $catStatus);
			 $subStatus= "status='Active'";
        $data['subcategoryDropdown'] = $this->Adminmodel->menuDropdown("sub_category", $subStatus);
			$this->load->view ( 'admin/addEditProduct', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditProductSubmit($id = NULL) {
		$hid = $_POST ['id'];
		$name = $_POST ['name'];
		$categoryid = $_POST ['category_id'];
		$subcategoryid = $_POST ['sub_category_id'];
		$description = $_POST ['content'];
		$act_price = $_POST ['price'];
		$dis_price = $_POST ['dis_price'];
		$cost_price = $_POST ['cost_price'];
		$quantity = $_POST ['quantity'];
		$currentdate = date ( "Y-m-d" );
		if (! empty ( $_FILES ['img'] ['name'] )) {
			$imageName = $_FILES ['img'] ['name'];
			$config ['upload_path'] = FCPATH . 'uploads/product/';
			$config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config ['max_size'] = '1024';
			$time = strtotime ( date ( 'Y-m-d H:i:s' ) );
			$config ['file_name'] = $time;
			$this->upload->initialize ( $config );
			if (! $this->upload->do_upload ( 'img' )) {
				$error = $this->upload->display_errors ();
				$data ['imgErr'] = $error;
				$failure = 1;
				$uploadedimgName = "";
			} else {
				$a = $this->upload->data ();
				$failure = 0;
				$uploadedimgName = $a ['file_name'];
			}
		} else {
			$uploadedimgName = "";
			$failure = 0;
		}
		if ($hid != '') {
			$where = "product_id=$hid";
			$productData = $this->Adminmodel->getDataById ( 'product', $where );
			$data ['productdata'] = $productData;
			$insertData = array (
					'product_name' => $name,
					'category_id' => $categoryid,
					'sub_category_id' => $subcategoryid,
					'actual_price' => number_format($act_price,2),
					'discount_price' => number_format($dis_price,2),
					'cost_price' => number_format($cost_price,2),
					'description' => $description,
					'quantity' => $quantity,
					 
					'image' => ($uploadedimgName) ? $uploadedimgName : $productData [0] ['image'] 
			);//print_r($insertData);die;
			$where = "product_id=$hid";
			$result = $this->Adminmodel->update ( 'product', $insertData, $where );
			 if ($result) {
                   echo 2;die;
                } else {
                    echo 0;die;
                }         
            
			
		} else {
			$where1 = "product_name='$name'";
			$productnameData = $this->Adminmodel->getDataById ( 'product', $where1 );
			if (! empty ( $productnameData )) {
				echo 3;
				die ();
			} 
			$subcatewhere = "sub_category_id = $subcategoryid";
			$subcategorydata = $this->Adminmodel->getDataById('sub_category',$subcatewhere);
			$categaryid = $subcategorydata[0]['category_id'];
			$insertData = array (
					'product_name' => $name,
					'category_id' => $categoryid,
					'sub_category_id' => $subcategoryid,
					'description' => $description,
					'actual_price' => number_format($act_price,2),
					'discount_price' => number_format($dis_price,2),
					'cost_price' => number_format($cost_price,2),
					'quantity' => $quantity,
					'status' => 'Active',
					'image' => $uploadedimgName 
			);
			$result = $this->Adminmodel->insert ( 'product', $insertData );
		}
		
		if ($result) {
			echo 1;die;
		} else {
			echo 0;die;
		}
	}
	function getSubCategoryByCategory(){
        $category_id = $_POST['category_id'];
        if(!empty($category_id)){
            $where = "category_id = $category_id AND status ='Active'";
            $categorydata = $this->Adminmodel->getDataById('sub_category',$where);
            if(!empty($categorydata)){
                $html = "<option value='0'>Select Sub Category</option>";
                foreach ($categorydata as $cat){
                    $subcatid = $cat['sub_category_id'];
                    $name = $cat['sub_category_name'];
                    $html.= "<option value='$subcatid'>$name</option>";
                }
                echo $html;
            }else{
                echo "";
            }
        }
    }
	 function deleteproduct()
    {
        $id = $_POST['id'];
        $where = "product_id = $id";
        $res = $this->Adminmodel->delete('product', $where);
        if ($res) {
           
            echo 1;
        } else {
            echo 0;
        }
    }
	  function changeProductStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "product_id =  '$id' ";
            $result = $this->Adminmodel->update('product', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
	

function Package() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data = "";
		$data['packagedata'] = $this->Adminmodel->getTableDataDesc("package");
			$data ['heading'] = "Manage Package";
			 $data ['activeMenu'] = 12;
            $data ['breadcrum'] = "Manage Package";
			$this->load->view ( 'admin/packageView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditPackage($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			//$data ['sub_catdata'] = $this->Adminmodel->getDropdown ( $table = "sub_category" );
			if ($id != '') {
				$where = "package_id=$id";
				$data ['packagedata'] = $this->Adminmodel->getDataById ( 'package', $where );
				$data ['heading'] = "Edit Package";
				 $data ['activeMenu'] = 12;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/Package'>Manage Package</a> / Edit Package";
			} else {
				$data = "";
				$data ['heading'] = "Add Package";
				 $data ['activeMenu'] = 12;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/Package'>Manage Package</a> / Add Package";
			}
			
			$this->load->view ( 'admin/addEditPackage', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditPackageSubmit() {
		$hid = $_POST ['id'];
		$name = $_POST ['name'];
		$currentdate = date ( "Y-m-d" );
		
		if ($hid != '') {
			$insertData = array (
					'package_name' => $name,
					//'package_percentage' => $percentage,
					//'min_value' => $value
					 
			);//print_r($insertData);die;
			$where = "package_id=$hid";
			$result = $this->Adminmodel->update ( 'package', $insertData, $where );
			//print_r($result);die;
			 if ($result) {
                   echo 2;die;
                } else {
                    echo 0;die;
                }         
            
			
		} else {
			$where1 = "package_name='$name'";
			$productnameData = $this->Adminmodel->getDataById ( 'package', $where1 );
			if (! empty ( $productnameData )) {
				echo 3;
				die ();
			} 
			$insertData = array (
					'package_name' => $name,
					//'package_percentage' => $percentage,
					//'min_value' => $value,
					'status' => 'Active',
			);
			$result = $this->Adminmodel->insert ( 'package', $insertData );
			if ($result) {
			echo 1;die;
		} else {
			echo 0;die;
		}
		}
		
	}
	  function changePackageStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "package_id =  '$id' ";
            $result = $this->Adminmodel->update('package', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
	
 function deletepackage()
    {
        $id = $_POST['id'];
        $where = "package_id = $id";
        $res = $this->Adminmodel->delete('package', $where);
        if ($res) {
           
            echo 1;
        } else {
            echo 0;
        }
    }
	function affiliate() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data = "";
		    $data['agentdata'] = $this->Adminmodel->getTableDataDesc("affiliate");
			$data ['heading'] = "Manage Affiliate";
			$data ['activeMenu'] = 11;
            $data ['breadcrum'] = "Manage Affiliate";
			$this->load->view ( 'admin/affiliateView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditAgent($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			if ($id != '') {
				$where = "affiliate_id=$id";
				$data ['agentdata'] = $this->Adminmodel->getDataById ( 'affiliate', $where );
				$data ['heading'] = "Edit Agent";
				 $data ['activeMenu'] = 11;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/affiliate'>Manage Affiliate</a> / Edit Affiliate";
			} else {
				$data = "";
				$data ['heading'] = "Add Affiliate";
				 $data ['activeMenu'] = 11;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/affiliate'>Manage Affiliate</a> / Add Affiliate";
			}
			
			$this->load->view ( 'admin/addEditAffiliate', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditAgentSubmit($id = NULL) {
		//echo "aaa";die;
		$hid = $_POST ['id'];
		$name = $_POST ['name'];
		$password = $_POST ['password'];
		$email = $_POST ['email'];
		$phone = $_POST ['phone'];
		//$discount = $_POST ['discount'];
		$uniqueid = mt_rand(100000, 999999);
		$currentdate = date ( "Y-m-d" );
		
		if ($hid != '') {
			$wheremobile = "phone = '$phone' AND affiliate_id !=$hid";
			$duplicatempbiledata = $this->Adminmodel->getDataById ( 'affiliate', $wheremobile );
			$whereemail = "email = '$email' AND affiliate_id !=$hid";
			$duplicateemaildata = $this->Adminmodel->getDataById ( 'affiliate', $whereemail );
			if ($duplicatempbiledata){
				echo "dupmob";
			} elseif ($duplicateemaildata){
				echo "dupemail";
			} else{
			$insertData = array (
					'affiliate_name' => $name,
					'email' => $email,
					'phone' => $phone,
					//'discount' => $discount,
					'password' => $password
					 
			);//print_r($insertData);die;
			$where = "affiliate_id=$hid";
			$result = $this->Adminmodel->update ( 'affiliate', $insertData, $where );
			//print_r($result);die;
			 if ($result) {
                   echo 2;
                } else {
                    echo 0;
                }  
			}      
		} else {
			$wheremobile = "phone = '$mobile'";
			$duplicatempbiledata = $this->Adminmodel->getDataById ( 'affiliate', $wheremobile );
			$whereemail = "email = '$email'";
			$duplicateemaildata = $this->Adminmodel->getDataById ( 'affiliate', $whereemail );
			if ($duplicatempbiledata){
				echo "dupmob";
			} elseif ($duplicateemaildata){
				echo "dupemail";
			} else{
			$insertData = array (
					'unique_id' => $uniqueid,
					'affiliate_name' => $name,
					'email' => $email,
					'phone' => $phone,
					//'discount' => $discount,
					'password' => $password,
					'status' => 'Active',
			);
			// print_r($insertData);die;
			$result = $this->Adminmodel->insert ( 'affiliate', $insertData );
			if ($result) {
			echo 1;
		} else {
			echo 0;
		}
		}
		}
	}
	
  function changeagentStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "affiliate_id =  '$id' ";
            $result = $this->Adminmodel->update('affiliate', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
	
 function deleteaffiliate()
    {
        $id = $_POST['id'];
        $where = "affiliate_id = $id";
        $res = $this->Adminmodel->delete('affiliate', $where);
        if ($res) {
           
            echo 1;
        } else {
            echo 0;
        }
    }
		function customer() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data = "";
			$where = "role_id=2";
			$data ['userdata'] = $this->Adminmodel->getDataByuserId ( 'user', $where );
			$data ['heading'] = "Manage Customer";
			 $data ['activeMenu'] = 10;
            $data ['breadcrum'] = "Manage Customer";
			$this->load->view ( 'admin/userView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditUser($id = NULL) {
		$sesUserId = $this->userSessionId ();
		
		if ($sesUserId) {
			//$data ['sub_catdata'] = $this->Adminmodel->getDropdown ( $table = "sub_category" );
			//print_r($id);die;
			if ($id != '') {
				$where = "user_id=$id";
				$data ['userdata'] = $this->Adminmodel->getDataById ( 'user', $where );
				//print_r($data ['userdata']);die;
			   $catStatus= "status='Active'";
               $data['categoryDropdown'] = $this->Adminmodel->menuDropdown("affiliate", $catStatus);
		       $packageStatus= "status='Active'";
               $data['packageDropdown'] = $this->Adminmodel->menuDropdown("package", $packageStatus);
			   $data ['heading'] = "Edit User";
			   $data ['activeMenu'] = 10;
			   $data ['breadcrum'] = "<a href='" . base_url () . "admin/customer'>Manage User</a> / Edit User";
			} else {
				 $data = "";
				 $catStatus= "status='Active'";
                 $data['categoryDropdown'] = $this->Adminmodel->menuDropdown("affiliate", $catStatus);
				 $data ['heading'] = "Add User";
				 $data ['activeMenu'] = 10;
				 $data ['breadcrum'] = "<a href='" . base_url () . "admin/customer'>Manage User</a> / Add User";
			}
			
			$this->load->view ( 'admin/addEdituse', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEdituserSubmit($id = NULL) {
		$hid = $_POST ['id'];
		$name = $_POST ['affiliate_id'];
		$package = $_POST ['package_id'];
		$dis_per = $_POST['dis_per'];
		$min_value = $_POST ['min_value'];
		$algorithm = $_POST ['algo'];
		if ($hid != '') {
			$insertData = array (
					'package_id' => $package,
					'agent_id' => $name,
					'package_percentage' => $dis_per,
					'min_value' => $min_value,
					'algorithm' =>$algorithm
					 
			);//print_r($insertData);die;
			$where = "user_id=$hid";
			$result = $this->Adminmodel->update ( 'user', $insertData, $where );
			//print_r($result);die;
			 if ($result) {
                   echo 2;
                } else {
                    echo 0;
                }         
		} else {
			$insertData = array (
					'unique_id' => $uniqueid,
					'name' => $name,
					'email' => $email,
					'phone' => $phone,
					'discount' => $discount,
					'status' => 'Active',
			);
			$result = $this->Adminmodel->insert ( 'affiliate', $insertData );
			if ($result) {
			echo 1;
		} else {
			echo 0;
		}
		}
	}
	  function changeuserStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "user_id =  '$id' ";
            $result = $this->Adminmodel->update('user', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
	  public function banner() {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
            $data['activeMenu'] = 3;
            $data ['breadcrum'] = "<a href=" . base_url() . "Admin/banner>Manage Banner</a>";
            $data['detailData'] = $this->Adminmodel->getTableDataDesc("banner");
            $this->load->view ( 'admin/bannerView',$data );
        }else{
            redirect('Admin');
        }
    }
    function addEditBanner($id = NULL) {
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
            if ($id != '') {
                $where = "banner_id=$id";
                $detailData = $this->Adminmodel->getDataById ( 'banner', $where );
                $data ['detailData'] = $detailData;
                $data ['breadcrum'] =  "<a href=" . base_url() . "Admin/banner>Manage Banner</a>";
            } else {
                $data ['breadcrum'] = "<a href=" . base_url() . "Admin/banner>Manage Banner</a>";
            }
            $data ['activeMenu'] = 3;
            $this->load->view ( 'admin/addEditBanner', $data );
        } else {
            redirect('Admin');
        }
    }

    
    function addEditBannerSubmit($id = NULL) {
        $sesUserId = $this->userSessionId ();
        $hid = $_POST ['id'];
        $name = $_POST ['name'];
        $content = $_POST ['content'];
         if (! empty ( $_FILES ['img'] ['name'] )) {
            $imageName = $_FILES ['img'] ['name'];
            $config ['upload_path'] = FCPATH . 'uploads/banner/';
            $config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config ['max_size'] = '1024';
            $time = strtotime ( date ( 'Y-m-d H:i:s' ) );
            $config ['file_name'] = $time;
            $this->upload->initialize ( $config );
            if (! $this->upload->do_upload ( 'img' )) {
                $error = $this->upload->display_errors ();
                $data ['imgErr'] = $error;
                $failure = 1;
                $uploadedimgName = "";
            } else {
                $a = $this->upload->data ();
                $failure = 0;
                $uploadedimgName = $a ['file_name'];
            }
        } else {
            $uploadedimgName = "";
            $failure = 0;
        }
        if (! empty ( $_FILES ['webimg'] ['name'] )) {
        	$webimageName = $_FILES ['webimg'] ['name'];
        	$config ['upload_path'] = FCPATH . 'uploads/banner/';
        	$config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
        	$config ['max_size'] = '1024';
        	$time = strtotime ( date ( 'Y-m-d H:i:s' ) );
        	$config ['file_name'] = $time;
        	$this->upload->initialize ( $config );
        	if (! $this->upload->do_upload ( 'webimg' )) {
        		$error = $this->upload->display_errors ();
        		$data ['imgErr'] = $error;
        		$failure = 1;
        		$webuploadedimgName = "";
        	} else {
        		$a = $this->upload->data ();
        		$failure = 0;
        		$webuploadedimgName = $a ['file_name'];
        	}
        } else {
        	$webuploadedimgName = "";
        	$failure = 0;
        }  
            if ($hid != '') {
                $where = "banner_id=$hid";
                $offerData = $this->Adminmodel->getDataById ( 'banner', $where );
                $insertData = array (
                        'title' => $name,
                        'description' => $content,
                        'image' => ($uploadedimgName) ? $uploadedimgName : $offerData [0] ['image'],
                		'web_image' => ($webuploadedimgName) ? $webuploadedimgName : $offerData [0] ['web_image'],
                );
                $where = "banner_id=$hid";
                $result = $this->Adminmodel->update ( 'banner', $insertData, $where );
                if ($result) {
                	/* $updatebannerData = array (
                			'web_image' => ($webuploadedimgName) ? $webuploadedimgName : $offerData [0] ['web_image'],
                	
                	);
                	$this->Adminmodel->updatewebbanner ( 'banner', $updatebannerData); */
                    echo 2;
                    die ();
                 } else {
                        echo 0;
                        die ();
                }
             } else {
             	$bannerData = $this->Adminmodel->getTableData ($table = 'banner');
            $insertData = array (
                    'title' => $name,
                    'description' => $content,
                    'image' => $uploadedimgName,
            		'web_image' => $webuploadedimgName,
					'status' => 'Active'
                   

            );
           // print_r($insertData);die;
            $result = $this->Adminmodel->insert ( 'banner', $insertData );
            if ($result) {
            	/* $updatebannerData = array (
            			'web_image' => ($webuploadedimgName) ? $webuploadedimgName : $bannerData [0] ['web_image'],
            			 
            	);
            	 $this->Adminmodel->updatewebbanner ( 'banner', $updatebannerData); */
                 echo 1;
                die ();
              } else {
                 echo 0;
                 die ();
             }
        }
        
    }

   


    function deleteBanner()
    {
        $id = $_POST['id'];
        $where = "banner_id = $id";
        $res = $this->Adminmodel->delete('banner', $where);
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
    }


    

    function changeBannerStatus()
    { 
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'In-Active') {
                $updateArr = array(
                    'status' => 'Inactive'
                );
            } else {
                $updateArr = array(
                    'status' => 'Active'
                );
            }
            $where = "banner_id =  '$id' ";
            $result = $this->Adminmodel->update('banner', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

  public function order($status=NULL) {
  	//echo $status;die;
        $sesUserId = $this->userSessionId ();
        if ($sesUserId) {
        	if ($status == 'Pending'){
        		$wherepending = "order_status='Pending'";
        		 $data['detailData'] = $this->Adminmodel->getDataById("manage_order",$wherepending);
        	}elseif ($status=='Process'){
        		$whereapproved = "order_status='Process'";
        		 $data['detailData'] = $this->Adminmodel->getDataById("manage_order",$whereapproved);
        	} elseif ($status=='Approved'){
        		$whereapproved = "order_status='Approved'";
        		 $data['detailData'] = $this->Adminmodel->getDataById("manage_order",$whereapproved);
        	} elseif ($status=='Today'){
        		$currentdate = date('Y-m-d');
        		$wheretodayorder = "order_date LIKE '$currentdate%'";
        		$data['detailData'] = $this->Adminmodel->getDataById("manage_order",$wheretodayorder);
        	}else {
        		$data['detailData'] = $this->Adminmodel->getTableData1Desc("manage_order");
        	}
            $data['activeMenu'] = 8;
            $data ['breadcrum'] = "<a href=" . base_url() . "Admin/order>Manage Order </a>";
           //print_r($data['detailData']);die;
            $this->load->view ( 'admin/orderView',$data );
        }else{
            redirect('Admin');
        }
    }

    function order_details(){
    	$sesUserId = $this->userSessionId ();
    	$orderid = $_POST['orderid'];
    	$marginData = $this->Adminmodel->getTableData("company_margin");
    	$data['company_margin'] = $marginData[0]['company_margin'];
    	$whereunique="unique_order_id='$orderid'";
    	$data['orderdata'] = $this->Adminmodel->getDataById ( 'manage_order', $whereunique );
    	$this->load->view('admin/ajax_orderDetailsView',$data);
    }
    function changeOrderStatus()
    { //echo " ddd";die;
        $id = $_POST['id'];
        $status = $_POST['status'];
        if ($id) {
            if ($status == 'Approved') {
                $updateArr = array(
                    'order_status' => 'Approved'
                );
            } else {
                $updateArr = array(
                    'order_status' => 'Cancel'
                );
            }
            $where = "unique_order_id =  '$id' ";
            $result = $this->Adminmodel->update('manage_order', $updateArr, $where);
            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
    /*Roushan
     * Date:-08-01-2019
     * method:- orderexportreport  */
  
    public function orderexpoert() {
    	$sesUserId = $this->userSessionId ();
    	if ($sesUserId) {
    		$fromdate = $_POST['fromdatepicker'];
    	    $todate = $_POST['todatepicker'];
    	    $fromdateformate = date("Y-m-d",strtotime($fromdate));
    	    $todateformate = date("Y-m-d",strtotime($todate));
    	    $whereunique = "(order_date >='$fromdateformate' AND order_date <='$todateformate') group by unique_order_id";
    	    $orderdata = $this->Adminmodel->getDataById ( 'manage_order', $whereunique );
    	    $fileName = 'data-'.time().'.xls';
    	   $objPHPExcel = new PHPExcel();
    	$objPHPExcel->setActiveSheetIndex(0);
    	// set Header
    	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'S.No');
    	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Order Date');
    	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Order Id');
    	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Customer Name');
    	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Total Bill Amount');
    	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Hospital Discount(%)');
    	$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Hospital Discount Amount');
    	$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Affiliate Discount(%)');
    	$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Affiliate Discount Amount');
    	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'After Discount Bill Amount');
    	$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Delivery Address');
    	$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Status');
    	// set Row
    	$rowCount = 2;
    	$i=1;
    	 foreach ($orderdata as $order) {
    	 	$order_date = date("d-m-Y h:i:s A",strtotime($order ['order_date']));
    	 	$orderid = $order ['unique_order_id'];
    	 	$affiliate_percentage = $order['dis_by_agent'];
    	 	$user_id = $order ['user_id'];
    	 	$where = "user_id= $user_id";
    	 	$usedata = $this->Adminmodel->getDataById ( 'user', $where );
    	 	$username=$usedata[0]['user_name'];
    	 	$affdis = $usedata[0]['package_percentage'];
    	 	$whereorderid = "unique_order_id='$orderid'";
    	 	$orderdetailsData = $this->Adminmodel->getDataById("manage_order",$whereorderid);
    	 	$address = $order ['address_id'];
    	 	$whereaddress = "delivery_address_id= $address";
    	 	$addressdata = $this->Adminmodel->getDataById ( 'delivery_address', $whereaddress );
    	 	 $addressname=$addressdata[0]['address'];
    	 	$total_discount_price = 0;
    	 	$total_cost_price = 0;
    	 	foreach ($orderdetailsData as $orderdetails){
    	 		$order_quantity = $orderdetails ['order_quantity'];
    	 		$pro_price = $orderdetails ['pro_price'];
    	 		$pro_cost_price = $orderdetails['pro_cost_price'];
    	 		$total_discount_price = $total_discount_price + ($pro_price * $order_quantity);
    	 		$total_cost_price = $total_cost_price + ($pro_cost_price * $order_quantity);
    	 	}
    	 	 
    	 	$status = $order['order_status'];
    	 	$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
    	 	$company_profit = (15/100);
    	 	$package_per = (($commulative_per/100)*1)-($affdis/100);
    	 	if ($package_per > $company_profit){
    	 		$new_total_dis = ($total_discount_price - $total_cost_price)*($affdis/100);
    	 		 
    	 	}else {
    	 		$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100-(15/100);
    	 	}
    	 	// echo $total_cost_price;
    	 	$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
    	 	if ($hostital_dis < 0){
    	 		$hostital_dis_per = 0;
    	 		$affiliate_earning = $new_total_dis - 0;
    	 		$total_bill_price = $total_discount_price - 0;
    	 	} else {
    	 		$hostital_dis_per = ($hostital_dis/$total_discount_price)*100;
    	 		$affiliate_earning = $new_total_dis - $hostital_dis;
    	 		$total_bill_price = $total_discount_price - $hostital_dis;
    	 	}
    		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $i);
    		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $order_date);
    		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $orderid);
    		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $username);
    		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $total_discount_price);
    		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $hostital_dis_per);
    		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $hostital_dis);
    		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $affdis);
    		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $affiliate_earning);
    		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $total_bill_price);
    		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $addressname);
    		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $status);
    		$rowCount++;
    		$i++;
    	} //die;
    	$object_writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    	 
    	//$objWriter = new PHPExcel_Writer_Excel2005($objPHPExcel);
    	//$objWriter->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
    	// download file
    	header("Content-Type: application/vnd.ms-excel");
    	header('Content-Disposition: attachment;filename="data-'.time().'.xls"');
        $object_writer->save('php://output');
    	}else{
    		redirect('Admin');
    	}
    }
	function productPrice($id = NULL){
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$productewhere = "product_id = $id";
			$productdata = $this->Adminmodel->getDataById('product',$productewhere);
			$data['productdata'] = $productdata;
			//print_r($productdata);die;
			$productid = $productdata[0]['product_id'];
			$data['productid'] = $productid;
			$data ['product_priceData'] = $this->Adminmodel->getDataById ('product_price_weight',$productewhere );
			$data ['heading'] = "Manage Product Price";
			$data ['breadcrum'] = "<a href=" . base_url() . "Admin/manageProduct/$productid>Manage Product</a>";
			$data ['activeMenu'] = 3;
			$this->load->view ( 'admin/productPriceView', $data );
		} else {
			redirect ( 'admin' );
		}	
	}
function addPrice($id=NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			//echo $id;die;
			
			if ($id != '') {
				
				$where = "product_id=$id";
				$data ['productaddprice'] = $this->Adminmodel->getDataById ( 'product', $where );
				//print_r($data ['productaddprice']);die;
				$data ['heading'] = "Add Price";
				$data ['breadcrum'] = "<a href=" . base_url() . "Admin/productPrice/$id>Manage Price</a>";
			$data ['activeMenu'] = 3;
			}
			$this->load->view ( 'admin/addEditPrice', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function editPrice($id=NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
				//echo $id;die;
			if ($id != '') {
				$where = "product_price_weight_id=$id";
				$productdata1 = $this->Adminmodel->getDataById ( 'product_price_weight', $where );
				 $data ['productdata1'] = $productdata1;
				 $productid = $productdata1[0]['product_id'];
				 $data ['heading'] = "Edit Price";
			    $data ['breadcrum'] = "<a href=" . base_url() . "Admin/productPrice/$id>Manage Price</a>";
			    $data ['activeMenu'] = 3;
			} 
			$this->load->view ( 'admin/addEditPrice', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditProductpriceSubmit($id = NULL) {
		$hid = $_POST ['priceid'];
		$product = $_POST ['productid'];
		//echo $product;die;
		$size = $_POST ['name'];
		//echo $product;die;
		$price = $_POST ['price'];
		$m_price = $_POST ['dis_price'];
		
		if ($hid != '') {
			
			
			$insertData = array (
					'product_id' => $product,
					'weight' => $size,
					'actual_price' => $price,
					'dis_price' => $m_price
			);//print_r($insertData);die;
			$where = "product_price_weight_id=$hid";
			$result = $this->Adminmodel->update ( 'product_price_weight', $insertData, $where );
			
		} else {
			
			$insertData = array (
					'product_id' => $product,
					'weight' => $size,
					'actual_price' => $price,
					'dis_price' => $m_price
			);
				
			// print_r($insertData);die;
			$result = $this->Adminmodel->insert ( 'product_price_weight', $insertData );
		}
	
		if ($result) {
			echo 1;
		} else {
			echo 0;
		}
	}
	function deletePrice() {
		$sesUserId = $this->userSessionId ();
		$pid = $_POST ['id'];
		$where = "product_price_weight_id=$pid";
		if ($sesUserId) {
			$res = $this->Adminmodel->delete ( 'product_price_weight', $where );
			if ($res) {
				echo 1;
			} else {
				echo 0;
			}
		}
	}
	/* Roushan
	 * date:-19/11/2018
	 * method: change password */
	function changePassword($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			
				$where = "user_id=$sesUserId";
				$data ['userdata'] = $this->Adminmodel->getDataById ( 'user', $where );
				$data ['heading'] = "Change password";
				$data ['activeMenu'] = 14;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/dashboard'>Manage Dashboard</a> / Change password";
			
				
			$this->load->view ( 'admin/changepasswordView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function changepassSubmit() {
		$hid = $_POST ['id'];
		$password = $_POST ['password'];
		if ($hid != '') {
			$insertData = array (
					'password' => $password,
			);
			//print_r($insertData);die;
			$where = "user_id=$hid";
			$result = $this->Adminmodel->update ( 'user', $insertData, $where );
			//print_r($result);die;
			if ($result) {
				echo 1;
			} else {
				echo 0;
			}
		} 
	}
	/* Roushan
	 * date:-19/11/2018
	 * method: my account */
	function myAccount($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$where = "user_id=$sesUserId";
			$data ['userdata'] = $this->Adminmodel->getDataById ( 'user', $where );
			$data ['heading'] = "My Account";
			$data ['activeMenu'] = 14;
			$data ['breadcrum'] = "<a href='" . base_url () . "admin/dashboard'>Manage Dashboard</a> / My Account";
			$this->load->view ( 'admin/myaccountView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function updatemyaccountSubmit() {
		$hid = $_POST ['id'];
		$name = $_POST ['name'];
		$email = $_POST ['email'];
		$contacts = $_POST ['contact'];
		$address = $_POST ['address'];
		if ($hid != '') {
			$insertData = array (
					'user_name' => $name,
					'email' => $email,
					'contact' => $contacts,
					'address' => $address,
			);
			//print_r($insertData);die;
			$where = "user_id=$hid";
			$result = $this->Adminmodel->update ( 'user', $insertData, $where );
			//print_r($result);die;
			if ($result) {
				echo 1;
			} else {
				echo 0;
			}
		}
	}
	function managecms(){
		$data ['cmsdata'] = $this->Adminmodel->getTableData($table='cms');
		$data ['heading'] = "Manage CMS";
		$data ['activeMenu'] = 3;
		$data ['breadcrum'] = "<a href='" . base_url () . "admin/dashboard'>Manage Dashboard</a> / Manage CMS";
		$this->load->view ( 'admin/cmsView', $data );
	}
	function addEditcms($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			if ($id){
			$where = "cms_id=$id";
			$data ['cmsdata'] = $this->Adminmodel->getDataById ( 'cms', $where );
			$data ['heading'] = "Edit Cms Page";
			$data ['activeMenu'] = 4;
			$data ['breadcrum'] = "<a href='" . base_url () . "admin/managecms'>Manage CMS</a> / Edit Cms";
			} else {
				$data ['userdata'] = "";
				$data ['heading'] = "My Account";
				$data ['activeMenu'] = 4;
				$data ['breadcrum'] = "<a href='" . base_url () . "admin/managecms'>Manage CMS</a> / Add Cms";
			}
			
			$this->load->view ( 'admin/addEditcms', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditCmsSubmit($id = NULL) {
		$sesUserId = $this->userSessionId ();
		$hid = $_POST ['id'];
		$name = $_POST ['name'];
		$content = $_POST ['content'];
		
		
		if ($hid != '') {
			if ($hid ==7){
				$fb = $_POST ['fb'];
				$tw = $_POST ['tw'];
				$gplus = $_POST ['g_plus'];
				$insta = $_POST ['insta'];
				$linkedin = $_POST ['lin'];
				$where = "cms_id=$hid";
				$offerData = $this->Adminmodel->getDataById ( 'cms', $where );
				$insertData = array (
						'title' => $name,
						'fb_link' => $fb,
						'tw_link' => $tw,
						'g_plus_link' => $gplus,
						'lin_link' => $linkedin,
						'insta_link' => $insta,
				);
				$where = "cms_id=$hid";
				$result = $this->Adminmodel->update ( 'cms', $insertData, $where );
				if ($result) {
					echo 2;
					die ();
				} else {
					echo 0;
					die ();
				}
			}elseif ($hid ==5){
				$contact = $_POST['contact'];
				$email = $_POST['email'];
				$where = "cms_id=$hid";
				$offerData = $this->Adminmodel->getDataById ( 'cms', $where );
				$insertData = array (
						'title' => $name,
						'contact' => $contact,
						'email' => $email,
						'description' => $content,
				);
				$where = "cms_id=$hid";
				$result = $this->Adminmodel->update ( 'cms', $insertData, $where );
				if ($result) {
					echo 2;
					die ();
				} else {
					echo 0;
					die ();
				}
			} else {
			
			$where = "cms_id=$hid";
			$offerData = $this->Adminmodel->getDataById ( 'cms', $where );
			$insertData = array (
					'title' => $name,
					'description' => $content,
					
	
			);
			$where = "cms_id=$hid";
			$result = $this->Adminmodel->update ( 'cms', $insertData, $where );
			if ($result) {
				echo 2;
				die ();
			} else {
				echo 0;
				die ();
			}
			}
		} else {
			$insertData = array (
					'title' => $name,
					'description' => $content,
          
	
			);
			// print_r($insertData);die;
			$result = $this->Adminmodel->insert ( 'cms', $insertData );
			if ($result) {
				echo 1;
				die ();
			} else {
				echo 0;
				die ();
			}
		}
	
	}
	function manageSlot() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data ['slotData'] = $this->Adminmodel->getTabledata ( $table = "slot" );
			$data ['heading'] = "Manage Slot";
			$data ['breadcrum'] = "Manage Slot";
			$data ['activeMenu'] = 7;
			$this->load->view ( 'admin/manageSlotView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditSlot($id=NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			if ($id != '') {
				$where = "slot_id=$id";
				$data ['slotData'] = $this->Adminmodel->getDataById ( 'slot', $where );
				$data ['heading'] = "Edit Slot";
				$data ['bredcrum'] = "<a href='" . base_url () . "admin/manageSlot'>Manage Slot</a> / Edit Slot";
			} else {
				$data = "";
				$data ['heading'] = "Add Slot";
				$data ['bredcrum'] = "<a href='" . base_url () . "admin/manageSlot'>Manage Slot</a> / Add Slot";
			}
			$data ['activeMenu'] = 7;
			$this->load->view ( 'admin/addEditSlotView', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function addEditSlotSubmit($id=NULL){
		$hid = $_POST ['id'];
		$starttime = $_POST['timepicker1'];
		$endtime = $_POST['timepicker2'];
		if ($hid != '') {
			$where = "slot_id=$hid";
			$slotData = $this->Adminmodel->getDataById ( 'slot', $where );
			$data ['slotdata'] = $slotData;
			$insertData = array (
					//'slot_date' => $slotdate,
					'start_time' => $starttime,
					'end_time' => $endtime,
			);
			$where = "slot_id=$hid";
			$result = $this->Adminmodel->update ( 'slot', $insertData, $where );
			if ($result) {
				echo 2;die;
			} else {
				echo 0;die;
			}
		} else {
			$where3 = "start_time='$starttime' AND end_time='$endtime'";
			$sloData = $this->Adminmodel->getDataById ( 'slot', $where3 );
			if (! empty ( $sloData )) {
				echo 3;
				die ();
			}
			$insertData = array (
					'start_time' => $starttime,
					'end_time' => $endtime,
					'status' => 'Active',
			);
			$result = $this->Adminmodel->insert ( 'slot', $insertData );
		}
		if ($result) {
			echo 1;die;
		} else {
			echo 0;die;
		}
	
	}
	function deleteSlot() {
		$sesUserId = $this->userSessionId ();
		$pid = $_POST ['id'];
		$where = "slot_id=$pid";
		if ($sesUserId) {
			$res = $this->Adminmodel->delete ( 'slot', $where );
			if ($res) {
				echo 1;
			} else {
				echo 0;
			}
		}
	}
	function inactive_slot_status() {
		$id = $_POST ['id'];
		if ($id) {
			$updatARr = array (
					'status' => "Inactive"
			);
			$where = "slot_id = $id";
			$result = $this->Adminmodel->update ( 'slot', $updatARr, $where );
			if ($result) {
				echo 1;
			} else {
				echo 2;
			}
		}
	}
	/*
	 *
	 * Method Name: Active status
	 * this methode is used for active category status
	 */
	function active_slot_status() {
		$id = $_POST ['id'];
		if ($id) {
			$updatARr = array (
					'status' => "Active"
			);
			$where = "slot_id = $id";
			$result = $this->Adminmodel->update ( 'slot', $updatARr, $where );
			if ($result) {
				echo 1;
			} else {
				echo 2;
			}
		}
	}
	
	function featureCollection() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data['detailData'] = $this->Adminmodel->getTableDataDesc("feature_collection");
			$data ['activeMenu'] = 9;
			$data ['breadcrum'] = "Manage Feature Collection";
			$this->load->view ( 'admin/featurecollectionView', $data );
		} else {
			redirect ( 'Admin' );
		}
	}
	
	
	function addEditFeaturecollection($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			if ($id != '') {
				$where = "feature_collection_id=$id";
				$detailData = $this->Adminmodel->getDataById ( 'feature_collection', $where );
				$data ['detailData'] = $detailData;
				$data ['breadcrum'] =  "<a href=" . base_url() . "Admin/featureCollection>Manage Feature Collection</a>";
			} else {
				$data ['breadcrum'] = "<a href=" . base_url() . "Admin/featureCollection>Manage Feature Collection</a>";
			}
			$data ['activeMenu'] = 9;
			$this->load->view ( 'admin/addEditFeaturecollection', $data );
		} else {
			redirect('Admin');
		}
	}
	 
	
	 
	function addEditFeaturecollectionSubmit($id = NULL) {
		$sesUserId = $this->userSessionId ();
		$hid = $_POST ['id'];
		$name = $_POST ['name'];
	
		if (! empty ( $_FILES ['img'] ['name'] )) {
			$imageName = $_FILES ['img'] ['name'];
			$config ['upload_path'] = FCPATH . 'uploads/feature_collection/';
			$config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config ['max_size'] = '1024';
			$time = strtotime ( date ( 'Y-m-d H:i:s' ) );
			$config ['file_name'] = $time;
			$this->upload->initialize ( $config );
			if (! $this->upload->do_upload ( 'img' )) {
				$error = $this->upload->display_errors ();
				$data ['imgErr'] = $error;
				$failure = 1;
				$uploadedimgName = "";
			} else {
				$a = $this->upload->data ();
				$failure = 0;
				$uploadedimgName = $a ['file_name'];
			}
		} else {
			$uploadedimgName = "";
			$failure = 0;
		}
		if ($hid != '') {
			$whereName = "feature_collection_name ='$name' AND feature_collection_id != $hid";
			$duplicateName = $this->Adminmodel->getDataById('feature_collection', $whereName);
			if (! empty($duplicateName)) {
				echo 3;
	
			}else {
				$where = "feature_collection_id=$hid";
				$offerData = $this->Adminmodel->getDataById ( 'feature_collection', $where );
				$insertData = array (
						'feature_collection_name' => $name,
						'image' => ($uploadedimgName) ? $uploadedimgName : $offerData [0] ['image'],
	
				);
				$where = "feature_collection_id=$hid";
				$result = $this->Adminmodel->update ( 'feature_collection', $insertData, $where );
				if ($result) {
					echo 2;
				} else {
					echo 0;
				}
			}
		} else {
			$whereName = "feature_collection_name ='$name'";
			$duplicateName = $this->Adminmodel->getDataById('feature_collection', $whereName);
			if (! empty($duplicateName)) {
				echo 3;
			}else {
				$insertData = array (
						'feature_collection_name' => $name,
						'image' => $uploadedimgName,
						'status' => 'Active',
	
				);
				$result = $this->Adminmodel->insert ( 'feature_collection', $insertData );
				if ($result) {
					echo 1;
				} else {
					echo 0;
				}
			}
		}
		 
	}
	
	
	 
	
	function deleteFeaturecollection()
	{
		$id = $_POST['id'];
		$where = "feature_collection_id = $id";
		$res = $this->Adminmodel->delete('feature_collection', $where);
		if ($res) {
			echo 1;
		} else {
			echo 0;
		}
	}
	function changeFeaturecollectionStatus()
	{
		$id = $_POST['id'];
		$status = $_POST['status'];
		if ($id) {
			if ($status == 'In-Active') {
				$updateArr = array(
						'status' => 'Deactive'
				);
			} else {
				$updateArr = array(
						'status' => 'Active'
				);
			}
			$where = "feature_collection_id =  '$id' ";
			$result = $this->Adminmodel->update('feature_collection', $updateArr, $where);
			if ($result) {
				echo 1;
			} else {
				echo 0;
			}
		}
	}
	function manageMargin() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data['marginData'] = $this->Adminmodel->getTableDataDesc("company_margin");
			$data ['activeMenu'] = 21;
			$data ['breadcrum'] = "Manage Company Margin";
			$this->load->view ( 'admin/managemarginView', $data );
		} else {
			redirect ( 'Admin' );
		}
	}
	function addEditmargin($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			if ($id != '') {
				$where = "company_margin_id=$id";
				$marginData = $this->Adminmodel->getDataById ( 'company_margin', $where );
				$data ['marginData'] = $marginData;
				$data ['breadcrum'] =  "<a href=" . base_url() . "Admin/manageMargin>Manage Company Margin</a>";
			} else {
				$data ['breadcrum'] = "<a href=" . base_url() . "Admin/manageMargin>Manage Company Margin</a>";
			}
			$data ['activeMenu'] = 21;
			$this->load->view ( 'admin/addEditMarginView', $data );
		} else {
			redirect('Admin');
		}
	}
	function addEditMarginSubmit($id = NULL) {
		$sesUserId = $this->userSessionId ();
		$hid = $_POST ['id'];
		$margin = $_POST ['margin'];
		if ($hid != '') {
				$insertData = array (
						'company_margin' => $margin,
				);
				$where = "company_margin_id=$hid";
				$result = $this->Adminmodel->update ( 'company_margin', $insertData, $where );
				if ($result) {
					echo 2;
				} else {
					echo 0;
				}
			
		} /* else {
			$whereName = "feature_collection_name ='$name'";
			$duplicateName = $this->Adminmodel->getDataById('feature_collection', $whereName);
			if (! empty($duplicateName)) {
				echo 3;
			}else {
				$insertData = array (
						'feature_collection_name' => $name,
						'image' => $uploadedimgName,
						'status' => 'Active',
	
				);
				$result = $this->Adminmodel->insert ( 'feature_collection', $insertData );
				if ($result) {
					echo 1;
				} else {
					echo 0;
				}
			}
		} */
			
	}
	function manageCustomertype() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data['customertypeData'] = $this->Adminmodel->getTableDataDesc("customer_type");
			$data ['activeMenu'] = 22;
			$data ['breadcrum'] = "Manage Customer Type";
			$this->load->view ( 'admin/manageCustomertypeView', $data);
		} else {
			redirect ( 'Admin' );
		}
	}
	function addEditcustomertype($id = NULL) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			if ($id != '') {
				$where = "customer_type_id=$id";
				$customertypeData = $this->Adminmodel->getDataById ( 'customer_type', $where );
				$data ['customertypeData'] = $customertypeData;
				$data ['breadcrum'] =  "<a href=" . base_url() . "Admin/manageCustomertype>Manage Customer Type</a>";
			} else {
				$data ['breadcrum'] = "<a href=" . base_url() . "Admin/manageCustomertype>Manage Customer Type</a>";
			}
			$data ['activeMenu'] = 22;
			$this->load->view ( 'admin/addEditCustomertypeView', $data );
		} else {
			redirect('Admin');
		}
	}
	function addEditCustomertypeSubmit($id = NULL) {
		$sesUserId = $this->userSessionId ();
		$hid = $_POST ['id'];
		$name = $_POST ['name'];
		if ($hid != '') {
			$whereName = "customer_type_name ='$name' AND customer_type_id !=$hid";
			$duplicateName = $this->Adminmodel->getDataById('customer_type', $whereName);
			if (! empty($duplicateName)) {
				echo 3;
			}else {
			$insertData = array (
					'customer_type_name' => $name
			);
			$where = "customer_type_id=$hid";
			$result = $this->Adminmodel->update ( 'customer_type', $insertData, $where );
			if ($result) {
				echo 2;
			} else {
				echo 0;
			}
			}	
		}  else {
		$whereName = "customer_type_name ='$name'";
		$duplicateName = $this->Adminmodel->getDataById('customer_type', $whereName);
		if (! empty($duplicateName)) {
		echo 3;
		}else {
		$insertData = array (
		  'customer_type_name' => $name
		);
		$result = $this->Adminmodel->insert ( 'customer_type', $insertData );
		if ($result) {
		echo 1;
		} else {
		echo 0;
		}
		}
		}
			
	}
	/*Roushan
	 * Date:-09-01-2019
	 * Method:-customerexportdata  */
	public function customerexpoertdata() {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$fromdate = $_POST['fromdatepicker'];
			$todate = $_POST['todatepicker'];
			$fromdateformate = date("Y-m-d",strtotime($fromdate));
			$todateformate = date("Y-m-d",strtotime($todate));
			$where = "(created_date >='$fromdateformate' AND created_date <='$todateformate')";
			$customerdata = $this->Adminmodel->getDataByuserId ( 'user', $where );
			$fileName = 'data-'.time().'.xls';
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'S.No');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Hospital or Diagnostic');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Contact No.');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Password');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Package');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Affiliate Name');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Min purchase value');
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Customer Discount');
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Licence No.');
			// set Row
			$rowCount = 2;
			$i=1;
			foreach ($customerdata as $detail) {
				      $id = $detail ['user_id'];
					  $username = $detail ['user_name'];
                      $email = $detail ['email'];
                      $password = $detail ['password'];
					  $contact = $detail ['contact'];
				      $adhar = $detail ['adhar_number'];
				      $pckgid = $detail ['package_id'];
				      $where = "package_id= $pckgid";
				      $packagedata = $this->Adminmodel->getDataById ( 'package', $where );
				      $packagename=$packagedata[0]['package_name'];
				      $min_value = $detail['min_value'];
				      $package_percentage = $detail['affiliate_percentage'];
			          $agentid = $detail ['agent_id'];
				      $where = "affiliate_id= $agentid";
				      $affiliatedata = $this->Adminmodel->getDataById ( 'affiliate', $where );
				      $agentname=$affiliatedata[0]['affiliate_name'];
				      $licence = $detail ['licence_number'];
				      $cityid = $detail ['city_id'];
				      $pinid = $detail ['pincode_id'];
				      $where = "city_id= $cityid";
				      $citydata = $this->Adminmodel->getDataById ( 'city', $where );
				      $cityname=$citydata[0]['city_name'];
				      $where1 = "pin_code_id=  $pinid";
				      $pindata = $this->Adminmodel->getDataById ( 'pin_code', $where1 );
				      $piname=$pindata[0]['pin_num'];
                      $status = $detail ['status'];
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $i);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $username);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $contact);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $password);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $packagename);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $agentname);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $min_value);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $package_percentage);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $licence);
				$rowCount++;
				$i++;
			} //die;
			$object_writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
			//$objWriter = new PHPExcel_Writer_Excel2005($objPHPExcel);
			//$objWriter->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			header('Content-Disposition: attachment;filename="customerdata-'.time().'.xls"');
			$object_writer->save('php://output');
		}else{
			redirect('Admin');
		}
	}
	/*Roshan
	 * Date:-09-01-2018
	 * Method:-addtocart  */
	function addtocart($userid=null) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$data = "";
			$catStatus= "status='Active'";
			$data['categoryDropdown'] = $this->Adminmodel->getDataByuserId("category", $catStatus);
			//print_r($data['categoryDropdown']);die;
			if(!empty($_POST)){
				$product = $_POST['product'];
				$quantity = $_POST['quantity'];
				
				$k = count ($product );
				//echo $k;die;
				for($i = 0; $i < $k; $i ++) {
					$whereproduct = "product_id= $product[$i]";
					$productdata = $this->Adminmodel->getDataById ( 'product', $whereproduct );
					$price = $productdata[0]['discount_price'];
					$insertArr = array (
							'user_id' => $userid,
							'product_id' => $product[$i],
							'quantity' => $quantity[$i],
							'price' => $price
					);
					//print_r($insertArr);die;
					$result = $this->Adminmodel->insert ( 'add_to_cart', $insertArr );
					
				}
				if ($result) {
					$this->session->set_flashdata('Successfully','cart has been updated');
					header('Refresh: 2; url= '. base_url().'Admin/customer');
					
				}else {
				$this->session->set_flashdata('Error','Try again');
				header("Refresh: 5; url='Admin/customer'");
			}
			}
			$data['userid'] = $userid;
			$where = "role_id=2";
			$data ['userdata'] = $this->Adminmodel->getDataByuserId ( 'user', $where );
			$data ['heading'] = "Manage add to cart";
			$data ['activeMenu'] = 10;
			$data ['breadcrum'] = "Manage add to cart";
			$this->load->view ( 'admin/addtocart', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function editorder($orderid=null) {
		$sesUserId = $this->userSessionId ();
		if ($sesUserId) {
			$whereorderid= "unique_order_id='$orderid'";
			$orderdata = $this->Adminmodel->getDataByuserId("manage_order", $whereorderid);
			$data['orderdata']=$orderdata;
			$userid = $orderdata[0]['user_id'];
			$catStatus= "status='Active'";
			$data['categoryDropdown'] = $this->Adminmodel->getDataByuserId("category", $catStatus);
			//print_r($data['categoryDropdown']);die;
			if(!empty($_POST)){
				$product = $_POST['product'];
				//$product1 = $_POST['product1'];
				$quantity = $_POST['quantity'];
			//print_r($product);die;
				$k = count ($product );
				//echo $k;die;
				for($i = 0; $i < $k; $i ++) {
					$whereproduct = "product_id= $product[$i]";
					$productdata = $this->Adminmodel->getDataById ( 'product', $whereproduct );
					$price = $productdata[0]['discount_price'];
					$insertArr = array (
							'user_id' => $userid,
							'product_id' => $product[$i],
							'quantity' => $quantity[$i],
							'price' => $price
					);
					//echo '<pre>';
					//print_r($insertArr);
					$result = $this->Adminmodel->insert ( 'add_to_cart', $insertArr );
					//}	
				}//die;
				if ($result) {
					$insertData = array (
							'is_edit' => '1'
							 
					
					);
					$result = $this->Adminmodel->update ( 'manage_order', $insertData, $whereorderid );
					$this->session->set_flashdata('Successfully','cart has been updated');
					header('Refresh: 2; url= '. base_url().'Admin/customer');
						
				}else {
					$this->session->set_flashdata('Error','Try again');
					header("Refresh: 5; url='Admin/customer'");
				}
			}
			$data['orderid'] = $orderid;
			$where = "role_id=2";
			$data ['userdata'] = $this->Adminmodel->getDataByuserId ( 'user', $where );
			$data ['heading'] = "Edit order";
			$data ['activeMenu'] = 10;
			$data ['breadcrum'] = "Edit order";
			$this->load->view ( 'admin/editorder', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	function getsubcatbycatid() {
		$sesUserId = $this->userSessionId ();
		$catid = $_POST ['catid'];
		$where = "category_id = $catid AND status = 'Active'";
		$subData = $this->Adminmodel->getDataById ( "sub_category", $where );
		$html = "<option value=''>Select sub category</option>";
		if (! empty ( $subData )) {
			foreach ( $subData as $row ) {
				$subcatid = $row ['sub_category_id'];
				$subcatname = $row ['sub_category_name'];
				$html .= "<option value='$subcatid'>$subcatname</option>";
			}
		}
		echo $html;
	}
	function getproductbysubcatid() {
		$sesUserId = $this->userSessionId ();
		$subcatid = $_POST ['subcatid'];
		$where = "sub_category_id = $subcatid AND status = 'Active'";
		$productData = $this->Adminmodel->getDataById ( "product", $where );
		$html = "<option value=''>Select product</option>";
		if (! empty ( $productData )) {
			foreach ( $productData as $row ) {
				$productid = $row ['product_id'];
				$productname = $row ['product_name'];
				$html .= "<option value='$productid'>$productname</option>";
			}
		}
		echo $html;
	}
	function getsubcatbyorderid() {
		$sesUserId = $this->userSessionId ();
		$catid = $_POST ['catid'];
		$where = "category_id = $catid AND status = 'Active'";
		$subData = $this->Adminmodel->getDataById ( "sub_category", $where );
		$html = "<option value='0'>Select sub category</option>";
		if (! empty ( $subData )) {
			foreach ( $subData as $row ) {
				$subcatid = $row ['sub_category_id'];
				$subcatname = $row ['sub_category_name'];
				$html .= "<option value='$subcatid'>$subcatname</option>";
			}
		}
		echo $html;
	}
	function downloadfile(){
		if (isset($_GET['file'])) {
			$file = $_GET['file'];
			if (file_exists($file)) {
				header('Content-Type: application');
				header("Content-Disposition: attachment; filename=\"$file\"");
				readfile($file);
			}
		}
	}
	function deleteOrder()
	{
		 $id = $_POST['manageorderid'];
		
		$where = "manage_order_id = $id";
		$res = $this->Adminmodel->delete('manage_order', $where);
		if ($res) {
			echo 1;
		} else {
			echo 0;
		}
	}
	function getproductprice() {
		$sesUserId = $this->userSessionId ();
		$proid = $_POST ['proid'];
		$where = "product_id = $proid AND status = 'Active'";
		$productData = $this->Adminmodel->getDataById ( "product", $where );
		$html = "";
		if (! empty ( $productData )) {
			foreach ( $productData as $row ) {
				$productid = $row ['product_id'];
				$productname = $row ['product_name'];
				
				$price = $row['discount_price'];
				$costprice = $row['cost_price'];
				$margin = number_format(100*($price - $costprice)/$price,2);
				$html .= "$price,$costprice,$margin";
			}
		}
		echo $html;
	}
    function logout()
    {
        $this->session->unset_userdata('adminData');
        header("location:" . base_url('Admin') . "");
    }
    
}
?>