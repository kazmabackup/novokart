<?php
defined('BASEPATH') or exit('No direct script access allowed');
include ('application/libraries/REST_Controller.php');

class Webservice extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        // load the pdo for db connection
        $this->pdo = $this->load->database('pdo', true);
        // define ( "SERVER_KEY", "AIzaSyDXDQONXkttFAU48uPl6QcL9EEakZXcpDo" );
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->helper('url');
        // load the models
        $this->load->model('Webservicemodel');
        include ('application/libraries/phpmailer/sendEmail.php');
        date_default_timezone_set("Asia/Calcutta");
    }
   

    function registration_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $name = $this->input->post('name');
		//print_r($name);die;
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $city_id = $this->input->post('city_id');
        $pincode_id = $this->input->post('pincode_id');
        $adhar = $this->input->post('kyc');
	    $licence = $this->input->post('licence');
        if (! $name  || ! $password || ! $phone || ! $city_id || ! $pincode_id || ! $licence) {
            $resArr['message'] = "Please pass all parameters";
        } else {
           $whereEmailNumber = "email ='$email' AND contact ='$phone'";
           $duplicateEmailNumber = $this->Webservicemodel->getDataById ( 'user', $whereEmailNumber );
           $whereemail = "email ='$email'";
           $duplicateEmail = $this->Webservicemodel->getDataById ( 'user', $whereemail );
           $wheremobile = "contact ='$phone'";
           $duplicateNumber = $this->Webservicemodel->getDataById ( 'user', $wheremobile );
            if (! empty ( $duplicateEmailNumber )) {
                $resArr ['result'] = 0;
                $resArr ['message'] = "A user with this email and mobile has already exists";
            } 
            else if (! empty ( $duplicateEmail )) {
                $resArr ['result'] = 0;
                $resArr ['message'] = "A user with this email has already exists";
            } else if (! empty ( $duplicateNumber )) {
                $resArr ['result'] = 0;
                $resArr ['message'] = "A user with this mobile no has already exists";
            } else {
            	$uniqueid = mt_rand(100000, 999999);
                $insertArr = array(
                    'user_name' => $name,
                    'email' => $email,
                    'contact' => $phone,
                    'password' => $password,
                    'role_id' => 2,
                	'unique_id' => $uniqueid,
                    'city_id' => $city_id,
                    'pincode_id' => $pincode_id,
                    'adhar_number' => $adhar,
					'licence_number' => $licence,
                	'created_date' => date("Y-m-d"),
                	'status' =>'Active'
                );
                $result = $this->Webservicemodel->insert('user', $insertArr);
                if ($result) {
                    $resArr['result'] = 1;
                    $resArr['message'] = "You have registered successfully";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed to registered. Please try again";
                }
            }
        }
        echo $this->response($resArr, 200);
    }
	function city_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
			$whereemail = "status = 'Active'";
			$citydata = $this->Webservicemodel->getDataById ( 'city', $whereemail );
			//print_r($citydata);die;
			if (! empty ( $citydata )) {
				foreach ( $citydata as $city ) {
					$id= $city ['city_id'];
					$listdata1 ['city_id'] = $city ['city_id'];
					$listdata1 ['city_name'] = $city ['city_name'];
					$wherepin = "city_id = $id AND status='Active'";
			$pindata = $this->Webservicemodel->getDataById ( 'pin_code', $wherepin );
			if (! empty ( $pindata )) {
				foreach ( $pindata as $pin ) {
					$listdata2 ['pin_id'] = $pin ['pin_code_id'];
					$listdata2 ['pin_num'] = $pin ['pin_num'];
					$listdata1 ['pindata'] [] = $listdata2;
					$listdata2 = "";
				}
			}else{
				$listdata0 ['result'] = 0;
				$listdata1 ['pindata']  = array();
			}
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['data'] [] = $listdata1;
					$listdata1 = "";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "No data found on server";
			}
		
		echo $this->response ( $resArr, 200 );
	}
	function login_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		 $email = $this->input->post ( 'contact' );
		 $password = $this->input->post ( 'password' );
		if (!$email || !$password) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "password = '$password' And (user_name='$email' OR contact='$email') ";
			$result = $this->Webservicemodel->getDataById ( 'user', $where );
			// print_r($result);die;
			if ($result) {
				$userid = $result [0] ['user_id'];
				$listData ['user_id'] = $result [0] ['user_id'];
			    $listData ['name'] = $result [0] ['user_name']; 
				$listData ['email'] = $result [0] ['email'];
				$listData ['mobile'] = $result [0] ['contact'];
				$listData ['city_id'] = $result [0] ['city_id'];
				$listData ['pin_id'] = $result [0] ['pincode_id'];
				$listData ['licence_number'] = $result [0] ['licence_number'];
				$listData ['adhar_number'] = $result [0] ['adhar_number'];
				$whereuser = "user_id = '$userid'";
				$itemdata = $this->Webservicemodel->getDataById ( 'add_to_cart', $whereuser );
				$itemcount = count($itemdata);
				if (!empty($itemdata)){
					$listData ['add_to_cart_item_count'] = $itemcount;
				} else {
					$listData ['add_to_cart_item_count'] = 0;
				}
				$city_id=$result [0] ['city_id'];
				$wherecity = "city_id = '$city_id'";
				$citydata = $this->Webservicemodel->getDataById ( 'city', $wherecity );
				$listData ['city_name'] = $citydata [0] ['city_name'];
				$pin_id=$result [0] ['pincode_id'];
				$wherepin = "pin_code_id = '$pin_id'";
				$pindata = $this->Webservicemodel->getDataById ( 'pin_code', $wherepin );
				$listData ['pin_num'] = $pindata [0] ['pin_num'];
				$resArr ['result'] = 1;
				$resArr ['message'] = "success";
				$resArr ['data'] [] = $listData;
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Invalid Credential";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
   
   function category_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$where = "status ='Active'";
		$buyerdata = $this->Webservicemodel->getDataById ( 'category',$where );
		if ($buyerdata) {
			foreach ( $buyerdata as $list ) {
				$catid = $list ['category_id'];
				$listData ['category_id'] = $catid;
				$listData ['category_name'] = $list ['category_name'];
				$listData ['category_image'] = base_url () . 'uploads/category/' . $list ['image'];
				$resArr ['result'] = 1;
				$resArr ['message'] = "Success.";
				$resArr ['Data'] [] = $listData;
			}
		} else {
			$resArr ['result'] = 0;
			$resArr ["message"] = "No Data found on server.";
		}
		echo $this->response ( $resArr, 200 );
	}
	 function banner_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$where = "status ='Active'";
		$buyerdata = $this->Webservicemodel->getDataById ( 'banner',$where );
		if ($buyerdata) {
			foreach ( $buyerdata as $list ) {
				$catid = $list ['banner_id'];
				$listData ['banner_id'] = $catid;
				$listData ['title'] = $list ['title'];
				$listData ['image'] = base_url () . 'uploads/banner/' . $list ['image'];
				$resArr ['result'] = 1;
				$resArr ['message'] = "Success.";
				$resArr ['Data'] [] = $listData;
			}
		} else {
			$resArr ['result'] = 0;
			$resArr ["message"] = "No Data found on server.";
		}
		echo $this->response ( $resArr, 200 );
	}
		function category_search_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$searchstring = $this->input->post ( 'search' );
		if (! $searchstring) {
			$resArr ["message"] = "Please pass search string";
		} else {
			$where = "category_name like '$searchstring%'";
			$productData = $this->Webservicemodel->getDataById ( 'category', $where );
			if ($productData) {
			foreach ( $productData as $list ) {
				$listData ['category_id'] =$list ['category_id'];
				$listData ['category_name'] =$list ['category_name'];
				$listData ['category_image'] = base_url () . 'uploads/category/' . $list ['image'];
				
				$resArr ['result'] = 1;
				$resArr ['message'] = "Success.";
				$resArr ['Data'] [] = $listData;
				$listData = "";
			}
		} else {
				$resArr ['result'] = 0;
				$resArr ["message"] = "No Data found on server.";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
		function sub_category_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$category_id = $this->input->post ( 'category_id' );
		$user_id = $this->input->post ( 'user_id' );
		if (! $category_id || ! $user_id) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "category_id = $category_id";
			$buyerdata = $this->Webservicemodel->getDataById ( 'sub_category', $where );
			if ($buyerdata) {
				foreach ( $buyerdata as $list ) {
					$listData ['category_id'] = $category_id;
					$subcatid = $list ['sub_category_id'];
					$listData ['sub_category_id'] = $subcatid;
					$listData ['sub_category_name'] = $list ['sub_category_name'];
				    $listData ['sub_category_image'] = base_url () . 'uploads/subcategory/' . $list ['image'];
				    $where = "category_id = $category_id AND sub_category_id = $subcatid ";
				    $productdata = $this->Webservicemodel->getDataById ( 'product', $where );
				    if ($productdata){
				    	$whereuserid = "user_id = $user_id";
				    	$userdata = $this->Webservicemodel->getDataById ( 'user', $whereuserid );
				    	$packageid = $userdata[0]['package_id'];
				    	$package_percentage = $userdata[0]['package_percentage'];
				    	foreach ($productdata as $list){
				    		$catid = $list ['product_id'];
					        $productlistData ['pro_id'] =$list ['product_id'];
					        $productlistData ['pro_name'] =$list ['product_name'];
					        $productlistData ['description'] = strip_tags(($list ['description']));
					       
					       /*  if ($package_percentage !=""){
					        	$wherepackageid = "package_id = $packageid";
					        	$packagedata = $this->Webservicemodel->getDataById ( 'package', $wherepackageid );
					        	$actualdicamount = ($list['discount_price'] * $package_percentage)/100;
					        	$productlistData ['price'] = $list ['actual_price'];
					        	$discount_price = $list['discount_price'] - $actualdicamount;
					        	$productlistData ['discount_price'] = round($discount_price);
					        	$productlistData ['discount_percent'] = round(($discount_price*100)/$list ['actual_price']);
					        } else { */
					        $productlistData ['price'] = $list ['actual_price'];
					        $productlistData ['discount_price'] = round($list['discount_price']);
					        $productlistData ['cost_price'] = round($list['cost_price']);
					        $productlistData ['discount_percent'] = round(($list['discount_price']*100)/$list ['actual_price']);
					        
					       
					        $productlistData ['quantity'] = $list ['quantity'];
					        $productlistData ['stock_quantity'] = $list ['quantity']-$list ['sold_quantity'];
					        $stockquantity=$list ['quantity']-$list ['sold_quantity'];
					if($stockquantity==0){
						$productlistData ['availablity'] = "Out of stock";
					}else{
						$productlistData ['availablity'] = "In stock";
					}
					$productlistData ['image'] = base_url () . 'Uploads/product/' . $list ['image'];
					$listData ['product_list'] [] = $productlistData;
					$productlistData = "";
				    	}
				    } else {
				    	$listData ['product_list']  = array();
				    }
					$resArr ['result'] = 1;
					$resArr ['message'] = "Success.";
					$resArr ['Data'] [] = $listData;
					$listData = "";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ["message"] = "No Data found on server.";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function product_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$category_id = $this->input->post ( 'category_id' );
		$sub_cat_id = $this->input->post ( 'sub_category_id' );
		if (! $category_id || ! $sub_cat_id) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "category_id = $category_id AND sub_category_id = $sub_cat_id ";
			$productdata = $this->Webservicemodel->getDataById ( 'product', $where );
		if ($productdata) {
			foreach ( $productdata as $list ) {
				$catid = $list ['product_id'];
				$listData ['pro_id'] =$list ['product_id'];
				$listData ['pro_name'] =$list ['product_name'];
					$listData ['description'] = strip_tags($list ['description']);
					$listData ['quantity'] = $list ['quantity'];
					$listData ['stock_quantity'] = $list ['quantity']-$list ['sold_quantity'];
					$stockquantity=$list ['quantity']-$list ['sold_quantity'];
					if($stockquantity==0){
						$listData ['availablity'] = "Out of stock";
					}else{
						$listData ['availablity'] = "In stock";
					}
						$listData ['image'] = base_url () . 'uploads/product/' . $list ['image'];
				$wherep = "product_id= $catid";
				$productPriceData = $this->Webservicemodel->getDataById ( 'product_price_weight', $wherep );
				if (! empty ( $productPriceData )) {
					foreach ( $productPriceData as $price ) {
						$productPriceid = $price ['product_price_weight_id'];
						$priceList ['product_weight_id'] = $productPriceid;
						$priceList ['pro_price'] =number_format($price ['actual_price'], 2, '.', '');
						$priceList ['dis_price'] =number_format($price ['dis_price'], 2, '.', '');
						$prodiscount=($price ['actual_price']-$price ['dis_price'])/$price ['actual_price'];
					    $prodiscountpercent=($prodiscount)*100;
					    $var = number_format($prodiscountpercent, 2, '.', '');
					    $priceList ['discount_percent'] = $var;
						$priceList ['size'] = $price ['weight'];
						$listData ['price_list'] [] = $priceList;
						$priceList = "";
					}
				}
				else{
				$listData0 ['result'] = 0;
				$listData ['price_list']  = array();
			}
				$resArr ['result'] = 1;
				$resArr ['message'] = "Success.";
				$resArr ['Data'] [] = $listData;
				$listData = "";
			}
		} else {
			$resArr ['result'] = 0;
			$resArr ["message"] = "No Data found on server.";
		   }
		}
		echo $this->response ( $resArr, 200 );
	}
	function my_profile_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		 $userid = $this->input->post ( 'user_id' );
		if (!$userid ) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "user_id = $userid ";
			$result = $this->Webservicemodel->getDataById ( 'user', $where );
			if ($result) {
				$userid = $result [0] ['user_id'];
				$listData ['user_id'] = $result [0] ['user_id'];
			    $listData ['name'] = $result [0] ['user_name'] ;
				$listData ['email'] = $result [0] ['email'];
			    $listData ['mobile'] = $result [0] ['contact'];
				$listData ['city_id'] = $result [0] ['city_id'];
				$listData ['pin_id'] = $result [0] ['pincode_id'];
				$city_id=$result [0] ['city_id'];
				$wherecity = "city_id = '$city_id'";
				$citydata = $this->Webservicemodel->getDataById ( 'city', $wherecity );
				$listData ['city_name'] = $citydata [0] ['city_name'];
				$pin_id=$result [0] ['pincode_id'];
				$wherepin = "pin_code_id = '$pin_id'";
				$pindata = $this->Webservicemodel->getDataById ( 'pin_code', $wherepin );
				$listData ['pin_num'] = $pindata [0] ['pin_num'];
				$resArr ['result'] = 1;
				$resArr ['message'] = "success";
				$resArr ['data'] [] = $listData;
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Successfull";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	/*Roushan
	 * Date:-21/11/2018  */
	function update_profile_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		$user_name = $this->input->post('user_name');
		$email = $this->input->post('email');
		$contact = $this->input->post('contact');
		$city_id = $this->input->post('city_id');
		$pincode_id = $this->input->post('pincode_id');
		$adhar = $this->input->post('adhar_number');
		$licence = $this->input->post('licence_number');
		if (!$userid || ! $user_name || ! $email || ! $contact || ! $city_id || ! $pincode_id || ! $adhar || ! $licence) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			 $insertArr = array(
                    'user_name' => $user_name,
                    'email' => $email,
                    'contact' => $contact,
                    'city_id' => $city_id,
                    'pincode_id' => $pincode_id,
                    'adhar_number' => $adhar,
					'licence_number' => $licence,
                );
			$where = "user_id=$userid";
			$result = $this->Webservicemodel->update ( 'user', $insertArr, $where );
			if ($result) {
				$resArr ['result'] = 1;
				$resArr ['message'] = "Your profile has been updated successfully";
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Your profile has not been updated successfully";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function change_password_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		$password = $this->input->post ( 'password' );
		if (! $userid || ! $password) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$insertArr = array (
					'password' => $password 
			);
			$where = "user_id=$userid";
			$result = $this->Webservicemodel->update ( 'user', $insertArr, $where );
			if ($result) {
				$resArr ['result'] = 1;
				$resArr ['message'] = "Successfully updated password";
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "not successfully updated password";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function forgot_password_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$email = $this->input->post ( 'contact' );
		if (! $email) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$whereuser = " contact = '$email'";
			$userDetails = $this->Webservicemodel->getDataById ( 'user', $whereuser );
			if (! empty ( $userDetails )) {
				$uniqueid = substr ( uniqid (), 1, 5 );
				$insertArr = array (
						'password' => $uniqueid 
				);
				$where = "contact = '$email'";
				$result = $this->Webservicemodel->update ( 'user', $insertArr, $where );
				if ($result) {
				$pdata = new StdClass ();
				$pdata->number = $email;
				$pdata->message = "Novokart new password: $uniqueid";
				$message [] = $pdata;
				$isSent = $this->Webservicemodel->textMessageIndia ( $message );
				$resArr ['result'] = 1;
				$resArr ['message'] = "Password send to  your number";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "This mobile no is not registerd with us";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	
	/* Roushan
	 * date :- 19/11/2018 */
	function product_search_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$searchstring = $this->input->post ( 'search' );
		$user_id = $this->input->post ( 'user_id' );
		if (! $searchstring || ! $user_id) {
			$resArr ["message"] = "Please pass search string";
		} else {
			$where = "product_name like '$searchstring%'";
			$productData = $this->Webservicemodel->getDataById ( 'product', $where );
			if ($productData) {
				$whereuserid = "user_id = $user_id";
				$userdata = $this->Webservicemodel->getDataById ( 'user', $whereuserid );
				$packageid = $userdata[0]['package_id'];
				$package_percentage = $userdata[0]['package_percentage'];
				foreach ( $productData as $list ) {
					$listData ['category_id'] = $list ['category_id'];
					$listData ['sub_category_id'] = $list ['sub_category_id'];
					$listData ['pro_id'] =$list ['product_id'];
					$listData ['pro_name'] =$list ['product_name'];
					$listData ['description'] = strip_tags($list ['description']);
					/* if ($package_percentage !=""){
						$wherepackageid = "package_id = $packageid";
						$packagedata = $this->Webservicemodel->getDataById ( 'package', $wherepackageid );
						//$package_percentage = $packagedata[0]['package_percentage'];
						$actualdicamount = ($list['discount_price'] * $package_percentage)/100;
						$productlistData ['price'] = $list ['actual_price'];
						$discount_price = $list['discount_price'];// - $actualdicamount;
						$productlistData ['discount_price'] = round($discount_price);
						$productlistData ['discount_percent'] = round(($discount_price*100)/$list ['actual_price']);
					} else { */
						$productlistData ['price'] = $list ['actual_price'];
						$productlistData ['discount_price'] = round($list['discount_price']);
						$productlistData ['cost_price'] = round($list['cost_price']);
						$productlistData ['discount_percent'] = round(($list['discount_price']*100)/$list ['actual_price']);
					
					$listData ['quantity'] = $list ['quantity'];
					$listData ['stock_quantity'] = $list ['quantity']-$list ['sold_quantity'];
					$stockquantity=$list ['quantity']-$list ['sold_quantity'];
					if($stockquantity==0){
						$listData ['availablity'] = "Out of stock";
					}else{
						$listData ['availablity'] = "In stock";
					}
					$listData ['image'] = base_url () . 'Upload/' . $list ['image'];
				
					
					$resArr ['result'] = 1;
					$resArr ['message'] = "Success.";
					$resArr ['Data'] [] = $listData;
					$listData = "";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ["message"] = "No Data found on server.";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	/* Roushan
	 * date - 21/11/2018 */
	function addto_cart_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$customerid = $this->input->post ( 'user_id' );
		$product_id = $this->input->post ( 'product_id' );
		$quantity = $this->input->post ( 'quantity' );
		if (! $customerid||! $product_id||! $quantity) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$wherecart = "user_id = $customerid AND product_id=$product_id";
			$cartduplicatedata = $this->Webservicemodel->getDataById ( 'add_to_cart', $wherecart );
			$wherepro = "product_id=$product_id";
			$pricedata = $this->Webservicemodel->getDataById ( 'product',$wherepro );
			$totalPrice = $pricedata[0]['discount_price'];
			if($cartduplicatedata){
				$resArr ['result'] = 0;
				$resArr ['message'] = "This Product Already exist";
			}else{
					
				$insertArr = array (
						'user_id' => $customerid,
						'product_id' => $product_id,
						'quantity' => $quantity,
						'price' => $totalPrice
				);
				$result = $this->Webservicemodel->insert ( 'add_to_cart', $insertArr );
				if ($result) {
					$resArr ['result'] = 1;
					$resArr ['message'] = "Your cart has been  Added successfully ";
				} else {
					$resArr ['result'] = 0;
					$resArr ['message'] = "Cart not successfully Added";
				}
	
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function update_cart_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$cartid = $this->input->post ( 'cart_id' );
		$quantity = $this->input->post ( 'quantity' );
		if (! $cartid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$wherecart = "add_to_cart_id = $cartid ";
			$cartdata = $this->Webservicemodel->getDataById ( 'add_to_cart', $wherecart );
			$userid=$cartdata[0]['user_id'];
			$productid=$cartdata[0]['product_id'];
			$wherepro = "product_id=$productid";
			$pricedata = $this->Webservicemodel->getDataById ( 'product',$wherepro );
			$totalPrice=$pricedata[0]['actual_price'];
			$wherecart1 = "user_id = $userid AND product_id=$productid";
			$cartduplicatedata = $this->Webservicemodel->getDataById ( 'add_to_cart', $wherecart1 );
			$insertArr = array (
					'quantity' => ($quantity) ? $quantity : $cartdata[0]['quantity'],
			);
			$whereup = "add_to_cart_id=$cartid";
			$result = $this->Webservicemodel->update ( 'add_to_cart', $insertArr, $whereup );
			if ($result) {
				$cartwhere = "user_id = $userid";
				$cartdata = $this->Webservicemodel->getDataById ( 'add_to_cart', $cartwhere );
				$userprofiledata = $this->Webservicemodel->getDataById ( 'user', $cartwhere );
				$package_percentage = $userprofiledata[0]['package_percentage'];
				if ($cartdata) {
					foreach ( $cartdata as $product ) {
						$listData ['add_to_cart_id'] = $product ['add_to_cart_id'];
						$listData ['pro_id'] = $product ['product_id'];
						$pid =$product ['product_id'];
						$wherep = "product_id = $pid";
						$proquandata = $this->Webservicemodel->getDataById ( 'product', $wherep );
						$listData ['pro_name'] = $proquandata[0] ['product_name'];
						//$listData ['stock_quantity'] = $proquandata[0] ['quantity'];
			           $listData ['stock_quantity'] = $proquandata[0] ['quantity'] - $proquandata[0] ['sold_quantity'];
					   $stockquantity = $proquandata[0] ['quantity'] - $proquandata[0] ['sold_quantity'];
						if($stockquantity==0){
							$listData ['availablity'] = "Out of stock";
						}else{
							$listData ['availablity'] = "In stock";
						}
			         $listData ['image'] = base_url () . 'uploads/product/' . $proquandata[0] ['image'];
			         $listData ['quantity'] = $product ['quantity'];
			         $quan= $product ['quantity'];
					 $disprice=$proquandata[0] ['discount_price'];
					 $listData ['price'] = ($disprice);
					 $listData ['quantity_price'] = (($disprice) *$quan);
						
						$resArr ['Data'] [] = $listData;
						$resArr ['result'] = 1;
						$resArr ['message'] = "Your cart has been  update successfully ";
					}
				}
			}else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Cart not successfully Updated";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function delete_cart_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$cartid = $this->input->post ( 'cart_id' );
		$userid = $this->input->post ( 'user_id' );
		if (!$cartid || !$userid ) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$wherecart_id = "add_to_cart_id=$cartid";
			$cartdata = $this->Webservicemodel->getDataById ( 'add_to_cart', $wherecart_id );
			if (!empty($cartdata)){
			$result= $this->Webservicemodel->delete ( 'add_to_cart', $wherecart_id );
			if ($result) {
				$userwhere = "user_id = $userid";
				$userdata = $this->Webservicemodel->getDataById ( 'add_to_cart', $userwhere );
				$userprofiledata = $this->Webservicemodel->getDataById ( 'user', $userwhere );
				//$package_percentage = $userprofiledata[0]['package_percentage'];
				if ($userdata) {
					foreach ( $userdata as $product ) {
						$listData ['add_to_cart_id'] = $product ['add_to_cart_id'];
						$listData ['product_id'] = $product ['product_id'];
						$pid =$product ['product_id'];
						$wherep = "product_id = $pid";
						$proquandata = $this->Webservicemodel->getDataById ( 'product', $wherep );
						$listData ['pro_name'] = $proquandata[0] ['product_name'];
						$listData ['stock_quantity'] = $proquandata[0] ['quantity']-$proquandata[0] ['sold_quantity'];
			            $listData ['image'] = base_url () . 'uploads/product/' . $proquandata[0] ['image'];
			            $listData ['quantity'] = $product ['quantity'];
			            $listData ['price'] = $product ['price'];
			            $listData ['discount_price'] = number_format($proquandata[0] ['discount_price'],2);
			            $listData ['discount_percent'] = round(($proquandata[0]['discount_price']*100)/$proquandata[0]['actual_price']);
						$resArr ['Data'] [] = $listData;
						
					}
					$resArr ['result'] = 1;
					$resArr ['message'] = "your cart has been deleted";
				} else {
					$resArr ['result'] = 1;
					$resArr ['message'] = "your cart is empty";
				}
			}else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Try again";
			}
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "data not found";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	
	function mycart_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$user_id = $this->input->post ( 'user_id' );
		if (! $user_id) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "user_id = $user_id";
			$cartdata = $this->Webservicemodel->getDataById ( 'add_to_cart', $where );
			if ($cartdata) {
				foreach ( $cartdata as $list ) {
					$listData ['pro_id'] = $list ['product_id'];
					$listData ['add_to_cart_id'] = $list ['add_to_cart_id'];
					$proid = $list ['product_id'];
					$where1 = "product_id = $proid";
					$prodata = $this->Webservicemodel->getDataById ( 'product', $where1 );
					$userwhere ="user_id = '$user_id'";
					$userprofiledata = $this->Webservicemodel->getDataById ( 'user', $userwhere );
					//$package_percentage = $userprofiledata[0]['package_percentage'];
					$listData ['pro_name'] = $prodata[0] ['product_name'];
					$listData ['image'] = base_url () . 'uploads/product/' . $prodata[0] ['image'];
					$listData ['quantity'] = $list ['quantity'];
					$listData ['stock_quantity'] = $prodata[0] ['quantity']-$prodata[0] ['sold_quantity'];
					$stockquantity=$prodata[0] ['quantity']-$prodata[0] ['sold_quantity'];
					if($stockquantity==0){
						$listData ['availablity'] = "Out of stock";
					}else{
						$listData ['availablity'] = "In stock";
					}
					$disprice=$prodata[0] ['discount_price'];
					
						$listData ['price'] = ($disprice);
					$resArr ['result'] = 1;
					$resArr ['message'] = "Success.";
					$resArr ['Data'] [] = $listData;
					$listData = "";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ["message"] = "No Data found on server.";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function affiliateid_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		$affiliate_id = $this->input->post ( 'affiliate_id' );
		if (!$userid || !$affiliate_id) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "user_id = '$userid'";
			$userData = $this->Webservicemodel->getDataById ( 'user', $where );
			$algorithm = $userData[0]['algorithm'];
			$affiliate_percentage = $userData[0]['package_percentage'];
		    $affdis  = $userData[0]['affiliate_percentage'];
		    $marginData = $this->Webservicemodel->getTableData("company_margin");
		    $company_margin = $marginData[0]['company_margin'];
			if ($userData) {
				$agent_id = $userData [0] ['agent_id'];
				$whereaffiliate = "unique_id='$affiliate_id'";
				$affilateData = $this->Webservicemodel->getDataById ( 'affiliate', $whereaffiliate );
				//print_r($affilateData);die;
				$agents_id = $affilateData [0] ['affiliate_id'];
				if ($agent_id==$agents_id){
					$wherecart = "user_id='$userid'";
					$cartData = $this->Webservicemodel->getDataById("add_to_cart",$wherecart);
					$partnersaving = 0;
					$total_cost_price = 0;
					$total_discount_price = 0;
					if ($cartData){
					foreach ($cartData as $cart){
						$cartid = $cart['add_to_cart_id'];
						$productid = $cart['product_id'];
						$userid = $cart['user_id'];
						$quantity = $cart['quantity'];
						
						$wherepro = "product_id='$productid'";
						$productData = $this->Webservicemodel->getDataById("product",$wherepro);
						//print_r($productData);
						$productname = $productData[0]['product_name'];
						$productimage =  $productData[0]['image'];
						$actualprice =  $productData[0]['actual_price'];
						$discountprice =  $productData[0]['discount_price'];
						$total_discount_price = $total_discount_price + ($discountprice * $quantity);
						$costprice = $productData[0]['cost_price'];
						$total_cost_price = $total_cost_price + ($costprice * $quantity);
						 
						
					}
				      $commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
                       $company_profit = ($company_margin/100);
                       if ($algorithm=='1'){
                       $package_per = (($commulative_per/100)*1)-($affdis/100);
                       if ($package_per > $company_profit){
                       	$new_total_dis = ($total_discount_price - $total_cost_price)*$affiliate_percentage/100;
                       }else {
                       	$new_total_dis = (($total_discount_price - $total_cost_price)*$affiliate_percentage/100)-$company_margin/100;
                       }
                       $hostital_dis = ($new_total_dis*$affdis/100);
                       $hostital_dis_per = ($hostital_dis/$total_discount_price)*100;
                       $affiliate_earning = $new_total_dis - $hostital_dis;
                       $listData ['name'] = $affilateData [0] ['affiliate_name'];
                       $listData ['email'] = $affilateData [0] ['email'];
                       $listData ['mobile'] = $affilateData [0] ['phone'];
                       //$listData ['affliate_percentage'] = $affilateData [0] ['discount'];
                       $listData ['discount_percentage'] = number_format($hostital_dis_per,2);
                       $resArr ['result'] = 1;
                       $resArr ['message'] = "success";
                       $resArr ['data'] [] = $listData;
                       } else {
                       	$package_per = ($commulative_per - $company_margin);
                       	if ($package_per > $affiliate_percentage){
                       		$new_total_dis = ($total_discount_price)*$affiliate_percentage/100;
                       		$hostital_dis = ($new_total_dis*$affdis/100);
                       		$hostital_dis_per = ($hostital_dis/$total_discount_price)*100;
                       		$affiliate_earning = $new_total_dis - $hostital_dis;
                       		$listData ['name'] = $affilateData [0] ['affiliate_name'];
                       		$listData ['email'] = $affilateData [0] ['email'];
                       		$listData ['mobile'] = $affilateData [0] ['phone'];
                       		//$listData ['affliate_percentage'] = $affilateData [0] ['discount'];
                       		$listData ['discount_percentage'] = number_format($hostital_dis_per,2);
                       		$resArr ['result'] = 1;
                       		$resArr ['message'] = "success";
                       		$resArr ['data'] [] = $listData;
                       	} else {//echo "ok";die;
                       		$resArr ['result'] = 0;
						    $resArr ['message'] = "CALL CUSTOMER CARE FOR MORE DETAILS";
                       	}
                       }
					} else {
						$resArr ['result'] = 0;
						$resArr ['message'] = "Cart is empty";
					}
				} else{
					$resArr ['result'] = 0;
					$resArr ['message'] = "Invalid affiliate id";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Invalid user id";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	/*
	 * @Author Roushan
	 * Date 23-11-2018
	 * Method Name: send_otp_post
	 * Logic: Validating all input field then sending otp
	 * o/p: To display Success message
	 * TODO:
	 */
	
	function send_otp_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		if (! $userid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$whereuserid = "user_id ='$userid'";
			$userData = $this->Webservicemodel->getDataById ( 'user', $whereuserid );
		     $contact = $userData[0]['contact'];
				$uniqueid = mt_rand(1000, 9999);
				//  $uniqueid = substr ( rand (), 1, 5 );
				$pdata = new StdClass();
				$pdata->number = $contact;
				//  $pdata->number = $mobile;
				$pdata->message = "Novokart OTP: $uniqueid";
				$message[] = $pdata;
				$sent = $this->Webservicemodel->textMessageIndia($message);
				if ($sent == "Fail") {
					$resArr ['result'] = 0;
					$resArr ['message'] = "Please try again";
				} else {
					$resArr ['result'] = 1;
					$resArr ['message'] = "Your OTP is sent on your Number";
					$resArr ['otp'] = "$uniqueid";
				}
			
		}
		echo $this->response ( $resArr, 200 );
	}
	function delivery_address_post()
	{
		$resArr['result'] = 0;
		$resArr['message'] = "";
		$user_id = $this->input->post('user_id');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$landmark = $this->input->post('landmark');
		$pincode = $this->input->post('pincode');
		$city = $this->input->post('city');
		if (! $user_id || ! $name  || ! $address || ! $landmark || ! $pincode || ! $city) {
			$resArr['message'] = "Please pass all parameters";
		} else {
				$insertArr = array(
						'name' => $name,
						'address' => $address,
						'landmark' => $landmark,
						'pincode' => $pincode,
						'user_id' => $user_id,
						'city' => $city,
				);
				$result = $this->Webservicemodel->insert('delivery_address', $insertArr);
				if ($result) {
					$where = "user_id = '$user_id'";
					$addressData = $this->Webservicemodel->getDataById ( 'delivery_address', $where );
					$listData ['delivery_address_id'] = $addressData[0]['delivery_address_id'];
					$listData ['name'] = $addressData[0]['name'];
					$listData ['address'] = $addressData[0]['address'];
					$listData ['landmark'] = $addressData[0]['landmark'];
					$listData ['pincode'] = $addressData[0]['pincode'];
					$listData ['city'] = $addressData[0]['city'];
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['data'] [] = $listData;
					$resArr['result'] = 1;
					$resArr['message'] = "Address has added successfully";
				} else {
					$resArr['result'] = 0;
					$resArr['message'] = "Failed to added. Please try again";
				}
			
		}
		echo $this->response($resArr, 200);
	}
	function delivery_address_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		if (!$userid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "user_id = '$userid'";
			$addressData = $this->Webservicemodel->getDataById ( 'delivery_address', $where );
				
			if ($addressData) {
					$listData ['delivery_address_id'] = $addressData[0]['delivery_address_id'];
					$listData ['name'] = $addressData[0]['name'];
					$listData ['address'] = $addressData[0]['address'];
					$listData ['landmark'] = $addressData[0]['landmark'];
					$listData ['pincode'] = $addressData[0]['pincode'];
					$listData ['city'] = $addressData[0]['city'];
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['data'] [] = $listData;
				
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "No data found on server";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function slot_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
			$where = "status = 'Active'";
			$slotData = $this->Webservicemodel->getDataById ( 'slot', $where );
			if ($slotData) {
				foreach ($slotData as $slot){
					$listData ['slot_id'] = $slot['slot_id'];
					$listData ['start_time'] = $slot['start_time'];
					$listData ['end_time'] = $slot['end_time'];
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['data'] [] = $listData;
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "No data found on server";
			}
		
		echo $this->response ( $resArr, 200 );
	}
	function minmunpurchase_amount_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		if (!$userid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
		$where = "user_id = '$userid'";
		$userData = $this->Webservicemodel->getDataById ( 'user', $where );
		$min_value = $userData[0]['min_value'];
		if ($min_value !=""){
			$listData ['package_id'] = $userData[0]['package_id'];
			$listData ['min_purchase_amount'] = $min_value;
			
		}else {
			$listData ['package_id'] = 0;
			$listData ['min_purchase_amount'] = 0;
		}
		$resArr ['result'] = 1;
		$resArr ['message'] = "success";
		$resArr ['data'] [] = $listData;
		}
		echo $this->response ( $resArr, 200 );
	}
	/* Roushan
	 * Date:-28-11-2018 */
	function order_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$user_id = $this->input->post ( 'user_id' );
		$slot_id = $this->input->post ( 'slot_id' );
		$payment_mode = $this->input->post ( 'paymentMode' );
		$locationid = $this->input->post ( 'location_id' );
		$address = $this->input->post ( 'address_id' );
		$productid = $this->input->post ( 'product_id' );
		$productquan = $this->input->post ( 'quantity' );
		$paytype = $this->input->post ( 'type' );
		//$delivercharge = $this->input->post ( 'delivery_charge' );
		if (!$user_id||!$slot_id||!$payment_mode||!$address ||!$locationid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$whereuser = "user_id= $user_id";
			$useragentData = $this->Webservicemodel->getDataById ( 'user', $whereuser );
			$algorithm = $useragentData[0]['algorithm'];
			$agent_id = $useragentData[0]['agent_id'];
			$discount_percentage = $useragentData[0]['package_percentage'];
			$affiliate_percentage = $useragentData[0]['affiliate_percentage'];
			if ($paytype=="cart") {
				$where = "user_id= $user_id";
				$userData = $this->Webservicemodel->getDataById ( 'add_to_cart', $where );
				//print_r($userData);die;
				//$order_no = uniqid ();
				$order_no = mt_rand(100000, 999999);
				$invoice_no = 'novo'.''.mt_rand(100000, 999999);
				if ($userData) {
					foreach ( $userData as $product ) {
						$quantity = $product ['quantity'];
						$product_id = $product ['product_id'];
						$wherepro = "product_id= $product_id";
						$product = $this->Webservicemodel->getDataById ( 'product', $wherepro );
						$tprice = $product[0]['actual_price'];
						$price = $product[0]['discount_price'];
						$costprice = $product[0]['cost_price'];
						
						$dbQuantity = $product[0]['quantity'];
						$soldqty = $product [0] ['sold_quantity'];
						$avlQuantity = $dbQuantity - $soldqty;
						$update_qty = $soldqty + $quantity;
						if ( $quantity > $avlQuantity && $dbQuantity !=0 ) {
							$resArr ['result'] = 0;
							$resArr ['message'] = "sorry! Product Out of stock";
						} else {
							$insertArr = array (
									'user_id' => $user_id,
									'pro_id' => $product_id,
									'slot_id' => $slot_id,
									'unique_order_id' => $order_no,
									'invoice_no' => $invoice_no,
									'address_id' => $address,
									'order_quantity' => $quantity,
									'pincode' => $locationid,
									'pro_price' => $price,
									'pro_cost_price' => $costprice,
									'order_mode' => $payment_mode,
									'dis_by_agent' =>$affiliate_percentage,
									'affiliate_percentage' => $discount_percentage,
									'agent_id' =>$agent_id,
									'algorithm' => $algorithm,
									'order_status' =>"Pending",
									'order_date' => date ('Y-m-d h:i:s a')
							);//print_r($insertArr);die;
							$result = $this->Webservicemodel->insert ( 'manage_order', $insertArr );
							$insertArr1 = array (
									'sold_quantity' => $update_qty
							);
							$result1 = $this->Webservicemodel->update ( 'product', $insertArr1, $wherepro );
							if ($result) {
								$whereus="user_id='$user_id'";
								 $this->Webservicemodel->delete ( 'add_to_cart', $whereus );
								 $resArr ['result'] = 1;
								 $resArr ['message'] = "Your order has been successfully placed";
							} else {
								$resArr ['result'] = 0;
								$resArr ['message'] = "Order has not successfully placed";
							}
						}
					}
				} else {
					$resArr ['result'] = 0;
					$resArr ['message'] = "Your cart is empty";
				}
			}else{
				$order_no = mt_rand(100000, 999999);
				$whereproduct = "product_id= $productid";
				$product = $this->Webservicemodel->getDataById ( 'product', $whereproduct );
				//print_r($product);die;
				$tprice=$product[0]['actual_price'];
				$dprice=$product[0]['discount_price'];
				if($dprice==0){
					$deliveryprice=$tprice;
				}else{
					$deliveryprice=$dprice;
				}
				$dbQuantity = $product[0]['quantity'];
				$soldqty = $product [0] ['sold_quantity'];
				$avlQuantity = $dbQuantity - $soldqty;
				$update_qty = $soldqty + $productquan;
				if ( $productquan > $avlQuantity && $dbQuantity !=0 ) {
					$resArr ['result'] = 0;
					$resArr ['message'] = "sorry! Product Out of stock";
				} else {
					$insertArr = array (
							'user_id' => $user_id,
							'pro_id' => $productid,
							'slot_id' => $slot_id,
							'unique_order_id' => $order_no,
							'address_id' => $address,
							'order_quantity' => $productquan,
							'pro_price' => $deliveryprice,
							'order_mode' => $payment_mode,
							'pincode' => $locationid,
							'dis_by_agent' =>123,
							'agent_id' =>$agent_id,
							'algorithm' => $algorithm,
							'order_date' => date ( 'Y-m-d' )
					);
					$result = $this->Webservicemodel->insert ( 'manage_order', $insertArr );
					$insertArr1 = array (
							'sold_quantity' => $update_qty
					);
					$result1 = $this->Webservicemodel->update ( 'product', $insertArr1, $whereproduct );
					if ($result) {
						$whereus="user_id='$user_id'";
						$result= $this->Webservicemodel->delete ( 'add_to_cart', $whereus );
						$resArr ['result'] = 1;
						$resArr ['message'] = "Your order has been successfully placed";
					} else {
						$resArr ['result'] = 0;
						$resArr ['message'] = "Order hs not successfully placed";
					}
				}
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	
	/*Roushan
	 * Date:-29-11-2018
	 * order_history  */
	function order_history_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		if (! $userid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			//$where = "(user_id= $userid) order by order_id desc";
			$where = "(user_id= $userid) GROUP BY  unique_order_id ORDER BY manage_order_id DESC ";
			$historyData = $this->Webservicemodel->getDataById ( 'manage_order', $where );
			$affliate_per = $historyData[0]['dis_by_agent'];
			$algorithm = $historyData[0]['algorithm'];
			$marginData = $this->Webservicemodel->getTableData("company_margin");
			$company_margin = $marginData[0]['company_margin'];
			if ($historyData) {
				foreach ( $historyData as $history ) {
					$productList ['order_id'] = $history ['unique_order_id'];
					$unique1=$history ['unique_order_id'];
					$affiliate_percentage = $history ['dis_by_agent'];
					$whereunique="unique_order_id='$unique1'";
					$historyData1 = $this->Webservicemodel->getDataById ( 'manage_order', $whereunique );
					$id=$history ['unique_order_id'];
					$quantity1=$history ['order_quantity'];
					$quandata = $this->Webservicemodel->invoiceprice ($id );
					$whereuser = "user_id='$userid'";
					$userData = $this->Webservicemodel->getDataById ( 'user', $whereuser );
					$affiliateid = $userData[0]['agent_id'];
					$whereaffiliate = "affiliate_id='$affiliateid'";
					$affiliateData = $this->Webservicemodel->getDataById ( 'affiliate', $whereaffiliate );
					$affdis = $userData[0]['package_percentage'];//$affiliateData[0]['discount'];
					//$package_per = $userData[0]['package_percentage'];
				if ($historyData1) {
                    		$total_discount_price=0;
                    		$total_cost_price = 0;
                    		foreach ( $historyData1 as $history1 ) {
                    			$pro_price2 = $history1['pro_price'];
                    			$pro_cost_price2 = $history1['pro_cost_price'];
                    			$quan = $history1['order_quantity'];
                    			$total_discount_price = $total_discount_price + ($pro_price2 * $quan);
                    			$total_cost_price = $total_cost_price + ($pro_cost_price2 * $quan);
                    		/* 	if ($package_per !=""){
                    				 $package_amount = ($total_price*$package_per)/100;
                    				 $totalprice = round($total_price - $package_amount);
                    			} else {
                    				$totalprice = 0;
                    				$package_amount = 0;
                    			}
                    			if ($affliate_per !=""){
                    				 $affliateearning = ($totalprice*$affiliate_discount)/100;
                    				$partner_earn = ($affliateearning*$affliate_per)/100;
                    				 $total_bill_price =$total_bill_price + ($totalprice - $partner_earn);
                    			} else {
                    				$total_bill_price = 0;
                    			 }*/
                    		}
                    		$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
                    		$company_profit = ($company_margin/100);
                    		if ($algorithm=='1'){
                    		$package_per = (($commulative_per/100)*1)-($affdis/100);
                    		if ($package_per > $company_profit){
                    			$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100;
                    		}else {
                    			$new_total_dis = (($total_discount_price - $total_cost_price)*$affdis/100)-$company_margin/100;
                    		}
                    		} else {
                    			$package_per = ($commulative_per - $company_margin);
                    			if ($package_per > $affdis){
                    				$new_total_dis = ($total_discount_price)*$affdis/100;
                    			}
                    		}
                    		//echo $new_total_dis;die;
                    		$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
                    		$hostital_dis_per = ($hostital_dis*$total_discount_price/100);
                    		$affiliate_earning = $new_total_dis - $hostital_dis;
                    		$total_bill_price = $total_discount_price - $hostital_dis;
                    	}
				    $productList ['total_price'] = $total_discount_price - $hostital_dis;
					$quandata1 = $this->Webservicemodel->invoicequantity ($id );
					$productList ['quantity'] = $quandata1[0] ['quantity'];
					$productList ['order_date'] = $history ['order_date'];
					$productList ['payment_mode'] = $history ['order_mode'];
					$order_status = $history ['order_status'];
					
					 $productList ['order_status'] = $order_status;
					
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['Data'] [] = $productList;
					//$productList = "";
					
				}//die;
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "no data found on server";
			}}
			//print_r($resArr);die;
		echo $this->response ( $resArr, 200 );
		
	}
	
	function order_detail_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$category_id = $this->input->post ( 'order_id' );
		if (! $category_id) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "unique_order_id = $category_id";
			$buyerdata = $this->Webservicemodel->getDataById ( 'manage_order', $where );
			$affiliate_percentage = $buyerdata[0]['dis_by_agent'];
			$algorithm = $buyerdata[0]['algorithm'];
			$marginData = $this->Webservicemodel->getTableData("company_margin");
			$company_margin = $marginData[0]['company_margin'];
			if ($buyerdata) {
				foreach ( $buyerdata as $list ) {
					$subcatid = $list ['pro_id'];
					$where = "product_id = $subcatid";
					$prodata = $this->Webservicemodel->getDataById ( 'product', $where );
					$listData ['pro_name'] =  $prodata[0] ['product_name'];
					$listData ['quantity'] =  $list ['order_quantity'];
					$userid = $list['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Webservicemodel->getDataById ( 'user', $whereuser );
					$affiliateid = $userData[0]['agent_id'];
					$affdis = $userData[0]['package_percentage'];
					$whereaffiliate = "affiliate_id='$affiliateid'";
					$affiliateData = $this->Webservicemodel->getDataById ( 'affiliate', $whereaffiliate );
					$affiliate_discount = $affiliateData[0]['discount'];
					//$package_amount = ($list ['pro_price']*$package_per)/100;
					$total_discount_price = ($list ['pro_price']);
					$total_cost_price = ($list['pro_cost_price']);
					$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
                    		$company_profit = ($company_margin/100);
                    		if ($algorithm=="1"){
                    		$package_per = (($commulative_per/100)*1)-($affdis/100);
                    		if ($package_per > $company_profit){
                    			$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100;
                    		}else {
                    			$new_total_dis = (($total_discount_price - $total_cost_price)*$affdis/100)-$company_margin/100;
                    		}
                    		} else {
                    			$package_per = ($commulative_per - $company_margin);
                    			if ($package_per > $affdis){
                    				$new_total_dis = ($total_discount_price)*$affdis/100;
                    			}
                    		}
                    		//echo $new_total_dis;die;
                    		$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
                    		$hostital_dis_per = ($hostital_dis*$total_discount_price/100);
                    		$affiliate_earning = $new_total_dis - $hostital_dis;
                    		$total_bill_price = $total_discount_price - $hostital_dis;
					 $listData ['pro_price'] =  $total_bill_price;
					$listData ['image'] = base_url () . 'uploads/product/' .  $prodata[0] ['image'];
						
					$resArr ['result'] = 1;
					$resArr ['message'] = "Success.";
					$resArr ['Data'] [] = $listData;
					$listData = "";
				}//die;
			} else {
				$resArr ['result'] = 0;
				$resArr ["message"] = "No Data found on server.";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	
	function my_earning_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		if (! $userid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			//$where = "(user_id= $userid) order by order_id desc";
			$where    = "(user_id= $userid) GROUP BY  unique_order_id ORDER BY manage_order_id DESC ";
			$historyData = $this->Webservicemodel->getDataById ( 'manage_order', $where );
			$affiliate_percentage = $historyData[0]['dis_by_agent'];
			$algorithm = $buyerdata[0]['algorithm'];
			$marginData = $this->Webservicemodel->getTableData("company_margin");
			$company_margin = $marginData[0]['company_margin'];
			if ($historyData) {
				foreach ( $historyData as $history ) {
					$productList ['order_id'] = $history ['unique_order_id'];
					$unique1=$history ['unique_order_id'];
					$whereunique="unique_order_id='$unique1'";
					$historyData1 = $this->Webservicemodel->getDataById ( 'manage_order', $whereunique );
					$id=$history ['unique_order_id'];
					$quantity1=$history ['order_quantity'];
					$quandata = $this->Webservicemodel->invoiceprice ($id );
					if ($historyData1) {
						$total_discount_price=0;
						$total_cost_price = 0;
						foreach ( $historyData1 as $history1 ) {
							$pro_id = $history1 ['pro_id'];
							$pro_price2 = $history1['pro_price'];
							$pro_cost_price2 = $history1['pro_cost_price'];
							$quan = $history1['order_quantity'];
						    $total_discount_price = $total_discount_price + ($pro_price2 * $quan);
						    $total_cost_price = $total_cost_price + ($pro_cost_price2 * $quan);
							$whereproduct = "product_id='$pro_id'";
							$productData = $this->Webservicemodel->getDataById ( 'product', $whereproduct );
							$actualprice = $productData[0]['discount_price'];
							$whereuser = "user_id='$userid'";
							$userData = $this->Webservicemodel->getDataById ( 'user', $whereuser );
							$affiliateid = $userData[0]['agent_id'];
							$whereaffiliate = "affiliate_id='$affiliateid'";
							$affiliateData = $this->Webservicemodel->getDataById ( 'affiliate', $whereaffiliate );
							$affdis = $userData[0]['package_percentage'];//$affiliateData[0]['discount'];
 							//$packagepercentage = $userData[0]['package_percentage'];
 						   // $afterpackage_discount = ($total_price*$packagepercentage)/100;
 						   // $afterpackage_amount = $total_price - $afterpackage_discount;
 						    /* if ($affliate_per !=""){
 						    	$affliateearning = ($afterpackage_amount * $affiliate_discount)/100;
 						    	 $partner_earn = ($affliateearning * $affliate_per)/100;
 						    	 $partenerearning = $partenerearning + $partner_earn;
 						    	$totalprice = $totalprice + ($afterpackage_amount - $partner_earn);
 						    } else {
 						    	$totalprice = 0;
 						    	$partenerearning = 0;
 						    } */
						}
						$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
						$company_profit = ($company_margin/100);
						if ($algorithm=="1"){
						$package_per = (($commulative_per/100)*1)-($affdis/100);
						if ($package_per > $company_profit){
							$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100;
						}else {
							$new_total_dis = (($total_discount_price - $total_cost_price)*$affdis/100)-$company_margin/100;
						}
						} else {
							$package_per = ($commulative_per - $company_margin);
							if ($package_per > $affdis){
								$new_total_dis = ($total_discount_price)*$affdis/100;
							}	
						}
						//echo $new_total_dis;die;
						$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
						$hostital_dis_per = ($hostital_dis*$total_discount_price/100);
						$affiliate_earning = $new_total_dis - $hostital_dis;
						$total_bill_price = $total_discount_price - $hostital_dis;
					}
					
					//print_r($affiliate_discount);
				    $productList ['total_price'] = $total_bill_price;
					$productList ['earning_price'] = number_format($hostital_dis,2);
					$quandata1 = $this->Webservicemodel->invoicequantity ($id );
					$productList ['quantity'] = $quandata1[0] ['quantity'];
					$productList ['order_date'] = $history ['order_date'];
					
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['Data'] [] = $productList;
					$productList = "";
				}//die;
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "no data found on server";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	/* Affliate service start */
	function affliate_login_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$email = $this->input->post ( 'contact' );
		$password = $this->input->post ( 'password' );
		if (!$email || !$password) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "password = '$password' And (email='$email' OR phone='$email') AND status='Active'";
			$result = $this->Webservicemodel->getDataById ( 'affiliate', $where );
			if ($result) {
				$affliateid = $result [0] ['affiliate_id'];
				$listData ['affiliate_id'] = $affliateid;
				$listData ['affiliate_name'] = $result [0] ['affiliate_name'];
				$listData ['email'] = $result [0] ['email'];
				$listData ['mobile'] = $result [0] ['phone'];
				$listData ['unique_id'] = $result [0] ['unique_id'];
				$listData ['discount_percentage'] = $result [0] ['discount'];
				$resArr ['result'] = 1;
				$resArr ['message'] = "success";
				$resArr ['data'] [] = $listData;
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Invalid Credential";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	
	function healthcare_list_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$affiliate_id = $this->input->post ( 'affiliate_id' );
		if (!$affiliate_id) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "agent_id = '$affiliate_id'";
			$result = $this->Webservicemodel->getDataById ( 'user', $where );
			if ($result) {
				foreach ($result as $res){
					$custmerid = $res['user_id'];
					$listData ['healtcare_id'] = $custmerid;
					$listData ['healtcare_name'] = $res['user_name'];
					$listData ['unique_id'] = $res['unique_id'];
					$affiliate_percentage = $res['affiliate_percentage'];
					if ($affiliate_percentage !=""){
					$listData ['percentage'] = $res['affiliate_percentage'];
					} else {
						$listData ['percentage'] = "0";
					}
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['data'] [] = $listData;
					$listData="";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "No data found";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	
	function update_healthcare_percentage_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$healthcareid = $this->input->post ( 'healthcare_id' );
		$affiliate_percentage = $this->post('affiliate_percentage');
		if (! $healthcareid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$whereuserid = "user_id = $healthcareid";
			$healthcaredata = $this->Webservicemodel->getDataById ( 'user', $whereuserid );
			$insertArr = array (
					'affiliate_percentage' => $affiliate_percentage,
			);
			$whereup = "user_id = $healthcareid";
			$result = $this->Webservicemodel->update ( 'user', $insertArr, $whereup );
			if ($result) {
				$resArr ['result'] = 1;
			    $resArr ['message'] = "Healtcare discount percentage has been  update successfully ";
				}
			 else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Healtcare discount percentage has not been successfully Updated";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
   function affliate_earning_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$affliateid = $this->input->post ( 'affiliate_id' );
		if (! $affliateid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			//$where = "(user_id= $userid) order by order_id desc";
			$where = "(agent_id= $affliateid) GROUP BY  unique_order_id ORDER BY manage_order_id DESC ";
			$historyData = $this->Webservicemodel->getDataById ( 'manage_order', $where );
			$affiliate_percentage = $historyData[0]['dis_by_agent'];
			$algorithm = $historyData[0]['algorithm'];
			$marginData = $this->Webservicemodel->getTableData("company_margin");
			$company_margin = $marginData[0]['company_margin'];
			if ($historyData) {
				foreach ( $historyData as $history ) {
				$productList ['order_id'] = $history ['unique_order_id'];
					$unique1=$history ['unique_order_id'];
					$whereunique="unique_order_id='$unique1'";
					$historyData1 = $this->Webservicemodel->getDataById ( 'manage_order', $whereunique );
					$userid = $historyData1[0]['user_id'];
					$id=$history ['unique_order_id'];
					$quantity1=$history ['order_quantity'];
					$quandata = $this->Webservicemodel->invoiceprice ($id );
					if ($historyData1) {
						$total_discount_price=0;
						$total_cost_price = 0;
						foreach ( $historyData1 as $history1 ) {
							$pro_id = $history1 ['pro_id'];
							$pro_price2 = $history1['pro_price'];
							$pro_cost_price2 = $history1['pro_cost_price'];
							$quan = $history1['order_quantity'];
						    $total_discount_price =$total_discount_price + ($pro_price2 * $quan);
						    $total_cost_price =$total_cost_price + ($pro_cost_price2 * $quan);
							$whereproduct = "product_id='$pro_id'";
							$productData = $this->Webservicemodel->getDataById ( 'product', $whereproduct );
							$actualprice = $productData[0]['discount_price'];
							$whereuser = "user_id='$userid'";
							$userData = $this->Webservicemodel->getDataById ( 'user', $whereuser );
							$affiliateid = $userData[0]['agent_id'];
							$whereaffiliate = "affiliate_id='$affiliateid'";
							$affiliateData = $this->Webservicemodel->getDataById ( 'affiliate', $whereaffiliate );
							$affdis = $userData[0]['package_percentage'];
 							//$packagepercentage = $userData[0]['package_percentage'];
 						    //$afterpackage_discount = ($total_price*$packagepercentage)/100;
 						      //$afterpackage_amount = $total_price - $afterpackage_discount;
 						  /*   if ($affliatepercentage !=""){
 						    	$affliateearning = ($afterpackage_amount * $affiliate_discount)/100;
 						    	 $affliattotalear = $affliattotalear + $affliateearning;
 						    	 $partner_earn = ($affliateearning * $affliatepercentage)/100;
 						    	 $partenerearning = $partenerearning + $partner_earn;
 						    	 $totalprice = $totalprice + ($afterpackage_amount - $partner_earn);
 						    } else {
 						    	$totalprice = 0;
 						    	$partenerearning = 0;
 						    } */
						}
						$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
						$company_profit = ($company_margin/100);
						if ($algorithm=="1"){
						$package_per = (($commulative_per/100)*1)-($affdis/100);
						if ($package_per > $company_profit){
							$new_total_dis = ($total_discount_price - $total_cost_price)*$affdis/100;
						}else {
							$new_total_dis = (($total_discount_price - $total_cost_price)*$affdis/100)-$company_margin/100;
						}
						} else {
							$package_per = ($commulative_per - $company_margin);
							if ($package_per > $affdis){
								$new_total_dis = ($total_discount_price)*$affdis/100;
							}
						}
						//echo $new_total_dis;die;
						$hostital_dis = ($new_total_dis*$affiliate_percentage/100);
						$hostital_dis_per = ($hostital_dis*$total_discount_price/100);
						$affiliate_earning = $new_total_dis - $hostital_dis;
						$total_bill_price = $total_discount_price - $hostital_dis;
					}
				    $productList ['total_price'] = $total_bill_price;
					$productList ['earning_price'] = $affiliate_earning;
					$quandata1 = $this->Webservicemodel->invoicequantity ($id );
					$productList ['order_date'] = $history ['order_date'];
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['Data'] [] = $productList;
					$productList = "";
				}//die;
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "no data found on server";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	function value_champion_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		
			//$where = "(user_id= $userid) order by order_id desc";
			$where = "status='Active'";
			$historyData = $this->Webservicemodel->getDataById ( 'affiliate', $where );
			
			$currentDate = date('Y-m-d');
			$currentyear = date('Y');
			$currentmonth = date('m');
			
			if ($historyData) {
				//print_r($monthlyData);die;
				foreach ( $historyData as $data ) {
					$userid = $data['affiliate_id'];
					$datalist['affiliate_id'] = $userid;
					$datalist['affliate_name'] = $data['affiliate_name'];
					//$productList ['order_id'] = $data ['unique_order_id'];
					$wheredate = "(order_date >= '$currentyear-$currentmonth-01' AND order_date <= '$currentDate' AND agent_id='$userid') GROUP BY  unique_order_id";
					$monthlyData = $this->Webservicemodel->getDataById ( 'manage_order', $wheredate );
					$unique1 = $monthlyData[0]['unique_order_id'];
					$whereunique = "unique_order_id='$unique1'";
					$historyData1 = $this->Webservicemodel->getDataById ( 'manage_order', $whereunique );
					//print_r($historyData1);
					$id = $monthlyData[0]['unique_order_id'];
					$quantity1 = $monthlyData[0] ['order_quantity'];
					
					 $quandata = $this->Webservicemodel->invoiceprice ($id );
					 if ($historyData1) {
						$total_price = 0;
						foreach ( $historyData1 as $history1 ) {
							$pro_price2 = $history1['pro_price'];
							$quan = $history1['order_quantity'];
							 $total_price = $total_price + ($pro_price2 * $quan);
						}
						
					} else {
						$total_price = 0;
					}
					
					$datalist ['total_sell_amount'] = $total_price;
					//$productList ['earning_price'] = $total_earning;
					$quandata1 = $this->Webservicemodel->invoicequantity ($id );
					//$datalist ['order_date'] = $history ['order_date']; 
					$resArr ['result'] = 1;
					$resArr ['message'] = "success";
					$resArr ['Data'] [] = $datalist;
					$datalist = "";
				}
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "no data found on server";
			}
		
		echo $this->response ( $resArr, 200 );
	}
	
	function affliate_profile_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$affliateid = $this->input->post ( 'affiliate_id' );
		if (! $affliateid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			$where = "affiliate_id = '$affliateid' AND status='Active'";
			$result = $this->Webservicemodel->getDataById ( 'affiliate', $where );
			if ($result) {
				$affliateid = $result [0] ['affiliate_id'];
				$listData ['affliate_id'] = $affliateid;
				$listData ['affliate_name'] = $result [0] ['affiliate_name'];
				$listData ['email'] = $result [0] ['email'];
				$listData ['mobile'] = $result [0] ['phone'];
				$listData ['unique_id'] = $result [0] ['unique_id'];
				$listData ['discount_percentage'] = $result [0] ['discount'];
				$resArr ['result'] = 1;
				$resArr ['message'] = "success";
				$resArr ['data'] [] = $listData;
			} else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Invalid Credential";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
	/* Affliate service end */
	function upload_order_post() {
		$resArr ['result'] = 0;
		$resArr ['message'] = "";
		$userid = $this->input->post ( 'user_id' );
		if (! $userid) {
			$resArr ['message'] = "Please pass all parameters";
		} else {
			if (! empty ( $_FILES ['order_file']['name'] )) {
				$imageName = $_FILES ['order_file']['name'];
				$config ['upload_path'] = FCPATH . 'uploads/orderfile/';
				$config ['allowed_types'] = '*';
				$config ['max_size'] = '1024';
				$time = strtotime ( date ( 'Y-m-d H:i:s' ) );
				$config ['file_name'] = $time;
				$this->upload->initialize ( $config );
				if (! $this->upload->do_upload ( 'order_file' )) {
					$error = $this->upload->display_errors ();
					$data ['imgErr'] = $error;
					$resArr ['message'] = $error;
					$failure = 1;
				} else {
					$a = $this->upload->data ();
					$failure = 0;
					$uploadedimgName = $a ['file_name'];
				}
			} else {
				$resArr ['message'] = "Please pass all parameters";
			}
			$whereuser = "user_id = '$userid'";
			$userData = $this->Webservicemodel->getDataById ( 'user', $whereuser );
			$algorithm = $userData[0]['algorithm'];
			$order_no = mt_rand(100000, 999999);
		    $invoice_no = 'novo'.''.mt_rand(100000, 999999);
		    $insertArr = array (
				'user_id' => $userid,
				'unique_order_id' => $order_no,
				'invoice_no' => $invoice_no,
				'order_file' =>$uploadedimgName,
				'order_status' =>"Pending",
		    	'algorithm' => $algorithm,
		    		'is_add' => "1",
				'order_date' => date ( 'Y-m-d h:i:s')
		   );
		   // print_r($insertArr);die;
		    $result = $this->Webservicemodel->insert ( 'manage_order', $insertArr );
			if ($result) {
				$resArr ['result'] = 1;
				$resArr ['message'] = "Order has been placed successfully";
			}
			else {
				$resArr ['result'] = 0;
				$resArr ['message'] = "Order has not been placed successfully";
			}
		}
		echo $this->response ( $resArr, 200 );
	}
}
