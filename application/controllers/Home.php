<?php
class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->pdo = $this->load->database('pdo', true);
		$this->load->library("excel");
		$this->load->helper('url');
		$this->load->helper('text');
		$this->load->model("Homemodel");
		date_default_timezone_set ( "Asia/Calcutta" );
		include ('application/libraries/phpmailer/sendEmail.php');
		//$datapro['productarr'] = $this->Homemodel->getTableData('product');
		
		//$this->load->view('frontend/autocompleteproduct',$datapro);
	
	}
	public function index()
	{
		$sesUserId = $this->userSessionId ();
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$wherekart = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$wherekart);
			$userData = $this->Homemodel->getDataById("user",$where);
			$data['package_percentage'] = $userData[0]['package_percentage'];
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$price = $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
					
					$total_price = $total_price + ($price * $quantity);
				}
				$data['total_price'] = number_format($total_price,2);
			    $data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['package_percentage'] = 0;
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$wherebanner = "status='Active'";
		$data['bannerData'] = $this->Homemodel->getDataById("banner",$wherebanner);
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wheremedial = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecontact = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wherecontact );
		//print_r($data ['contactdata']);die;
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$whereproduct = "status='Active'";
		$data ['newproductdata'] = $this->Homemodel->getnewProduct ( 'product', $whereproduct );
		$wherestore = "cms_id=9";
		$data ['featurecontentdata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wherefeatureproduct = "status='Active'";
		$data ['featureproductdata'] = $this->Homemodel->getnewProduct ( 'feature_collection', $wherefeatureproduct );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		//print_r($data ['newproductdata']);die;
		$data['activeMenu'] = "1";
		$this->load->view('frontend/index',$data);

	}
	function login()
	{
		 $user = $this->input->post('user');
		$pass = $this->input->post('pass');
		$where ="(email ='$user' OR contact = '$user') AND password='$pass' AND role_id=2";
		$checklogin = $this->Homemodel->checklogin($where);
		//print_r($checklogin);die;
		if (! empty($checklogin)) {
			$session = array(
					'id' => $checklogin[0]['user_id'],
					'userName' => $checklogin[0]['user_name'],
					'user_type' => $checklogin[0]['role_id'],
					'email' => $checklogin[0]['email']
			);
			$sesrole = $this->session->set_userdata('userData', $session);
			$productsessionData = $this->session->userdata('productsessionData');
			if (!empty($productsessionData)){
				$productid = $productsessionData['product_id'];
				$quantity = $productsessionData['quantity'];
				$totalPrice = $productsessionData['price'];
				$user_id = $checklogin[0]['user_id'];
				$wherecart = "user_id = $user_id AND product_id = $productid";
				$cartduplicatedata = $this->Homemodel->getDataById ( 'add_to_cart', $wherecart );
				if($cartduplicatedata){
					echo "dup";die;
				}else{
				$insertArr = array (
						'user_id' => $checklogin[0]['user_id'],
						'product_id' => $productid,
						'quantity' => $quantity,
						'price' => $totalPrice
				);
				//print_r($insertArr);die;
			$this->Homemodel->insert ( 'add_to_cart', $insertArr );
			}
			}
			//print_r($productsessionData);die;
			echo 1;
		} else {
			echo 0;
		}
	}
	/*Roushan
	 * method:-forgotpassword
	 * date:-31-12-2018  */
	function forgotpass()
	{
		$mobile = $this->input->post('mobile');
		$where ="contact ='$mobile' AND role_id=2";
		$userData = $this->Homemodel->getDataById('user',$where);
	    if (! empty ( $userData )) {
				$uniqueid = substr ( uniqid (), 1, 5 );
				$insertArr = array (
						'password' => $uniqueid 
				);
				$where = "contact = '$mobile'";
				$result = $this->Homemodel->update ( 'user', $insertArr, $where );
				if ($result) {
				$pdata = new StdClass ();
				$pdata->number = $mobile;
				$pdata->message = "Novokart new password: $uniqueid";
				$message [] = $pdata;
				$isSent = $this->Homemodel->textMessageIndia ( $message );
				if ($isSent=="Success"){
					echo 1;
				} else {
					echo 2;
				}
				}
			} else {
				echo 0;
			}
	}
	function registration(){
		$name = $_POST['name'];
		$mobile = $_POST['mobile'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$customer_type_id = $_POST['customer_type'];
		$city = $_POST['city'];
		$pin = $_POST['pin'];
		$licence_no = $_POST['licence_no'];
		/* if (! empty ( $_FILES ['licence_img'] ['name'] )) {
			$imageName = $_FILES ['licence_img'] ['name'];
			$config ['upload_path'] = FCPATH . 'uploads/userfile/';
			$config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config ['max_size'] = '1024';
			$time = strtotime ( date ( 'Y-m-d H:i:s' ) );
			$config ['file_name'] = $time;
			$this->upload->initialize ( $config );
			if (! $this->upload->do_upload ( 'licence_img' )) {
				$error = $this->upload->display_errors ();
				$data ['imgErr'] = $error;
				$failure = 1;
				$uploadedimgName = "";
			} else {
				$a = $this->upload->data ();
				$failure = 0;
				$uploadedimgName = $a ['file_name'];
			}
		} else {
			$uploadedimgName = "";
			$failure = 0;
		} */
		$pan_no = $_POST['pan_no'];
		/*$aadhar_img = $_POST['aadhar_img'];
	 	if (! empty ( $_FILES ['aadhar_img'] ['name'] )) {
			$imageName = $_FILES ['aadhar_img'] ['name'];
			$config ['upload_path'] = FCPATH . 'uploads/userfile/';
			$config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config ['max_size'] = '1024';
			$time = strtotime ( date ( 'Y-m-d H:i:s' ) );
			$config ['file_name'] = $time;
			$this->upload->initialize ( $config );
			if (! $this->upload->do_upload ( 'aadhar_img' )) {
				$error = $this->upload->display_errors ();
				$data ['imgErr'] = $error;
				$failure = 1;
				$uploadedimgName = "";
			} else {
				$a = $this->upload->data ();
				$failure = 0;
				$uploadedimgName = $a ['file_name'];
			}
		} else {
			$uploadedimgName = "";
			$failure = 0;
		} */
		$wheremobile = "contact = '$mobile' AND role_id=2";
		$duplicatempbiledata = $this->Homemodel->getDataById ( 'user', $wheremobile );
		$whereemail = "email = '$email' AND role_id=2";
		$duplicateemaildata = $this->Homemodel->getDataById ( 'user', $whereemail );
		if ($duplicatempbiledata){
			echo "dupmob";
		} elseif ($duplicateemaildata){
			echo "dupemail";
		} else{
		$uniqueid = mt_rand(100000, 999999);
		$insertArr = array(
                    'user_name' => $name,
                    'email' => $email,
                    'contact' => $mobile,
                    'password' => $password,
                    'role_id' => 2,
                	'unique_id' => $uniqueid,
				    'customer_type_id' => $customer_type_id,
                    'city_id' => $city,
                    'pincode_id' => $pin,
                    'adhar_number' => $pan_no,
					'licence_number' => $licence_no,
				    'created_date' => date("Y-m-d"),
                	'status' =>'Active'
                );
		//print_r($insertArr);die;
                $result = $this->Homemodel->insert('user', $insertArr);
                if ($result){
                	echo 1;
                } else {
                	echo 0;
                }
		}
	}
	public function profile()
	{
		$sesUserId = $this->userSessionId ();
		$whereuser = "user_id='$sesUserId'";
		$data ['userDatadata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereadmin = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereadmin );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
		
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "";
		$this->load->view('frontend/agent-profile',$data);
	}
	/*Roushan 
	 * date:-07-01-2019
	 * method:-editProfile  */
	public function editProfile()
	{
		$sesUserId = $this->userSessionId ();
		if ($sesUserId){
		$whereuser = "user_id='$sesUserId'";
		$data ['userDatadata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereadmin = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereadmin );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
	
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
			if (!empty($_POST)){
			$name = $_POST['name'];
			$email = $_POST['email'];
			$mobile = $_POST['mobile'];
			$address = $_POST['address'];
			$aadhar_no = $_POST['aadhar_no'];
			$insertArr = array (
					'user_name' => $name,
					'email' => $email,
					'contact' =>$mobile,
					'address' => $address,
					'adhar_number' => $aadhar_no
			);
			$where = "user_id = '$sesUserId'";
			$result = $this->Homemodel->update ( 'user', $insertArr, $where );
			if ($result){
				$this->session->set_flashdata('Successfully','Profile is Successfully updated');
				redirect('Home/editProfile','refresh');
			} else {
				$this->session->set_flashdata('Error','Failed To update Profile');
				redirect('Home/editProfile','refresh');
			}
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		} else {
			redirect('Home');
		}
		$data['activeMenu'] = "";
		//$data['message'] = "";
		$this->load->view('frontend/edit_profileView',$data);
	}
	public function change_password()
	{
		$sesUserId = $this->userSessionId ();
		$whereuser = "user_id='$sesUserId'";
		$data ['userDatadata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereadmin = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereadmin );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$where = "user_id='$sesUserId'";
		$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
		$cartcount = count($cartData);
		if (!empty($cartData)){
			$total_price = 0;
			foreach ($cartData as $cart){
				$quantity = $cart['quantity'];
				$userid = $cart['user_id'];
				$whereuser = "user_id='$userid'";
				$userData = $this->Homemodel->getDataById("user",$whereuser);
				$package_percentage = $userData[0]['package_percentage'];
				$total_price = $total_price + $cart['price'] ;
				//$packageprice = ($price*$package_percentage)/100;
				//$real_price = $price - $packageprice;
		
				//$total_price = $total_price + ($real_price * $quantity);
			}
			$data['total_price'] = $total_price;
			$data['cartcount'] = $cartcount;
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "";
		//$data['message'] = "";
		$this->load->view('frontend/change_passwordView',$data);
	}
	public function changepasswordsubmit()
	{
		$sesUserId = $this->userSessionId ();
		if ($sesUserId){
		$newpass = $_POST['new_pass'];
		$insertArr = array (
				'password' => $newpass
		);
		$where = "user_id = '$sesUserId'";
		$result = $this->Homemodel->update ( 'user', $insertArr, $where );
		if ($result){
			echo 1;
		} else {
			echo 0;
		}
		}
		
	}
	public function aboutus()
	{
		$sesUserId = $this->userSessionId ();
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$whereabout = "cms_id=1";
		$data ['aboutdata'] = $this->Homemodel->getDataById ( 'cms', $whereabout );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
		
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "2";
		$this->load->view('frontend/aboutus',$data);
	
	}
	public function benifits()
	{
		$sesUserId = $this->userSessionId ();
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$whereabout = "cms_id=1";
		$data ['aboutdata'] = $this->Homemodel->getDataById ( 'cms', $whereabout );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherebenifits = "cms_id=10";
		$data ['benifitsdata'] = $this->Homemodel->getDataById ( 'cms', $wherebenifits );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
	
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "5";
		$this->load->view('frontend/benifits',$data);
	
	}
	public function deliveryinfo()
	{
		$sesUserId = $this->userSessionId ();
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereadmin = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereadmin );
		$whereinfo = "cms_id=3";
		$data ['deliverinfodata'] = $this->Homemodel->getDataById ( 'cms', $whereinfo );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
		
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "";
		$this->load->view('frontend/deliveryinfo',$data);
	
	}
	public function privacypolicy()
	{
		$sesUserId = $this->userSessionId ();
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereadmin = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereadmin );
		$whereprivacy = "cms_id=2";
		$data ['privacydata'] = $this->Homemodel->getDataById ( 'cms', $whereprivacy );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price+$cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
		
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "";
		$this->load->view('frontend/privacypolicy',$data);
	
	}
	public function support()
	{
		$sesUserId = $this->userSessionId ();
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wheresupport = "cms_id=4";
		$data ['supportdata'] = $this->Homemodel->getDataById ( 'cms', $wheresupport );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
		
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "";
		$this->load->view('frontend/support',$data);
	
	}
	public function contact()
	{
		$sesUserId = $this->userSessionId ();
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecontact = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wherecontact );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price = $total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
		
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "";
		$this->load->view('frontend/contact',$data);
	
	}
	public function career()
	{
		$sesUserId = $this->userSessionId ();
		$data['bannerData'] = $this->Homemodel->getTableData("banner");
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price =$total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
		
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "4";
		$this->load->view('frontend/career',$data);
	
	}
	public function productList($id=NULL)
	{    
		$sesUserId = $this->userSessionId ();
	if ($sesUserId){
		$where = "user_id='$sesUserId'";
		$wherekart = "user_id='$sesUserId'";
		$cartData = $this->Homemodel->getDataById("add_to_cart",$wherekart);
		$userData = $this->Homemodel->getDataById("user",$where);
		$data['package_percentage'] = $userData[0]['package_percentage'];
		//print_r($cartData);die;
		$cartcount = count($cartData);
		if (!empty($cartData)){
			$total_price = 0;
			foreach ($cartData as $cart){
				$quantity = $cart['quantity'];
				$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price =$total_price + ($cart['price'] *$quantity) ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
					
					//$total_price = $total_price + ($real_price * $quantity);
			}
			$data['total_price'] = $total_price;
			$data['cartcount'] = $cartcount;
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
	} else {
		$data['package_percentage'] = 0;
		$data['total_price'] = 0;
		$data['cartcount'] = 0;
	}
	    $data['id'] = $id;
	    $data['user_id'] = $sesUserId;
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		if (!empty($id)){
		$whereproduct = "sub_category_id = $id AND status='Active'";
		} else{
			$whereproduct = "status='Active'";
		}
		$data ['productlistdata'] = $this->Homemodel->getDataById ( 'product', $whereproduct );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		$data['activeMenu'] = "3";
		$this->load->view('frontend/productlistView',$data);
	}
	public function filterproduct($id=NULL)
	{    $sesUserId = $this->userSessionId ();
	if ($sesUserId){
		$where = "user_id='$sesUserId'";
		$wherekart = "user_id='$sesUserId'";
		$cartData = $this->Homemodel->getDataById("add_to_cart",$wherekart);
		$userData = $this->Homemodel->getDataById("user",$where);
		$data['package_percentage'] = $userData[0]['package_percentage'];
		//print_r($cartData);die;
		$cartcount = count($cartData);
		if (!empty($cartData)){
			$total_price = 0;
			foreach ($cartData as $cart){
				$quantity = $cart['quantity'];
				$userid = $cart['user_id'];
				$whereuser = "user_id='$userid'";
				$userData = $this->Homemodel->getDataById("user",$whereuser);
				$package_percentage = $userData[0]['package_percentage'];
				$total_price = $total_price + $cart['price'] ;
				//$packageprice = ($price*$package_percentage)/100;
				//$real_price = $price - $packageprice;
					
				//$total_price = $total_price + ($real_price * $quantity);
			}
			$data['total_price'] = $total_price;
			$data['cartcount'] = $cartcount;
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
	} else {
		$data['package_percentage'] = 0;
		$data['total_price'] = 0;
		$data['cartcount'] = 0;
	}
	$data['id'] = $id;
	$data['user_id'] = $sesUserId;
	$data['categoryData'] = $this->Homemodel->getTableData("category");
	$whereuser = "user_id=1";
	$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
	$wherecareer = "cms_id=8";
	$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
	$wheremedia = "cms_id=7";
	$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
	$wherestore = "cms_id=6";
	$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
	$whereproduct = "category_id = $id";
	$data ['productlistdata'] = $this->Homemodel->getDataById ( 'product', $whereproduct );
	$wheremedial = "cms_id=5";
	$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
	$wherecity = "status='Active'";
	$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
	$wherepin = "status='Active'";
	$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
	$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
	$data['activeMenu'] = "3";
	$this->load->view('frontend/filterproduct',$data);
	}
	
	function catsortbyprice(){
		 $slct2 = $_POST['slct2'];
		 $catid = $_POST['catid'];
		if ($slct2==1){
			$whereproduct = "(category_id = '$catid' AND status='Active') order by discount_price ASC";
			
		} else {
			$whereproduct = "(category_id = '$catid' AND status='Active') order by discount_price desc";
		}
		$data ['productlistdata'] = $this->Homemodel->getDataById ( 'product', $whereproduct );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		//print_r($data ['productlistdata']);die;
		$this->load->view('frontend/ajaxproductlist',$data);
	}
	function sortbyprice(){
		$slct2 = $_POST['slct2'];
		$id = $_POST['subcatid'];
		if ($slct2==1){
			$whereproduct = "(sub_category_id = $id AND status='Active') order by discount_price ASC";
				
		} else {
			$whereproduct = "(sub_category_id = $id AND status='Active') order by discount_price desc";
		}
		$data ['productlistdata'] = $this->Homemodel->getDataById ( 'product', $whereproduct );
		//print_r($data ['productlistdata']);die;
		$this->load->view('frontend/ajaxproductlist',$data);
	}
	function pricerange(){
		$categoryId =$_POST['categoryId'];
		$min_price=$_POST['min_price'];
		$max_price=$_POST['max_price'];
		if (!empty($categoryId)){	
		$whereproduct = "category_id = '$categoryId' AND discount_price BETWEEN '$min_price' AND '$max_price' AND status='Active' order by discount_price DESC";				
		} else {
		$whereproduct = "discount_price BETWEEN '$min_price' AND '$max_price' AND status='Active' order by discount_price DESC";
		}
		
		$data ['productlistdata'] = $this->Homemodel->getDataById ( 'product', $whereproduct );
		//print_r($data ['productlistdata']);die;
		$this->load->view('frontend/ajaxproductlist',$data);
	}
	public function productDetails($id=NULL)
	{  
	 $sesUserId = $this->userSessionId();
	if ($sesUserId){
		$where = "user_id='$sesUserId'";
		$wherekart = "user_id='$sesUserId'";
		$cartData = $this->Homemodel->getDataById("add_to_cart",$wherekart);
		$userData = $this->Homemodel->getDataById("user",$where);
		$data['package_percentage'] = $userData[0]['package_percentage'];
		//print_r($cartData);die;
		$cartcount = count($cartData);
		if (!empty($cartData)){
			$total_price = 0;
			foreach ($cartData as $cart){
				$quantity = $cart['quantity'];
				$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price =$total_price + $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
					
					//$total_price = $total_price + ($real_price * $quantity);
			}
			$data['total_price'] = $total_price;
			$data['cartcount'] = $cartcount;
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
	} else {
		$data['package_percentage'] = 0;
		$data['total_price'] = 0;
		$data['cartcount'] = 0;
	}
	$data['user_id'] = $sesUserId;
	$data['categoryData'] = $this->Homemodel->getTableData("category");
	$whereuser = "user_id=1";
	$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
	$wherecareer = "cms_id=8";
	$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
	$wheremedia = "cms_id=7";
	$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
	$wherestore = "cms_id=6";
	$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
	if (is_numeric($id)){//echo "pp";die;
		$whereproduct = "product_id = $id";
	}else  {//echo "pp1";die;
           
		$whereproduct = "product_name = '$id'";
	}
	
	$productdata = $this->Homemodel->getDataById ( 'product', $whereproduct );
	$data ['productdata'] = $productdata;
	$data['sub_cat_id'] = $productdata[0]['sub_category_id'];
	//print_r($data ['productdata']);die;
	$wheremedial = "cms_id=5";
	$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
	$wherecity = "status='Active'";
	$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
	$wherepin = "status='Active'";
	$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
	$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
	$data['activeMenu'] = "3";
	
	$this->load->view('frontend/productDetailsView',$data);
	
	}
	
	function add_to_cart(){
		 $sesUserId = $this->userSessionId ();
		 $productid = $_POST['productid'];
		 $quantity = $_POST['quantity'];
		
		 $wherepro = "product_id=$productid";
		 $pricedata = $this->Homemodel->getDataById ( 'product',$wherepro );
		 $totalPrice = $pricedata[0]['discount_price'];
		 if ($sesUserId){
		 	$wherecart = "user_id = $sesUserId AND product_id = $productid";
		 	$cartduplicatedata = $this->Homemodel->getDataById ( 'add_to_cart', $wherecart );
		if($cartduplicatedata){
			echo "dup";
		}else{
		$insertArr = array (
				'user_id' => $sesUserId,
				'product_id' => $productid,
				'quantity' => $quantity,
				'price' => number_format($totalPrice,2)
		);
		//print_r($insertArr);die;
		$result = $this->Homemodel->insert ( 'add_to_cart', $insertArr );
		if ($result){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			//print_r($cartData);die;
			$cartcount = count($cartData);
			echo $cartcount;
		} else {
			echo 0;
		}
		}
		 } else {
		 	$insertArr = array (
		 			'product_id' => $productid,
		 			'quantity' => $quantity,
		 			'price' => number_format($totalPrice,2)
		 	);
		 	//print_r($insertArr);die;
		 	$loginData = $this->session->set_userdata ( 'productsessionData', $insertArr );
		 	echo "login";
		 }
	}
	function my_cart(){
		$sesUserId = $this->userSessionId ();
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$useragentData = $this->Homemodel->getDataById ( 'user', $where );
			//print_r($expression)
			 $data['purchase_value'] = $useragentData[0]['min_value'];
			 $wherekart = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$wherekart);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$price =  $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$total_price = $total_price + $price;
					
					$total_price = $total_price + ($price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
			
			$data['cartData'] = $cartData;
		    $data['activeMenu'] = "0";
		    $this->load->view('frontend/mycartView',$data);
		   } else {
			header("location:" . base_url('Home') . "");
		}
	}
	function update_cart() {
		    $cartid = $_POST['cartid'];
		    $quantity = $_POST['quantity'];
			$wherecart = "add_to_cart_id = $cartid ";
			$cartdata = $this->Homemodel->getDataById ( 'add_to_cart', $wherecart );
			$userid=$cartdata[0]['user_id'];
			$productid=$cartdata[0]['product_id'];
			$wherepro = "product_id=$productid";
			$pricedata = $this->Homemodel->getDataById ( 'product',$wherepro );
			$totalPrice=$pricedata[0]['actual_price'];
			$wherecart1 = "user_id = $userid AND product_id=$productid";
			$cartduplicatedata = $this->Homemodel->getDataById ( 'add_to_cart', $wherecart1 );
			$insertArr = array (
					'quantity' => ($quantity) ? $quantity : $cartdata[0]['quantity'],
			);
			$whereup = "add_to_cart_id=$cartid";
			$result = $this->Homemodel->update ( 'add_to_cart', $insertArr, $whereup );
			if ($result) {
				echo 1;
			}else {
				echo 0;
			}
	}
	function deletecart() {
		$sesUserId = $this->userSessionId ();
		$cid = $_POST ['id'];
		$where = "add_to_cart_id=$cid";
		if ($sesUserId) {
			$res = $this->Homemodel->delete ( 'add_to_cart', $where );
			if ($res) {
				echo 1;
			} else {
				echo 0;
			}
		}
	}
	function sendaffliateid() {
		$sesUserId = $this->userSessionId ();
		$affliateid = $_POST ['affliateid'];
		if (!empty($sesUserId)) {
			$whereuser = "user_id = '$sesUserId'";
			$userData = $this->Homemodel->getDataById ( 'user', $whereuser );
			if ($userData) {
				$agent_id = $userData [0] ['agent_id'];
				$whereaffiliate = "unique_id='$affliateid'";
				$affilateData = $this->Homemodel->getDataById ( 'affiliate', $whereaffiliate );
				//print_r($affilateData);die;
				if (!empty($affilateData)){
				$agents_id = $affilateData [0] ['affiliate_id'];
				if ($agent_id==$agents_id){
					$discount_percentage = $userData [0] ['affiliate_percentage'];
					echo $agents_id;
				} else{
					echo 0;
				}
				} else {
					echo 0;
				}
			} 
		}
	}
	function partnersaving($id = NULL){
		$sesUserId = $this->userSessionId ();
		$marginData = $this->Homemodel->getTableData("company_margin");
		$company_margin = $marginData[0]['company_margin'];
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		if ($sesUserId){
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$price =  $cart['price'] ;
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
						
					$total_price = $total_price + ($price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$where = "user_id='$sesUserId'";
		$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
		$userData = $this->Homemodel->getDataById("user",$where);
		$algorithm = $userData[0]['algorithm'];
		$data['algorithm'] = $algorithm;
		$data['cartData'] = $cartData;
		$partnersaving = 0;
		$total_cost_price = 0;
		$total_discount_price = 0;
		foreach ($cartData as $cart){
			$cartid = $cart['add_to_cart_id'];
			$productid = $cart['product_id'];
			$userid = $cart['user_id'];
			$whereuser = "user_id='$userid'";
			$userData = $this->Homemodel->getDataById("user",$whereuser);
			$affiliate_percentage = $userData[0]['package_percentage'];
			$affdis = $userData[0]['affiliate_percentage'];
			$wherepro = "product_id='$productid'";
			$productData = $this->Homemodel->getDataById("product",$wherepro);
			$productname = $productData[0]['product_name'];
			$productimage =  $productData[0]['image'];
			$quantity = $cart['quantity'];
			
			$actualprice =  $productData[0]['actual_price'];
			$discountprice =  $productData[0]['discount_price'];
			$total_discount_price = $total_discount_price + ($discountprice* $quantity);
			$costprice = $productData[0]['cost_price'];
			$total_cost_price = $total_cost_price + ($costprice * $quantity);
			$packageprice = ($discountprice*$package_percentage)/100;
			$real_price = $discountprice - $packageprice;
			$whereaffliate = "affiliate_id='$id'";
			$affiliateData = $this->Homemodel->getDataById("affiliate",$whereaffliate);
			//$affdis = $affiliateData[0]['discount'];
			$price = $real_price;//$cart['price'];
			$total_price = $price;
		}
		$commulative_per = (($total_discount_price - $total_cost_price)/$total_discount_price)*100;
		$company_profit = ($company_margin/100);
		if ($algorithm=='1'){
			$data['algo2'] = "1";
		$package_per = (($commulative_per/100)*1)-($affdis/100);
		if ($package_per > $company_profit){
			$new_total_dis = ($total_discount_price - $total_cost_price)*$affiliate_percentage/100;
			
		}else {
			$new_total_dis = (($total_discount_price - $total_cost_price)*$affiliate_percentage/100)-$company_margin/100;
		}
		// echo $new_total_dis;
		
		} else {//echo $commulative_per;die;
			$package_per = ($commulative_per - $company_margin);
			if ($package_per > $affiliate_percentage){
				$new_total_dis = ($total_discount_price)*$affiliate_percentage/100;
				 $data['algo2'] = "1";
			} else {//echo "ok";die;
				$new_total_dis = 0;
				 $data['algo2'] = "";
			}
		}
		$hostital_dis = ($new_total_dis*$affdis/100);
		$data['hostital_dis_per'] = ($hostital_dis/$total_discount_price)*100;
		$affiliate_earning = $new_total_dis - $hostital_dis;
		$data['affliate_id'] = $id;
		$data['activeMenu'] = "0";
		$this->load->view('frontend/partner_savingView',$data);
	}
	function sendotp() {
		$sesUserId = $this->userSessionId ();
		if (!empty($sesUserId)) {
			$whereuser = "user_id = '$sesUserId'";
			$userData = $this->Homemodel->getDataById ( 'user', $whereuser );
			if ($userData) {
				$contact = $userData [0] ['contact'];
				$uniqueid = mt_rand(1000, 9999);
				$pdata = new StdClass();
				$pdata->number = $contact;
				$pdata->message = "Novokart OTP: $uniqueid";
				$message[] = $pdata;
				$sent = $this->Homemodel->textMessageIndia($message);
				//echo $sent;die;
				if ($sent == "Fail") {
					$data['result'] = 0;
				} else {
					$data['result'] = $uniqueid;
				}
				}
			}
			$this->load->view('frontend/ajaxotp',$data);
	}
	function validateotp(){
		$oldotp = $_POST['oldotp'];
		$newotp = implode("", $_POST['otp']);
		if ($oldotp==$newotp){
			echo 1;
		}else {
			echo 0;
		}
	}
	function pay_now() {
		$sesUserId = $this->userSessionId ();
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$data['slotData'] = $this->Homemodel->getTableData("slot");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		if ($sesUserId){
			$whereaddress = "user_id='$sesUserId'";
			$data['addressData'] = $this->Homemodel->getDataById("delivery_address",$whereaddress);
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$price = $cart['price'];
					$packageprice = ($price*$package_percentage)/100;
					$real_price = $price - $packageprice;
					$total_price = $total_price + ($price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
			
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		
		$data['activeMenu'] = "0";
		$this->load->view('frontend/pay_nowView',$data);
	}
	
	function confirm_order() {
		$sesUserId = $this->userSessionId ();
		$payment_mode = $_POST ['payment_mode'];
		$slot = $_POST ['slot'];
		$address = $_POST ['address'];
		
		if (!empty($sesUserId)) {
			$whereuser = "user_id= $sesUserId";
			$useragentData = $this->Homemodel->getDataById ( 'user', $whereuser );
			$algorithm = $useragentData[0]['algorithm'];
			$whereuserorder = "(user_id= $sesUserId AND order_status='Pending' AND is_edit='1') group by unique_order_id";
			$manageorderData = $this->Homemodel->getDataById ( 'manage_order', $whereuserorder );
			if (!empty($manageorderData)){
				$countorder = count($manageorderData);
				if ($countorder > 1){//print_r($manageorderData);die;
					echo "dup";die;
				} else {
				$order_no = $manageorderData[0]['unique_order_id'];
				$invoice_no = $manageorderData[0]['invoice_no'];
				}
				
			} else {
				$order_no = mt_rand(100000, 999999);
				$invoice_no = 'novo'.''.mt_rand(100000, 999999);
			}
			$agent_id = $useragentData[0]['agent_id'];
			$discount_percentage = $useragentData[0]['package_percentage'];
			$affiliate_percentage = $useragentData[0]['affiliate_percentage'];
			$purchase_value = $useragentData[0]['min_value'];
			$addressData = $this->Homemodel->getDataById ( 'delivery_address', $whereuser );
			if (empty($addressData)){
			$insertArr = array(
					'address' => $address,
					'user_id' => $sesUserId,
			);
			$res = $this->Homemodel->insert('delivery_address', $insertArr);
			} else {
				$res = $addressData[0]['delivery_address_id'];
			}
			if ($res) {
			    $where = "user_id= $sesUserId";
				$userData = $this->Homemodel->getDataById ( 'add_to_cart', $where );
				if ($userData) {
					$total_amount = 0;
					foreach ( $userData as $product ) {
						$quantity = $product ['quantity'];
						$product_id = $product ['product_id'];
						$wherepro = "product_id= $product_id";
						$product = $this->Homemodel->getDataById ( 'product', $wherepro );
						$tprice = $product[0]['actual_price'];
						$price = $product[0]['discount_price'];
						$total_amount = $total_amount + $price;
						$costprice = $product[0]['cost_price'];
						$dbQuantity = $product[0]['quantity'];
						$soldqty = $product [0] ['sold_quantity'];
						$avlQuantity = $dbQuantity - $soldqty;
						$update_qty = $soldqty + $quantity;
						if ( $quantity > $avlQuantity && $dbQuantity !=0 ) {
							echo 3;die;
						} else {
							$insertArr = array (
									'user_id' => $sesUserId,
									'pro_id' => $product_id,
									'slot_id' => $slot,
									'unique_order_id' => $order_no,
									'invoice_no' => $invoice_no,
									'address_id' => $res,
									'order_quantity' => $quantity,
									'pincode' => "700064",
									'pro_price' => $price,
									'pro_cost_price' => $costprice,
									'dis_by_agent' => $affiliate_percentage,
									'affiliate_percentage' => $discount_percentage,
									'order_mode' => $payment_mode,
									'order_status' =>"Pending",
									'agent_id' =>$agent_id,
									'algorithm' => $algorithm,
									'order_date' => date ( 'Y-m-d h:i:s')
							);
							//print_r($insertArr);die;
							$result = $this->Homemodel->insert ( 'manage_order', $insertArr );
							$insertArr1 = array (
									'sold_quantity' => $update_qty
							);
							$result1 = $this->Homemodel->update ( 'product', $insertArr1, $wherepro );
							
		}
		}
		if ($result) {
			$whereus="user_id='$sesUserId'";
			/* $insertcaertArr1 = array (
					'is_shown' => 1
			);
			$this->Homemodel->update ( 'add_to_cart', $insertcaertArr1, $whereus); */
			$this->Homemodel->delete ( 'add_to_cart', $whereus);
			if (!empty($manageorderData)){
			$order_no = $manageorderData[0]['unique_order_id'];
			$invoice_no = $manageorderData[0]['invoice_no'];
			$orderfile = $manageorderData[0]['order_file'];
			$whereupadate="unique_order_id='$order_no' AND 	invoice_no='$invoice_no' AND user_id='$sesUserId'";
			$insertorderArr = array (
					'order_file' => $orderfile
			);
			$this->Homemodel->update ( 'manage_order', $insertorderArr, $whereupadate);
			$whereuserorderold = "(user_id= $sesUserId AND order_status='Pending' AND is_edit='1')";
			$manageorderoldData = $this->Homemodel->getDataById ( 'manage_order', $whereuserorderold );
			if (!empty($manageorderoldData)){
			foreach ($manageorderoldData as $old){
			$manage_order_id = $old['manage_order_id'];
			$wheredelete = "manage_order_id='$manage_order_id'";
			$this->Homemodel->delete ( 'manage_order', $wheredelete );
			}
			}
			}
			echo 1;
		} else {
			echo 0;
		}
				}
			}
		}
	}
	
	function order_history(){
	    $sesUserId = $this->userSessionId ();
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$data['slotData'] = $this->Homemodel->getTableData("slot");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		if ($sesUserId){
			$whereaddress = "user_id='$sesUserId'";
			$data['addressData'] = $this->Homemodel->getDataById("delivery_address",$whereaddress);
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price =$total_price + $cart['price'];
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
			$whereorder = "(user_id= $sesUserId) GROUP BY  unique_order_id ORDER BY manage_order_id DESC ";
			$data['historyData'] = $this->Homemodel->getDataById ( 'manage_order', $whereorder );
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		
		$data['activeMenu'] = "0";
		$this->load->view('frontend/order_historyView',$data);
	}
	function my_earning(){
		$sesUserId = $this->userSessionId ();
		$whereuser = "user_id='$sesUserId'";
		$data ['userDatadata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$data['slotData'] = $this->Homemodel->getTableData("slot");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		if ($sesUserId){
			$whereaddress = "user_id='$sesUserId'";
			$data['addressData'] = $this->Homemodel->getDataById("delivery_address",$whereaddress);
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price =$total_price + $cart['price'];
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}
			$whereorder = "(user_id= $sesUserId) GROUP BY  unique_order_id ORDER BY manage_order_id DESC ";
			$data['earningData'] = $this->Homemodel->getDataById ( 'manage_order', $whereorder );
		} else {
			$data['total_price'] = 0;
			$data['cartcount'] = 0;
		}
		$data['activeMenu'] = "0";
		$this->load->view('frontend/myearningView',$data);
	}
	function invoice()
	{
		ini_set('memory_limit', '256M');
		// load library
		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		// retrieve data from model
		//$data['news'] = $this->mpdf_model->get_news();
		$data['title'] = "items";
	
		
		// boost the memory limit if it's low ;)
		$html = $this->load->view('frontend/invoice', $data, true);
		// render the view into HTML
		$pdf->WriteHTML($html);
		// write the HTML into the PDF
		$output = 'invoice' . date('Y_m_d_H_i_s') . '_.pdf';
		$pdf->Output("$output", 'D');
		ob_clean();
		//$pdf->Output('uploads/'.$output, 'F');
		
	}
	function getcitybypin() {
		$cityid = $_POST ['cityid'];
		$where = "city_id = $cityid";
		$pinData = $this->Homemodel->getDataById ( "pin_code", $where );
		$selectedpin = $pinData [0] ['pin_code_id'];
		
		$html = "<option value='0'>Select Pin code</option>";
		if (! empty ( $pinData )) {
			$selected = "";
			foreach ($pinData as $row ) {
				$pinid = $row ['pin_code_id'];
				$pinno = $row ['pin_num'];
				if ($selectedpin == $pinid) {
					$selected = "selected";
				} else {
					$selected = "";
				}
				$html .= "<option value='$pinid' $selected>$pinno</option>";
			}
		}
		echo $html;
	}
	function get_autocomplete(){
		if (isset($_GET['term'])) {
			 $term = $_GET['term'];
			$where = "product_name LIKE '%$term%'";
		$searchData = $this->Homemodel->getDataById ( "product", $where );
		//print_r($searchData);die;;
			if (count($searchData) > 0) {
				foreach ($searchData as $row)
					$arr_result[] = $row->product_name;
				   echo json_encode($arr_result);
			}
		}
	}
	function search() {
		$name = $_POST ['name'];
		
		//$selectedpin = $pinData [0] ['pin_code_id'];
	
		$html = "<option value='0'>Select City</option>";
		if (! empty ( $pinData )) {
			$selected = "";
			foreach ($pinData as $row ) {
				$pinid = $row ['pin_code_id'];
				$pinno = $row ['pin_num'];
				if ($selectedpin == $pinid) {
					$selected = "selected";
				} else {
					$selected = "";
				}
				$html .= "<option value='$pinid' $selected>$pinno</option>";
			}
		}
		echo $html;
	}
	function order_details(){
		$sesUserId = $this->userSessionId ();
		$orderid = $_POST['orderid'];
		$whereunique="unique_order_id='$orderid'";
		$data['orderdata'] = $this->Homemodel->getDataById ( 'manage_order', $whereunique );
		$this->load->view('frontend/ajax_orderDetailsView',$data);
	}
	function upload_order(){
		$sesUserId = $this->userSessionId ();
	    $whereuser = "user_id='$sesUserId'";
		$data ['userDatadata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$data['categoryData'] = $this->Homemodel->getTableData("category");
		$data['slotData'] = $this->Homemodel->getTableData("slot");
		$whereuser = "user_id=1";
		$data ['admindata'] = $this->Homemodel->getDataById ( 'user', $whereuser );
		$wherecareer = "cms_id=8";
		$data ['careerdata'] = $this->Homemodel->getDataById ( 'cms', $wherecareer );
		$wheremedia = "cms_id=7";
		$data ['socialmediadata'] = $this->Homemodel->getDataById ( 'cms', $wheremedia );
		$wherestore = "cms_id=6";
		$data ['aboutstoredata'] = $this->Homemodel->getDataById ( 'cms', $wherestore );
		$wheremedial = "cms_id=5";
		$data ['contactdata'] = $this->Homemodel->getDataById ( 'cms', $wheremedial );
		$wherecity = "status='Active'";
		$data['citydata'] = $this->Homemodel->getDataById("city",$wherecity);
		$wherepin = "status='Active'";
		$data['pindata'] = $this->Homemodel->getDataById("pin_code",$wherepin);
		$data['customertypeData'] = $this->Homemodel->getTableData("customer_type");
		if ($sesUserId){
			$whereaddress = "user_id='$sesUserId'";
			$data['addressData'] = $this->Homemodel->getDataById("delivery_address",$whereaddress);
			$where = "user_id='$sesUserId'";
			$cartData = $this->Homemodel->getDataById("add_to_cart",$where);
			$cartcount = count($cartData);
			if (!empty($cartData)){
				$total_price = 0;
				foreach ($cartData as $cart){
					$quantity = $cart['quantity'];
					$userid = $cart['user_id'];
					$whereuser = "user_id='$userid'";
					$userData = $this->Homemodel->getDataById("user",$whereuser);
					$package_percentage = $userData[0]['package_percentage'];
					$total_price =$total_price + $cart['price'];
					//$packageprice = ($price*$package_percentage)/100;
					//$real_price = $price - $packageprice;
					//$total_price = $total_price + ($real_price * $quantity);
				}
				$data['total_price'] = $total_price;
				$data['cartcount'] = $cartcount;
			} else {
				$data['total_price'] = 0;
				$data['cartcount'] = 0;
			}}
			$data['activeMenu'] = "0";
		$this->load->view('frontend/order-prescription',$data);
	
	}
	public function uploadorder()
	{
		$sesUserId = $this->userSessionId ();
		$whereuser = "user_id = '$sesUserId'";
		$userData = $this->Homemodel->getDataById ( 'user', $whereuser );
		$algorithm = $userData[0]['algorithm'];
		if (! empty ( $_FILES ['img']['name'] )) {//echo "ok";die;
			$imageName = $_FILES ['img']['name'];
			$config ['upload_path'] = FCPATH . 'uploads/orderfile/';
			$config ['allowed_types'] = '*';
			$config ['max_size'] = '1024';
			$time = strtotime ( date ( 'Y-m-d H:i:s' ) );
			$config ['file_name'] = $time;
			$this->upload->initialize ( $config );
			if (! $this->upload->do_upload ( 'img' )) {
				$error = $this->upload->display_errors ();
				 $data ['imgErr'] = $error;
				$failure = 1;
				$uploadedimgName = "";
			} else {
				$a = $this->upload->data ();
				$failure = 0;
				$uploadedimgName = $a ['file_name'];
			}
		} 
		$order_no = mt_rand(100000, 999999);
		$invoice_no = 'novo'.''.mt_rand(100000, 999999);
		$insertArr = array (
				'user_id' => $sesUserId,
				'unique_order_id' => $order_no,
				'invoice_no' => $invoice_no,
				'order_file' =>$uploadedimgName,
				'order_status' =>"Pending",
				'algorithm' => $algorithm,
				'is_add' => "1",
				'order_date' => date ( 'Y-m-d h:i:s')
		);
		//print_r($insertArr);die;
		$result = $this->Homemodel->insert ( 'manage_order', $insertArr );
	if ($result){
		echo 1;
	} else {
		echo 0;
	}
	}
	function userSessionId()
	{
		$sessionUserId = $this->session->userdata('userData');
		return $sessionUserId['id'];
	}
	function logout()
	{
		$this->session->unset_userdata('userData');
		header("location:" . base_url('Home') . "");
	}
}