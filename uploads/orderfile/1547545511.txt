https://uat2yesmoney.easypay.co.in/epMoney/bbps/validate

 Request
-------------------
{
  "HEADER": {
    "MID": "EP00009",
    "OP": "TSTOBBP",
    "ST": "TALKTIME",
    "TXN_AMOUNT": 0,
    "AID": "MANMPA2193"
  },
  "DATA": {
    "CN": "9903586536",
    "CUSTOMER_MOBILE": "9903586536",
    "BILLER_ID": "TSTO00000NAT01",
    "ADDINFO1": "a=9903586536",
    "ADDINFO2": "a b=9903586536",
    "ADDINFO3": "a b c=9903586536",
    "ADDINFO4": "a b c d=9903586536",
    "ADDINFO5": "a b c d e=9903586536",
    "ORDER_ID": "BILRDTLRFAGNT1543057346127"
  }
}

Response
-------------
{
    "RESP_CODE": "101",
    "RESPONSE": "REQUEST_FAILED",
    "RESP_MSG": "Request Failed, Mandatory Tag(s) Not Present",
    "DATA": ""
}
